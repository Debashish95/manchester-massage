//
//  Config.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 18/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation

enum Config {
    
    enum api {
        static let login            = "login"
        static let register         = "signup"
        static let sendOTP          = "forgetpass_sendotp"
        static let changePassword   = "changepassv2"
        
    }
    
    static let apiEndPoint = "https://api.manchestermassage.co.uk/postdata/"
    static let mainUrl = "https://api.manchestermassage.co.uk/"
    static let key = "alphamassage876ty67"
    static let salt = "iuhnji8fd7659kij9j02wed440wq"
//    https://developer.pampertree.co.uk/Data/GetService?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j

}

