//
//  STRINGS.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 17/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation

var CURRENCY_SYMBOL = "£"
extension String{
    var isValidName: Bool{
        let RegEx = "\\w{7,18}"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: self)
    }
    

    var isValidPostalCode: Bool{
        let pinCode = "^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$"
        let pinPred = NSPredicate(format:"SELF MATCHES %@", pinCode)
        return pinPred.evaluate(with: self)
        
    }
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    var isBlank: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    //validate Password
    var isValidPassword: Bool {
        let password = self.trimmingCharacters(in: CharacterSet.whitespaces)
        let passwordRegx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&<>*~:`-]).{8,}$"
        let passwordCheck = NSPredicate(format: "SELF MATCHES %@",passwordRegx)
        return passwordCheck.evaluate(with: password)
    }
    
    //validate PhoneNumber
    var isPhoneNumber: Bool {
        let charcter  = CharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined()
        return  self == filtered

    }
    
    var toData: Data{
        return self.data(using: .utf8) ?? Data()
    }
    
    //MARK:-Extension of String which allow to Display HTML data in a label or textView
    private var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func convertDate(format: String) -> Date?{
        let df = DateFormatter()
        df.dateFormat = format
        return df.date(from: self)
    }
    
    func convertDate(currentFormat: String, requiredFormat: String) -> String{
        let df = DateFormatter()
        df.dateFormat = currentFormat
        if let currentDate = df.date(from: self) {
            df.dateFormat = requiredFormat
            return df.string(from: currentDate)
        }
        return self
    }
    
    var fromBase64: String? {
        guard let data = Data(base64Encoded: self) else{
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    var toBase64: String {
        return Data(self.utf8).base64EncodedString()
    }
    
    public func makeMaskedString(maskedText: String, repeatAfter: Int) -> String{
        var maskedString = ""
        var index = 0
        self.forEach { ch in
            if (index % repeatAfter == 0 && index > 0){
                maskedString.append(maskedText)
            }
            maskedString.append(ch)
            index += 1
        }
        return maskedString == "" ? self : maskedString
    }
    
    
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func formattedString() -> String{
        let formattedText = replacingOccurrences(of: ",", with: "")
        let floatValue = Float(formattedText) ?? 0.0
        var formattedString = String(format: "%.2f", floatValue)
        formattedString = formattedString.replacingOccurrences(of: ".00", with: "")
        return formattedString
    }
    func value() -> Float{
        let formattedText = replacingOccurrences(of: ",", with: "")
        return Float(formattedText) ?? 0.0
    }
}

struct Message {
    static let apiError = "Error in data fetching. Please try again!"
}


extension UserDefaults{
    
    func saveData(value: Any, key: UserDefaults.keys){
        UserDefaults.standard.set(value, forKey: key.value)
        UserDefaults.standard.synchronize()
    }
    
    func deleteData(forKey: UserDefaults.keys){
        UserDefaults.standard.removeObject(forKey: forKey.rawValue)
        UserDefaults.standard.synchronize()
    }
    func fetchData(forKey: UserDefaults.keys)-> String{
        return UserDefaults.standard.string(forKey: forKey.rawValue) ?? ""
    }
    
    func fetchData(forKey: UserDefaults.keys)-> Int{
        return UserDefaults.standard.integer(forKey: forKey.rawValue)
    }
    
    func fetchData(forKey: UserDefaults.keys)-> Bool{
        return UserDefaults.standard.bool(forKey: forKey.rawValue)
    }
    
    enum keys: String{
        
        case userId, userFirstName, userLastName, userEmail,userPhone, userStatus, userSocialLogin, userSocialLoginId, skipLogin, userImage, _savedEmail, _savedPassword, _isRememberMe,
            referalCode, referalLink
        var value: String{
            return self.rawValue
        }
    }

    func saveUserCredential(email: String, password: String){
        UserDefaults.standard.saveData(value: email, key: ._savedEmail)
        UserDefaults.standard.saveData(value: password.toBase64, key: ._savedPassword)
        UserDefaults.standard.saveData(value: true, key: ._isRememberMe)
    }
    
    func fetchUserCredential() -> (email: String, password: String){
        let encodedPassword: String = UserDefaults.standard.fetchData(forKey: ._savedPassword)
        return (UserDefaults.standard.fetchData(forKey: ._savedEmail),  encodedPassword.fromBase64 ?? "")
    }
    func deleteUserCredential(){
        UserDefaults.standard.deleteData(forKey: ._savedEmail)
        UserDefaults.standard.deleteData(forKey: ._savedPassword)
        UserDefaults.standard.saveData(value: false, key: ._isRememberMe)
    }
}

