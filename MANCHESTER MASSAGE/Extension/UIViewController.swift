//
//  UIViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 14/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
extension UIViewController{
    
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date ?? Date())
    }
    func convertDateFormater(_ date: Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date)
    }
    
    var appD : AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func setNavigationBar(titleText: String, isBackButtonRequired:Bool = false, isRightMenuRequired:Bool = false){
        
        self.navigationItem.title = titleText
//        title = title
//        navigationBar.topItem?.title = title
//        title = title
        //        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        if isBackButtonRequired{
            let backButton = UIButton(type: .custom)
            backButton.frame = CGRect(x: -10, y: 0, width: 40, height: 40)
            backButton.addTarget(self, action: #selector(self.backButtonAction), for: .touchUpInside)
            backButton.setImage(UIImage(named: "back1"), for: .normal)
            backButton.contentMode = .left
            backButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
            let leftBarButton = UIBarButtonItem(customView: backButton)
            self.navigationItem.leftBarButtonItem = leftBarButton
            
        }
        
        if isRightMenuRequired{
//            let backButton = UIButton(type: .custom)
//            backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//            backButton.addTarget(self, action: #selector(self.cartButtonAction), for: .touchUpInside)
//            backButton.setLeftImage(image: FAType.FAShoppingCart, color: .gray, state: .normal)
//            let leftBarButton = ENMBadgedBarButtonItem(customView: backButton, value: "10")
//            self.navigationItem.rightBarButtonItem = leftBarButton
            
        }
    }
    
    func setHomePageNavBr(title: String, isMenuRequired:Bool = false){
        
        self.navigationController?.navigationBar.topItem?.title = title
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 1.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
        
        if isMenuRequired{
            let menuButton = UIButton(type: .custom)
            menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            menuButton.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)
            menuButton.setLeftImage(image: FAType.FABars, color: .black, state: .normal)
            /*menuButton.setTitle("Menu", for: UIControl.State())
            menuButton.setTitleColor(.black, for: .normal)
            let imageSize: CGSize = menuButton.imageView?.image?.size ?? .zero
            menuButton.titleEdgeInsets = UIEdgeInsets(top: 1, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: menuButton.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: menuButton.titleLabel!.font ?? UIFont.systemFont(ofSize: 12)])
            menuButton.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + 1), left: 0.0, bottom: 0.0, right: -titleSize.width)*/
            let leftBarButton = UIBarButtonItem(customView: menuButton)
            self.navigationItem.leftBarButtonItem = leftBarButton
            
        }
    }
    
    @objc func backButtonAction(){
        
        self.navigationController?.dismiss(animated: true, completion: {
            return
        })
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func menuButtonAction(){}
    
    @objc func cartButtonAction(){
//        if UserDefaults.standard.fetchData(forKey: .skipLogin){
//            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
//            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
//                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
//                destination.isFromLeftMenu = true
//                let navigationController = UINavigationController(rootViewController: destination)
//                navigationController.modalPresentationStyle = .fullScreen
//                self.present(navigationController, animated: false, completion: nil)
//            }
//            alert.addAction(yesAction)
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//
//            }
//            alert.addAction(cancelAction)
//
//            self.present(alert, animated: false, completion: nil)
//        }else{
            let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
            self.navigationController?.pushViewController(detination, animated: true)
        //}
        
    }
    
    @objc func productCartButtonAction(){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
        }else{
            let destination = PhysicalCartViewController(nibName: "PhysicalCartViewController", bundle: nil)
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    @objc func wishListButtonAction(){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
        }else{
            let destination = ProductListViewController(nibName: "ProductListViewController", bundle: nil)
            destination.isFromWishList = true
            destination.pageTitle = "My WishList"
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
    }
    
    func addActivityIndicator(type: NVActivityIndicatorType = .ballSpinFadeLoader) -> NVActivityIndicatorView{
        let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: type, color: ThemeColor.buttonColor, padding: .zero)
        
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        return activityIndicator
    }
    
    
    var topViewController: UIViewController?{
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    func showAlert(title: String = "Alert!", message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}
