//
//  UIButton.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 15/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    func setLeftImage(image: FAType, color: UIColor = .white, state: UIControl.State = .normal, size: CGSize = CGSize(width: 30, height: 30)){
        let origImage = UIImage(icon: image, size: size)
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        self.setImage(tintedImage, for: state)
        self.tintColor = color
    }
    
    func setSupportButton(image: FAType, title: String, alignment: UIControl.ContentHorizontalAlignment = .left, color: UIColor = .darkGray){
        self.setLeftImage(image: image, color: color, state: .normal)
        self.setTitleColor(color, for: .normal)
        self.setTitle(title, for: .normal)
        self.contentHorizontalAlignment = alignment
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }
    
    func setSupportFooterButton(title: String){
        self.setTitleColor(.darkGray, for: .normal)
        self.setTitle(title, for: .normal)
        self.contentHorizontalAlignment = .left
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }
    
    func dropShadow(cornnerRadius: CGFloat = 8.0, shadowColour: UIColor = .darkGray, shadowOfSetWidth: CGFloat = 0, shadowOfSetHeight: CGFloat = 5.0, shadowOpacity: CGFloat = 0.2) {
        layer.cornerRadius = cornnerRadius
        layer.shadowColor = shadowColour.cgColor
        layer.shadowOffset = CGSize(width: shadowOfSetWidth, height: shadowOfSetHeight)
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornnerRadius)
        
        layer.shadowPath = shadowPath.cgPath
        
        layer.shadowOpacity = Float(shadowOpacity)
        backgroundColor = .white
        
    }
    
    func disable() {
        isEnabled = false
        alpha = 0.5
    }
    
    func enable() {
        isEnabled = true
        alpha = 1.0
    }
}
extension UIView{
    
    func dropShadowView(cornnerRadius: CGFloat = 8.0, shadowColour: UIColor = .darkGray, shadowOfSetWidth: CGFloat = 0, shadowOfSetHeight: CGFloat = 5.0, shadowOpacity: CGFloat = 0.2) {
        layer.cornerRadius = cornnerRadius
        layer.shadowColor = shadowColour.cgColor
        layer.shadowOffset = CGSize(width: shadowOfSetWidth, height: shadowOfSetHeight)
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornnerRadius)
        
        layer.shadowPath = shadowPath.cgPath
        
        layer.shadowOpacity = Float(shadowOpacity)
        backgroundColor = .white
        
    }
}
