//
//  UITextField.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 15/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{

    func addButtomBorder(color: CGColor = UIColor.gray.cgColor){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - 2, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = color
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
    }
    
    func setTextFieldImage(image: UIImage, direction: ImageDirection = .left){
        let imageView = UIImageView()
        let image = image
        imageView.image = image
        
        if direction == .left{
            self.leftView = imageView
        }
        else{
            self.rightView = imageView;
        }
    }
    
    func setTextFieldImage(image: FAType, direction: ImageDirection = .left){
        let imageView = UIImageView()
        let origImage = UIImage(icon: image, size: CGSize(width: 30, height: 30))
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        imageView.image = tintedImage
        imageView.tintColor = .gray
        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        if direction == .left{
            self.leftView = imageView
            self.leftViewMode = .always
            self.leftView = imageView
        }
        else{
            self.rightView = imageView
            self.rightViewMode = .always
            self.rightView = imageView
        }
    }
    
    func setPlaceholder(placeholder: String, color: UIColor){
        self.attributedPlaceholder = NSAttributedString(string: placeholder,
        attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    func enablePasswordEye(){
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "eye_close"), for: .normal)
        button.setImage(UIImage(named: "eye"), for: .selected)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.togglePasswordShowHide(sender:)), for: .touchUpInside)
        self.rightView = button
        self.rightViewMode = .always
    }
    
    @objc func togglePasswordShowHide(sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.isSecureTextEntry = !sender.isSelected
    }
}

enum ImageDirection{
    case left, right
}

@IBDesignable class BottomBorderTextField: UITextField {
    
    @IBInspectable
    var bottomBorderColour : UIColor = UIColor.darkGray
    
    override func layoutSubviews() {
//        self.addButtomBorder(color: bottomBorderColour.cgColor)
    }
}


class PaddingTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
