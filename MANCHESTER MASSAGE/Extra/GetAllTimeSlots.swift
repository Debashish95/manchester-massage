//
//  GetAllTimeSlots.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 29/11/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation
import Alamofire

func GetTimeSlotForBooking(
    serviceId: String,
    duration: Int,
    excludeDate: [String] = [String](),
    excludeTimeSlot: [String] = [String](),
    excludeEmployee: [EmployeeModel] = [EmployeeModel](),
    requiredTherapist: Int = 1,
    completionHandler: @escaping (_ data: [EmployeeDateTimeSlot]) -> ()
) {
    let requestURL = "\(Config.mainUrl)Data/GetAllTimeSlot?key=\(Config.key)&salt=\(Config.salt)&vn_id=4&services=[\(serviceId)]&theraview=Y&skip_blankdate=1"
    print("Request URL: \(requestURL)")
    let urlConvertible = URL(string: requestURL)!
    var urlRequest = URLRequest(url: urlConvertible)
    urlRequest.httpMethod = "GET"
    let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
        if let _ =  error {
            completionHandler([EmployeeDateTimeSlot]())
            return
        }
        
        guard let data = data,
              let responseJson = try? JSONSerialization.jsonObject(
                with: data, options: .allowFragments) as? NSDictionary
        else {
            completionHandler([EmployeeDateTimeSlot]())
            return
        }
        let result = responseJson["result"] as? NSArray ?? NSArray()
        var finalData = [EmployeeDateTimeSlot]()
        result.forEach { currentEmployeeDateTimeSlot in
            let currentResult = currentEmployeeDateTimeSlot as? NSDictionary ?? NSDictionary()
            let date = currentResult["date"] as? String ?? "2001-01-01"
            let dateToShow = date.convertDate(
                currentFormat: "yyyy-MM-dd",
                requiredFormat: "EE, MMM dd")
            let isBooking = currentResult["is_booking"] as? String ?? "0"
            let slots = currentResult["slots"] as? NSArray ?? NSArray()
            var finalTimeSlots = [TimeSlot]()
            slots.forEach { timeSlots in
                let currentTimeSlots = timeSlots as? NSDictionary ?? NSDictionary()
                let currentTime = currentTimeSlots["time"] as? String ?? "00:00 AM"
                let allAvailableEmployee = currentTimeSlots["theras"] as? NSArray ?? NSArray()
                var finalEmployeeData = [EmployeeModel]()
                allAvailableEmployee.forEach { employeeData in
                    let currentEmployeeData = employeeData as? NSDictionary ?? NSDictionary()
                    let empData = EmployeeModel(
                        id: currentEmployeeData["id"] as? String ?? "",
                        user_name: currentEmployeeData["user_name"] as? String ?? "",
                        user_lname: currentEmployeeData["user_lname"] as? String ?? ""
                    )
                    finalEmployeeData.append(empData)
                }
                let currentTimeSlot = TimeSlot(
                    time: currentTime,
                    therapists: finalEmployeeData
                )
                finalTimeSlots.append(currentTimeSlot)
            }
            
            let employeeDateTimeSlot = EmployeeDateTimeSlot(
                date: date,
                dateToShow: dateToShow,
                isBooking: isBooking,
                duration: currentResult["duration"] as? String ?? "0",
                timeSlots: finalTimeSlots,
                isBookingAvailable: (isBooking == "1"))
            finalData.append(employeeDateTimeSlot)
        }
        completionHandler(finalData)

    }
    dataTask.resume()
}
