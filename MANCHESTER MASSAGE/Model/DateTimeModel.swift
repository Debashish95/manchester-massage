//
//  DateTimeModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Durga on 17/09/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
struct DateTimeModel{
    
    var date: String
    var available: String
    var times: [String]
    var therapists: [EmployeeModel] = [EmployeeModel]()
}

struct EmployeeDateModel{
    var employee: EmployeeModel
    var date: [DateTimeModel]
}

struct EmployeeDateTimeSlot{
    var date: String = ""
    var dateToShow: String = ""
    var isBooking: String = ""
    var duration: String = "0"
    var timeSlots: [TimeSlot] = [TimeSlot]()
    var isBookingAvailable = false
}

struct TimeSlot {
    var time: String = ""
    var therapists: [EmployeeModel] = [EmployeeModel]()
}
