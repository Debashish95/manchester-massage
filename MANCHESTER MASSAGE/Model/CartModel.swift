//
//  CartModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
//class CartModel {
//
//    var services: [ServiceItemsModel]
//    var massageName: String
//    var massageDuration: String
//    var massagePrice: String
//    var bookingTime: String
//    var bookingDate: String
//    var bookingDay: String
//    var therapistName: String
//    var isForMe: Bool
//
//    var otherFullName: String
//    var otherPhoneNumber: String
//    var otherEmail: String
//
//    init(massageName: String = "", massageDuration: String = "", massagePrice: String = "",
//         bookingTime: String = "", bookingDate: String = "", bookingDay: String = "", therapistName: String = "", isForMe: Bool = true, otherFullName: String = "", otherPhoneNumber: String = "", otherEmail: String = ""){
//
//        self.massageName = massageName
//        self.massageDuration = massageDuration
//        self.massagePrice = massagePrice
//        self.bookingTime = bookingTime
//        self.bookingDate = bookingDate
//        self.bookingDay = bookingDay
//        self.therapistName = therapistName
//        self.isForMe = isForMe
//
//        self.otherFullName = otherFullName
//        self.otherPhoneNumber = otherPhoneNumber
//        self.otherEmail = otherEmail
//    }
//}

struct CartModel{
    var itemId: Int
    var services: [ServiceItemsModel]
    var cat_id: String
    var serviceID: String
    var vendor_id: String
    var tot_amount: String
    var final_amount: String
    var isForMe: Bool
    
    var bookingTime: String
    var bookingEndTime: String
    var bookingDay: String
    var bookingDate: String
    
    var cust_name: String
    var cust_email: String
    var cust_phone: String
    var therapists: [EmployeeModel]
    var qty: String
    var other_name:String
    var notes: String
    
    init(itemId: Int, services: [ServiceItemsModel], cat_id: String, vendor_id: String, tot_amount: String, final_amount: String, isForMe: Bool = true,bookingTime: String = "", bookingEndTime: String = "", bookingDay: String = "", bookingDate: String = "", cust_name: String = "", cust_email: String = "", cust_phone: String = "", therapists: [EmployeeModel] = [EmployeeModel](), serviceID: String = "", qty: String = "1", other_name:String = "", notes: String = ""){
        self.itemId = itemId
        self.services = services
        self.cat_id = cat_id
        self.vendor_id = vendor_id
        self.tot_amount = tot_amount
        self.final_amount = final_amount
        self.isForMe = isForMe
        
        self.bookingTime = bookingTime
        self.bookingEndTime = bookingEndTime
        self.bookingDay = bookingDay
        self.bookingDate = bookingDate
        
        self.cust_name = cust_name
        self.cust_email = cust_email
        self.cust_phone = cust_phone
        
        self.therapists = therapists
        self.serviceID = serviceID
        self.qty = qty
        self.other_name = other_name
        self.notes = notes
        
        
    }
    
    public static func ==(lhs: CartModel, rhs: CartModel) -> Bool{
        return
        lhs.itemId == rhs.itemId
    }
}
