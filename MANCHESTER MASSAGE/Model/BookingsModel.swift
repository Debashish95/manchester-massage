//
//  BookingsModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 27/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
struct BookingsModel{
    
    
    /**
     
     {
     "cust_id": "38",
     "orderid": "9783415235",
     "bookdt": "2020-03-06",
     "timeslot": "12:00 PM",
     "status": "0",
     "cust_fname": "Durga",
     "cust_lname": "Ballabha Panigrahi",
     "cust_email": "panigrahi.durga9@gmail.com",
     "cust_phone": "9090",
     "salon_id": "4",
     "salon": "Manchester Massage",
     "services": [
     {
     "service_id": "15",
     "service_name": "Couples Sports Massage (2 Hours)",
     "qty": "1",
     "base_price": "100",
     "subtotal": "100",
     "therapist": "50",
     "therepist_nm": "Therapist 4",
     "cust_name": "Durga Ballabha Panigrahi",
     "book_dt": "2020-03-06",
     "book_tym": "12:00 PM"
     }
     ]
     }
     
     "cust_id": "53",
     "orderid": "4396213657",
     "bookdt": "2020-10-26",
     "timeslot": "10:00 AM",
     "status": "0",
     "cust_fname": "Durga",
     "cust_lname": "Panigrahi",
     "cust_email": "durga@gmail.com",
     "cust_phone": "9090450564",
     "salon_id": "4",
     "salon": "Manchester Massage",
     "services": [
       {
         "service_id": "65",
         "cat_id": null,
         "sv_id": null,
         "duration": null,
         "service_name": "Deep Tissue (2 Hours)",
         "qty": "1",
         "base_price": "0",
         "subtotal": "60",
         "therapist": "1",
         "therapist_arr": "[1, 2]",
         "therepist_nm": "ADMIN ADMIN",
         "cust_name": "Deep Tissue (2 Hours)",
         "book_dt": "2020-10-17",
         "book_tym": "11:00 AM",
         "book_end_tym": "11:00 AM"
       }
     ]
     
     
     */
    var status             : String
    var orderid            : String
    var cust_id            : String
    var firstName          : String
    var lastName           : String
    var customerEmail      : String
    var serviceType        : String
    var servicePrice       : String
    var cust_phone         : String
    var therapist_id       : String
    var salon_id           : String
    var salon              : String
    var services           : [servicelist]
    var bookdt             : String
    var timeslot           : String
    var notes              : String
    var transaction_type   : String
    var is_review          : String
    var booking_id         : String
    var reviews            : ReviewModel?
    var share_url          : String
    var booked_on          : String
    var amount             : String
    var cancellationStatus : CancellationStatus = .none
}
struct servicelist{
    var serviceId          : String
    var sub_orderid        : String
    var serviceName        : String
    var therapistName      : String
    var bookingDate        : String
    var bookingTime        : String
    var bookEndTime        : String
    var custName           : String
    var therapistArr       : String
    var therepistNm        : String
    var qty                : String
    var basePrice          : String
    var subTotal           : String
    var duration           : String
    var cart_id            : String
    var cat_thumb          : String
    var cleanup_time       : String
    var notes              : String
    var bookingStatus      : String
}

enum CancellationStatus: String {
    case partial = " 🟡 Partically Cancelled"
    case full = " 🔴 Cancelled"
    case none = ""
}
