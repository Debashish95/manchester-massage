//
//  GiftModel.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 20/05/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation

struct GiftModel {
    var category: String = ""
    var card_code: String = ""
    var amount: String = ""
    var send_via_post: String = ""
    var valid_till: String = ""
    var created_on: String = ""
    var receiver_address: String = ""
    var receiver_email: String = ""
    var status: String = ""
    var Status_info: String = ""
}
