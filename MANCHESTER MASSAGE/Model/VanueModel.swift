//
//  VanueModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 09/03/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation

struct EmployeeModel {
    var id: String
    var user_name: String
    var user_lname: String
    var user_email: String
    var phone: String
    
    init(id: String = "", user_name: String = "", user_lname: String = "", user_email: String = "", phone: String = ""){
        self.id = id
        self.user_name = user_name
        self.user_lname = user_lname
        self.user_email = user_email
        self.phone = phone
    }
}
