//
//  OfferModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
class OfferModel {
    var offerId: String
    var offerName: String
    var offerImage: String
    var offerDiscount: String
    var cartId:String
    var main_price:String
    var offer_price:String
    var duration:String
    var cleanUpTime:String
    var offerText: String
    var isBeauty: Bool = false
    
    init(offerId: String = "", offerName: String = "", offerImage: String = "", offerDescription: String = "", offerDiscount: String = "",cartId: String = "",main_price: String = "", offer_price: String = "",duration: String = "", offerText: String = "", cleanUpTime: String = "", isBeauty: Bool){
        self.offerId = offerId
        self.cartId = cartId
        self.offerName = offerName
        self.offerImage = offerImage
        self.offerDiscount = offerDiscount
        
        self.main_price = main_price
        self.offer_price = offer_price
        self.duration = duration
        self.offerText = offerText
        self.cleanUpTime = cleanUpTime
        
        self.isBeauty = isBeauty
    }
}
