//
//  ParentCategory.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation
struct ParentCategory {
    var cat_id: String = ""
    var cat_nm: String = ""
    var thumb: String = ""
    var banner: String = ""
}
