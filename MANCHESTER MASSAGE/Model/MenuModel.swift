//
//  MenuModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 17/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation
import UIKit

class MenuModel {
    var menuName: String
    var menuImageFA: FAType?
    var menuImage: UIImage?
    var isEnabled: Bool
    
    init(menuName: String = "", menuImage: FAType = .FABan, isEnabled: Bool = true){
        self.menuName = menuName
        if menuImage != .FABan{
            self.menuImageFA = menuImage
        }
        self.isEnabled = isEnabled
    }
    
    init(menuName: String, menuImage: UIImage = UIImage(), isEnabled: Bool){
        self.menuName = menuName
        self.menuImage = menuImage
        self.isEnabled = isEnabled
    }
}
