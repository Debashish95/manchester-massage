//
//  ServiceModel.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 18/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import Foundation

class ServiceModel {
    var serviceId: String
    var serviceName: String
    var serviceImage: String
    var serviceDescription: String
    var servicePrice: String
    var serviceMaxPrice: String
    var discount: String
    var duration: String
    var cleanUpTime: String
    var rating: String
    var reviewCount: String
    var sm_id: String
    
    init(serviceId: String = "", serviceName: String = "", serviceImage: String = "", serviceDescription: String = "", servicePrice: String = "", serviceMaxPrice: String = "", discount: String = "", duration: String = "", cleanUpTime: String, rating: String = "", reviewCount: String = "", sm_id: String = ""){
        self.serviceId = serviceId
        self.serviceName = serviceName
        self.serviceImage = serviceImage
        self.serviceDescription = serviceDescription
        self.servicePrice = servicePrice
        self.serviceMaxPrice = serviceMaxPrice
        self.discount = discount
        self.duration = duration
        self.cleanUpTime = cleanUpTime
        self.rating = rating
        self.reviewCount = reviewCount
        self.sm_id = sm_id
    }
}


struct ServiceDetailsModel{
    
    var smId: String
    var serviceID: String
    var catId: String
    var serviceTitle: String
    var serviceItems: [ServiceItemsModel]
}

struct ServiceItemsModel{
    var serviceID: String
    var name: String
    var duration: String
    var cleanupTime: String
    var main_price: String
    var offerPrice: String
    var therapistNumber: String
    var shareLink: String = ""
    
}
