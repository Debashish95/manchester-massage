//
//  WalletModel.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 05/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation

struct WalletModel {
    var tran_id = ""
    var cust_id = ""
    var info = ""
    var tran_type = ""
    var amount = ""
    var balance = ""
    var insert_date = ""
}

struct CardModel {
    var cardID = ""
    var brand = ""
    var expMonth = ""
    var expYear = ""
    var last4Digit = ""
    var cvvText = ""
    var isSelected = false
}
