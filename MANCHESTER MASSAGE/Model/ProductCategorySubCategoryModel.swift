//
//  ProductCategorySubCategoryModel.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 16/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation

struct ProductCategoryModel {
    var cat_id: String = ""
    var parent_cat_id: String = ""
    var cat_nm: String = ""
    var cat_url: String = ""
    var thumb: String = ""
    var subcats: [ProductCategoryModel] = [ProductCategoryModel]()
}

struct ProductListModel {
    var prod_id: String = ""
    var p_name: String = ""
    var is_featured: String = ""
    var cat_id: String = ""
    var brand_id: String = ""
    var p_url: String = ""
    var base_price: String = ""
    var discount_price: String = ""
    var thumb: String = ""
    var isInCart: Bool = false
    var cartQuantity: String = "0"
    var isInWishList: Bool = false
    var small_description: String = ""
    var big_description = ""
    var galleyImage = [GalleryImage]()
}

struct GalleryImage {
    var m_id: String = ""
    var typ_id: String = ""
    var media_url: String = ""
}


public struct ProductCartModel: Codable {
    var prod_id: String = ""
    var prod_name: String = ""
    var unit_price: String = "0"
    var price: String = "0"
    var base_price: String = "0"
    var discount_price: String = "0"
    var qnty: String = "0"
    var thumb: String = ""
    var small_description = ""
}
