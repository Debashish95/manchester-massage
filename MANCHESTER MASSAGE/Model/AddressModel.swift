//
//  AddressModel.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation

struct AddressModel {
    var addressName     : String = ""
    var addressId       : String = ""
    var fullName        : String = ""
    var phone           : String = ""
    var postalCode      : String = ""
    var addressLine1    : String = ""
    var addressLine2    : String = ""
    var town            : String = ""
    var country         : String = ""
    var security        : String = ""
    var weekendDelivery : String = ""
    var isPrimary       : Bool = false
}
