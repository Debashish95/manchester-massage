//
//  OrderListModel.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 15/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import Foundation
//struct OrderListModel {
//
//    var orderId     : String = ""
//    var productName : String = ""
//    var productImage: String = ""
//    var deliveryDate: String = ""
//}
//
//
struct OrderDetailsModel {
    var orderId             : String = ""
    var orderDate           : String = ""
    var deliveryDate        : String = ""
    var products            : [ProductListModel] = [ProductListModel]()
    var shippingAddress     : AddressModel = AddressModel()
    var invoiceURL          : String = ""
}

struct OrderListModel {
    var vendor_id   : String = ""
    var order_id    : String = ""
    var amount      : String = ""
    var created_on  : String = ""
    var address     : AddressModel = AddressModel()
    var products    : [ProductCartModel] = [ProductCartModel]()
    var track       : [TrackModel] = [TrackModel]()
    var invoiceURL  : String = ""
}

struct TrackModel {
    var ecom_order_id       : String = ""
    var track_status_id     : String = ""
    var track_status_name   : String = ""
    var msg                 : String = ""
    var timestamp           : String = ""
    var track_status_icon   : String = ""
}
