//
//  UpComingBookingTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 02/02/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class UpComingBookingTableViewCell: UITableViewCell {

    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var topImage: UIImageView!
    
    @IBOutlet weak var massageTime: UILabel!
    @IBOutlet weak var massageDate: UILabel!
    @IBOutlet weak var manageBooking: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        massageTime.text = "12:00 PM"
//        userNameLabel.text = "Manchester Massage for " + UserDefaults.standard.fetchData(forKey: .userFirstName) + " " + UserDefaults.standard.fetchData(forKey: .userLastName)
//        userNameLabel.contentMode = .center
        massageDate.text = "Tuesday \n 11 Mar 2020"
        
        massageDate.textColor = massageTime.textColor
        massageDate.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        
//        userImage.image = UIImage(icon: .FAUser, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor)
//        userImage.tintColor = ThemeColor.buttonColor
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

