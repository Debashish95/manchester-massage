//
//  PhysicalCartTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 19/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class PhysicalCartTableViewCell: UITableViewCell {
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var discountTextLabel: UILabel!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productQuantity: MMStepper!
    
    var data: ProductCartModel!{
        didSet{
            thumbImage.kf.setImage(with: URL(string: data.thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                self.thumbImage.contentMode = .scaleAspectFill
            }
            productName.text = data.prod_name
            productPriceLabel.attributedText = makeAttributedText(firstText: "\(CURRENCY_SYMBOL)\(data.base_price.formattedString())", secondText: "\(CURRENCY_SYMBOL)\(data.discount_price.formattedString())")
            productQuantity.value = (Double(data.qnty) ?? 0)
            let baseValue = Double(data.base_price) ?? 0
            let priceValue = Double(data.discount_price) ?? 0
            if baseValue == .zero{
                discountTextLabel.text = "0% Off"
            }else{
                discountTextLabel.text = "\(Int(((baseValue - priceValue) / baseValue) * 100))% Off"
            }
           
            
            deleteButton.setLeftImage(image: .FATrash, color: .red, state: .normal, size: CGSize(width: 30, height: 30))
           
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.productQuantity.layer.borderWidth = 1.0
        self.productQuantity.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.productQuantity.layer.cornerRadius = self.productQuantity.bounds.height / 2
        self.productQuantity.clipsToBounds = true
        self.productQuantity.borderWidth = 1.0
        self.productQuantity.borderColor = ThemeColor.buttonColor
        
        self.thumbImage.layer.cornerRadius = 4
        self.thumbImage.clipsToBounds = true
        
        discountView.roundCorners(corners: .bottomRight, radius: 10)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func makeAttributedText(firstText: String, secondText: String)->NSMutableAttributedString{
        let firstAttributedText = NSMutableAttributedString(string: firstText)
        firstAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.strikethroughStyle: 2,
                NSAttributedString.Key.strikethroughColor: UIColor.gray,
                NSAttributedString.Key.foregroundColor: UIColor.gray
            ],
            range: NSRange(location: 0, length: firstText.count))
        
        let secondAttributedText = NSMutableAttributedString(string: secondText)
        secondAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .semibold),
                NSAttributedString.Key.strikethroughColor: UIColor.black,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ],
            range: NSRange(location: 0, length: secondText.count))
        firstAttributedText.append(NSAttributedString(string: " "))
        firstAttributedText.append(secondAttributedText)
        return firstAttributedText
    }
}
