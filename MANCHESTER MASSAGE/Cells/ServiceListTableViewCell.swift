//
//  ServiceListTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 19/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

class ServiceListTableViewCell: UITableViewCell {

    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var serviceImage: UIImageView!
   
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var discountValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titleLabel.textColor = UIColor.black
        descriptionLabel.textColor = UIColor.gray
        detailsButton.setTitle("Details", for: .normal)
        bookButton.backgroundColor = ThemeColor.bookNowButtonColor
        detailsButton.backgroundColor = ThemeColor.buttonColor
        backView.cornnerRadius = 20
        
        
        discount.textColor = .white
        discount.font = UIFont.systemFont(ofSize: 14)
        discount.backgroundColor = UIColor(hexString: "#0071B0")
        discount.layer.cornerRadius = discount.frame.height / 2
        discount.clipsToBounds = true
        
        
        discountValue.textColor = .white
        discountValue.font = UIFont.systemFont(ofSize: 14)
        discountValue.backgroundColor = UIColor(hexString: "#0071B0")
//        discountValue.layer.cornerRadius = discountValue.frame.height / 2
//        discountValue.clipsToBounds = true
        
        favouriteButton.setLeftImage(image: .FAHeart, color: ThemeColor.buttonColor, state: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(data: ServiceModel, isBeauty: Bool = false){
        discount.text = data.discount
        if isBeauty{
            discountValue.text = "Was £\(data.serviceMaxPrice.formattedString()), now £\(data.servicePrice.formattedString())\nfor \(data.duration) Treatment"
        }else{
            discountValue.text = "Was £\(data.serviceMaxPrice.formattedString()), now £\(data.servicePrice.formattedString())\nfor \(data.duration) Massage"
        }
        
        titleLabel.text = data.serviceName
        descriptionLabel.text = data.serviceDescription
        var finalURLString = ""
        if data.serviceImage.starts(with: "http"){
            finalURLString = data.serviceImage
        }else{
            finalURLString = Config.mainUrl + "uploads/cat/thumb/" + data.serviceImage
        }
        serviceImage.kf.setImage(with: URL(string: finalURLString), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
            self.serviceImage.contentMode = .scaleAspectFill
        }
        bookButton.setTitle("Book Now", for: .normal)
    }
    
}
