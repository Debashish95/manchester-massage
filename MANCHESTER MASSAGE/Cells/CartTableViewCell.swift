//
//  CartTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 29/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        yesButton.setTitleColor(.black, for: .normal)
        yesButton.setTitleColor(.black, for: .selected)
        yesButton.setLeftImage(image: .FACircleO, color: .black, state: .normal)
        yesButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
        yesButton.addTarget(self, action: #selector(yesButtonAction), for: .touchUpInside)
        yesButton.isSelected = true
        
        deleteButton.setLeftImage(image: .FATrashO, color: .red, state: .normal)
        deleteButton.setTitleColor(.red, for: UIControl.State())
        
        editButton.setLeftImage(image: .FAPencil, color: ThemeColor.buttonColor, state: .normal)
        editButton.setTitleColor(.red, for: UIControl.State())
        
        noButton.setTitleColor(.black, for: .normal)
        noButton.setTitleColor(.black, for: .selected)
        noButton.setLeftImage(image: .FACircleO, color: .black, state: .normal)
        noButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
        noButton.addTarget(self, action: #selector(noButtonAction), for: .touchUpInside)
        
        nameTextField.placeholder = "Full Name"
        nameTextField.delegate = self
        emailTextField.delegate = self
        phoneTextField.placeholder = "Phone No."
        emailTextField.placeholder = "Email ID"
        
    }
    
    func setData(cartData: CartModel){
        serviceNameLabel.text = cartData.services[0].name
        let durationInt = (Int(cartData.services[0].duration) ?? 0) * 60
        let cleanupInt = (Int(cartData.services[0].cleanupTime) ?? 0) * 60
        
        var durationText = ""
        if cleanupInt == 0{
            durationText =  (durationInt - cleanupInt).secondsToTime()
        }else{
            durationText =  (durationInt - cleanupInt).secondsToTime().appending(", \(cleanupInt.secondsToTime()) clean up")
        }
        durationLabel.attributedText = makeAttributedString(key: "Duration: ", value: durationText)
        priceLabel.attributedText = makeAttributedString(key: "Price: ", value: (CURRENCY_SYMBOL + cartData.services[0].offerPrice))
        
        if(cartData.isForMe){
            yesButton.isSelected = true
            noButton.isSelected = false
        }
        else{
            yesButton.isSelected = false
            noButton.isSelected = true
           
        }
      //  timeLabel.text = ("Time: " + cartData.bookingTime + " " + cartData.bookingDay + ", " + cartData.bookingDate)
        
    }
    
    @objc func yesButtonAction(){
        yesButton.isSelected = true
        noButton.isSelected = false
    }
    @objc func noButtonAction(){
        yesButton.isSelected = false
        noButton.isSelected = true
    }
    
    func reloadView(tag: Int){
        nameTextField.isHidden = yesButton.isSelected
        phoneTextField.isHidden = yesButton.isSelected
        emailTextField.isHidden = yesButton.isSelected
        stackView.isHidden = yesButton.isSelected
        
        nameTextField.text = appD.serviceCart[tag].cust_name
        emailTextField.text = appD.serviceCart[tag].cust_email
        phoneTextField.text = appD.serviceCart[tag].cust_phone
        
    }
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        attributedString.append(two)
        return attributedString
    }
    
    let appD = UIApplication.shared.delegate as! AppDelegate
}

extension CartTableViewCell: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.accessibilityLabel == "name"{
            appD.serviceCart[nameTextField.tag].cust_name = textField.text ?? ""
        }else if textField.accessibilityLabel == "email"{
            appD.serviceCart[emailTextField.tag].cust_email = textField.text ?? ""
        }
    }
}
