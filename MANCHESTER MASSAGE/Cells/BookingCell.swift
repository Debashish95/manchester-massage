//
//  UpComingBookingTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 02/02/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class BookingCell: UITableViewCell {

    @IBOutlet weak var appointmentLabel: UILabel!
    @IBOutlet weak var appointmentDateImage: UIImageView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var durationImage: UIImageView!
    @IBOutlet weak var paymentImage: UIImageView!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var massageName: UILabel!
    @IBOutlet weak var therapistName: UILabel!
    @IBOutlet weak var durationTxt: UILabel!
    @IBOutlet weak var watchImage: UIImageView!
    @IBOutlet weak var watchImage2: UIImageView!
    @IBOutlet weak var bookImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        watchImage.setFAIconWithName(icon: .FAUser, textColor: ThemeColor.buttonColor)
        watchImage2.tintColor = ThemeColor.buttonColor
        durationImage.setFAIconWithName(icon: .FAClockO, textColor: ThemeColor.buttonColor)
        appointmentDateImage.setFAIconWithName(icon: .FAClockO, textColor: ThemeColor.buttonColor)
        bookImage.tintColor = ThemeColor.buttonColor
        
        paymentImage.setFAIconWithName(icon: .FAGbp, textColor: ThemeColor.buttonColor)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
