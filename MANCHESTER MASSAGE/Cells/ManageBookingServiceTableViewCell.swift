//
//  ManageBookingServiceTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 04/12/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class ManageBookingServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var bookedForLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var therapistLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var rescheduleButton: UIButton!
    
    
    let appD = UIApplication.shared.delegate as! AppDelegate
    
    var data: servicelist!{
        didSet{
            bookedForLabel.attributedText = makeAttributedString(firstString: "Booked For : ", secondString: (data.custName == "" ? "N/A" : data.custName))
            serviceNameLabel.attributedText = makeAttributedString(firstString: "Service : ", secondString: data.serviceName)
            
            paymentLabel.attributedText = makeAttributedString(firstString: "Prepaid Amount : ", secondString: "\(CURRENCY_SYMBOL)\(data.subTotal)")
            
            
            let totalDuration = convertDuration(duartion: data.duration, cleanUp: data.cleanup_time)
            
            if Int(totalDuration.1) == 0{
                durationLabel.attributedText = makeAttributedString(firstString: "Duration : ", secondString: totalDuration.0)
            }else{
                durationLabel.attributedText = makeAttributedString(firstString: "Duration : ", secondString: "\(totalDuration.0), \(totalDuration.1) clean up")
            }
            
            var therapistNames = [String]()
            data.therapistArr = data.therapistArr.replacingOccurrences(of: "[", with: "")
            data.therapistArr = data.therapistArr.replacingOccurrences(of: "]", with: "")
            data.therapistArr = data.therapistArr.replacingOccurrences(of: "\\", with: "")
            data.therapistArr = data.therapistArr.replacingOccurrences(of: "|", with: "")
            data.therapistArr = data.therapistArr.replacingOccurrences(of: "\"", with: "")
            data.therapistArr = data.therapistArr.replacingOccurrences(of: "\'", with: "")
            data.therapistArr = data.therapistArr.replacingOccurrences(of: " ", with: "")
            let finalTherapistIDS = data.therapistArr.components(separatedBy: ",")
            for index in 0..<finalTherapistIDS.count{
                let currentTherapistID = finalTherapistIDS[index]
                if let currentEmployee = appD.employeeDetails.first(where: {$0.id == currentTherapistID}){
                    therapistNames.append(currentEmployee.user_name + " " + currentEmployee.user_lname)
                }
            }
            
            therapistLabel.attributedText = makeAttributedString(firstString: "Therapist : ", secondString: therapistNames.joined(separator: ", "))
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    fileprivate func makeAttributedString(firstString: String, secondString: String) -> NSAttributedString{
        
        let firstMutableString = NSMutableAttributedString(string: firstString)
        firstMutableString.addAttributes(
            [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium)],
            range: NSRange(location: 0, length: firstMutableString.length))
        
        let secondMutableString = NSMutableAttributedString(string: secondString)
        secondMutableString.addAttributes(
            [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium)],
            range: NSRange(location: 0, length: secondMutableString.length))
        firstMutableString.append(secondMutableString)
        return firstMutableString
    }
    
    func convertDuration(duartion: String, cleanUp: String) -> (String, String){
        let durationInt = (Int(duartion) ?? 0) * 60
        let cleanUpInt = (Int(cleanUp) ?? 0) * 60
        let finalTime = (durationInt - cleanUpInt)
        return (finalTime.secondsToTime(), cleanUpInt.secondsToTime())
    }
    
}
