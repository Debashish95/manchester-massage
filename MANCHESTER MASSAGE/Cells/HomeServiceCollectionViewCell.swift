//
//  HomeServiceCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 18/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

class HomeServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var reviewView1: CosmosView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var offerPriceLabel: UILabel!
    @IBOutlet weak var mainPriceLabel: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var bookNowBtn: UIButton!
    var homeViewController: HomeViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bookNowBtn.backgroundColor = ThemeColor.bookNowButtonColor
        offerPriceLabel.textColor = ThemeColor.bookNowButtonColor
        
        self.reviewView1.settings.fillMode = .precise
        self.ratingView.settings.fillMode = .precise
        self.ratingView.rating = 0
        self.ratingView.text = ""
        self.ratingView.settings.textFont = .boldSystemFont(ofSize: 14)
    
    }
    
    func setData(data: ServiceModel, isBeauty: Bool){
        serviceImage.kf.setImage(with: URL(string: data.serviceImage), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
            self.serviceImage.contentMode = .scaleAspectFill
        }
        serviceName.text = data.serviceName
        
        discountLabel.text = data.discount
        discountLabel.textColor = .white
        discountLabel.font = UIFont.systemFont(ofSize: 10)
        discountLabel.backgroundColor = ThemeColor.bookNowButtonColor
        
        reviewView1.rating = Double(data.rating) ?? 0
        ratingView.rating = Double(data.rating) ?? 0
        ratingView.text = "(\(data.reviewCount) review(s))"
        ratingView.isHidden = true
        
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "£" + data.serviceMaxPrice.formattedString())
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        mainPriceLabel.attributedText = attributeString
        
        offerPriceLabel.text = "£" + data.servicePrice.formattedString()
        if isBeauty{
            durationLabel.text = "for \(data.duration) Treatment"
        }else{
            durationLabel.text = "for \(data.duration) Massage"
        }
        
       // 
    }

   
    
}


@IBDesignable class GradientView: UIView{
    override func draw(_ rect: CGRect) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.5).cgColor]
        
        self.layer.addSublayer(gradientLayer)
    }
}
