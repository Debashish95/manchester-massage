//
//  ProductCategoryCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 16/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    var data: ProductCategoryModel!{
        didSet{
            categoryLabel.text = data.cat_nm
            categoryImageView.kf.setImage(with: URL(string: data.thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                self.categoryImageView.contentMode = .scaleAspectFill
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
