//
//  OrderDetailsTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 17/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class OrderDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var viewInvoiceNextImage: UIImageView!
    @IBOutlet weak var buyAgainNextImage: UIImageView!
//    @IBOutlet weak var writeReviewNextImage: UIImageView!
    
    
    @IBOutlet weak var securityCodeLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressTextLabel: UILabel!
    @IBOutlet weak var addressNameLabel: UILabel!
    
//    @IBOutlet weak var writeReviewButton: UIButton!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var viewInvoiceButton: UIButton!
    @IBOutlet weak var backView: CardView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewInvoiceNextImage.setFAIconWithName(icon: .FAAngleRight, textColor: .darkGray)
        buyAgainNextImage.setFAIconWithName(icon: .FAAngleRight, textColor: .darkGray)
//        writeReviewNextImage.setFAIconWithName(icon: .FAAngleRight, textColor: .darkGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        viewInvoiceButton.layer.cornerRadius = 5//viewInvoiceButton.layer.bounds.height / 2
        viewInvoiceButton.layer.borderWidth = 1
        viewInvoiceButton.layer.borderColor = UIColor.lightGray.cgColor
        viewInvoiceButton.clipsToBounds = true
        
        buyNowButton.layer.cornerRadius = 5
        buyNowButton.clipsToBounds = true
        buyNowButton.layer.borderWidth = 1
        buyNowButton.layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
