//
//  ManageBookingFooterTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 04/12/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class ManageBookingFooterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var menuTitleLabel: UILabel!
    @IBOutlet weak var menuRightImageView: UIImageView!
    
    @IBOutlet weak var backView: CardView!
    
    var data: BookingMenuModel!{
        didSet{
            leftImageView.image = data.image
            menuTitleLabel.text = data.menuName
            if data.accessibilityName == "share"{
                leftImageView.setFAIconWithName(icon: .FAShareAlt, textColor: ThemeColor.buttonColor)
            }else if data.accessibilityName == "rebook"{
                leftImageView.setFAIconWithName(icon: .FAUndo, textColor: ThemeColor.buttonColor)
            }else if data.accessibilityName.contains("review"){
                leftImageView.setFAIconWithName(icon: .FAStarO, textColor: ThemeColor.buttonColor)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftImageView.tintColor = ThemeColor.buttonColor
        menuRightImageView.setFAIconWithName(icon: .FAChevronRight, textColor: .gray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
