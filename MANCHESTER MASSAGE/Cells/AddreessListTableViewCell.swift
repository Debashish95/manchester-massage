//
//  AddreessListTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class AddreessListTableViewCell: UITableViewCell {

    @IBOutlet weak var radioButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var securityCodeLabel: UILabel!
    @IBOutlet weak var weekEndDeliveryLabel: UILabel!
    
    @IBOutlet weak var defaultAddressButton: UIButton!
    
    var isFromCartPage: Bool = false

    var data: AddressModel!{
        didSet{
            fullNameLabel.text = data.fullName + "(\(data.addressName))"
            var addressArray = [String]()
            addressArray.append(data.addressLine1)
            if data.addressLine2 != ""{
                addressArray.append(data.addressLine2)
            }
            addressArray.append(data.town + " \(data.postalCode)")
            addressArray.append(data.country)
            addressLabel.attributedText = makeAttributedString(key: "Address: ", value: addressArray.joined(separator: ","))
            
            phoneLabel.attributedText = makeAttributedString(key: "Phone: ", value: data.phone)
            securityCodeLabel.attributedText = makeAttributedString(key: "Security Code: ", value: (data.security == "" ? "N/A" : data.security))
            var weekEndDelivery = ""
            switch data.weekendDelivery {
            case "0":
                weekEndDelivery = "N/A"
                break
            case "1":
                weekEndDelivery = "Saturday"
                break
            case "2":
                weekEndDelivery = "Sunday"
                break
            case "3":
                weekEndDelivery = "Both Saturday and Sunday"
                break
            default:
                break
            }
            weekEndDeliveryLabel.attributedText = makeAttributedString(key: "Weekend Delivery: ", value: weekEndDelivery)
            radioButtonWidth.constant = isFromCartPage ? 40 : 0
            if isFromCartPage{
                defaultAddressButton.isSelected = data.isPrimary
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
 
        deleteButton.setLeftImage(image: .FATrashO, color: .red, state: .normal)
        deleteButton.setTitleColor(.red, for: UIControl.State())
        
        editButton.setLeftImage(image: .FAPencil, color: ThemeColor.buttonColor, state: .normal)
        editButton.setTitleColor(.red, for: UIControl.State())
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        attributedString.append(two)
        return attributedString
    }
    
}
