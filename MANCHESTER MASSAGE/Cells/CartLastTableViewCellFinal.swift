//
//  CartLastTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 09/03/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import MapKit
class CartLastTableViewCellFinal: UITableViewCell {

    @IBOutlet weak var payAtDeskButton: UIButton!
    @IBOutlet weak var cardPayment: UIButton!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var addMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addMore.layer.cornerRadius = addMore.frame.height / 2
        addMore.clipsToBounds = true
        addMore.backgroundColor = ThemeColor.buttonColor
        addMore.setLeftImage(image: .FAPlus, color: .white, state: .normal)
        
        payAtDeskButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        payAtDeskButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        
        cardPayment.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        cardPayment.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
       
        payAtDeskButton.setTitle("Pay at Desk", for: UIControl.State())
        payAtDeskButton.setTitleColor(.darkGray, for: .normal)
        payAtDeskButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        
        cardPayment.setTitle("Card Payment", for: UIControl.State())
        cardPayment.setTitleColor(.darkGray, for: .normal)
        cardPayment.setTitleColor(ThemeColor.buttonColor, for: .selected)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
