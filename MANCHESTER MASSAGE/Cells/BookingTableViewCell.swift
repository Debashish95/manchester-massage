//
//  BookingTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 27/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class BookingTableViewCell: UITableViewCell {
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var bookingButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        customerName.text = ""
        serviceLabel.text = ""
        typeLabel.text = ""
        dateLabel.text = ""
        
        bookingButton.setTitle("Book Again", for: .normal)
        bookingButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        bookingButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
    }
    
    func setData(data: BookingsModel){
        self.customerName.text = data.firstName + " " + data.lastName
       // self.serviceLabel.text = "Service: " + data.serviceName
       // self.typeLabel.text = "Type: " + data.serviceType + ", £50 (\(data.therapistName))"
       // self.dateLabel.text = "Date:" + data.bookingDate
    }
    
}
