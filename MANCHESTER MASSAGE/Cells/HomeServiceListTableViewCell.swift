//
//  HomeServiceListTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 18/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class HomeServiceListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var homeViewController: HomeViewController!
    var services = [ServiceModel]()
    var isBeautyService: Bool!{
        didSet{
            if isBeautyService{
                seeAllButton.setTitle("View All Beauty Treatments", for: .normal)
            }else{
                seeAllButton.setTitle("View All Massage Sevices", for: .normal)
            }
            titleLabel.text = isBeautyService ? "BEAUTY TREATMENTS": "MASSAGE SERVICES"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "HomeServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeServiceCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
        
        
        seeAllButton.setTitleColor(.white, for: .normal)
        seeAllButton.backgroundColor = ThemeColor.buttonColor
        seeAllButton.layer.cornerRadius = seeAllButton.frame.height / 2
        seeAllButton.clipsToBounds = true
        seeAllButton.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(16), weight: .semibold)
        seeAllButton.addTarget(self, action: #selector(seeAllButtonAction), for: .touchUpInside)
        
        
        
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: CGFloat(18), weight: .bold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: [ServiceModel], from viewController: HomeViewController){
        services = data
        homeViewController = viewController
        collectionView.reloadData()
    }
    @objc func seeAllButtonAction(){
        homeViewController.seeAllButtonAction(isBeauty: isBeautyService)
    }
}

extension HomeServiceListTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count > 4 ? 4 : services.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "HomeServiceCollectionViewCell", for: indexPath) as! HomeServiceCollectionViewCell
        cell.setData(data: services[indexPath.item], isBeauty: isBeautyService)
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1.2
        cell.layer.cornerRadius = 10
        cell.discountLabel.layer.cornerRadius = cell.discountLabel.frame.height / 2
        cell.discountLabel.clipsToBounds = true
        cell.bookNowBtn.tag = indexPath.row
        cell.bookNowBtn.addTarget(self, action: #selector(bookNow(sender:)), for: .touchUpInside)
        
        cell.detailsButton.tag = indexPath.row
        cell.detailsButton.addTarget(self, action: #selector(self.serviceDetailsBtnAction(sender:)), for: .touchUpInside)
        
        cell.clipsToBounds = true
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = UIScreen.main.bounds.width
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: collectionViewWidth / 2.2, height: ((collectionViewWidth / 2.0)))
        }else{
            return CGSize(width: collectionViewWidth / 2.35, height: ((collectionViewWidth / 2.0)))
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        homeViewController.collectionView(collectionView, didSelectItemAt: indexPath, isBeauty: isBeautyService)
    }
    
    @objc func bookNow(sender: UIButton){
        homeViewController.setData(catID: self.services[sender.tag].serviceId)
    }
    
    @objc func serviceDetailsBtnAction(sender: UIButton){
        homeViewController.serviceDetailsBtnAction(sender: sender, isBeauty: isBeautyService)
    }
    
}
