//
//  HomeOffersTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class HomeOffersTableViewCell: UITableViewCell {

    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var offerName: UILabel!
    @IBOutlet weak var offerDescription: UILabel!
    @IBOutlet weak var offerDiscount: UILabel!
    
    @IBOutlet weak var detailsBtn: UIButton!
    @IBOutlet weak var bookNow: UIButton!
    @IBOutlet weak var btnStack: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.backgroundColor = UIColor(hexString: "#f1f1f1")
        
        detailsBtn.setTitle("Details", for: .normal)
        bookNow.setTitle("Book Now", for: .normal)
        bookNow.backgroundColor = ThemeColor.bookNowButtonColor
        detailsBtn.backgroundColor = ThemeColor.buttonColor
        
        detailsBtn.layer.cornerRadius = 5
        detailsBtn.layer.masksToBounds = true
        detailsBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        bookNow.layer.cornerRadius = 5
        bookNow.layer.masksToBounds = true
        bookNow.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        
        
    }
    
    func setData(data: OfferModel, from: HomeViewController){
        offerImage.kf.setImage(with: URL(string: data.offerImage), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
            self.offerImage.contentMode = .scaleAspectFill
        }
        offerName.text = data.offerName
        
       // let descriptionText = "as £\(data.main_price), Now £\(data.offer_price) for \(data.duration) Massage"W
        
        
        
        let one = NSMutableAttributedString(string:"Was ")
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: "£\(data.main_price)")
               attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
        
      //  let attributedString = NSMutableAttributedString(string:)
        let two = NSMutableAttributedString(string:", Now ")
        let attrs = [NSAttributedString.Key.foregroundColor : ThemeColor.bookNowButtonColor, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string: "£\(data.offer_price)", attributes:attrs)
        
        var last = NSMutableAttributedString(string:"\nfor \(data.duration) Massage")
        if data.isBeauty{
            last = NSMutableAttributedString(string:"\nfor \(data.duration) Service")
        }
         
        one.append(attributedString)
        one.append(two)
        one.append(boldString)
        one.append(last)
        
        offerDescription.numberOfLines = 0
        offerDescription.attributedText = one//data.offerText
        offerDiscount.text = data.offerDiscount
        offerDiscount.textColor = .white
        offerDiscount.font = UIFont.systemFont(ofSize: 10)
        offerDiscount.backgroundColor = ThemeColor.buttonColor
    }
    
}

extension UIStackView {
    func customize(backgroundColor: UIColor = .clear, radiusSize: CGFloat = 10.0) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = backgroundColor
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)

        subView.layer.cornerRadius = radiusSize
        subView.layer.masksToBounds = true
        subView.clipsToBounds = true
    }
}
