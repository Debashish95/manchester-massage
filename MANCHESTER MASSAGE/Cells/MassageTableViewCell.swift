//
//  MassageTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 09/03/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class MassageTableViewCell: UITableViewCell {

    @IBOutlet weak var radioImage: UIImageView!
    @IBOutlet weak var massageLabel: UILabel!
    
    var data: ServiceItemsModel!{
        didSet{
            radioImage.image = UIImage(icon: .FACircleO, size: CGSize(width: 25, height: 25))
            radioImage.tintColor = .black
           // let name = NSAttributedString(string: (data.name + "\n"))
            let name = NSAttributedString(string: (data.name + "\n"), attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .bold)])

            let durationInt = (Int(data.duration) ?? 0 ) * 60
            let cleanupTimeInt = (Int(data.cleanupTime) ?? 0 ) * 60
            let duration = (durationInt - cleanupTimeInt).secondsToTime().appending(", ")
            if cleanupTimeInt > 0{
                duration
                    .appending(", ")
                    .appending(cleanupTimeInt.secondsToTime()).appending(" cleanup\n")
            }else{
                duration
                    .appending("\n")
            }
                
            let durationAttributedText = NSAttributedString(string: duration, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .bold)])
            
            let mainPrice = CURRENCY_SYMBOL + data.main_price.formattedString()
            let mainPriceAttributedText = NSAttributedString(string: mainPrice, attributes: [NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor: UIColor.gray,
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: .bold)])
            
            let offerPrice = CURRENCY_SYMBOL + data.offerPrice.formattedString() + " "
            let offerPriceAttributedString = NSAttributedString(string: offerPrice, attributes: [NSAttributedString.Key.foregroundColor: ThemeColor.bookNowButtonColor,
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .bold)])
            
            let finalAttributedString = NSMutableAttributedString()
            finalAttributedString.append(name)
            finalAttributedString.append(durationAttributedText)
            finalAttributedString.append(offerPriceAttributedString)
            finalAttributedString.append(mainPriceAttributedText)
            
            massageLabel.attributedText = finalAttributedString
                
        }
    }
    
    var isSelectedCell: Bool = false{
        didSet{
            if isSelectedCell{
               
                radioImage.setFAIconWithName(icon: .FADotCircleO, textColor: ThemeColor.buttonColor, orientation: .up, backgroundColor: .clear, size: CGSize(width: 25, height: 25))
            }else{
                radioImage.image = UIImage(icon: .FACircleO, size: CGSize(width: 25, height: 25))
                radioImage.tintColor = .black
            }
            
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
