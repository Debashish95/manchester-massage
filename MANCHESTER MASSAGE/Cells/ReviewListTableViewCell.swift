//
//  ReviewListTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 19/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Cosmos
class ReviewListTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ratingView.settings.fillMode = .precise
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
