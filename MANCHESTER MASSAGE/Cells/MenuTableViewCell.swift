//
//  MenuTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 17/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var bottomDividerView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        menuNameLabel.text = ""
        menuNameLabel.textColor = .white
        menuNameLabel.backgroundColor = .clear
        contentView.backgroundColor = .clear
        menuNameLabel.font = UIFont.systemFont(ofSize: CGFloat(15), weight: .bold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: MenuModel){
        self.menuNameLabel.text = data.menuName
        if data.menuImageFA == nil{
            self.menuImage.image = data.menuImage
        }
        else{
            self.menuImage.image = UIImage(icon: data.menuImageFA!, size: CGSize(width:30, height: 30))
        }
        self.menuImage.image = self.menuImage.image?.withRenderingMode(.alwaysTemplate)
        self.menuImage.tintColor = .white
        menuNameLabel.textColor = data.isEnabled ? .white : .lightGray
        menuImage.tintColor = data.isEnabled ? .white : .lightGray
        self.isUserInteractionEnabled = data.isEnabled
        
    }
    
}
