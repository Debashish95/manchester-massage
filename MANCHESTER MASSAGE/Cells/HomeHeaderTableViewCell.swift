//
//  HomeHeaderTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 18/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Kingfisher

class HomeHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bannerView: AACarousel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.textAlignment = .center
        titleLabel.textColor = ThemeColor.buttonColor
        titleLabel.font = UIFont.systemFont(ofSize: CGFloat(15), weight: .bold)
        titleLabel.text = "Massage Therapies for Men & Women"
        bannerView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(bannerData: [String]){
        
        bannerView.setCarouselData(paths: bannerData, describedTitle: [""], isAutoScroll: true, timer: 5.0, defaultImage: "")
        //optional method
        bannerView.setCarouselOpaque(layer: false, describedTitle: true, pageIndicator: false)
        bannerView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: UIColor.white, describedTitleColor: UIColor.white, layerColor: UIColor.clear)
    }
    
}
extension HomeHeaderTableViewCell: AACarouselDelegate{
    func downloadImages(_ url: String, _ index: Int) {
        let imageView = UIImageView()

        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage(), options: nil, progressBlock: nil) { (result) in
            switch result {
            case .success(let value):
                self.bannerView.images[index] = value.image
            case .failure(let error):
                print("Job failed0: \(error.localizedDescription)")
            }
            imageView.contentMode = .scaleAspectFill
        }
    }
    
    /// callBackFirstDisplayView : function for loading Image for Sliding in Banner
    /// - Returns: Void
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        imageView.kf.setImage(with:URL(string: url[index]), placeholder: UIImage(), options: nil, progressBlock: nil) { (result) in
            
            imageView.contentMode = .scaleAspectFill
        }
    }
}
