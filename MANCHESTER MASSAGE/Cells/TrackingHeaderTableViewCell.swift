//
//  TrackingHeaderTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 29/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class TrackingHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var trackingStatusImage: UIImageView!
    @IBOutlet weak var showHideTrackingStatus: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        trackingStatusImage.setFAIconWithName(icon: .FAAngleRight, textColor: .darkGray)
        showHideTrackingStatus.layer.cornerRadius = 5//viewInvoiceButton.layer.bounds.height / 2
        showHideTrackingStatus.layer.borderWidth = 1
        showHideTrackingStatus.layer.borderColor = UIColor.lightGray.cgColor
        showHideTrackingStatus.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
