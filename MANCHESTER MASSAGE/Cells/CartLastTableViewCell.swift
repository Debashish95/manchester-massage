//
//  CartLastTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 09/03/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import MapKit
class CartLastTableViewCell: UITableViewCell {

    @IBOutlet weak var grandTotalLabel: UILabel!
    
    @IBOutlet weak var addMore: UIButton!
    //@IBOutlet weak var cardPaymentButton: UIButton!
    //@IBOutlet weak var payAtDeskButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapAddress: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addMore.layer.cornerRadius = addMore.frame.height / 2
        addMore.clipsToBounds = true
        addMore.backgroundColor = ThemeColor.buttonColor
        addMore.setLeftImage(image: .FAPlus, color: .white, state: .normal)
        mapAddress.titleLabel?.numberOfLines = 3
        mapAddress.setSupportButton(image: .FALocationArrow, title: "Manchester Massage \n28a Swan street, Manchester Northern Quarters, M4 5JQ, UK", color: .black)
        
        let artwork = Artwork(
          title: "Manchester Massage",
          locationName: "",
          discipline: "",
          coordinate: CLLocationCoordinate2D(latitude: 53.4853806, longitude:  -2.2340228))
        
        
        let oahuCenter = CLLocation(latitude: 53.4853806, longitude:  -2.2340228)
        let region = MKCoordinateRegion(
          center: oahuCenter.coordinate,
          latitudinalMeters: 0,
          longitudinalMeters: 0)
        if #available(iOS 13.0, *) {
            mapView.setCameraBoundary(
                MKMapView.CameraBoundary(coordinateRegion: region),
                animated: true)
            let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 1000)
            mapView.setCameraZoomRange(zoomRange, animated: true)
        } else {
            // Fallback on earlier versions
        }
        
        
        
        mapView.addAnnotation(artwork)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gotoMapView))
        tapGesture.numberOfTapsRequired = 1
        mapView.addGestureRecognizer(tapGesture)
        mapAddress.addTarget(self, action: #selector(gotoMapView), for: .touchUpInside)
        //addMore.addTarget(self, action: #selector(self.bookNowAction), for: .touchUpInside)
    }
    
    @objc func gotoMapView(){
        let accessValue = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = accessValue
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            
            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
