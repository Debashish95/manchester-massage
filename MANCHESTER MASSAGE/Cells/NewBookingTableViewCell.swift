//
//  NewBookingTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 02/12/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class NewBookingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cancellationStatusLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var mainServiceLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var manageBooking: UIButton!
    
    var data: BookingsModel!{
        didSet{
//            serviceData = data.service
//            let firstString = NSMutableAttributedString(string: "Date: ")
////            firstString.addAttributes(
////                [
////                    NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)],
////                range: NSRange(location: 0, length: firstString.length))
//            let secondText = NSMutableAttributedString(string: "\(self.convertDate(data.bookdt, currentFormat: "yyyy-MM-dd", requiredFormat: "dd MMMM yyyy, EEE")), \(data.timeslot)")
//
//            firstString.append(secondText)
//            dateLabel.attributedText = firstString
//            mainServiceLabel.text = data.services.first?.serviceName ?? ""
//            if data.services.count > 1{
//                mainServiceLabel.text! += "( + \(data.services.count - 1) more)"
//            }
//            paymentLabel.text = "Prepaid \(CURRENCY_SYMBOL)\(data.amount)"
//            cancellationStatusLabel.text = data.cancellationStatus.rawValue
//            manageBooking.contentHorizontalAlignment = data.cancellationStatus == .none ? .center : .right
////            manageBooking.contentHorizontalAlignment = .right
        }
    }
    
    var serviceData: servicelist!{
        didSet{
            
            let firstString = NSMutableAttributedString(string: "Date: ")
//            firstString.addAttributes(
//                [
//                    NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)],
//                range: NSRange(location: 0, length: firstString.length))
            let secondText = NSMutableAttributedString(string: "\(self.convertDate(serviceData.bookingDate, currentFormat: "yyyy-MM-dd", requiredFormat: "dd MMMM yyyy, EEE")), \(serviceData.bookingTime)")
            
            firstString.append(secondText)
            dateLabel.attributedText = firstString
            mainServiceLabel.text = serviceData.serviceName
            if data.services.count > 1{
                mainServiceLabel.text! += "( + \(data.services.count - 1) more)"
            }
            paymentLabel.text = "Prepaid \(CURRENCY_SYMBOL)\(serviceData.basePrice)"
            
            cancellationStatusLabel.text = serviceData.bookingStatus == "2" ? CancellationStatus.full.rawValue : CancellationStatus.none.rawValue
//            manageBooking.contentHorizontalAlignment = serviceData.bookingStatus == .none ? .center : .right
            manageBooking.contentHorizontalAlignment = .right
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rightImageView.setFAIconWithName(icon: .FAChevronRight, textColor: UIColor.gray)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func convertDate(_ date: String, currentFormat: String, requiredFormat: String) -> String{
        let df = DateFormatter()
        df.dateFormat = currentFormat
        let curretDate = df.date(from: date) ?? Date()
        df.dateFormat = requiredFormat
        return df.string(from: curretDate)
    }
    
}
