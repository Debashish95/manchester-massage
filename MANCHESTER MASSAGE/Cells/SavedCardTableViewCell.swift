//
//  SavedCardTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 16/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class SavedCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    
    var data: CardModel!{
        didSet{
            selectedImageView.isHidden = !data.isSelected
            cvvTextField.isHidden = !data.isSelected
            expiryLabel.text = "\(data.expMonth)/\(data.expYear)"
            cardNumberLabel.text = "XXXX-XXXX-XXXX-\(data.last4Digit)"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        selectedImageView.setFAIconWithName(icon: .FACheckCircle, textColor: ThemeColor.buttonColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
