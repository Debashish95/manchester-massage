//
//  CartTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 29/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class CartTableViewCellFinal: UITableViewCell {

    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
//    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var therapistLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    
//    @IBOutlet weak var yesButton: UIButton!
//    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var notesButton: UIButton!
    
    
    
    
//    @IBOutlet weak var nameTextField: UITextField!
//    @IBOutlet weak var phoneTextField: UITextField!
//    @IBOutlet weak var emailTextField: UITextField!
//    
//    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        yesButton.setTitleColor(.black, for: .normal)
//        yesButton.setTitleColor(.black, for: .selected)
//        yesButton.setLeftImage(image: .FACircleO, color: .black, state: .normal)
//        yesButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
//        yesButton.addTarget(self, action: #selector(yesButtonAction), for: .touchUpInside)
//        yesButton.isSelected = true
        
        deleteButton.setLeftImage(image: .FATrashO, color: .red, state: .normal)
        deleteButton.setTitle("Remove", for: UIControl.State())
        deleteButton.setTitleColor(.gray, for: UIControl.State())
        deleteButton.titleLabel?.numberOfLines = 2
        editButton.titleLabel?.numberOfLines = 2
        editButton.setLeftImage(image: .FAPencil, color: ThemeColor.buttonColor, state: .normal)
        editButton.setTitle("Edit", for: UIControl.State())
        editButton.setTitleColor(.gray, for: UIControl.State())
        
        notesButton.titleLabel?.numberOfLines = 2
        notesButton.setLeftImage(image: .FAPencilSquareO, color: ThemeColor.buttonColor, state: .normal)
        notesButton.setTitle("Notes", for: UIControl.State())
        notesButton.setTitleColor(.gray, for: UIControl.State())
        
        
//        noButton.setTitleColor(.black, for: .normal)
//        noButton.setTitleColor(.black, for: .selected)
//        noButton.setLeftImage(image: .FACircleO, color: .black, state: .normal)
//        noButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
//        noButton.addTarget(self, action: #selector(noButtonAction), for: .touchUpInside)
        
//        nameTextField.placeholder = "Full Name"
//        phoneTextField.placeholder = "Phone No."
//        emailTextField.placeholder = "Email ID"
        
    }
    
    func setData(cartData: CartModel){
        serviceNameLabel.text = cartData.services[0].name
        let durationInt = (Int(cartData.services[0].duration) ?? 0) * 60
        let cleanupInt = (Int(cartData.services[0].cleanupTime) ?? 0) * 60
        var durationText = ""
        if cleanupInt == 0{
            durationText =  (durationInt - cleanupInt).secondsToTime()
        }else{
            durationText =  (durationInt - cleanupInt).secondsToTime().appending(", \(cleanupInt.secondsToTime()) clean up")
        }
        durationLabel.attributedText = makeAttributedString(key: "Duration: ", value: durationText)
        priceLabel.attributedText = makeAttributedString(key: "Price: ", value: (CURRENCY_SYMBOL + cartData.services[0].offerPrice))
        timeLabel.attributedText = makeAttributedString(key: "Time: ", value: (cartData.bookingTime + ", " + getDay(dateString:cartData.bookingDay) + ", " + getDateString(dateString: cartData.bookingDate)))
        print("Booking Date: \(cartData.bookingDate)")
        var therapistName = ""
        for index in 0..<cartData.therapists.count{
            therapistName += "\(cartData.therapists[index].user_name) \(cartData.therapists[index].user_lname), "
        }
        if therapistName.count > 1{
            let _ = therapistName.removeLast()
            let _ = therapistName.removeLast()
        }else if therapistName.count > 0{
            let _ = therapistName.removeLast()
        }
        
        if !cartData.cust_name.isEmpty{
            customerNameLabel.attributedText = makeAttributedString(key: "For: ", value: cartData.cust_name)
        }else{
            customerNameLabel.attributedText = makeAttributedString(key: "For: ", value: "N/A")
        }
        
        therapistLabel.attributedText = makeAttributedString(key: "Therapist: ", value: therapistName)
        if cartData.notes.count > 0{
            notesLabel.attributedText = makeAttributedString(key: "Notes: ", value: cartData.notes)
        }else{
            notesLabel.text = ""
        }
        
    }
    
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        attributedString.append(two)
        return attributedString
    }
    
//    @objc func yesButtonAction(){
//        yesButton.isSelected = true
//        noButton.isSelected = false
//    }
//    @objc func noButtonAction(){
//        yesButton.isSelected = false
//        noButton.isSelected = true
//    }
    
//    func reloadView(tag: Int){
//        nameTextField.isHidden = yesButton.isSelected
//        phoneTextField.isHidden = yesButton.isSelected
//        emailTextField.isHidden = yesButton.isSelected
//        stackView.isHidden = yesButton.isSelected
//
//        nameTextField.text = appD.serviceCart[tag].cust_name
//        emailTextField.text = appD.serviceCart[tag].cust_email
//        phoneTextField.text = appD.serviceCart[tag].cust_phone
//
//    }
    
    let appD = UIApplication.shared.delegate as! AppDelegate
    
    func getDateString(dateString: String) -> String{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateFromString = df.date(from: dateString) ?? Date()
        df.dateFormat = "dd MMM yyyy"
        let finalString = df.string(from: dateFromString) 
        return finalString
    }
    func getDay(dateString: String) -> String{
        let df = DateFormatter()
        df.dateFormat = "EEEE"
        let dateFromString = df.date(from: dateString) ?? Date()
        df.dateFormat = "EEE"
        let finalString = df.string(from: dateFromString)
        return finalString
    }
}
