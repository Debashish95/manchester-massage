//
//  ProductListCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 18/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class ProductListCollectionViewCell: UICollectionViewCell {
    
//    @IBOutlet weak var cartLabel: UILabel!
//    @IBOutlet weak var cartButtonView: UIView!
    @IBOutlet weak var stepperStack: UIStackView!
    @IBOutlet weak var wishButton: UIButton!
    @IBOutlet weak var addToCartStepper: MMStepper!
    @IBOutlet weak var addToCardButton: UIButton!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var cartButton: UIButton!
    
    
    var data: ProductListModel!{
        didSet{
            productNameLabel.text = data.p_name
            productPriceLabel.attributedText = makeAttributedText(firstText: "\(CURRENCY_SYMBOL)\(data.base_price.formattedString())", secondText: "\(CURRENCY_SYMBOL)\(data.discount_price.formattedString())")
            
            productImage.kf.setImage(with: URL(string: data.thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                self.productImage.contentMode = .scaleAspectFill
            }
            addToCartStepper.value = Double(data.cartQuantity) ?? 0
            
//            self.addToCardButton.layer.cornerRadius = self.addToCardButton.bounds.height / 2
//            self.addToCardButton.clipsToBounds = true
//            
//            self.addToCartStepper.layer.borderWidth = 1.0
//            self.addToCartStepper.layer.borderColor = ThemeColor.buttonColor.cgColor
//            self.addToCartStepper.layer.cornerRadius = self.addToCartStepper.bounds.height / 2
//            self.addToCartStepper.clipsToBounds = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        addToCardButton.backgroundColor = ThemeColor.buttonColor
        addToCardButton.setTitle("Add To Cart", for: .normal)
        addToCardButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        addToCardButton.setTitleColor(.white, for: .normal)
        self.addToCardButton.layer.cornerRadius = self.addToCardButton.bounds.height / 2
        self.addToCardButton.clipsToBounds = true
        
        
        wishButton.backgroundColor = .white
        wishButton.setFAIcon(icon: .FAHeartO, iconSize: 20, forState: .normal)
        wishButton.setFAIcon(icon: .FAHeart, iconSize: 20, forState: .selected)
        wishButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        
        self.addToCartStepper.layer.borderWidth = 1.0
        self.addToCartStepper.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.addToCartStepper.layer.cornerRadius = self.addToCartStepper.bounds.height / 2
        self.addToCartStepper.clipsToBounds = true
        self.addToCartStepper.borderWidth = 1.0
        self.addToCartStepper.borderColor = ThemeColor.buttonColor
        
    }
    
    func makeAttributedText(firstText: String, secondText: String)->NSMutableAttributedString{
        let firstAttributedText = NSMutableAttributedString(string: firstText)
        firstAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),
                NSAttributedString.Key.strikethroughStyle: 2,
                NSAttributedString.Key.strikethroughColor: UIColor.gray,
                NSAttributedString.Key.foregroundColor: UIColor.gray
            ],
            range: NSRange(location: 0, length: firstText.count))
        
        let secondAttributedText = NSMutableAttributedString(string: secondText)
        secondAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .semibold),
                NSAttributedString.Key.strikethroughColor: UIColor.black,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ],
            range: NSRange(location: 0, length: secondText.count))
        firstAttributedText.append(NSAttributedString(string: " "))
        firstAttributedText.append(secondAttributedText)
        return firstAttributedText
    }

}
