//
//  HomeOfferHeaderView.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class HomeOfferHeaderView: UIView {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var bottomLineView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpView(){
        Bundle.main.loadNibNamed("HomeOfferHeaderView", owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        bottomLineView.backgroundColor = ThemeColor.buttonColor
    }
}
