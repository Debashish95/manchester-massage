//
//  GiftCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 20/05/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class GiftCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var cardValidDateLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var giftImageView: UIImageView!
    @IBOutlet weak var giftOuterView: UIView!
    
    var isSent: Bool = false
    
    var data: GiftModel!{
        didSet{
            cardNumberLabel.text = data.card_code
            cardValidDateLabel.attributedText = makeAttributedText(firstText: "Valid Till: ", secondText: data.valid_till)//data.valid_till
            amountLabel.text = data.amount
            if data.status == "1"{
                redeemButton.backgroundColor = UIColor(hexString: "#02be6e")
                if isSent{
                    redeemButton.isEnabled = false
                    redeemButton.setTitle(data.Status_info, for: .normal)
                    if data.receiver_email != ""{
                        emailAddressLabel.text = ""
//                            makeAttributedText(firstText: "To: ", secondText: data.receiver_email, secondFont: UIFont.systemFont(ofSize: 15, weight: .semibold))
                    }else{
                        emailAddressLabel.attributedText = makeAttributedText(firstText: "Delivery Address: ", secondText: data.receiver_address, secondFont: UIFont.systemFont(ofSize: 15, weight: .semibold))
                    }
                }else{
                    emailAddressLabel.text = ""
                    redeemButton.setTitle("Redeem", for: .normal)
                    redeemButton.isEnabled = true
                }
                
            }else{
                redeemButton.isEnabled = false
                redeemButton.backgroundColor = UIColor.lightGray
                redeemButton.setTitle(data.Status_info, for: .normal)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cardView.backgroundColor = ThemeColor.buttonColor
        
       
        
        
    }
    
    func makeAttributedText(firstText: String,
                            secondText: String,
                            firstFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                            secondFont: UIFont = UIFont.systemFont(ofSize: 17, weight: .semibold)
    )->NSMutableAttributedString{
        let firstAttributedText = NSMutableAttributedString(string: firstText)
        firstAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : firstFont,
                NSAttributedString.Key.strikethroughColor: UIColor.white,
                NSAttributedString.Key.foregroundColor: UIColor.white
            ],
            range: NSRange(location: 0, length: firstText.count))
        
        let secondAttributedText = NSMutableAttributedString(string: secondText)
        secondAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : secondFont,
                NSAttributedString.Key.strikethroughColor: UIColor.white,
                NSAttributedString.Key.foregroundColor: UIColor.white
            ],
            range: NSRange(location: 0, length: secondText.count))
        firstAttributedText.append(NSAttributedString(string: " "))
        firstAttributedText.append(secondAttributedText)
        return firstAttributedText
    }

}
