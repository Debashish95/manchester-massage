//
//  DateTimeCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/09/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class DateTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var dataLabel: UILabel!
    
    var isChoosen: Bool!{
        didSet{
            if isChoosen{
                dataLabel.textColor = .white
                backView.backgroundColor = ThemeColor.buttonColor
            }else{
                dataLabel.textColor = ThemeColor.buttonColor
                backView.backgroundColor = .white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dataLabel.textColor = ThemeColor.buttonColor
    }

}
