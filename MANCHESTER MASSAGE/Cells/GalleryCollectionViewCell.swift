//
//  GalleryCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 23/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    var data: String!{
        didSet{
            imageView.kf.setImage(with: URL(string: data)!)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

}
