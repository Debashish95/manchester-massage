//
//  ReviewsTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 24/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell {

    @IBOutlet weak var reviewImageView: UIImageView!
    @IBOutlet weak var headerLabel: UIButton!
    @IBOutlet weak var viewMoreButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
