//
//  WalletTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 05/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class WalletTableViewCell: UITableViewCell {

    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var transactionIDLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var data: WalletModel!{
        didSet{
            amountLabel.text = "\(CURRENCY_SYMBOL)\(data.amount.formattedString())"
            transactionTypeLabel.text = data.tran_type == "C" ? "Credit" : "Debit"
            transactionTypeLabel.textColor = data.tran_type == "C" ? UIColor(hexString: "#28a745") : UIColor(hexString: "#dc3545")
            transactionIDLabel.text = data.tran_id.makeMaskedString(maskedText: "-", repeatAfter: 4).uppercased()
            createdDateLabel.text = data.insert_date
            infoLabel.text = data.info
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
