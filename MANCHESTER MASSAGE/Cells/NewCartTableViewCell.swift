//
//  NewCartTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 20/02/22.
//  Copyright © 2022 Users. All rights reserved.
//

import UIKit

class NewCartTableViewCell: UITableViewCell {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var selectTimeSlotButton: UIButton!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var therapistLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var notesButton: UIButton!
    
    var delegate: NewCartTableViewCellDelegate?
    
    var cartData: CartModel! {
        didSet {
            serviceNameLabel.text = cartData.services.first?.name ?? ""
            durationLabel.attributedText = makeAttributedString(key: "Duration: ", value: getDuration())
            priceLabel.attributedText = makeAttributedString(
                key: "Price: ",
                value: (CURRENCY_SYMBOL + (cartData.services.first?.offerPrice ?? "0").formattedString())
            )
            
            if cartData.bookingTime == "" {
                timeLabel.isHidden = true
                therapistLabel.isHidden = true
            } else {
                timeLabel.isHidden = false
                therapistLabel.isHidden = false
                timeLabel.attributedText = makeAttributedString(
                    key: "Time: ",
                    value: getFormattedDate(
                        date: cartData.bookingDate,
                        time:  cartData.bookingTime
                    )
                )
                
                therapistLabel.attributedText = makeAttributedString(
                    key: "Therapist: ",
                    value: getTherapistNames()
                )
            }
            
            if cartData.notes.isEmpty {
                notesLabel.isHidden = true
            } else {
                notesLabel.isHidden = false
                notesLabel.attributedText = makeAttributedString(
                    key: "Note: ",
                    value: cartData.notes)
            }
            
            if cartData.isForMe {
                yesButtonAction()
            } else {
                nameTextField.text = cartData.cust_name
                emailTextField.text = cartData.cust_email
                noButtonAction()
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        serviceNameLabel.font = .systemFont(ofSize: 17, weight: .semibold)
        notesLabel.numberOfLines = 0
        
        yesButton.setTitleColor(.black, for: .normal)
        yesButton.setTitleColor(.black, for: .selected)
        yesButton.setLeftImage(image: .FACircleO, color: .black, state: .normal)
        yesButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
        
        noButton.setTitleColor(.black, for: .normal)
        noButton.setTitleColor(.black, for: .selected)
        noButton.setLeftImage(image: .FACircleO, color: .black, state: .normal)
        noButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
//        noButton.addTarget(self, action: #selector(noButtonAction), for: .touchUpInside)
        
        removeButton.setLeftImage(image: .FATrashO, color: .red, state: .normal)
        removeButton.setTitle("Remove", for: UIControl.State())
        removeButton.setTitleColor(.gray, for: UIControl.State())
        removeButton.titleLabel?.numberOfLines = 2
        editButton.titleLabel?.numberOfLines = 2
        editButton.setLeftImage(image: .FAPencil, color: ThemeColor.buttonColor, state: .normal)
        editButton.setTitle("Edit", for: UIControl.State())
        editButton.setTitleColor(.gray, for: UIControl.State())
        
        notesButton.titleLabel?.numberOfLines = 2
        notesButton.setLeftImage(image: .FAPencilSquareO, color: ThemeColor.buttonColor, state: .normal)
        notesButton.setTitle("Notes", for: UIControl.State())
        notesButton.setTitleColor(.gray, for: UIControl.State())
        
        nameTextField.accessibilityLabel = "name"
        emailTextField.accessibilityLabel = "email"
        
        nameTextField.setPlaceholder(placeholder: "Enter Name", color: .gray)
        emailTextField.setPlaceholder(placeholder: "Enter Email Address", color: .gray)
        
        nameTextField.addTarget(self, action: #selector(textFieldChangeAction(textField:)), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textFieldChangeAction(textField:)), for: .editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @objc func textFieldChangeAction(textField: UITextField) {
        delegate?.getTextFieldValue(textField: textField)
    }
    
    @objc func yesButtonAction(){
        yesButton.isSelected = true
        noButton.isSelected = false
        
        nameTextField.text = ""
        emailTextField.text = ""
        
        nameTextField.isHidden = true
        emailTextField.isHidden = true
    }
    @objc func noButtonAction(){
        yesButton.isSelected = false
        noButton.isSelected = true
        
        nameTextField.isHidden = false
        emailTextField.isHidden = false
    }
    
    private func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 14), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        two.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15), range: NSMakeRange(0, two.length))
        attributedString.append(two)
        return attributedString
    }
    
    private func getDuration() -> String {
        let durationInt = (Int(cartData.services.first?.duration ?? "0") ?? 0) * 60
        let cleanupInt = (Int(cartData.services.first?.cleanupTime ?? "0") ?? 0) * 60
        var durationText = ""
        if cleanupInt == 0 {
            durationText =  (durationInt - cleanupInt).secondsToTime()
        } else {
            durationText =  (durationInt - cleanupInt).secondsToTime().appending(", \(cleanupInt.secondsToTime()) clean up")
        }
        return durationText
    }
    
    private func getFormattedDate(date: String, time: String) -> String{
        var finalDateString = "\(date) \(time)"
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        if let date = df.date(from: finalDateString) {
            df.dateFormat = "hh:mm a, EEE, dd MMM yyyy"
            finalDateString = df.string(from: date)
        }
        return finalDateString
    }
    
    private func getTherapistNames() -> String{
        let finalTherapists = cartData.therapists.map({ "\($0.user_name) \($0.user_lname)" })
        return finalTherapists.joined(separator: ", ")
    }
}

protocol NewCartTableViewCellDelegate {
    func getTextFieldValue(textField: UITextField)
}
