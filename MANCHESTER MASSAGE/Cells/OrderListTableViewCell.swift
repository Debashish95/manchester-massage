//
//  OrderListTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 15/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class OrderListTableViewCell: UITableViewCell {

    @IBOutlet weak var writeReviewButton: UIButton!
    @IBOutlet weak var nextImageView: UIImageView!
    @IBOutlet weak var deliveryDateLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    var productData: ProductCartModel!{
        didSet{
            productImageView.kf.setImage(with: URL(string: productData.thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                self.productImageView.contentMode = .scaleAspectFill
            }
            productNameLabel.text = productData.prod_name
            deliveryDateLabel.text = productData.small_description
            deliveryDateLabel.numberOfLines = 2
            priceLabel.isHidden = false
            priceLabel.attributedText = makeAttributedPriceString(key: "Price: ", value: "\(CURRENCY_SYMBOL)\(productData.price.formattedString())", key2: "Quantity: ", value2: productData.qnty)
            priceLabel.numberOfLines = 2
            nextImageView.isHidden = true
            writeReviewButton.isHidden = false
        }
    }
    var data: OrderListModel!{
        didSet{
            productImageView.kf.setImage(with: URL(string: data.products.first?.thumb ?? ""), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                self.productImageView.contentMode = .scaleAspectFill
            }
            productNameLabel.text = "Order ID: #\(data.order_id)"
            productNameLabel.font = UIFont.boldSystemFont(ofSize: 15)
            
            deliveryDateLabel.font = UIFont.boldSystemFont(ofSize: 13)
            deliveryDateLabel.numberOfLines = 2
            if data.track.count > 0{
                deliveryDateLabel.attributedText = self.makeAttributedString(key: "Status: ", value: (data.track.last?.msg ?? ""))
            }else{
                deliveryDateLabel.attributedText = self.makeAttributedString(key: "Ordered: ", value: data.created_on)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        writeReviewButton.backgroundColor = ThemeColor.bookNowButtonColor
        nextImageView.setFAIconWithName(icon: .FAAngleRight, textColor: .darkGray)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        attributedString.append(two)
        return attributedString
    }
    
    func makeAttributedPriceString(key: String, value: String, key2: String, value2: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        attributedString.append(two)
        
        if key2.count > 0{
            attributedString.append(NSAttributedString(string: " | "))
            let attributedString2: NSMutableAttributedString =  NSMutableAttributedString(string: key2)
            attributedString2.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: NSMakeRange(0, attributedString2.length))
            let two2 = NSMutableAttributedString(string: value2)
            attributedString2.append(two2)
            
            attributedString.append(attributedString2)
        }
        
        
        return attributedString
    }
    
}
