//
//  ParentCategoryTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class ParentCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var bannerImageVIew: UIImageView!
    @IBOutlet weak var backView: CardView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var data: ParentCategory!{
        didSet{
            bannerImageVIew.kf.setImage(with: URL(string: data.banner))
            thumbImageView.kf.setImage(with: URL(string: data.thumb))
            nameLabel.text = data.cat_nm
        }
    }
    
}
