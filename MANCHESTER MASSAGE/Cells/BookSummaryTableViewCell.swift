//
//  BookSummaryTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 18/10/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class BookSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var notesText: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var therapistNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rescheduleButton: UIButton!
    
    var data: Dictionary<String, String>!{
        didSet{
            var finalTherapistListString = ""
            let stringArray = data["therapistArr"]?.components(separatedBy: ",") ?? [""]
            var finalEmpList = [EmployeeModel]()
            let _ = stringArray.map({ (str) -> Void in
                var tempStr = str.replacingOccurrences(of: "[", with: "")
                tempStr = tempStr.replacingOccurrences(of: "]", with: "")
                if let empData = self.appD.employeeDetails.filter({$0.id == tempStr}).first{
                    finalEmpList.append(empData)
                    finalTherapistListString += empData.user_name + " " + empData.user_lname + ", "
                }
            })
            if finalTherapistListString.count > 2{
                finalTherapistListString.removeLast(2)
            }
            if let notes = data["notes"] {
                if notes.count == 0{
                    notesLabel.text = ""
                    notesText.text = ""
                }else{
                    notesLabel.text = "Notes:"
                    notesText.text = notes
                }
            }
            if finalEmpList.count == 0{
                finalTherapistListString = ""
                var finalTherapistListArray = [String]()
                if let data = data["therapistArr"]?.data(using: .utf8){
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String]
                        {
                            jsonArray.forEach { (empDataStr) in
                                if let empData = self.appD.employeeDetails.filter({$0.id == empDataStr}).first{
                                    finalTherapistListArray.append(empData.user_name + " " + empData.user_lname)
                                }
                                
                            }
                            finalTherapistListString = finalTherapistListArray.joined(separator: ", ")
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                }
            }
            serviceNameLabel.text = data["serviceName"]
            userNameLabel.text = data["userName"]
            therapistNameLabel.text = "With \(finalTherapistListString)"
            timeLabel.text = data["time"]
            rescheduleButton.setTitle(data["rescheduleTitle"], for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        serviceNameLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        notesLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        notesLabel.textColor = ThemeColor.buttonColor
        notesText.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        userNameLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        therapistNameLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        timeLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        rescheduleButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        rescheduleButton.setTitleColor(.white, for: .normal)
        rescheduleButton.backgroundColor = ThemeColor.bookNowButtonColor
        rescheduleButton.layer.cornerRadius = 5
        rescheduleButton.layer.masksToBounds = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var appD: AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
}

