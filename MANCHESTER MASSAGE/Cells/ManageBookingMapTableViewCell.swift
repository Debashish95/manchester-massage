//
//  ManageBookingMapTableViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/12/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import MapKit

class ManageBookingMapTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: CardView!
    @IBOutlet weak var mapView: UIImageView!
    @IBOutlet weak var mapAddressLabel: UILabel!
    @IBOutlet weak var getDirectionButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    let appD = UIApplication.shared.delegate as! AppDelegate
    
    func setUpCell() {
        // Initialization code
        
        let latitude = Double(self.appD.vanue_latitude) ?? 0.0
        let longitude = Double(self.appD.vanue_longitude) ?? 0.0
        let mapSnapshotOptions = MKMapSnapshotter.Options()
        
        // Set the region of the map that is rendered.
        let location = CLLocationCoordinate2DMake(latitude, longitude)
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapSnapshotOptions.region = region
        
        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale
        
        // Set the size of the image output.
        mapSnapshotOptions.size = mapView.frame.size
        
        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
        
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        snapShotter.start { (snapshot:MKMapSnapshotter.Snapshot?, error: Error?) in
            guard let image = snapshot?.image else{
                return
            }
            self.mapView.image = image
            self.mapView.contentMode = .scaleAspectFill
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.gotoMapView))
            self.mapView.isUserInteractionEnabled = true
            self.mapView.addGestureRecognizer(tapGesture)
        }
        
        mapAddressLabel.text = "Manchester Massage \n28a Swan street, Manchester Northern Quarters, M4 5JQ, UK"
        mapAddressLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        mapAddressLabel.numberOfLines = 3
        
        
        getDirectionButton.setSupportButton(image: .FALocationArrow, title: "Get Direction")
        getDirectionButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        getDirectionButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        getDirectionButton.setFATitleColor(color: ThemeColor.buttonColor, forState: .normal)
        getDirectionButton.setLeftImage(image: .FALocationArrow, color: ThemeColor.buttonColor, state: .normal)
        getDirectionButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        getDirectionButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        getDirectionButton.addTarget(self, action: #selector(addressButtonAction(sender:)), for: .touchUpInside)
        getDirectionButton.accessibilityValue = mapAddressLabel.text
        
        shareButton.setSupportButton(image: .FAShareAlt, title: "Share")
        shareButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        shareButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        shareButton.setFATitleColor(color: ThemeColor.buttonColor, forState: .normal)
        shareButton.setLeftImage(image: .FAShareAlt, color: ThemeColor.buttonColor, state: .normal)
        shareButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        shareButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        shareButton.addTarget(self, action: #selector(shareButtonAction(sender:)), for: .touchUpInside)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @objc func gotoMapView(){
        let accessValue = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = accessValue
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            
            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    
    @objc func addressButtonAction(sender: Any){

        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }

            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    
    @objc func shareButtonAction(sender: UIButton){
        let text = "Manchester Massage 28a Swan St, Manchester M4 5JQ, United Kingdom https://maps.app.goo.gl/MzoBQbszekTKRdAx8"
        let shareAll = [text]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        self.topMostController()?.present(activityViewController, animated: true, completion: nil)
    }
    
}
