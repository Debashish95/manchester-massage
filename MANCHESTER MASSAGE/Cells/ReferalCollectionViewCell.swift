//
//  ReferalCollectionViewCell.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 12/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class ReferalCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var referalDateLabel: UILabel!
    @IBOutlet weak var referalAmountLabel: UILabel!
    
    var data: WalletModel!{
        didSet{
            referalDateLabel.text = data.insert_date
            referalAmountLabel.text = "\(CURRENCY_SYMBOL)\(data.amount)"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
