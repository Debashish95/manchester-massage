//
//  HomeTabViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 07/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class HomeTabViewController: UITabBarController {

    var isComingFromFinalBooking = false
    var isComingFromProductPage = false
    var sharingCategory = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//        UITabBar.appearance().barTintColor = .black
        UITabBar.appearance().unselectedItemTintColor = UIColor.black
        UITabBar.appearance().tintColor = ThemeColor.buttonColor
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13)]
        appearance.setTitleTextAttributes(attributes, for: .normal)
        
        self.tabBarItem.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13)
        ], for: .normal)
        
        if !isIPad{
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        }
        
        
        let myTabBarItem1 = UITabBarItem()
        myTabBarItem1.image = UIImage(named: "home-1")
        myTabBarItem1.selectedImage = UIImage(named: "home-1")
        myTabBarItem1.title = "Home"
        let firstViewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        firstViewController.sharingCategory = sharingCategory
        let navBar1 = UINavigationController(rootViewController: firstViewController)
        navBar1.tabBarItem = myTabBarItem1
        navBar1.title = "Home"
        
        
        let myTabBarItem2 = UITabBarItem()
        myTabBarItem2.image = UIImage(named: "calendar")
        myTabBarItem2.selectedImage = UIImage(named: "calendar")
        myTabBarItem2.title = "My Bookings"
        //NewBookingViewController Bookings  MyBookingsViewController
//        let secondViewController = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "MyBookingsViewController") as! MyBookingsViewController
        let secondViewController = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "NewBookingViewController") as! NewBookingViewController
        let navBar2 = UINavigationController(rootViewController: secondViewController)
        navBar2.tabBarItem = myTabBarItem2
        navBar2.title = "My Bookings"
        
        
        let myTabBarItemMassage = UITabBarItem()
        myTabBarItemMassage.image = UIImage(named: "massage")
        myTabBarItemMassage.selectedImage = UIImage(named: "massage")
        myTabBarItemMassage.title = "Massage"
        let massageViewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
        let navBarMassage = UINavigationController(rootViewController: massageViewController)
        navBarMassage.tabBarItem = myTabBarItemMassage
        navBarMassage.title = "Massage"
        
        
        let myTabBarItemBeauty = UITabBarItem()
        myTabBarItemBeauty.image = UIImage(named: "beauty")
        myTabBarItemBeauty.selectedImage = UIImage(named: "beauty")
        myTabBarItemBeauty.title = "Beauty"
        let beautyViewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
        beautyViewController.isBeautyService = true
        let navBarBeauty = UINavigationController(rootViewController: beautyViewController)
        navBarBeauty.tabBarItem = myTabBarItemBeauty
        navBarBeauty.title = "Beauty"
        
        /**
         
         let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
         destination.serviceList = self.appD.serviceData
         self.navigationController?.pushViewController(destination, animated: true)
         */
        
        /*let myTabBarItem3 = UITabBarItem()
        myTabBarItem3.image = UIImage(icon: .FACubes, size: CGSize(width: 30, height: 30))
        myTabBarItem3.selectedImage = UIImage(icon: .FACubes, size: CGSize(width: 30, height: 30))
        myTabBarItem3.title = "Products"
        let thirdViewController =  ProductCategoryViewController(nibName: "ProductCategoryViewController", bundle: nil)
        thirdViewController.isFromFinalOrder = isComingFromProductPage
        let navBar3 = UINavigationController(rootViewController: thirdViewController)
        navBar3.tabBarItem = myTabBarItem3
        navBar3.title = "Products"*/
        
        let myTabBarItem4 = UITabBarItem()
        myTabBarItem4.image = UIImage(icon: .FAHeartO, size: CGSize(width: 30, height: 30))
        myTabBarItem4.selectedImage = UIImage(icon: .FAHeartO, size: CGSize(width: 30, height: 30))
        myTabBarItem4.title = "Favourite"
        let fourthViewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavouriteViewController") as! FavouriteViewController
        let navBar4 = UINavigationController(rootViewController: fourthViewController)
        navBar4.tabBarItem = myTabBarItem4
        navBar4.title = "Favourite"
        
        
        /*let myTabBarItem5 = UITabBarItem()
        myTabBarItem5.image = UIImage(icon: .FASupport, size: CGSize(width: 30, height: 30))
        myTabBarItem5.selectedImage = UIImage(icon: .FASupport, size: CGSize(width: 30, height: 30))
        myTabBarItem5.title = "Support"
        let fifthViewController =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        let navBar5 = UINavigationController(rootViewController: fifthViewController)
        navBar5.tabBarItem = myTabBarItem5
        navBar5.title = "Support"*/
        
        
        viewControllers = [navBar1, navBar2, navBarMassage, navBarBeauty, navBar4]
        
        if self.isComingFromFinalBooking{
            self.selectedIndex = 1
            isComingFromFinalBooking = false
        }else if self.isComingFromProductPage{
            self.selectedIndex = 0
            isComingFromProductPage = false
        }
                
    }

}
