//
//  AppDelegate.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 13/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FBSDKCoreKit
import GoogleSignIn
import Stripe
import Alamofire
import FirebaseMessaging
//import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var serviceData = [ServiceModel]()
    var beautyServiceData = [ServiceModel]()
    var serviceCart = [CartModel]()
    var employeeDetails = [EmployeeModel]()
    var vanue_latitude = ""
    var vanue_longitude = ""
    var timeSlots = [String]()
    var termsStaticString = ""
    var benefitsOfMassage = ""
    var aboutStaticString = ""
    var privacyStaticString = ""
    var fcmTokenToRegister = ""
    var parentCategoryData = [ParentCategory]()
    var selectedParentCategory = ParentCategory()
    var cartList = [ProductCartModel]()
    var wishList = [ProductListModel]()
    var itemsInCart = ""

    fileprivate func gotoHomePage(sharingCategory: String = "") {
        //        TWTRTwitter.sharedInstance().start(withConsumerKey: "WywqV7oZ15fqrkRCGvK3DTpDA", consumerSecret: "SnZ13Sk5HPn1ALC6DXZ4CW8Rkmn5lj4NvreN5eSeYRxqooDgNV")
        if (UserDefaults.standard.fetchData(forKey: .userEmail) != "") || (UserDefaults.standard.fetchData(forKey: .skipLogin)){
            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
            //            let navigationController = UINavigationController(rootViewController: destination)
            destination.sharingCategory = sharingCategory
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.backgroundColor = UIColor.white
            window?.rootViewController = destination
            window?.makeKeyAndVisible()
            
            //            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            //            let navigationController = UINavigationController(rootViewController: destination)
            //            window = UIWindow(frame: UIScreen.main.bounds)
            //            window?.rootViewController = navigationController
            //            window?.makeKeyAndVisible()
            //            let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
            //            let navigationController = UINavigationController(rootViewController: destination)
            //            self.window?.rootViewController = navigationController
            //            self.window?.makeKeyAndVisible()
        }
        else{
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
//            let navigationController = UINavigationController(rootViewController: destination)
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.backgroundColor = UIColor.white
            window?.rootViewController = destination//navigationController
            window?.makeKeyAndVisible()
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        /**
         Access Token: 4761335472-troWcu8LstAFXYnRxXZ3VihX4SkCcqjD5IRk5bN
         Access Token Secret: otLArOqrwOjigSb30nOrK1dnEU5YdUgAPX9YOEbOm5zD1
         */
//        TWTRTwitter.sharedInstance().start(withConsumerKey:"jYHtrCfUAdYeMCW6AAmUAKK6B", consumerSecret:"6AcLL9a5TcLn1RK3rJHJh14IbLCs3b4hlSBv3PHztUT1gEQSDN")
    
        getStaticPageDetails()
        /**
         
         Live Sec. Key: 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
         
         Live Pub. Key:
         pk_live_51GxbWeLL60kQSHtAQVrEjKpIf9bhrcU4hgWXxU5zTikmNNNBE8IgcaQ3TjmJJhtXULCxpjQUaH2pElP3mvYUSPHd00XOhsXxPN
         */
//        pk_live_51GxbWeLL60kQSHtAQVrEjKpIf9bhrcU4hgWXxU5zTikmNNNBE8IgcaQ3TjmJJhtXULCxpjQUaH2pElP3mvYUSPHd00XOhsXxPN
//        Stripe.setDefaultPublishableKey("pk_test_51HQwTkHg6EECwr9AQnsi4fWC3u8jsilOXy0aAwO74H2Fn7WdBELl1Hloaeu0xxbFdBytaehv4fC5XNpRBHCUIuHQ00cdkmv7RI")
        
        sleep(2)
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = "1000140111011-9svkcp8m1prh173elao82e2j612ml01v.apps.googleusercontent.com"
        
        gotoHomePage()
        
        getParentCategoryList()
        

        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.badge, .alert, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in
            
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        return true
    }
    

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        switch socialLoginAction {
        case .google:
            return GIDSignIn.sharedInstance().handle(url)
        case .facebook:
            return ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            )
//        case .twitter:
//            return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        default:
            return false
        }
    }

    func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplication.ExtensionPointIdentifier) -> Bool {
        return !(extensionPointIdentifier == UIApplication.ExtensionPointIdentifier.keyboard)
    }
    
    func topViewControllerWithRootViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        if rootViewController == nil {
            return nil
        }
        if rootViewController!.isKind(of: UITabBarController.self) {
            let tabBarController: UITabBarController = (rootViewController as? UITabBarController)!
            return self.topViewControllerWithRootViewController(tabBarController.selectedViewController)
        }
        else {
            if rootViewController!.isKind(of: UINavigationController.self) {
                let navigationController: UINavigationController = (rootViewController as? UINavigationController)!
                return self.topViewControllerWithRootViewController(navigationController.visibleViewController)
            }
            else {
                if (rootViewController!.presentedViewController != nil) {
                    let presentedViewController: UIViewController = rootViewController!.presentedViewController!
                    return self.topViewControllerWithRootViewController(presentedViewController)
                }
                else {
                    return rootViewController
                }
            }
        }
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    fileprivate func getStaticPageDetails(){
        let apiURL = Config.mainUrl + "Data/PageContent?key=\(Config.key)&salt=\(Config.salt)"
        Alamofire.request(apiURL, method: .get).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.aboutStaticString = resposeJSON["about_us"] as? String ?? ""
            self.termsStaticString = resposeJSON["terms"] as? String ?? ""
            self.privacyStaticString = resposeJSON["privacy"] as? String ?? ""
            self.benefitsOfMassage = resposeJSON["benifit"] as? String ?? ""
        }
        
    }

    func getCartList(){
        self.cartList.removeAll()
        let reuestURL = Config.mainUrl + "Data/Getcart?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    var itemsInCart = 0
                    for index in 0..<result.count{
                        let currentResult = result[index] as? NSDictionary ?? NSDictionary()

                        let prod_id = currentResult["prod_id"] as? String ?? ""
                        let prod_name = currentResult["prod_name"] as? String ?? ""
                        let unit_price = currentResult["unit_price"] as? String ?? "0"
                        let price = currentResult["price"] as? String ?? "0"
                        let qnty = currentResult["qnty"] as? String ?? "0"
                        let thumb = currentResult["thumb"] as? String ?? ""
                        let base_price = currentResult["base_price"] as? String ?? ""
                        let discount_price = currentResult["discount_price"] as? String ?? ""
                        itemsInCart += (Int(qnty) ?? 0)
                        let finalCartProduct = ProductCartModel(prod_id: prod_id, prod_name: prod_name, unit_price: unit_price, price: price, base_price: base_price, discount_price: discount_price, qnty: qnty, thumb: thumb)
                        self.cartList.append(finalCartProduct)
                        
                    }
                    self.itemsInCart = "\(itemsInCart)"
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changePhysicalCart"), object: nil)
//                self.getProductList()
            }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let url = userActivity.webpageURL, let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            return false
        }
        
        if let category_value = components.fragment {
            if (category_value == "") || (url.absoluteString.contains("fb/user_login")) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }else{
                gotoHomePage(sharingCategory: category_value)
                print(category_value)
                return true
            }
            
        }else{
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        return false
    }
}


extension AppDelegate: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let messageId = userInfo["gcm.message_id"]{
            print("Message ID: \(messageId)")
        }
        completionHandler([.alert, .sound, .badge])
    }
}


extension AppDelegate: MessagingDelegate{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("FCM Token: \(fcmToken)")
        fcmTokenToRegister = fcmToken
    }
}


// Mark:- Parent Category API Call
extension AppDelegate{
    
    fileprivate func getParentCategoryList(){
        
        let reuestURL = Config.mainUrl + "Data/GetAllCats?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                
                let results = resposeJSON["result"] as? NSArray ?? NSArray()
                for index in 0..<results.count{
                    let currentObject = results[index] as? NSDictionary ?? NSDictionary()
                    let cat_id = currentObject["cat_id"] as? String ?? ""
                    let cat_nm = currentObject["cat_nm"] as? String ?? ""
                    let thumb = currentObject["thumb"] as? String ?? ""
                    let banner = currentObject["thumb"] as? String ?? ""
                    let currentCategory = ParentCategory(cat_id: cat_id, cat_nm: cat_nm, thumb: thumb, banner: banner)
                    self.parentCategoryData.append(currentCategory)
                }
                
                self.selectedParentCategory = self.parentCategoryData.first(where: {!$0.cat_nm.lowercased().contains("beauty")}) ?? ParentCategory()
            }
    }
}
