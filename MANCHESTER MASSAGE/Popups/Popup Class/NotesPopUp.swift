//
//  NotesPopUp.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 27/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class NotesPopUp: UIViewController, UITextViewDelegate {

    @IBOutlet weak var notesHeaderLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var notesHeaderText = ""
    var notesBodyText = ""
    var delegate: NotesPopUpDelegate?
    
    var index: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.notesHeaderLabel.text = notesHeaderText
        self.notesTextView.text = notesBodyText
        
        notesTextView.delegate = self
        notesTextView.tintColor = ThemeColor.buttonColor
        
        
        cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveButtonAction), for: .touchUpInside)
        
        
        saveButton.isEnabled = (notesBodyText.count > 0)
        
        self.view.layer.cornerRadius = 8
        self.view.clipsToBounds = true
    }
    
    @objc func cancelButtonAction(){
        self.dismiss(animated: true) {
            self.delegate?.cancelButtonAction(textView: self.notesTextView, index: self.index)
        }
        
    }
    
    @objc func saveButtonAction(){
        self.dismiss(animated: true) {
            self.delegate?.saveButtonAction(textView: self.notesTextView, index: self.index)
        }
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 0{
            saveButton.isEnabled = true
        }
        
    }

}

protocol NotesPopUpDelegate{
    func saveButtonAction(textView: UITextView, index: Int)
    func cancelButtonAction(textView: UITextView, index: Int)
}
