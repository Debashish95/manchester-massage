//
//  CancelPopUpViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 07/03/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class CancelPopUpViewController: UIViewController {
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var requestCancellationButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelPolicyLabel: UILabel!
    @IBOutlet weak var cancelTextLabel: UILabel!
    
    var delegate: CancelPopUpViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cardView.backgroundColor = ThemeColor.buttonColor
        
        titleLabel.text = "CANCEL BOOKING"
        titleLabel.textColor = .white
        titleLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        
        cancelPolicyLabel.text = "Cancellation Policy"
        cancelPolicyLabel.textColor = .black
        cancelPolicyLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        
        cancelTextLabel.text = "If you are unable to attend your booking you can cancel it but please give the venue as much as warning as possible."
        cancelTextLabel.textColor = .black
        cancelTextLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        cancelTextLabel.numberOfLines = 0
        
        requestCancellationButton.backgroundColor = ThemeColor.buttonColor
        requestCancellationButton.setTitle("REQUEST CANCELLATION", for: .normal)
        requestCancellationButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        requestCancellationButton.setTitleColor(.white, for: .normal)
        requestCancellationButton.addTarget(self, action: #selector(requestCancellButtonaction), for: .touchUpInside)
        
        cancelButton.backgroundColor = .white
        cancelButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonaction), for: .touchUpInside)
        cancelButton.setTitle("CLOSE", for: .normal)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        requestCancellationButton.layer.cornerRadius = 10
        requestCancellationButton.clipsToBounds = true
    }

    @objc func requestCancellButtonaction(){
        self.dismiss(animated: true) {
            self.delegate?.confirmCancellationAction()
        }
    }
    
    @objc func cancelButtonaction(){
        self.dismiss(animated: true, completion: nil)
    }

}

protocol CancelPopUpViewControllerDelegate {
    func confirmCancellationAction()
}
