//
//  ChangePasswordPopUp.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 10/03/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class ChangePasswordPopUp: UIViewController {

    
    @IBOutlet weak var currentPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    var isFirstTimeLoad = true
    var delegate: ChangePasswordPopUpDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonAction), for: .touchUpInside)
        self.sendButton.addTarget(self, action: #selector(self.sendButtonAction), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool){
        if isFirstTimeLoad{
            isFirstTimeLoad = false
            setupView()
        }
        
    }
    func setupView(){
        
        self.currentPassword.setPlaceholder(placeholder: "Current Password", color: .gray)
        self.currentPassword.layer.cornerRadius = self.currentPassword.frame.height / 2
        self.currentPassword.layer.borderColor = UIColor.gray.cgColor
        self.currentPassword.layer.borderWidth = 1.0
        self.currentPassword.clipsToBounds = true
        self.currentPassword.enablePasswordEye()
        
        self.newPassword.setPlaceholder(placeholder: "New Password", color: .gray)
        self.newPassword.layer.cornerRadius = self.newPassword.frame.height / 2
        self.newPassword.layer.borderColor = UIColor.gray.cgColor
        self.newPassword.layer.borderWidth = 1.0
        self.newPassword.clipsToBounds = true
        self.newPassword.enablePasswordEye()
        
        self.confirmPassword.setPlaceholder(placeholder: "Re-enter Password", color: .gray)
        self.confirmPassword.layer.cornerRadius = self.confirmPassword.frame.height / 2
        self.confirmPassword.layer.borderColor = UIColor.gray.cgColor
        self.confirmPassword.layer.borderWidth = 1.0
        self.confirmPassword.clipsToBounds = true
        self.confirmPassword.enablePasswordEye()
        
        self.cancelButton.layer.borderWidth = 1.0
        self.cancelButton.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.cancelButton.layer.cornerRadius = self.cancelButton.frame.height / 2
        self.cancelButton.clipsToBounds = true
        self.cancelButton.setTitleColor(.black, for: .normal)
        self.cancelButton.setTitle("CANCEL", for: .normal)
        self.cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        
        self.sendButton.backgroundColor = ThemeColor.buttonColor
        self.sendButton.layer.cornerRadius = self.sendButton.frame.height / 2
        self.sendButton.clipsToBounds = true
        self.sendButton.setTitleColor(.white, for: .normal)
        self.sendButton.setTitle("SUBMIT", for: .normal)
        self.sendButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        
    }
    
        
        
    @objc func cancelButtonAction(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func sendButtonAction(){
        if currentPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            self.topViewController?.view.makeToast(message: "Please enter current password")
            return
        }
        
        if newPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            self.topViewController?.view.makeToast(message: "Please enter new password")
            return
        }
        if !(self.newPassword.text?.isValidPassword ?? true){
            self.topViewController?.view.makeToast(message: "Password should contain atleast one uppercase, atleast one digit, atleast one lowercase, atleast one symbol and min 8 characters")
            return
        }
        if confirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            self.topViewController?.view.makeToast(message: "Please re-enter password")
            return
        }
        
        if !(self.confirmPassword.text?.isValidPassword ?? true){
            self.topViewController?.view.makeToast(message: "Password should contain atleast one uppercase, atleast one digit, atleast one lowercase, atleast one symbol and min 8 characters")
            return
        }
        
        if newPassword.text != confirmPassword.text{
            self.topViewController?.view.makeToast(message: "Passwords do not match.")
            return
        }
        
        if newPassword.text == currentPassword.text{
            self.topViewController?.view.makeToast(message: "Old password and new password are same. Please enter new password.")
            return
        }
        
        self.dismiss(animated: false){
            self.delegate.changePassword(newPassword: self.confirmPassword.text!, oldPassword: self.currentPassword.text!)
        }
        
    }
}

protocol ChangePasswordPopUpDelegate {
    func changePassword(newPassword: String, oldPassword: String)
}
