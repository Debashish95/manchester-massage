//
//  ImageViewerViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 24/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Kingfisher

class ImageViewerViewController: UIViewController {
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var imageURL: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.alpha = 0
        
        let image = UIImage(named: "cross")?.withRenderingMode(.alwaysTemplate)
        closeButton.setImage(image, for: UIControl.State())
        closeButton.tintColor = .white
        closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
        
        UIView.animate(withDuration: 2, delay: 0.5, options: .curveEaseIn, animations: {
            self.view.alpha = 1
            self.imageView.kf.setImage(with: URL(string: self.imageURL)!)
        }, completion: nil)
        
        
        
        
    }
    
    @objc func closeButtonAction(){
        self.dismiss(animated: false, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
