//
//  EditProfileViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var updateProfileButton: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    var activityIndicator: NVActivityIndicatorView!
    var delegate: EditProfileViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        backView.backgroundColor = .clear
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureAction))
        tapGesture.numberOfTapsRequired = 1
        self.backView.addGestureRecognizer(tapGesture)
        
        fullNameTextField.borderStyle = .none
        emailTextField.borderStyle = .none
        phoneNumberTextField.borderStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fullNameTextField.addButtomBorder()
        emailTextField.addButtomBorder()
        phoneNumberTextField.addButtomBorder()
        
        fullNameTextField.text = UserDefaults.standard.fetchData(forKey: .userFirstName) + " " + UserDefaults.standard.fetchData(forKey: .userLastName)
        emailTextField.text = UserDefaults.standard.fetchData(forKey: .userEmail)
        emailTextField.isEnabled = false
        phoneNumberTextField.text = UserDefaults.standard.fetchData(forKey: .userPhone)
        
        fullNameTextField.keyboardType = .namePhonePad
        emailTextField.keyboardType = .emailAddress
        phoneNumberTextField.keyboardType = .phonePad
        
        updateProfileButton.backgroundColor = ThemeColor.buttonColor
        updateProfileButton.layer.cornerRadius = updateProfileButton.frame.height / 2
        updateProfileButton.clipsToBounds = true
        updateProfileButton.setTitleColor(.white, for: .normal)
        updateProfileButton.setTitle("UPDATE", for: .normal)
        updateProfileButton.addTarget(self, action: #selector(updateProfileAction), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        let options: UIView.AnimationOptions = [.transitionCrossDissolve]
        UIView.animate(withDuration: 0.4, delay: 0.3, options: options, animations: {
            [weak self] in
            self?.backView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }, completion: nil)
    }
    
    @objc func tapGestureAction(){
        self.backView.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func updateProfileAction(){
        if (self.fullNameTextField.text?.isBlank ?? true){
            self.tabBarController?.view.makeToast(message: "Please Enter Name")
        }
        else if (self.fullNameTextField.text?.isValidName ?? true){
            self.tabBarController?.view.makeToast(message: "Invalid Name")
        }
        else if (self.phoneNumberTextField.text?.isBlank ?? true){
            self.tabBarController?.view.makeToast(message: "Please Enter Phone Number")
        }
        else if !(self.phoneNumberTextField.text?.isPhoneNumber ?? true){
            self.tabBarController?.view.makeToast(message: "Invalid Phone Number")
        }
        else{
            let names = (self.fullNameTextField.text ?? "").components(separatedBy: " ")
            let firstName = (names.count > 0) ? names[0] : ""
            let lastName = (names.count > 1) ? names[1] : ""
            let paramsHead = ["Content-Type": "multipart/form-data"]
            let bodyParams = [
                "salt": Config.salt.toData,
                "key": Config.key.toData,
                "c_id": (UserDefaults.standard.fetchData(forKey: .userId)).toData,
                "c_fname": firstName.toData,
                "c_lname": lastName.toData,
                "c_phone": (self.phoneNumberTextField.text ?? "").toData
                ] as [String : Data]
            self.activityIndicator.startAnimating()
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in bodyParams{
                    multipartFormData.append(value, withName: key)
                }
                
            }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "CustomerUpdate" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
                
                switch encodingResult{
                case .failure( _):
                    self.activityIndicator.stopAnimating()
                    break;
                case .success(let request, true, _):
                    request.responseJSON(completionHandler: { (response) in
                        
                        let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                        self.activityIndicator.stopAnimating()
                        
                        let status = resposeJSON["status"] as? String ?? ""
                        let message = resposeJSON["msg"] as? String ?? ""
                        
                        if (status == "OK" && message == "Updated"){
                            let info = resposeJSON["info"] as? NSDictionary ?? NSDictionary()
                            let c_id = info["c_id"] as? String ?? ""
                            let c_fname = info["c_fname"] as? String ?? ""
                            let c_lname = info["c_lname"] as? String ?? ""
                            let c_phone = info["c_phone"] as? String ?? ""
                            
                            UserDefaults.standard.saveData(value: c_id, key: .userId)
                            UserDefaults.standard.saveData(value: c_fname, key: .userFirstName)
                            UserDefaults.standard.saveData(value: c_lname, key: .userLastName)
                            UserDefaults.standard.saveData(value: c_phone, key: .userPhone)
                            UserDefaults.standard.saveData(value: status, key: .userStatus)
                            
                            let presentingViewController = self.presentingViewController
                            let alertController = UIAlertController(title: "Success", message: "Profile updated successfully!", preferredStyle: .alert)
                            let alertOKButton = UIAlertAction(title: "Ok", style: .default) { (alertAction) in
                                self.delegate?.updateProfile()
                            }
                            alertController.addAction(alertOKButton)
                            self.dismiss(animated: true) {
                                presentingViewController?.present(alertController, animated: true, completion: nil)
                            }
                            
                        }
                        else{
                            self.tabBarController?.view.makeToast(message: message)
                        }
                    })
                default:
                    self.activityIndicator.stopAnimating()
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    break;
                }})
        }
    }
}

protocol EditProfileViewControllerDelegate{
    func updateProfile()
}
