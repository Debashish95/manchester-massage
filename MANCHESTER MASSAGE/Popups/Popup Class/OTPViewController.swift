//
//  OTPViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 15/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class OTPViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    var delegate: OTPViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonAction), for: .touchUpInside)
        self.sendButton.addTarget(self, action: #selector(self.sendButtonAction), for: .touchUpInside)
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
    }

    override func viewDidLayoutSubviews() {
        
        self.mainView.layer.cornerRadius = 20
        self.mainView.clipsToBounds = true
        
        self.emailTextField.keyboardType = .emailAddress
        self.emailTextField.setPlaceholder(placeholder: "Email Address", color: .gray)
        self.emailTextField.layer.cornerRadius = self.emailTextField.frame.height / 2
        self.emailTextField.layer.borderColor = UIColor.gray.cgColor
        self.emailTextField.layer.borderWidth = 1.0
        self.emailTextField.clipsToBounds = true
        
        self.cancelButton.layer.borderWidth = 1.0
        self.cancelButton.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.cancelButton.layer.cornerRadius = self.cancelButton.frame.height / 2
        self.cancelButton.clipsToBounds = true
        self.cancelButton.setTitleColor(.black, for: .normal)
        self.cancelButton.setTitle("CANCEL", for: .normal)
        self.cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        
        self.sendButton.backgroundColor = ThemeColor.buttonColor
        self.sendButton.layer.cornerRadius = self.sendButton.frame.height / 2
        self.sendButton.clipsToBounds = true
        self.sendButton.setTitleColor(.white, for: .normal)
        self.sendButton.setTitle("SEND", for: .normal)
        self.sendButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        
        
    }
    @objc func cancelButtonAction(){
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func sendButtonAction(){
        sendOTP()
    }
    
    func sendOTP(){
        if emailTextField.text?.isBlank ?? false{
            self.view.makeToast(message: "Please enter email address.")
            return
        }
        
        if !(emailTextField.text?.isValidEmail ?? true){
            self.view.makeToast(message: "Please enter a valid email address.")
            return
        }
        

        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "typ": Config.api.login.toData,
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "email": (self.emailTextField.text ?? "").toData
            ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + Config.api.sendOTP , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    let status = result["status"] as? String ?? ""
                    let message = result["type"] as? String ?? ""
                    
                    if status == "Ok"{
                        self.delegate?.getEmailAddress(email: (self.emailTextField.text ?? ""))
                        let alert = UIAlertController(title: "Success!", message: "OTP sent to your registered email address successfully.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else{
                        self.view.makeToast(message: message)
                    }
                    
                })
            default:
                self.activityIndicator.stopAnimating()
                self.view.makeToast(message: Message.apiError)
                break;
            }})
    }

}

protocol OTPViewControllerDelegate {
    func getEmailAddress(email: String)
}
