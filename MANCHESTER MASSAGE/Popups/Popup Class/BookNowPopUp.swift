//
//  BookNowPopUp.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 21/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
enum BookingPopUp {
    case home, serviceList, serviceDetails, cartPage
}
class BookNowPopUp: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var popupTitleLabel: UILabel!
    
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var addServiceButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    var fromPage: BookingPopUp!
    
//    @IBOutlet weak var timePickerView: APJTextPickerView!
//    @IBOutlet weak var datePickerView: APJTextPickerView!
//    @IBOutlet weak var therapistPickerView: APJTextPickerView!
//    @IBOutlet weak var therapistPicker2: APJTextPickerView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backView: UIView!
    
    var serviceData: ServiceDetailsModel!
    var currentService = [ServiceItemsModel]()
    var delegate: BookNowPopUpDelegate?
    var selectedIndex: Int = 0
    var cartItemIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.currentService.append(self.serviceData.serviceItems[0])
        
        self.nameText.setTextFieldImage(image: FAType.FAUser, direction: .right)
        self.emailText.setTextFieldImage(image: FAType.FAEnvelope, direction: .right)
        
        backView.backgroundColor = UIColor.clear
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureAction))
        tapGesture.numberOfTapsRequired = 1
        self.backView.addGestureRecognizer(tapGesture)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "MassageTableViewCell", bundle: nil), forCellReuseIdentifier: "MassageTableViewCell")
        backView.backgroundColor = UIColor.clear
        mainView.backgroundColor = ThemeColor.buttonColor
        
        self.strings = self.appD.employeeDetails.map { (employee) -> String in
            return (employee.user_name + " " + employee.user_lname)
        }
        
        self.popupTitleLabel.text = self.serviceData.serviceTitle
        
        let items = serviceData.serviceItems.count > 6 ? 6 : serviceData.serviceItems.count
        tableViewHeight.constant = CGFloat(items  * 50)
        _initTextDatePickerView()
        _initTextTimePickerView()
        initTextStringsPickerView()
        setUpUI()
    }
    
    func setUpUI() {
        
        
        self.nameText.addButtomBorder()
        self.emailText.addButtomBorder()
        yesBtn.setTitleColor(.black, for: .normal)
        yesBtn.setTitleColor(.black, for: .selected)
        yesBtn.setLeftImage(image: .FACircleO, color: .black, state: .normal)
        yesBtn.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
        yesBtn.addTarget(self, action: #selector(yesButtonAction), for: .touchUpInside)
        yesBtn.isSelected = true
        
        
        noBtn.setTitleColor(.black, for: .normal)
        noBtn.setTitleColor(.black, for: .selected)
        noBtn.setLeftImage(image: .FACircleO, color: .black, state: .normal)
        noBtn.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
        noBtn.addTarget(self, action: #selector(noButtonAction), for: .touchUpInside)
        
//        timePickerView.placeholder = "Select Time"
//        datePickerView.placeholder = "Select Date"
//        therapistPickerView.placeholder = "Select a therapist"
//        therapistPicker2.placeholder = "Select a therapist"
        
        /*if confirmButton.isSelected{
            confirmButton.backgroundColor = UIColor.white
            confirmButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
            confirmButton.clipsToBounds = true
            confirmButton.addTarget(self, action: #selector(confirmButtonAction(sender:)), for: .touchUpInside)
            confirmButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
            confirmButton.setTitle("ADD MORE SERVICES", for: .normal)
            return
        }
        else{
            
        }*/
        
        confirmButton.backgroundColor = UIColor.white
        confirmButton.setTitleColor(.white, for: UIControl.State())
        confirmButton.setTitle("Cancel", for: .normal)
        confirmButton.setTitle("ADD MORE SERVICES", for: .selected)
        confirmButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        confirmButton.backgroundColor = ThemeColor.bookNowButtonColor
        confirmButton.layer.cornerRadius = 5
        confirmButton.layer.masksToBounds = true
        confirmButton.addTarget(self, action: #selector(confirmButtonAction(sender:)), for: .touchUpInside)
        addServiceButton.backgroundColor = ThemeColor.buttonColor
        addServiceButton.layer.cornerRadius = 10
        addServiceButton.clipsToBounds = true
        addServiceButton.setTitle("CHOOSE SERVICE", for: .normal)
        addServiceButton.setTitle("GO TO CART", for: .selected)
        addServiceButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        addServiceButton.addTarget(self, action: #selector(addServiceButtonAction(sender:)), for: .touchUpInside)
        
        let options: UIView.AnimationOptions = [.transitionCrossDissolve]
        UIView.animate(withDuration: 0.4, delay: 0.3, options: options, animations: {
            [weak self] in
            self?.backView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }, completion: nil)
        
        if fromPage == .cartPage{
            if !self.appD.serviceCart[cartItemIndex].isForMe{
                noButtonAction()
                self.nameText.text = self.appD.serviceCart[cartItemIndex].cust_name
                self.emailText.text = self.appD.serviceCart[cartItemIndex].cust_email
            }else{
                yesButtonAction()
            }
        }
        
    }
    
    
    @objc func yesButtonAction(){
        
        self.nameText.isHidden = true
        self.emailText.isHidden = true
        yesBtn.isSelected = true
        noBtn.isSelected = false
    }
    @objc func noButtonAction(){
        self.nameText.isHidden = false
        self.emailText.isHidden = false
        yesBtn.isSelected = false
        noBtn.isSelected = true
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: private methods
    private func _initTextDatePickerView() {
//        datePickerView.pickerDelegate = self
//        datePickerView.dateFormatter.dateStyle = .medium
//        datePickerView.datePicker?.minimumDate = Date()
//        datePickerView.setRightViewFAIcon(icon: .FAAngleDown)
    }
    
    private func _initTextTimePickerView() {
//        timePickerView.pickerDelegate = self
//        timePickerView.type = .strings
//        timePickerView.pickerDelegate = self
//        timePickerView.dataSource = self
//        timePickerView.setRightViewFAIcon(icon: .FAAngleDown)
    }
    
    fileprivate var strings = [String]()
    private func initTextStringsPickerView() {
//        therapistPickerView.type = .strings
//        therapistPickerView.pickerDelegate = self
//        therapistPickerView.dataSource = self
//        therapistPickerView.setRightViewFAIcon(icon: .FAAngleDown)
//
//        therapistPicker2.type = .strings
//        therapistPicker2.pickerDelegate = self
//        therapistPicker2.dataSource = self
//        therapistPicker2.setRightViewFAIcon(icon: .FAAngleDown)
//        therapistPicker2.isHidden = true
    }
    @objc func tapGestureAction(){
        self.backView.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func addServiceButtonAction(sender: UIButton){
        if !sender.isSelected{
            if addToCartAction(){
                if fromPage == .cartPage{
                    self.backView.backgroundColor = UIColor.clear
                    self.dismiss(animated: true) {
                        self.delegate?.addServiceButtonActions()
                    }
                }else{
                    addServiceButton.isSelected = true
                    confirmButton.isSelected = true
                    self.mainView.layoutIfNeeded()
                }
                
            }
            
        }
        else{
            self.backView.backgroundColor = UIColor.clear
            self.dismiss(animated: true) {
                self.delegate?.addServiceButtonActions()
            }
        }
        
    }
    
    @objc func confirmButtonAction(sender: UIButton){
        if sender.isSelected{
//            let _ = addToCartAction()
            self.dismiss(animated: true)
        }
        else{
            self.backView.backgroundColor = UIColor.clear
            self.dismiss(animated: true) {
                self.delegate?.confirmButtonAction()
            }
        }
        
    }
    
    fileprivate func resetForm(){
        self.yesButtonAction()
        self.nameText.text = ""
        self.emailText.text = ""
        selectedIndex = -1
        self.currentService.removeAll()
        tableView.reloadData()
    }
    fileprivate func addToCartAction() -> Bool{
        
        if currentService.count == 0{
            self.tabBarController?.view.makeToast(message: "Please select a service")
            return false
        }
        
//        if !self.nameText.isHidden && (self.nameText.text?.isEmpty ?? true){
//            self.tabBarController?.view.makeToast(message: "Please enter the name")
//            return false
//        }
        
        var cartData = CartModel(itemId: (self.appD.serviceCart.count + 1), services: self.currentService, cat_id: serviceData.catId, vendor_id: "4", tot_amount: currentService[0].main_price, final_amount: currentService[0].offerPrice)
       
        cartData.serviceID = serviceData.smId
        cartData.therapists.removeAll()
        cartData.isForMe = self.nameText.isHidden
        cartData.cust_name = self.nameText.text ?? ""
        cartData.cust_email = self.emailText.text ?? ""
        
        if fromPage == .cartPage{
            self.appD.serviceCart.remove(at: self.cartItemIndex)
            self.appD.serviceCart.insert(cartData, at: self.cartItemIndex)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
            self.tabBarController?.view.makeToast(message: "Service has been updated in cart successfully")
            return true
        }else{
            self.appD.serviceCart.append(cartData)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
            self.tabBarController?.view.makeToast(message: "Service added to cart successfully")
    //        resetForm()
            return true
        }
        
        
        
        
    }
}

//extension BookNowPopUp: APJTextPickerViewDelegate {
//
//    func textPickerView(_ textPickerView: APJTextPickerView, didSelectDate date: Date?) {
//        guard let date = date else { return }
//        print("Date Selected: \(date)")
//        let dateformatter = DateFormatter() // 2.2
//        dateformatter.dateStyle = .medium // 2.3
//        if textPickerView == timePickerView{
//            let dateformatter = DateFormatter()
//            dateformatter.dateFormat = "hh:mm a"
//        }
//    }
//
//    func textPickerView(_ textPickerView: APJTextPickerView, didSelectString row: Int) {
//        //        print("City Selected: \(strings[row])")
//    }
//
//    func textPickerView(_ textPickerView: APJTextPickerView, titleForRow row: Int) -> String? {
//        if textPickerView == timePickerView{
////            let df = DateFormatter()
////            df.dateFormat = "HH:mm"
////            let time = df.date(from: self.appD.timeSlots[row])
////            df.dateFormat = "hh:mm a"
//            return self.appD.timeSlots[row]
//        }else{
//            return strings[row]
//        }
//    }
//}
//
//extension BookNowPopUp: APJTextPickerViewDataSource {
//    func numberOfRows(in pickerView: APJTextPickerView) -> Int {
//        if pickerView == timePickerView{
//            return self.appD.timeSlots.count
//        }else{
//            return strings.count
//        }
//
//    }
//}

protocol BookNowPopUpDelegate {
    func addServiceButtonActions()
    func confirmButtonAction()
}

extension BookNowPopUpDelegate{
    func addServiceButtonActions(){}
    func confirmButtonAction(){}
}

extension BookNowPopUp: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.serviceData.serviceItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MassageTableViewCell", for: indexPath) as! MassageTableViewCell
        cell.data = serviceData.serviceItems[indexPath.row]
        cell.isSelectedCell = (selectedIndex == indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        currentService.removeAll()
        currentService.append(self.serviceData.serviceItems[indexPath.row])
//        if Int(serviceData.serviceItems[indexPath.row].therapistNumber) ?? 0 > 1{
//            therapistPicker2.isHidden = false
//        }
//        else{
//            therapistPicker2.isHidden = true
//        }
    }
}
