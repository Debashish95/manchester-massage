//
//  NewTimeSlotPopUp.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 21/02/22.
//  Copyright © 2022 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol NewTimeSlotPopUpDelegate {
    func getTimeSlot(service: CartModel, date: String, time: String, therapists: [EmployeeModel])
}

class NewTimeSlotPopUp: UIViewController {

    @IBOutlet weak var chooseOtherDateTherapistLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var insideView: UIView!
    
    @IBOutlet weak var backToSummaryButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var appointmentTimeCollectionView: UICollectionView!
    @IBOutlet weak var appontmentDateBottomView: UIView!
    @IBOutlet weak var appointmentDateCollectionView: UICollectionView!
    @IBOutlet weak var therapistCollectionView: UICollectionView!
    @IBOutlet weak var therapistButtomView: UIView!
    
    var activityIndicator: NVActivityIndicatorView!
    var currentService: CartModel?
    var delegate: NewTimeSlotPopUpDelegate?
    var data: [EmployeeDateTimeSlot] = [EmployeeDateTimeSlot]() {
        didSet{
            updateData()
        }
    }
    let AllTherapistId = "-1199009"
    var availableTherapist = [EmployeeModel]()
    var selectedTherapistIndex = 0 {
        didSet{
            if selectedTherapistIndex == 0 {
                previousTherapistId = AllTherapistId
            } else {
                previousTherapistId = availableTherapist[selectedTherapistIndex - 1].id
            }
            therapistCollectionView.reloadData()
        }
    }
    
    var timeData = [TimeSlot](){
        didSet {
           
        }
    }
    var selectedTimeIndex = -1 {
        didSet{
            appointmentTimeCollectionView.reloadData()
            if selectedTimeIndex >= 0 && selectedTimeIndex < timeData.count {
                availableTherapist = timeData[selectedTimeIndex].therapists
                previousTime = timeData[selectedTimeIndex].time
            } else {
                availableTherapist.removeAll()
            }
            if let index = adjustTherapistIndexAccordingToPreviousId() {
                selectedTherapistIndex = index + 1
            } else {
                selectedTherapistIndex = 0
            }
            
        }
    }
    
    var selectedDateIndex = -1 {
        didSet {
            appointmentDateCollectionView.reloadData()
            timeData = data[selectedDateIndex].timeSlots
            previousDate = data[selectedDateIndex].date
            selectedTimeIndex = adjustTimeIndexAccordingToPreviousTime() ?? 0
        }
    }
    
    private var previousDate = ""
    private var previousTime = ""
    private var previousTherapistId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initialSetUp()
        if let _currentService =  currentService {
            getTimeSlot(
                serviceId: _currentService.services.first?.serviceID ?? "",
                duration:  Int(_currentService.services.first?.duration ?? "0") ?? 0
            )
        }
        
    }
    
    fileprivate func initialSetUp() {
        activityIndicator = addActivityIndicator()
        view.addSubview(activityIndicator)
        insideView.backgroundColor = .white
        mainView.layer.cornerRadius = 20
        mainView.clipsToBounds = true
        mainView.backgroundColor = ThemeColor.buttonColor
        insideView.layer.cornerRadius = 20.0
        insideView.clipsToBounds = true
        
        therapistButtomView.backgroundColor = ThemeColor.buttonColor
        appontmentDateBottomView.backgroundColor = ThemeColor.buttonColor
        
        appointmentTimeCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        therapistCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        appointmentDateCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        
        appointmentTimeCollectionView.dataSource = self
        appointmentTimeCollectionView.delegate = self

        therapistCollectionView.dataSource = self
        therapistCollectionView.delegate = self

        appointmentDateCollectionView.dataSource = self
        appointmentDateCollectionView.delegate = self
        
        confirmButton.backgroundColor = ThemeColor.buttonColor
        confirmButton.layer.cornerRadius = 10
        confirmButton.clipsToBounds = true
        confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
        backToSummaryButton.setTitleColor(ThemeColor.buttonColor, for: UIControl.State())
        backToSummaryButton.addTarget(self, action: #selector(backToSummaryAction), for: .touchUpInside)
    }
    
    @objc func backToSummaryAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func updateData() {
        therapistCollectionView.reloadData()
        appointmentDateCollectionView.reloadData()
        appointmentTimeCollectionView.reloadData()
    }
    
    func getTimeSlot(serviceId: String,
                     duration: Int,
                     excludeDate: [String] = [String](),
                     excludeTimeSlot: [String] = [String](),
                     excludeEmployee: [EmployeeModel] = [EmployeeModel](),
                     requiredTherapist: Int = 1) {
        activityIndicator.startAnimating()
        GetTimeSlotForBooking(serviceId: serviceId, duration: duration) { data in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.data = self.excludeExistingSlots(data: data)
                if self.data.count > 0 {
                    self.selectedDateIndex = 0
                    self.chooseOtherDateTherapistLabel.isHidden = true
                } else {
                    self.chooseOtherDateTherapistLabel.isHidden = false
                    self.tabBarController?.view.makeToast(message: "No Data Found!!")
                }
            }
        }
    }
    
    @objc func confirmButtonAction() {
        guard let currentService = currentService else { return }
        var requiredTherapist = Int(currentService.services.first?.therapistNumber ?? "1") ?? 1
        var therapists = [EmployeeModel]()
        if availableTherapist.count > 0 {
            if selectedTherapistIndex > 0 {
                therapists.append(availableTherapist[selectedTherapistIndex - 1])
                availableTherapist.remove(at: selectedTherapistIndex - 1)
            } else {
                therapists.append(availableTherapist[0])
                availableTherapist.remove(at: 0)
            }
            requiredTherapist = requiredTherapist - 1
            for _ in 0..<requiredTherapist {
                if availableTherapist.count > 0 {
                    therapists.append(availableTherapist[0])
                    availableTherapist.remove(at: 0)
                }
            }
        }
        self.delegate?.getTimeSlot(
            service: currentService,
            date: data[selectedDateIndex].date,
            time: timeData[selectedTimeIndex].time,
            therapists: therapists
        )
        self.dismiss(animated: true, completion: nil)
        
    }
    
    private func excludeExistingSlots(data: [EmployeeDateTimeSlot]) -> [EmployeeDateTimeSlot]{
        var filteredData = data
        for index in 0..<appD.serviceCart.count {
            let currentService = appD.serviceCart[index]
            print("Service Name: \(currentService.services.first?.name ?? "")")
            
            // REMOVE BOOKED THERAPISTS
            let therapistIds = currentService.therapists.map({$0.id})
            if let currentDateIndex = data.firstIndex(
                where: {$0.date == currentService.bookingDate}
            ),
               let currentTimeSlotIndex = filteredData[currentDateIndex].timeSlots.firstIndex(
                where: {$0.time == currentService.bookingTime }) {
                let bookedTherapists = filteredData[currentDateIndex].timeSlots[currentTimeSlotIndex].therapists.filter({ currentEmployee in
                    return therapistIds.contains(currentEmployee.id)
                })
                
                let timeInterval = getTimeInterval(startTime: currentService.bookingTime, endTime: currentService.bookingEndTime)
                
                
                for ind1 in 0..<timeInterval {
                    if filteredData[currentDateIndex].timeSlots.count > (currentTimeSlotIndex + ind1) {
                        filteredData[currentDateIndex].timeSlots[currentTimeSlotIndex + ind1].therapists.removeAll { empData in
                            if let service = self.currentService,
                               currentService == service {
                                return false
                            }
                            return bookedTherapists.contains(where: {$0.id == empData.id})
                        }
                    }
                }

            }
        }
        
        // REMOVE TIMES ALREADY BOOKED FOR ME
        if let _currentService = self.currentService,
           _currentService.isForMe {
            let allForMe = appD.serviceCart.filter { cartData in
                return (cartData.bookingTime != "" &&
                        cartData.isForMe &&
                        !(cartData == _currentService))
            }
            for ind3 in 0..<allForMe.count {
                let currentForMe = allForMe[ind3]
                if let dtIndex = filteredData.firstIndex(where: {
                    $0.date == currentForMe.bookingDate
                }),
                   let timeIndex = filteredData[dtIndex].timeSlots.firstIndex(where: {
                       $0.time == currentForMe.bookingTime
                   })
                {
                    let timeInterval = getTimeInterval(startTime: currentForMe.bookingTime, endTime: currentForMe.bookingEndTime)
                    var times = [String]()
                    for ind1 in 0..<timeInterval {
                        if filteredData[dtIndex].timeSlots.count > (timeIndex + ind1) {
                            times.append(filteredData[dtIndex].timeSlots[timeIndex + ind1].time)
                        }
                    }
                    
                    let timeDurationCurrentService = Int(_currentService.services.first?.duration ?? "0") ?? 1
                    let timeIntervalFinal = Int(ceil(Double(timeDurationCurrentService) / Double(60)))
                    for ind1 in 1..<timeIntervalFinal {
                        if filteredData[dtIndex].timeSlots.count > (timeIndex - ind1) &&
                            (timeIndex - ind1) >= 0 {
                            times.append(filteredData[dtIndex].timeSlots[timeIndex - ind1].time)
                        }
                    }
                    
                    filteredData[dtIndex].timeSlots.removeAll(where: {
                        times.contains($0.time)
                    })
                }
            }
        }
        filteredData = removeBlankSlots(data: filteredData)
        return filteredData
    }
    
    private func removeBlankSlots(data: [EmployeeDateTimeSlot]) -> [EmployeeDateTimeSlot] {
        var finalData = data
        finalData.removeAll { empTimeData in
            var timeSlot = empTimeData.timeSlots
            timeSlot.removeAll(where: {$0.therapists.count == 0})
            return timeSlot.count == 0
        }
        return finalData
    }
    
    private func getTimeInterval(startTime: String, endTime: String) -> Int{
        if let startTime = startTime.convertDate(format: "hh:mm a"),
           let endTime = endTime.convertDate(format: "hh:mm a") {
            let diffComponents = Calendar.current.dateComponents([.minute], from: startTime, to: endTime)
            if let minutesDiff = diffComponents.minute {
                let timeInterval = Int(ceil(Double(minutesDiff) / Double(60)))
                return timeInterval
            }
        }
        return 0
    }
    
    private func adjustTimeIndexAccordingToPreviousTime() -> Int? {
        guard !previousTime.isBlank else { return nil }
        return timeData.firstIndex(where: {$0.time == previousTime})
    }
    
    private func adjustTherapistIndexAccordingToPreviousId() -> Int? {
        guard !previousTherapistId.isBlank else { return nil }
        return availableTherapist.firstIndex(where: {$0.id == previousTherapistId})
    }
    
}


extension NewTimeSlotPopUp: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == therapistCollectionView {
            return availableTherapist.count + 1
        } else if collectionView == appointmentDateCollectionView {
            return data.count
        } else if collectionView == appointmentTimeCollectionView {
            return timeData.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTimeCollectionViewCell", for: indexPath) as! DateTimeCollectionViewCell
        if collectionView == therapistCollectionView {
            if indexPath.item == 0 {
                cell.dataLabel.text = "All Therapists"
            } else {
                let availableThera = availableTherapist[indexPath.item - 1]
                cell.dataLabel.text = availableThera.user_name + " " + availableThera.user_lname
            }
            cell.isChoosen = indexPath.item == selectedTherapistIndex
        }
        
        if collectionView == appointmentDateCollectionView {
            cell.dataLabel.text = data[indexPath.item].dateToShow
            cell.isChoosen = indexPath.item == selectedDateIndex
        }
        
        if collectionView == appointmentTimeCollectionView {
            cell.dataLabel.text = timeData[indexPath.item].time
            cell.isChoosen = indexPath.item == selectedTimeIndex
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == appointmentTimeCollectionView{
            return CGSize(width: 80, height: 40)
        }
        return CGSize(width: 120, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView ==  appointmentDateCollectionView {
            selectedDateIndex = indexPath.item
        } else if collectionView ==  appointmentTimeCollectionView {
            selectedTimeIndex = indexPath.item
        } else if collectionView ==  therapistCollectionView {
            selectedTherapistIndex = indexPath.item
        }
    }
}
