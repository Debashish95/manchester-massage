//
//  SelectDateTimePopUp.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 13/09/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class RescheduleBooking: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var insideView: UIView!
    
    @IBOutlet weak var backToSummaryButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var appointmentTimeCollectionView: UICollectionView!
    @IBOutlet weak var appontmentDateBottomView: UIView!
    @IBOutlet weak var appointmentDateCollectionView: UICollectionView!
    @IBOutlet weak var therapistCollectionView: UICollectionView!
    @IBOutlet weak var therapistButtomView: UIView!
    var activityIndicator: NVActivityIndicatorView!
    var delegate: RescheduleBookingDelegate?
    
    var startingIndex = 0
    
    var selectedTherapistIndex = 0
    var selectedDateIndex = 0
    var selectedTimeIndex = 0
    
    var allEmployeeTimeSlots = [EmployeeDateTimeSlot]()
    var currentEmployeeTimeSlots = [EmployeeDateTimeSlot]()
    var tempEmpDataTime = [EmployeeDateTimeSlot]()
    
    var servicesArray = [String]()
    var servicesArrayString = ""
    var minimumTherapistRequired = 0
    var currentCartService: CartModel!
    var bookedServices = [CartModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        insideView.backgroundColor = .white
        mainView.layer.cornerRadius = 20
        mainView.clipsToBounds = true
        mainView.backgroundColor = ThemeColor.buttonColor
        insideView.layer.cornerRadius = 20.0
        insideView.clipsToBounds = true
        
        therapistButtomView.backgroundColor = ThemeColor.buttonColor
        appontmentDateBottomView.backgroundColor = ThemeColor.buttonColor
        
        appointmentTimeCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        therapistCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        appointmentDateCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        
        appointmentTimeCollectionView.dataSource = self
        appointmentTimeCollectionView.delegate = self
        
        therapistCollectionView.dataSource = self
        therapistCollectionView.delegate = self
        
        appointmentDateCollectionView.dataSource = self
        appointmentDateCollectionView.delegate = self
        
        confirmButton.backgroundColor = ThemeColor.buttonColor
        confirmButton.layer.cornerRadius = 10
        confirmButton.clipsToBounds = true
        confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
        backToSummaryButton.setTitleColor(ThemeColor.buttonColor, for: UIControl.State())
        backToSummaryButton.addTarget(self, action: #selector(backToSummaryAction), for: .touchUpInside)
        
        self.minimumTherapistRequired = (Int(self.bookedServices[self.startingIndex].services.first?.therapistNumber ?? "0") ?? 0)
        servicesArray.removeAll()
        servicesArray.append(self.bookedServices[self.startingIndex].serviceID)
        
        if servicesArray.count > 0{
            servicesArrayString = "["
            servicesArray.forEach { (serviceID) in
                servicesArrayString.append("%22\(serviceID)%22,")
            }
            servicesArrayString.removeLast()
            servicesArrayString.append("]")
        }
        
        self.getAllDateTimeSlot()
    }
    
    @objc
    func backToSummaryAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc
    func confirmButtonAction(){
        setupCartData(cartData: &self.bookedServices[self.startingIndex])
        delegate?.selectPopUpData(data: self.bookedServices)
        self.dismiss(animated: true) {
//            let destination = FinalBookingViewController(nibName: "FinalBookingViewController", bundle: nil)
        }
        
    }
    
    fileprivate func setupCartData(cartData: inout CartModel){
       
        cartData.bookingDate = self.allEmployeeTimeSlots[self.selectedDateIndex].date
        cartData.bookingDay = self.getFormattedDate(strDate: cartData.bookingDate, currentFomat: "yyyy-MM-dd", expectedFromat: "EEEE")
        cartData.bookingTime = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].time
        if let service = cartData.services.first{
            let timeToAdd = Int(service.duration) ?? 0
            let df = DateFormatter()
            df.timeZone = .current
            df.dateFormat = "hh:mm a"
            let curDate = df.date(from: cartData.bookingTime)!
            let endTm = Calendar.current.date(byAdding: .minute, value: timeToAdd, to: curDate)!
            let endTMString = df.string(from: endTm)
            cartData.bookingEndTime = endTMString
            
            
            if service.therapistNumber == "2"{
                let firstTh = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists[self.selectedTherapistIndex]
                for index in 0..<self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists.count{
                    if self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists[index].id != firstTh.id{
                        cartData.therapists = [firstTh, self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists[index]]
                    }
                }
                
            }else{
                cartData.therapists = [self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists[self.selectedTherapistIndex]]
            }
        }
    }
    
    
    func getAllDateTimeSlot(){
        
        self.activityIndicator.startAnimating()
        let reuestURL = "\(Config.mainUrl)Data/GetAllTimeSlot?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&vn_id=4&services=\(servicesArrayString)&theraview=Y&skip_blankdate=1"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                self.allEmployeeTimeSlots = [EmployeeDateTimeSlot]()
                if (resposeJSON["status"] as? Bool ?? false){
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<result.count{
                        let currentResult = result[index] as? NSDictionary ?? NSDictionary()
                        let date = currentResult["date"] as? String ?? "2001-01-01"
                        let dateToShow = self.getFormattedDate(strDate: date, currentFomat: "yyyy-MM-dd", expectedFromat: "EE, MMM dd")
                        let isBooking = currentResult["is_booking"] as? String ?? "0"
                        let slots = currentResult["slots"] as? NSArray ?? NSArray()
                        var timeSlots = [TimeSlot]()
                        for index1 in 0..<slots.count{
                            let currentSlot = slots[index1] as? NSDictionary ?? NSDictionary()
                            let time = currentSlot["time"] as? String ?? "00:00 AM"
                            let theras = currentSlot["theras"] as? NSArray ?? NSArray()
                            var allTheapists = [EmployeeModel]()
                            for index2 in 0..<theras.count{
                                let currentTherapist = theras[index2] as? NSDictionary ?? NSDictionary()
                                let theraID = currentTherapist["id"] as? String ?? ""
                                if let therapist = self.appD.employeeDetails.first(where: {$0.id == theraID}){
                                    allTheapists.append(therapist)
                                }
                            }
                            if allTheapists.count >= self.minimumTherapistRequired{
                                timeSlots.append(TimeSlot(time: time, therapists: allTheapists))
                            }
                        }
                        self.allEmployeeTimeSlots.append(EmployeeDateTimeSlot(date: date, dateToShow: dateToShow, isBooking: isBooking, timeSlots: timeSlots))
                    }
                    
                    for index in 0..<self.startingIndex{
                        let currentItem = self.bookedServices[index]
                        //Remove All Therapists & Booked Time Slots
                        for ind1 in 0..<self.allEmployeeTimeSlots.count{
                            for ind2 in 0..<self.allEmployeeTimeSlots[ind1].timeSlots.count{
                                if self.allEmployeeTimeSlots[ind1].timeSlots[ind2].time == currentItem.bookingTime && self.allEmployeeTimeSlots[ind1].date == currentItem.bookingDate{
                                    for ind3 in 0..<currentItem.therapists.count{
                                        self.allEmployeeTimeSlots[ind1].timeSlots[ind2].therapists.removeAll(where: {$0.id == currentItem.therapists[ind3].id})
                                        
                                        if currentItem.services.first?.duration == "120"{
                                            if ind2 < (self.allEmployeeTimeSlots[ind1].timeSlots.count - 1) {
                                                self.allEmployeeTimeSlots[ind1].timeSlots[ind2+1].therapists.removeAll(where: {$0.id == currentItem.therapists[ind3].id})
                                            }
                                            
                                        }
                                        
                                        if let currentService = self.bookedServices[self.startingIndex].services.first{
                                            if currentService.duration == "120"{
                                                if ind2 > 0{
                                                    self.allEmployeeTimeSlots[ind1].timeSlots[ind2-1].therapists.removeAll(where: {$0.id == currentItem.therapists[ind3].id})
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    
                    for index in (self.startingIndex + 1)..<self.bookedServices.count{
                        let currentItem = self.bookedServices[index]
                        //Remove All Therapists & Booked Time Slots
                        for ind1 in 0..<self.allEmployeeTimeSlots.count{
                            for ind2 in 0..<self.allEmployeeTimeSlots[ind1].timeSlots.count{
                                if self.allEmployeeTimeSlots[ind1].timeSlots[ind2].time == currentItem.bookingTime && self.allEmployeeTimeSlots[ind1].date == currentItem.bookingDate{
                                    for ind3 in 0..<currentItem.therapists.count{
                                        self.allEmployeeTimeSlots[ind1].timeSlots[ind2].therapists.removeAll(where: {$0.id == currentItem.therapists[ind3].id})
                                        if currentItem.services.first?.duration == "120"{
                                            if ind2 < (self.allEmployeeTimeSlots[ind1].timeSlots.count - 1) {
                                                self.allEmployeeTimeSlots[ind1].timeSlots[ind2+1].therapists.removeAll(where: {$0.id == currentItem.therapists[ind3].id})
                                            }
                                           
                                        }
                                        
                                        if let currentService = self.bookedServices[self.startingIndex].services.first{
                                            if currentService.duration == "120"{
                                                if ind2 > 0{
                                                    self.allEmployeeTimeSlots[ind1].timeSlots[ind2-1].therapists.removeAll(where: {$0.id == currentItem.therapists[ind3].id})
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if let currentService = self.bookedServices[self.startingIndex].services.first{
                        if currentService.duration == "120"{
                            for index in 0..<self.allEmployeeTimeSlots.count{
                                self.allEmployeeTimeSlots[index].timeSlots.removeAll(where: {$0.time == "07:00 PM"})
                            }
                        }
                    }
                    self.selectedDateIndex = self.allEmployeeTimeSlots.firstIndex(where: { (dateData) -> Bool in
                        return dateData.date == self.currentCartService.bookingDate
                    }) ?? 0
                    if self.allEmployeeTimeSlots.count > 0{
                        self.selectedTimeIndex = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.firstIndex(where: { (timeData) -> Bool in
                            return timeData.time == self.currentCartService.bookingTime
                        }) ?? 0
                        if self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count > 0{
                            self.currentCartService.therapists.forEach { (empData) in
                                self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists.append(empData)
                            }
                            self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists.sort { (emp1, emp2) -> Bool in
                                return (emp1.id) < emp2.id
                            }
                        }
                        if self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count > 0{
                            self.selectedTherapistIndex = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists.firstIndex(where: { (therapistData) -> Bool in
                                let therapistToSelect = self.currentCartService.therapists.first?.id ?? "0"
                                return therapistData.id == therapistToSelect
                            }) ?? 0
                        }
                    }
                    
                    self.appointmentTimeCollectionView.reloadData()
                    self.appointmentDateCollectionView.reloadData()
                    self.therapistCollectionView.reloadData()
                    if self.allEmployeeTimeSlots.first?.timeSlots.count ?? 0 == 0{
                        self.disable(sender: self.confirmButton)
                    }else{
                        self.enable(sender: self.confirmButton)
                    }
                }

                
            }
        
    }
    
    func disable(sender: UIButton){
        sender.isEnabled = false
        sender.alpha = 0.5
    }
    
    func enable(sender: UIButton){
        sender.isEnabled = true
        sender.alpha = 1.0
    }
    
    func getFormattedDate(strDate: String , currentFomat:String, expectedFromat: String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = currentFomat
        dateFormatterGet.timeZone = .current
        let date : Date = dateFormatterGet.date(from: strDate) ?? Date()
        
        dateFormatterGet.dateFormat = expectedFromat
        return dateFormatterGet.string(from: date)
    }

}

extension RescheduleBooking: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.allEmployeeTimeSlots.count == 0{
            return 0
        }
        
        if collectionView == therapistCollectionView{
            if self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count > selectedTimeIndex{
                return self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[selectedTimeIndex].therapists.count
            }
            return 0
        }else if collectionView == appointmentTimeCollectionView{
            return self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count
        }else{
            return self.allEmployeeTimeSlots.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTimeCollectionViewCell", for: indexPath) as! DateTimeCollectionViewCell
        if collectionView == therapistCollectionView{
            cell.isChoosen = indexPath.item == selectedTherapistIndex
            cell.dataLabel.text = "\(self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[selectedTimeIndex].therapists[indexPath.item].user_name) \(self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[selectedTimeIndex].therapists[indexPath.item].user_lname)"
            
        }else if collectionView == appointmentTimeCollectionView{
            cell.dataLabel.text = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[indexPath.item].time
            cell.isChoosen = indexPath.item == selectedTimeIndex
        }
        else{
            cell.dataLabel.text = self.allEmployeeTimeSlots[indexPath.item].dateToShow
            cell.isChoosen = indexPath.item == selectedDateIndex
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == appointmentTimeCollectionView{
            return CGSize(width: 80, height: 40)
        }
        return CGSize(width: 120, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == appointmentTimeCollectionView{
            self.selectedTimeIndex = indexPath.item
            
        }else if collectionView == appointmentDateCollectionView{
            self.selectedDateIndex = indexPath.item
        }else if collectionView == therapistCollectionView{
            self.selectedTherapistIndex = indexPath.item
        }
        
        if self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count == 0{
            self.disable(sender: self.confirmButton)
        }else{
            self.enable(sender: self.confirmButton)
        }
        appointmentDateCollectionView.reloadData()
        appointmentTimeCollectionView.reloadData()
        therapistCollectionView.reloadData()
    }
}

protocol RescheduleBookingDelegate {
    func selectPopUpData(data: [CartModel])
}
