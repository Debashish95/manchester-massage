//
//  LeftViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 15/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class LeftViewController: UIViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextLabel: UILabel!
    @IBOutlet weak var emailTextLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    var parentVC: UIViewController!
    var menuItems = [MenuModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainView.backgroundColor = ThemeColor.buttonColor.withAlphaComponent(0.75)
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            nameTextLabel.text = "Hello!"
            emailTextLabel.text = "Please login to use all features"
        }else{
            nameTextLabel.text = (UserDefaults.standard.fetchData(forKey: .userFirstName) + " " + UserDefaults.standard.fetchData(forKey: .userLastName)).uppercased()
            emailTextLabel.text = UserDefaults.standard.fetchData(forKey: .userEmail)
            if let imageURL =  URL(string: UserDefaults.standard.fetchData(forKey: .userImage)){
                profileImage.kf.setImage(with:imageURL)
            }
            
        }
        
        
        nameTextLabel.textColor = .white
        emailTextLabel.textColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableView.backgroundColor = .clear
        
        if self.parentVC.isKind(of: HomeViewController.self) {
            self.setupServiceMenu()
        }else if self.parentVC.isKind(of: ProductCategoryViewController.self) {
            self.setupProductMenu()
            
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dissmissVC))
        tapGesture.numberOfTouchesRequired = 1
        self.backView.addGestureRecognizer(tapGesture)
        
        closeButton.backgroundColor = .white
        closeButton.setImage(UIImage(named: "cross"), for: UIControl.State())
        closeButton.layer.cornerRadius = closeButton.frame.height / 2
        closeButton.clipsToBounds = true
        closeButton.addTarget(self, action: #selector(dissmissVC), for: .touchUpInside)
        
    }
    override func viewDidLayoutSubviews() {
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.clipsToBounds = true
        if let imageURL =  URL(string: UserDefaults.standard.fetchData(forKey: .userImage)){
            profileImage.kf.setImage(with:imageURL)
        }else{
            profileImage.image = UIImage(named: "couple_massage")
        }
        
        
    }
    @objc func dissmissVC(){
        self.dismiss(animated: false, completion: nil)
    }
    
    fileprivate func setupProductMenu(){
        
        let menu0 = MenuModel(menuName: "Product Category".uppercased(), menuImage: .FACubes, isEnabled: true)
        menuItems.append(menu0)
        
        let menu1 = MenuModel(menuName: "My Orders".uppercased(), menuImage: .FAShoppingBag, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menu1)
        
        let menu2 = MenuModel(menuName: "My Wishlist".uppercased(), menuImage: .FAHeart, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menu2)
        
        let menu3 = MenuModel(menuName: "My Cart".uppercased(), menuImage: .FAShoppingCart, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menu3)
        
        let menu4 = MenuModel(menuName: "My Address".uppercased(), menuImage: .FAMapMarker, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menu4)
        
        let menu5 = MenuModel(menuName: "Back To Main Menu".uppercased(), menuImage: .FAChevronLeft, isEnabled: true)
        menuItems.append(menu5)
        
        /*let menu5 = MenuModel(menuName: "Review".uppercased(), menuImage: .FAStarO, isEnabled: true)
        menuItems.append(menu5)
        
        let menu6 = MenuModel(menuName: "Coupons".uppercased(), menuImage: .FATicket, isEnabled: true)
        menuItems.append(menu6)*/
    }
    
    fileprivate func setupServiceMenu(){
        let menu1 = MenuModel(menuName: "HOME", menuImage: .FAHome)
        menuItems.append(menu1)
        
        
        //        let menu3 = MenuModel(menuName: "MY BOOKINGS", menuImage: .FABookmark, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        //        menuItems.append(menu3)
        //
        //        let menu5 = MenuModel(menuName: "MY FAVOURITES", menuImage: .FAHeartO, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        //        menuItems.append(menu5)
        
        let menu6 = MenuModel(menuName: "GALLERY", menuImage: .FAFileImageO, isEnabled: true)
        menuItems.append(menu6)
        
        
        let menu4 = MenuModel(menuName: "Benefits of Massage".uppercased(), menuImage: UIImage(named: "benefits_massage")!, isEnabled: true)
        menuItems.append(menu4)
        
        let menu7 = MenuModel(menuName: "REVIEWS", menuImage: .FAStarO, isEnabled: true)
        menuItems.append(menu7)
        
        //        let menu9 = MenuModel(menuName: "Change Category", menuImage: .FAUndo, isEnabled: true)
        //        menuItems.append(menu9)
        
        let menu2 = MenuModel(menuName: "PROFILE", menuImage: .FAUser, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menu2)
        
        let menuGiftCard = MenuModel(menuName: "GIFT CARDS", menuImage: .FAGift, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menuGiftCard)
        
        let menuWallet = MenuModel(menuName: "WALLETS", menuImage: UIImage(named: "wallet") ?? UIImage(), isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menuWallet)
        
        let menuRefer = MenuModel(menuName: "REFER and EARN", menuImage: UIImage(named: "refer") ?? UIImage(), isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menuRefer)
        
        
                /*let menu4 = MenuModel(menuName: "VOUCHERS", menuImage: .FATicket, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
                menuItems.append(menu4)*/
        
        let menu8 = MenuModel(menuName: "PRODUCTS", menuImage: .FACubes, isEnabled: true)
        menuItems.append(menu8)
        
        let menuSupport = MenuModel(menuName: "SUPPORT", menuImage: .FASupport)
        menuItems.append(menuSupport)
        
        
        //        let menu6 = MenuModel()
        //        menuItems.append(menu6)
        //        let menu7 = MenuModel()
        //        menuItems.append(menu7)
        //        let menu8 = MenuModel()
        //        menuItems.append(menu8)
        
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let menu10 = MenuModel(menuName: "LOGIN", menuImage: .FASignIn)
            menuItems.append(menu10)
        }else{
            let menu9 = MenuModel(menuName: "LOGOUT", menuImage: .FASignOut)
            menuItems.append(menu9)
        }
    }
}


extension LeftViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.setData(data: menuItems[indexPath.row])
        if indexPath.row == 0{
            cell.backView.backgroundColor = UIColor(hexString: "#B0FFFFFF",alpha: 0.8)
            cell.menuNameLabel.textColor = ThemeColor.buttonColor
            cell.menuImage.tintColor = ThemeColor.buttonColor
            cell.backView.layer.cornerRadius = 10.0
            cell.backView.clipsToBounds = true
        }
        else{
            cell.backView.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let presentingViewController = self.presentingViewController
        self.dismiss(animated: false) {
            switch self.menuItems[indexPath.row].menuName {
            case "LOGIN":
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                presentingViewController?.present(navigationController, animated: true, completion: nil)
            case "LOGOUT":
                let alert = UIAlertController(title: "Alert!", message: "Do you want to logout?", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (action) in
                    UserDefaults.standard.deleteData(forKey: .userFirstName)
                    UserDefaults.standard.deleteData(forKey: .userLastName)
                    UserDefaults.standard.deleteData(forKey: .userEmail)
                    UserDefaults.standard.deleteData(forKey: .userSocialLogin)
                    UserDefaults.standard.deleteData(forKey: .userSocialLoginId)
                    UserDefaults.standard.deleteData(forKey: .userId)
                    UserDefaults.standard.deleteData(forKey: .userPhone)
                    UserDefaults.standard.deleteData(forKey: .userStatus)
                    UserDefaults.standard.deleteData(forKey: .skipLogin)
                    UserDefaults.standard.deleteData(forKey: .userImage)
                    self.appD.serviceCart.removeAll()
                    let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                    let navigationController = UINavigationController(rootViewController: destination)
                    self.appD.window?.rootViewController = navigationController
                    self.appD.window?.makeKeyAndVisible()
                }
                let noAction = UIAlertAction(title: "No", style: .cancel) { (action) in
                }
                alert.addAction(yesAction)
                alert.addAction(noAction)
                presentingViewController?.present(alert, animated: false, completion: nil)
            case "SUPPORT":
                let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                presentingViewController?.present(navigationController, animated: true, completion: nil)
                break;
            case "PROFILE":
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
//                let navigationController = UINavigationController(rootViewController: destination)
//                navigationController.modalPresentationStyle = .fullScreen
//                presentingViewController?.present(navigationController, animated: true, completion: nil)
                break;
            case "MY BOOKINGS":
                let destination = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "MyBookingsViewController") as! MyBookingsViewController
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                presentingViewController?.present(navigationController, animated: true, completion: nil)
                break;
            case "Back To Main Menu".uppercased():
                self.topViewController?.dismiss(animated: true, completion: nil)
                break
            case "Benefits of Massage".uppercased():
                let destnation = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
                destnation.pageTitle = "Benefits of Massage".uppercased()
                destnation.pageContents = self.appD.benefitsOfMassage
                destnation.isFromLeftMenu = true
                self.parentVC.navigationController?.pushViewController(destnation, animated: true)
                
//            case "MY FAVOURITES":
//                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavouriteViewController") as! FavouriteViewController
//                self.parentVC.navigationController?.pushViewController(destnation, animated: true)
//                break;
//
            case "GALLERY":
                let destination = GalleryViewController(nibName: "GalleryViewController", bundle: nil)
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break;
            case "REVIEWS":
                let destination = ReviewsViewController(nibName: "ReviewsViewController", bundle: nil)
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break;
                
            case "PRODUCTS":
                let destination = ProductCategoryViewController(nibName: "ProductCategoryViewController", bundle: nil)
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                presentingViewController?.present(navigationController, animated: true, completion: nil)
                break;
                
            case "Change Category":
                let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
                let navigationController = UINavigationController(rootViewController: destination)
                self.appD.window?.rootViewController = navigationController
                self.appD.window?.makeKeyAndVisible()
                
            case "MY CART":
                let destination = PhysicalCartViewController(nibName: "PhysicalCartViewController", bundle: nil)
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break;
                
            case "MY ADDRESS":
                let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break;
                
            case "MY WISHLIST":
                let destination = ProductListViewController(nibName: "ProductListViewController", bundle: nil)
                destination.isFromWishList = true
                destination.pageTitle = "My WishList"
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break
                
            case "MY ORDERS":
                let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "MyOrderListViewController") as! MyOrderListViewController
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break
            case "GIFT CARDS":
                let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "GiftViewController") as! GiftViewController
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break
                
            case "WALLETS":
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break
            case "REFER and EARN":
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReferViewController") as! ReferViewController
                self.parentVC.navigationController?.pushViewController(destination, animated: true)
                break
            default:
                break;
            }
        }

    }
}
