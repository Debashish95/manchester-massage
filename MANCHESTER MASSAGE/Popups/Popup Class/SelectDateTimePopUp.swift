//
//  SelectDateTimePopUp.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 13/09/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class SelectDateTimePopUp: UIViewController {
    
    @IBOutlet weak var chooseOtherDateTherapistLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var insideView: UIView!
    
    @IBOutlet weak var backToSummaryButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var appointmentTimeCollectionView: UICollectionView!
    @IBOutlet weak var appontmentDateBottomView: UIView!
    @IBOutlet weak var appointmentDateCollectionView: UICollectionView!
    @IBOutlet weak var therapistCollectionView: UICollectionView!
    @IBOutlet weak var therapistButtomView: UIView!
    var activityIndicator: NVActivityIndicatorView!
    var delegate: SelectDateTimePopUpDelegate?
    
    var cartViewController: CartViewController!
    var selectedTherapistIndex = 0
    var selectedDateIndex = 0
    var selectedTimeIndex = 0
    
    var allEmployeeTimeSlots = [EmployeeDateTimeSlot]()
    var currentEmployeeTimeSlots = [EmployeeDateTimeSlot]()
    var tempEmpDataTime = [EmployeeDateTimeSlot]()
    
    var servicesArray = [String]()
    var servicesArrayString = ""
    var minimumTherapistRequired = 0
    var is2HrsMassageExits = false
    
    var currentReScheduleService: servicelist!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        insideView.backgroundColor = .white
        mainView.layer.cornerRadius = 20
        mainView.clipsToBounds = true
        mainView.backgroundColor = ThemeColor.buttonColor
        insideView.layer.cornerRadius = 20.0
        insideView.clipsToBounds = true
        
        therapistButtomView.backgroundColor = ThemeColor.buttonColor
        appontmentDateBottomView.backgroundColor = ThemeColor.buttonColor
        
        appointmentTimeCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        therapistCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        appointmentDateCollectionView.register(UINib(nibName: "DateTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DateTimeCollectionViewCell")
        
        appointmentTimeCollectionView.dataSource = self
        appointmentTimeCollectionView.delegate = self
        
        therapistCollectionView.dataSource = self
        therapistCollectionView.delegate = self
        
        appointmentDateCollectionView.dataSource = self
        appointmentDateCollectionView.delegate = self
        
        confirmButton.backgroundColor = ThemeColor.buttonColor
        confirmButton.layer.cornerRadius = 10
        confirmButton.clipsToBounds = true
        confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
        backToSummaryButton.setTitleColor(ThemeColor.buttonColor, for: UIControl.State())
        backToSummaryButton.addTarget(self, action: #selector(backToSummaryAction), for: .touchUpInside)
  
        if cartViewController != nil{
            var friendTotal = 0
            var friendIndex = 0
            var ownTotal    = 0
            var ownAddable = true
            servicesArray.removeAll()
            
            for index in 0..<self.appD.serviceCart.count{
                let currentItem = self.appD.serviceCart[index]
                if currentItem.isForMe{
                    ownTotal = ownTotal + (Int(currentItem.services.first?.duration ?? "0") ?? 0)
                    servicesArray.append(currentItem.services.first?.serviceID ?? "")
                    if (ownAddable){
                        minimumTherapistRequired += Int(currentItem.services.first?.therapistNumber ?? "0") ?? 0
                        ownAddable = false;
                    }
                }else{
                    minimumTherapistRequired += Int(currentItem.services.first?.therapistNumber ?? "0") ?? 0
                    if (friendTotal<=Int(currentItem.services.first?.duration ?? "0") ?? 0){
                        friendTotal = Int(currentItem.services.first?.duration ?? "0") ?? 0
                        friendIndex = index;
                    }
                }
            }
            
            if (ownTotal<friendTotal){
                servicesArray.removeAll()
                servicesArray.append(self.appD.serviceCart[friendIndex].services.first?.serviceID ?? "")
            }
        }
        
        
        if servicesArray.count > 0{
            servicesArrayString = "["
            servicesArray.forEach { (serviceID) in
                servicesArrayString.append("%22\(serviceID)%22,")
            }
            servicesArrayString.removeLast()
            servicesArrayString.append("]")
        }
        
        chooseOtherDateTherapistLabel.textColor = ThemeColor.buttonColor
        chooseOtherDateTherapistLabel.isHidden = true
        
        
        for index in 0..<appD.serviceCart.count{
            if appD.serviceCart[index].services.first?.duration == "120"{
                is2HrsMassageExits = true
                break
            }
        }
        
        self.getAllDateTimeSlot()
    }
    
    @objc
    func backToSummaryAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc
    func confirmButtonAction(){
        
        var therapist = [EmployeeModel]()
        var finalTime = ""
        var finalTherapists = ""
        if self.selectedTherapistIndex == 0{
            let dateToSelect = self.allEmployeeTimeSlots[selectedDateIndex].date
            finalTime = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].time
            if currentReScheduleService != nil{
                let getAllTimes = getEndDateTimeTherapists(startTime: finalTime, StartDate: dateToSelect, duration: currentReScheduleService.duration , requiredTherapist:  "\(currentReScheduleService.therapistArr.components(separatedBy: ",").count)", serviceID: "")
                therapist = getAllTimes.4
                
                let finalTherapists1 = therapist.map({ (empData) -> String in
                    return empData.id
                })
                finalTherapists = "["
                finalTherapists1.forEach { (str) in
                    finalTherapists += str + ","
                }
                finalTherapists.removeLast()
                finalTherapists += "]"
                
            }
            
        }else{
            let dateToSelect = self.allEmployeeTimeSlots[selectedDateIndex].date
            if let currentData =  self.currentEmployeeTimeSlots.first(where: {$0.date == dateToSelect}){
                finalTime = currentData.timeSlots[selectedTimeIndex].time
                finalTherapists = "[\(currentData.timeSlots[selectedTimeIndex].therapists.first?.id ?? "")]"
            }
            
        }
        let data = [
            "therapist": finalTherapists,
            "date": self.allEmployeeTimeSlots[self.selectedDateIndex].date,
            "time": finalTime
            ] as [String : Any]
        delegate?.selectPopUpData(data: data)
        if cartViewController == nil{
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        setupCartData()
        self.dismiss(animated: true) {
            let destination = FinalBookingViewController(nibName: "FinalBookingViewController", bundle: nil)
            self.cartViewController.navigationController?.pushViewController(destination, animated: true)
        }
        
    }
    
    fileprivate func setupCartData(){
        self.tempEmpDataTime = self.allEmployeeTimeSlots
        for index in 0..<self.appD.serviceCart.count{
            var localServiceCart = [CartModel]()
            for ind in 0..<index{
                localServiceCart.append(self.appD.serviceCart[ind])
            }
            var currentService = self.appD.serviceCart[index]
            if currentService.isForMe{
                self.appD.serviceCart[index].cust_name = UserDefaults.standard.fetchData(forKey: .userFirstName)
                currentService.cust_name = UserDefaults.standard.fetchData(forKey: .userFirstName)
            }
            
            var startDate = ""
            var startTime = ""
            if let currentData = localServiceCart.last(where: {$0.cust_name == currentService.cust_name && $0.isForMe}){
                startDate = currentData.bookingDate
                startTime = currentData.bookingEndTime
            }else{
                if self.selectedTherapistIndex == 0{
                    startDate = self.allEmployeeTimeSlots[self.selectedDateIndex].date
                    startTime = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].time
                }else{
                    if let currentDateData = self.currentEmployeeTimeSlots.first(where: {$0.date == self.allEmployeeTimeSlots[self.selectedDateIndex].date}){
                        startDate = currentDateData.date
                        startTime = currentDateData.timeSlots[self.selectedTimeIndex].time
                    }
                }
                
            }
            
            let getAllTimes = getEndDateTimeTherapists(startTime: startTime, StartDate: startDate, duration: currentService.services.first?.duration ?? "0", requiredTherapist: currentService.services.first?.therapistNumber ?? "0", serviceID: "\(index)")
            self.appD.serviceCart[index].bookingTime = getAllTimes.1
            self.appD.serviceCart[index].bookingDate = getAllTimes.0
            self.appD.serviceCart[index].bookingDay = getAllTimes.3
            self.appD.serviceCart[index].bookingEndTime = getAllTimes.2
            self.appD.serviceCart[index].therapists = getAllTimes.4
        }
    }
    
    fileprivate func getEndDateTimeTherapists(startTime: String, StartDate: String, duration: String, requiredTherapist: String, serviceID: String) -> (String, String, String, String, [EmployeeModel]){
        
        var (finalStartDate, bookingDay, finalStartTime, endTime) : (String, String, String, String) = ("", "", "", "")
        let durationInt = Int(duration) ?? 0
        var closeTime = Date()
        var todaysDate = Date()
        if durationInt == 60{
            let formatter = DateFormatter()
            formatter.timeZone = .current
            formatter.dateFormat = "hh:mm a"
            closeTime = formatter.date(from: "07:00 PM") ?? Date()
            todaysDate = formatter.date(from: startTime) ?? Date()
        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            closeTime = formatter.date(from: "06:00 PM") ?? Date()
            todaysDate = formatter.date(from: startTime) ?? Date()
        }
        
        print("Today: \(todaysDate) || Close Date: \(closeTime)")
        
        if todaysDate<=closeTime{
            let df1 = DateFormatter()
            df1.dateFormat = "yyyy-MM-dd"
            let tempDt = df1.date(from: StartDate) ?? Date()
            df1.dateFormat = "EEEE"
            bookingDay = df1.string(from: tempDt)
            finalStartDate = StartDate
            finalStartTime = startTime
            let endTimeDt = Calendar.current.date(byAdding: .minute, value: durationInt, to: todaysDate) ?? Date()
            df1.dateFormat = "hh:mm a"
            endTime = df1.string(from: endTimeDt)
        }else{
            let df1 = DateFormatter()
            df1.dateFormat = "yyyy-MM-dd"
            let toDayDate = df1.date(from: StartDate) ?? Date()
            let nextDt = Calendar.current.date(byAdding: .day, value: 1, to: toDayDate) ?? Date()
            let nextStartDate = df1.string(from: nextDt)
            return getEndDateTimeTherapists(startTime: "10:00 AM", StartDate: nextStartDate, duration: duration, requiredTherapist: requiredTherapist, serviceID: serviceID)
        }
        
        let therapists = getMyTherapist(finalStartDate: finalStartDate, finalStartTime: finalStartTime, endTime: endTime, requiredTherapist: requiredTherapist, duration: durationInt, serviceID: serviceID)
        if (therapists.count >= (Int(requiredTherapist) ?? 0)){
            return (finalStartDate, finalStartTime, endTime, bookingDay, therapists)
        }else{
            
            if todaysDate<closeTime{
                let nextTime = self.getNextHour(StartTime: startTime, duration: durationInt)
                return getEndDateTimeTherapists(startTime: nextTime, StartDate: StartDate, duration: duration, requiredTherapist: requiredTherapist, serviceID: serviceID)
            }else{
                let nextStartDate = self.getNextDate(StartDate: StartDate)
                return getEndDateTimeTherapists(startTime: "10:00 AM", StartDate: nextStartDate, duration: duration, requiredTherapist: requiredTherapist, serviceID: serviceID)
            }
        }
        
    }
    
    fileprivate func getMyTherapist(finalStartDate: String, finalStartTime: String, endTime: String, requiredTherapist: String, duration: Int, serviceID: String) -> [EmployeeModel]{
        
        var allAvailableTherapists = [EmployeeModel]()
        let requiredTherapistInt = Int(requiredTherapist) ?? 0
        if self.selectedTherapistIndex == 0{
            let filteredData = self.tempEmpDataTime.filter({$0.date == finalStartDate && $0.timeSlots.contains(where: {$0.time == finalStartTime})})
            for index in 0..<filteredData.count{
                let availableTherapists = filteredData[index].timeSlots.filter({$0.time == finalStartTime})
                
                for ind1 in 0..<availableTherapists.count{
                    if availableTherapists[ind1].therapists.count >= requiredTherapistInt{
                        if requiredTherapistInt == 1{
                            allAvailableTherapists.append(availableTherapists[ind1].therapists[0])
                            break;
                        }else if requiredTherapistInt == 2{
                            allAvailableTherapists.append(availableTherapists[ind1].therapists[0])
                            allAvailableTherapists.append(availableTherapists[ind1].therapists[1])
                            break;
                        }
                    }
                }
            }
            
            // remove the selected therapists
            for index in 0..<self.tempEmpDataTime.count{
                if self.tempEmpDataTime[index].date == finalStartDate{
                    allAvailableTherapists.forEach { (allocatedTherapist) in
                        for inddd in 0..<self.tempEmpDataTime[index].timeSlots.count{
                            if self.tempEmpDataTime[index].timeSlots[inddd].time == finalStartTime{
                                self.tempEmpDataTime[index].timeSlots[inddd].therapists.removeAll(where: {$0.id == allocatedTherapist.id})
                            }
                        }
                    }
                }
            }
            return allAvailableTherapists
        }else{
            var filteredData = self.tempEmpDataTime.filter({$0.date == finalStartDate && $0.timeSlots.contains(where: {$0.time == finalStartTime})})
            if serviceID == "0"{
                if (self.currentEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists.count > 0){
                    filteredData = self.tempEmpDataTime.filter({$0.date == finalStartDate && $0.timeSlots.contains(where: {$0.time == finalStartTime && $0.therapists.contains(where: {$0.id == self.currentEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedTimeIndex].therapists.first?.id ?? ""})})})
                }
                
            }
            for index in 0..<filteredData.count{
                let availableTherapists = filteredData[index].timeSlots.filter({$0.time == finalStartTime})
                
                for ind1 in 0..<availableTherapists.count{
                    if availableTherapists[ind1].therapists.count >= requiredTherapistInt{
                        if requiredTherapistInt == 1{
//                            allAvailableTherapists.append(availableTherapists[ind1].therapists[0])
                            if let firstTherapist = availableTherapists[ind1].therapists.first(where: { (empDat) -> Bool in
                                return empDat.id == self.currentEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedDateIndex].therapists.first?.id
                            }){
                                allAvailableTherapists.append(firstTherapist)
                            }
                            
                            break;
                        }else if requiredTherapistInt == 2{
                            if let firstTherapist = availableTherapists[ind1].therapists.first(where: { (empDat) -> Bool in
                                return empDat.id == self.currentEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedDateIndex].therapists.first?.id
                            }){
                                allAvailableTherapists.append(firstTherapist)
                            }
                            if let secondTherapist = availableTherapists[ind1].therapists.first(where: { (empDat) -> Bool in
                                return empDat.id != self.currentEmployeeTimeSlots[self.selectedDateIndex].timeSlots[self.selectedDateIndex].therapists.first?.id
                            }){
                                allAvailableTherapists.append(secondTherapist)
                            }
//                            allAvailableTherapists.append(availableTherapists[ind1].therapists[0])
//                            allAvailableTherapists.append(availableTherapists[ind1].therapists[1])
                            break;
                        }
                    }
                }
            }
            
            // remove the selected therapists
            for index in 0..<self.tempEmpDataTime.count{
                if self.tempEmpDataTime[index].date == finalStartDate{
                    allAvailableTherapists.forEach { (allocatedTherapist) in
                        for inddd in 0..<self.tempEmpDataTime[index].timeSlots.count{
                            if self.tempEmpDataTime[index].timeSlots[inddd].time == finalStartTime{
                                self.tempEmpDataTime[index].timeSlots[inddd].therapists.removeAll(where: {$0.id == allocatedTherapist.id})
                            }
                        }
                    }
                }
            }
            return allAvailableTherapists
        }

    }
    
    private func getNextDate(StartDate: String) -> String{
        let df1 = DateFormatter()
        df1.dateFormat = "yyyy-MM-dd"
        let toDayDate = df1.date(from: StartDate) ?? Date()
        let nextDt = Calendar.current.date(byAdding: .day, value: 1, to: toDayDate) ?? Date()
        let nextStartDate = df1.string(from: nextDt)
        return nextStartDate
    }
    
    private func getNextHour(StartTime: String, duration: Int) -> String{
        let df1 = DateFormatter()
        df1.dateFormat = "hh:mm a"
        let toDayDate = df1.date(from: StartTime) ?? Date()
        let nextDt = Calendar.current.date(byAdding: .minute, value: duration, to: toDayDate) ?? Date()
        let nextStartDate = df1.string(from: nextDt)
        return nextStartDate
    }
    
    
    func getAllDateTimeSlot(){
        
        self.activityIndicator.startAnimating()
        let reuestURL = "\(Config.mainUrl)Data/GetAllTimeSlot?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&vn_id=4&services=\(servicesArrayString)&theraview=Y&skip_blankdate=1"
        print("Request URL: \(reuestURL)")
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                self.allEmployeeTimeSlots = [EmployeeDateTimeSlot]()
                if (resposeJSON["status"] as? Bool ?? false){
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<result.count{
                        let currentResult = result[index] as? NSDictionary ?? NSDictionary()
                        let date = currentResult["date"] as? String ?? "2001-01-01"
                        let dateToShow = self.getFormattedDate(strDate: date, currentFomat: "yyyy-MM-dd", expectedFromat: "EE, MMM dd")
                        let isBooking = currentResult["is_booking"] as? String ?? "0"
                        let slots = currentResult["slots"] as? NSArray ?? NSArray()
                        var timeSlots = [TimeSlot]()
                        for index1 in 0..<slots.count{
                            let currentSlot = slots[index1] as? NSDictionary ?? NSDictionary()
                            let time = currentSlot["time"] as? String ?? "00:00 AM"
                            let theras = currentSlot["theras"] as? NSArray ?? NSArray()
                            var allTheapists = [EmployeeModel]()
                            for index2 in 0..<theras.count{
                                let currentTherapist = theras[index2] as? NSDictionary ?? NSDictionary()
                                let theraID = currentTherapist["id"] as? String ?? ""
                                if let therapist = self.appD.employeeDetails.first(where: {$0.id == theraID}){
                                    allTheapists.append(therapist)
                                }
                            }
                            if !(time == "07:00 PM" && self.is2HrsMassageExits){
                                if allTheapists.count >= self.minimumTherapistRequired{
                                    timeSlots.append(TimeSlot(time: time, therapists: allTheapists))
                                }
                            }
                        }
                        self.allEmployeeTimeSlots.append(EmployeeDateTimeSlot(date: date, dateToShow: dateToShow, isBooking: isBooking, timeSlots: timeSlots))
                        self.appointmentTimeCollectionView.reloadData()
                        self.appointmentDateCollectionView.reloadData()
                        self.therapistCollectionView.reloadData()
                    }
                    if self.allEmployeeTimeSlots.first?.timeSlots.count ?? 0 == 0{
                        self.disable(sender: self.confirmButton)
                    }else{
                        self.enable(sender: self.confirmButton)
                    }
                }

                
            }
        
    }
    
    func getDateTimeSlotWithId(id:String){
        
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetTimeSlotV2?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&therapist_id=\(id)&services=\(servicesArrayString)&skip_blankdate=1"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                if (resposeJSON["status"] as? Bool ?? false){
                    
                    let employee = self.appD.employeeDetails.first(where: {$0.id == id}) ?? EmployeeModel()
                    self.currentEmployeeTimeSlots.removeAll()
                    let results = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<results.count{
                        let currentResult = results[index] as? NSDictionary ?? NSDictionary()
                        let date = currentResult["date"] as? String ?? "2001-01-01"
                        let dateToShow = self.getFormattedDate(strDate: date, currentFomat: "yyyy-MM-dd", expectedFromat: "EE, MMM dd")
                        let is_booking = currentResult["is_booking"] as? String ?? "0"
                        
                        let timeSlots = currentResult["slots"] as? [String] ?? [String]()
                        var finalTimeSlots = [TimeSlot]()
                        timeSlots.forEach { (slot) in
                            if !(slot == "07:00 PM" && self.is2HrsMassageExits){
                                var availableSlots = [TimeSlot]()
                                for index in 0..<self.allEmployeeTimeSlots.count{
                                    if self.allEmployeeTimeSlots[index].date == self.allEmployeeTimeSlots[self.selectedDateIndex].date{
                                        for ind1 in 0..<self.allEmployeeTimeSlots[index].timeSlots.count{
                                            if self.allEmployeeTimeSlots[index].timeSlots[ind1].therapists.count >= self.minimumTherapistRequired{
                                                availableSlots.append(self.allEmployeeTimeSlots[index].timeSlots[ind1])
                                            }
                                        }
                                        
                                    }
                                }
                                if availableSlots.contains(where: { (timeSlot) -> Bool in
                                    return timeSlot.time == slot
                                }){
                                    finalTimeSlots.append(TimeSlot(time: slot, therapists: [employee]))
                                }
                                
                            }
                        }
                        
                        let currentEmployeeTimeSlot = EmployeeDateTimeSlot(date: date, dateToShow: dateToShow, isBooking: is_booking, timeSlots: finalTimeSlots)
                        self.currentEmployeeTimeSlots.append(currentEmployeeTimeSlot)
                    }
                  
                    if self.allEmployeeTimeSlots.count <= self.selectedDateIndex{
                        return
                    }
                    if let currentDateData = self.currentEmployeeTimeSlots.first(where: {$0.date == self.allEmployeeTimeSlots[self.selectedDateIndex].date}){
                        
                        if currentDateData.timeSlots.count == 0{
                            self.disable(sender: self.confirmButton)
                        }else{
                            self.enable(sender: self.confirmButton)
                        }
                    }
                    
                    
                    self.appointmentTimeCollectionView.reloadData()
                }
                
                
            }
        
    }
    
    func disable(sender: UIButton){
        sender.isEnabled = false
        sender.alpha = 0.5
        chooseOtherDateTherapistLabel.isHidden = false
    }
    
    func enable(sender: UIButton){
        sender.isEnabled = true
        sender.alpha = 1.0
        chooseOtherDateTherapistLabel.isHidden = true
    }
    
    func getFormattedDate(strDate: String , currentFomat:String, expectedFromat: String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = currentFomat
        dateFormatterGet.timeZone = .current
        let date : Date = dateFormatterGet.date(from: strDate) ?? Date()
        
        dateFormatterGet.dateFormat = expectedFromat
        return dateFormatterGet.string(from: date)
    }

}

extension SelectDateTimePopUp: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == therapistCollectionView{
            return self.appD.employeeDetails.count + 1
        }else if collectionView == appointmentTimeCollectionView{
            if self.selectedTherapistIndex == 0{
                return self.allEmployeeTimeSlots.count == 0 ?  0 : self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count
            }else{
                if let currentDateData = self.currentEmployeeTimeSlots.first(where: {$0.date == self.allEmployeeTimeSlots[self.selectedDateIndex].date}){
                    return currentDateData.timeSlots.count
                }else{
                    return 0
                }
            }
            
        }else{
            return self.allEmployeeTimeSlots.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTimeCollectionViewCell", for: indexPath) as! DateTimeCollectionViewCell
        if collectionView == therapistCollectionView{
            cell.isChoosen = indexPath.item == selectedTherapistIndex
            if indexPath.item == 0{
                cell.dataLabel.text = "All Therapists"
            }else{
                cell.dataLabel.text = "\(self.appD.employeeDetails[indexPath.item - 1].user_name) \(self.appD.employeeDetails[indexPath.item - 1].user_lname)"
            }
            
        }else if collectionView == appointmentTimeCollectionView{
            if self.selectedTherapistIndex == 0{
                cell.dataLabel.text = self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots[indexPath.item].time
            }else{
                
                if let currentDateData = self.currentEmployeeTimeSlots.first(where: {$0.date == self.allEmployeeTimeSlots[self.selectedDateIndex].date}){
                    cell.dataLabel.text = currentDateData.timeSlots[indexPath.item].time
                }else{
                    cell.dataLabel.text = ""
                }
            }
            cell.isChoosen = indexPath.item == selectedTimeIndex
        }
        else{
            cell.dataLabel.text = self.allEmployeeTimeSlots[indexPath.item].dateToShow
            cell.isChoosen = indexPath.item == selectedDateIndex
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == appointmentTimeCollectionView{
            return CGSize(width: 80, height: 40)
        }
        return CGSize(width: 120, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedTimeIndex = 0
        if collectionView == appointmentTimeCollectionView{
            self.selectedTimeIndex = indexPath.item
            
        }else if collectionView == appointmentDateCollectionView{
            self.selectedDateIndex = indexPath.item
            if self.selectedTherapistIndex == 0{
                let timeSlots = self.allEmployeeTimeSlots.count == 0 ?  0 : self.allEmployeeTimeSlots[self.selectedDateIndex].timeSlots.count
                if timeSlots == 0{
                    self.disable(sender: self.confirmButton)
                }else{
                    self.enable(sender: self.confirmButton)
                }
                self.appointmentTimeCollectionView.reloadData()
                self.appointmentDateCollectionView.reloadData()
            }else{
                self.getDateTimeSlotWithId(id: self.appD.employeeDetails[self.selectedTherapistIndex - 1].id)
            }
            
        }else if collectionView == therapistCollectionView{
            
            self.selectedTherapistIndex = indexPath.item
            if indexPath.item == 0{
                self.getAllDateTimeSlot()
            }else{
                self.getDateTimeSlotWithId(id: self.appD.employeeDetails[indexPath.item - 1].id)
            }
        }
        collectionView.reloadData()
    }
}

protocol SelectDateTimePopUpDelegate {
    func selectPopUpData(data: Dictionary<String, Any>)
}


struct DateTime{
    var date: Date = Date()
    var dateString: String = ""
}
