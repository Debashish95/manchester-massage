//
//  ProductDetailsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 28/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ProductDetailsViewController: UIViewController {

    var activityIndicator: NVActivityIndicatorView!
    var cartButton: UIButton!
    var wishButton: UIButton!
    
    @IBOutlet weak var wishButton1: UIButton!
    
    var rightBarButton: ENMBadgedBarButtonItem!
    var rightBarButton2: ENMBadgedBarButtonItem!
    
    var data = ProductListModel()
    var pageTitle = ""
    var bannerArray = [String]()
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productDescriptionTextView: UITextView!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var addToCartStepper: MMStepper!
    
    @IBOutlet weak var bannerView: AACarousel!
    
    var productID = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "", isBackButtonRequired: true, isRightMenuRequired: true)
        
        cartButton = UIButton(type: .custom)
        cartButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        cartButton.addTarget(self, action: #selector(self.productCartButtonAction), for: .touchUpInside)
        cartButton.setLeftImage(image: FAType.FAShoppingCart, color: .black, state: .normal)
        rightBarButton = ENMBadgedBarButtonItem(customView: cartButton, value: "0")
        rightBarButton.badgeBackgroundColor = .red
        rightBarButton.badgeTextColor = .white
        rightBarButton.badgeValue = self.appD.itemsInCart
        
        wishButton = UIButton(type: .custom)
        wishButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        wishButton.addTarget(self, action: #selector(self.wishListButtonAction), for: .touchUpInside)
        wishButton.setLeftImage(image: FAType.FAHeart, color: .black, state: .normal)
        rightBarButton2 = ENMBadgedBarButtonItem(customView: wishButton, value: "0")
        rightBarButton2.badgeBackgroundColor = .red
        rightBarButton2.badgeTextColor = .white
        rightBarButton2.badgeValue = ""
//        self.navigationItem.rightBarButtonItems = [rightBarButton, rightBarButton2]
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeValue), name: NSNotification.Name("changePhysicalCart"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadProductData), name: NSNotification.Name("getProductDetails"), object: nil)
        
        self.apiCallForProductDetails(self.productID)
        setupData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateData()
    }
    fileprivate func setupData(){
        
        
        bannerArray = data.galleyImage.map({ (img) -> String in
            return img.media_url
        })
        bannerView.delegate = self
        bannerView.setCarouselData(paths: bannerArray, describedTitle: [""], isAutoScroll: false, timer: 5.0, defaultImage: "")
        
        //optional method
        bannerView.setCarouselOpaque(layer: false, describedTitle: true, pageIndicator: false)
        bannerView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: UIColor.white, describedTitleColor: UIColor.white, layerColor: UIColor.clear)
        productNameLabel.text = data.p_name
        productPriceLabel.attributedText = makeAttributedText(
            firstText: "\(CURRENCY_SYMBOL)\(data.base_price.formattedString())",
            secondText: "\(CURRENCY_SYMBOL)\(data.discount_price.formattedString())")
        productDescriptionTextView.text = data.big_description
        
        wishButton1.backgroundColor = .white
        wishButton1.setFAIcon(icon: .FAHeartO, iconSize: 25, forState: .normal)
        wishButton1.setFAIcon(icon: .FAHeart, iconSize: 25, forState: .selected)
        wishButton1.setTitleColor(ThemeColor.buttonColor, for: .normal)
        self.view.bringSubviewToFront(wishButton1)
        
        self.wishButton1.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.wishButton1.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.wishButton1.layer.shadowOpacity = 1.0
        self.wishButton1.layer.shadowRadius = 0.0
        self.wishButton1.layer.masksToBounds = false
        self.wishButton1.layer.cornerRadius = self.wishButton1.bounds.height / 2
        self.wishButton1.clipsToBounds = true
        self.wishButton1.isSelected = data.isInWishList
        self.wishButton1.addTarget(self, action: #selector(addRemoveWishList(sender:)), for: .touchUpInside)
        
        addToCartButton.backgroundColor = ThemeColor.buttonColor
        addToCartButton.setTitle("Add To Cart", for: .normal)
        addToCartButton.setTitle("Go To Cart", for: .selected)
        addToCartButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        addToCartButton.setTitleColor(.white, for: [.normal, .selected])
        self.addToCartButton.clipsToBounds = true
        
        self.addToCartStepper.layer.borderWidth = 1.0
        self.addToCartStepper.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.addToCartStepper.layer.cornerRadius = self.addToCartStepper.bounds.height / 2
        self.addToCartStepper.clipsToBounds = true
        self.addToCartStepper.borderWidth = 1.0
        self.addToCartStepper.borderColor = ThemeColor.buttonColor
        self.updateData()
        
        addToCartStepper.leftButton.addTarget(self, action: #selector(removeFromCartButtonAction(sender:)), for: .touchUpInside)
        addToCartStepper.rightButton.addTarget(self, action: #selector(addToCartButtonAction(sender:)), for: .touchUpInside)
        self.addToCartButton.addTarget(self, action: #selector(self.addToCartButtonAction(sender:)), for: .touchUpInside)
    }
    
    @objc func updateBadgeValue(){
        rightBarButton.badgeValue = self.appD.itemsInCart
        rightBarButton2.badgeValue = ""
    }
    
    fileprivate func updateData(){
        self.addToCartStepper.isHidden = !data.isInCart
        self.addToCartButton.isSelected = data.isInCart
        self.addToCartStepper.value = Double(data.cartQuantity) ?? 0
    }

    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makeAttributedText(firstText: String, secondText: String)->NSMutableAttributedString{
        let firstAttributedText = NSMutableAttributedString(string: firstText)
        firstAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),
                NSAttributedString.Key.strikethroughStyle: 2,
                NSAttributedString.Key.strikethroughColor: UIColor.gray,
                NSAttributedString.Key.foregroundColor: UIColor.gray
            ],
            range: NSRange(location: 0, length: firstText.count))
        
        let secondAttributedText = NSMutableAttributedString(string: secondText)
        secondAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: .semibold),
                NSAttributedString.Key.strikethroughColor: UIColor.black,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ],
            range: NSRange(location: 0, length: secondText.count))
        
        let discountPercent: Double = ((Double(data.base_price) ?? 0) - (Double(data.discount_price) ?? 0)) / (Double(data.base_price) ?? 0) * 100.0
        var discountPercentInt = 0
        if !discountPercent.isNaN{
            discountPercentInt = Int(discountPercent)
        }
        let thirdAttributedText = NSMutableAttributedString(string: " (\(discountPercentInt)% Off)")
        thirdAttributedText.addAttributes(
            [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .semibold),
                NSAttributedString.Key.foregroundColor: ThemeColor.buttonColor
            ],
            range: NSRange(location: 0, length: (" (\(discountPercentInt)% Off)").count))
        
        secondAttributedText.append(NSAttributedString(string: " "))
        secondAttributedText.append(firstAttributedText)
        secondAttributedText.append(thirdAttributedText)
        return secondAttributedText
    }

    fileprivate func apiCallForProductDetails(_ productId: String) {
        let reuestURL = Config.mainUrl + "Data/GetProducts?key=\(Config.key)&salt=\(Config.salt)&prod_id=\(productId)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let currentProduct = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    let prod_id = currentProduct["prod_id"] as? String ?? ""
                    let p_name = currentProduct["p_name"] as? String ?? ""
                    let is_featured = currentProduct["is_featured"] as? String ?? ""
                    let cat_id = currentProduct["cat_id"] as? String ?? ""
                    let brand_id = currentProduct["brand_id"] as? String ?? ""
                    let p_url = currentProduct["p_url"] as? String ?? ""
                    let base_price = currentProduct["base_price"] as? String ?? "0.0"
                    let discount_price = currentProduct["discount_price"] as? String ?? ""
                    let thumb = currentProduct["thumb"] as? String ?? ""
                    let big_description = currentProduct["big_description"] as? String ?? ""
                    
                    let galleyImage = currentProduct["gal_img"] as? NSArray ?? NSArray()
                    var galleryImages = [GalleryImage]()
                    for index2 in 0..<galleyImage.count{
                        let currentGallery = galleyImage[index2] as? NSDictionary ?? NSDictionary()
                        let m_id = currentGallery["m_id"] as? String ?? ""
                        let typ_id = currentGallery["typ_id"] as? String ?? ""
                        let media_url = currentGallery["media_url"] as? String ?? ""
                        let galleyObj = GalleryImage(m_id: m_id, typ_id: typ_id, media_url: media_url)
                        galleryImages.append(galleyObj)
                        
                    }
                    var finalData = ProductListModel(
                        prod_id: prod_id, p_name: p_name, is_featured: is_featured, cat_id: cat_id, brand_id: brand_id, p_url: p_url, base_price: base_price, discount_price: discount_price, thumb: thumb, big_description: big_description, galleyImage: galleryImages
                    )
                    finalData.isInWishList =  currentProduct["iswish"] as? Bool ?? false
                    finalData.isInCart = currentProduct["iscart"] as? Bool ?? false
                    finalData.cartQuantity = currentProduct["cart_count"] as? String ?? "0"
                    self.data = finalData
                    self.setupData()
                }
            }
    }
    
    @objc func reloadProductData(){
        let productId = data.prod_id
        apiCallForProductDetails(productId)
    }
    
    @objc func addRemoveWishList(sender: UIButton){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
            return
        }
        let product_id = self.data.prod_id
        var apiName = "addtowishlist"
        if sender.isSelected{
            apiName = "removewishlist"
        }
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId).toData,
            "prod_id": product_id.toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + apiName , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let status = resposeJSON["status"] as? Bool ?? false
                    let msg = resposeJSON["msg"] as? String ?? ""
                    if status{
                        self.data.isInWishList = !self.data.isInWishList
                        self.wishButton1.isSelected = self.data.isInWishList
                        NotificationCenter.default.post(name: NSNotification.Name("wishListChanged"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name("getUpdatedWishList"), object: nil)
                        
                        self.tabBarController?.view.makeToast(message: msg)
                        
                    }else{
                        self.tabBarController?.view.makeToast(message: msg)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
        
    }
    
    @objc func addToCartButtonAction(sender: UIButton){
        if !sender.isSelected{
            addRemoveToCartAction(apiName: "addtocart", product_id: self.data.prod_id)
        }else{
            productCartButtonAction()
        }
    }
    @objc func removeFromCartButtonAction(sender: UIButton){
        addRemoveToCartAction(apiName: "removecart", product_id: self.data.prod_id)
    }
    
    func addRemoveToCartAction(apiName: String, product_id: String, quantity: String = "1"){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
            return
        }
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId).toData,
            "prod_count": quantity.toData,
            "prod_id": product_id.toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + apiName , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let status = resposeJSON["status"] as? Bool ?? false
                    let msg = resposeJSON["msg"] as? String ?? ""
                    if status{
                        
                        let result = resposeJSON["result"] as? NSArray ?? NSArray()
                        for index in 0..<result.count{
                            let resultObject = result[index] as? NSDictionary ?? NSDictionary()
                            let product_id = resultObject["prod_id"] as? String ?? ""
                            if product_id == self.data.prod_id{
                                self.data.isInCart = true
                                self.data.cartQuantity = resultObject["qnty"] as? String ?? "0"
                                break
                            }else{
                                self.data.isInCart = false
                            }
                        }
                        self.updateData()
                        self.appD.getCartList()
                        NotificationCenter.default.post(name: NSNotification.Name("getProductList"), object: nil)
                        if apiName == "addtocart"{
                            self.tabBarController?.view.makeToast(message: "Product has been added to cart successfully.")
                        }else{
                            self.tabBarController?.view.makeToast(message: "Product has been removed from cart successfully.")
                        }
                       
                    }else{
                        self.tabBarController?.view.makeToast(message: msg)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})

    }

}

extension ProductDetailsViewController: AACarouselDelegate{
    func downloadImages(_ url: String, _ index: Int) {
        let imageView = UIImageView()

        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
            switch result {
            case .success(let value):
                self.bannerView.images[index] = value.image
            case .failure(let error):
                print("Job failed0: \(error.localizedDescription)")
            }
            imageView.contentMode = .scaleAspectFill
        }
    }
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
            imageView.contentMode = .scaleAspectFill
        }
    }
}
