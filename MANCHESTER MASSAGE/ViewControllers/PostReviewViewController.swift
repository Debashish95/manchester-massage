//
//  PostReviewViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 18/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import Cosmos

class PostReviewViewController: UIViewController {
    
    @IBOutlet weak var serviceRating: CosmosView!
    @IBOutlet weak var staffRating: CosmosView!
    @IBOutlet weak var cleanRating: CosmosView!
    @IBOutlet weak var overallRating: CosmosView!
    
    @IBOutlet weak var addReviewLabel: UILabel!
    @IBOutlet weak var reviewText: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    var reviewData: ReviewModel?
    
    var bookingID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Post Review", isBackButtonRequired: true)
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        setupRatingView(sender: serviceRating)
        setupRatingView(sender: staffRating)
        setupRatingView(sender: cleanRating)
        setupRatingView(sender: overallRating)
        
        submitButton.setTitle("Submit", for: .normal)
        submitButton.addTarget(self, action: #selector(self.submitButtonAction), for: .touchUpInside)
        
        submitButton.backgroundColor = ThemeColor.buttonColor
        submitButton.layer.cornerRadius = submitButton.bounds.height / 2
        submitButton.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
        if let data = reviewData{
            serviceRating.isUserInteractionEnabled = false
            staffRating.isUserInteractionEnabled = false
            cleanRating.isUserInteractionEnabled = false
            overallRating.isUserInteractionEnabled = false
            reviewText.isUserInteractionEnabled = false
            submitButton.isHidden = true
            reviewText.text = data.review
            serviceRating.rating = Double(data.service_review) ?? 0
            staffRating.rating = Double(data.staff_review) ?? 0
            cleanRating.rating = Double(data.cleanliness_review) ?? 0
            overallRating.rating = Double(data.overall_review) ?? 0
            addReviewLabel.text = "Review"
            self.title = "My Ratings & Review"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        reviewText.layer.borderWidth = 1
        reviewText.layer.cornerRadius = 5
        reviewText.layer.borderColor = ThemeColor.buttonColor.cgColor
        reviewText.clipsToBounds = true
    }
    
    func setupRatingView(sender: CosmosView){
        sender.rating = 0
        sender.settings.fillMode = .precise
    }
    
    @objc func dismissKeyBoard(){
        self.view.endEditing(true)
    }
    
    @objc func submitButtonAction(){
        self.view.endEditing(true)
        self.activityIndicator.startAnimating()
        let bodyParams = [
            "key": Config.key,
            "salt": Config.salt,
            "bookingid": bookingID,
            "service_review": "\(serviceRating.rating)",
            "staff_review": "\(staffRating.rating)",
            "cleanliness_review": "\(cleanRating.rating)",
            "overall_review": "\(overallRating.rating)",
            "review_text": "\(reviewText.text ?? "")"
        ] as [String:String]
        
        Alamofire.request( Config.apiEndPoint + "reviewentry", method: .post, parameters: bodyParams, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                print(response)
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                
                let status = resposeJSON["status"] as? Int ?? 0
                let message = resposeJSON["msg"] as? String ?? ""
                
                if status == 0{
                    let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    }
                    alert.addAction(yesAction)
                    self.present(alert, animated: false, completion: nil)
                }
                else{
                    let alert = UIAlertController(title: "Success!", message: "Review has been posted successfully.", preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideReviewButton"), object: nil, userInfo: bodyParams)
                        self.navigationController?.popViewController(animated: true)
                    }
                    alert.addAction(yesAction)
                    self.present(alert, animated: false, completion: nil)
                }
            }
    }
    
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
