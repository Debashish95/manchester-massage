//
//  FinalBookingViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/09/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class FinalBookingViewController: UIViewController {

    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmBookingButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: NVActivityIndicatorView!
    var totalPrice = ""
    var cardPaymentButton: UIButton!
    var payDeskButton: UIButton!
    var textView: UITextView!{
        didSet{
            textView.layer.borderWidth = 1
            textView.layer.borderColor = ThemeColor.buttonColor.cgColor
            textView.layer.cornerRadius = 3
            textView.tintColor = ThemeColor.buttonColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "Book Massage", isBackButtonRequired: true, isRightMenuRequired: false)
        tableView.register(UINib(nibName: "CartTableViewCellFinal", bundle: nil), forCellReuseIdentifier: "CartTableViewCellFinal")
        tableView.register(UINib(nibName: "CartLastTableViewCellFinal", bundle: nil), forCellReuseIdentifier: "CartLastTableViewCellFinal")
        tableView.dataSource = self
        tableView.delegate = self
        
        bottomHeight.constant = (self.tabBarController?.tabBar.bounds.height ?? 0) + 5
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.appD.serviceCart.count == 0{
            confirmBookingButton.isHidden = true
            let alert = UIAlertController(title: "Alert!", message: "No services found in cart.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                self.appD.serviceCart.removeAll()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(yesAction)
            self.present(alert, animated: false, completion: nil)
        }
        
        updateCartPrice()
    }
    override func viewDidAppear(_ animated: Bool) {
        confirmBookingButton.backgroundColor = ThemeColor.buttonColor
        
        confirmBookingButton.layer.cornerRadius = confirmBookingButton.frame.height / 2
        confirmBookingButton.clipsToBounds = true
        confirmBookingButton.addTarget(self, action: #selector(confirmBookingButtonAction), for: .touchUpInside)
    }
    
    @objc func confirmBookingButtonAction(){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
        }else{
            if self.cardPaymentButton.isSelected{
                let destination = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
//                PaymentViewController(nibName: "PaymentViewController", bundle: nil)
                destination.notes = self.textView.text
                self.navigationController?.pushViewController(destination, animated: true)
            }else if self.payDeskButton.isSelected{
                finalBooking()
            }
        }
        
        
    }
    fileprivate func finalBooking(){
        
        var totalAmount = Float(0)
        var finalAmount = Float(0)
        var currentServiceCart = [[String: Any]()]
        currentServiceCart.removeAll()
        
        for index in 0..<appD.serviceCart.count{
            if (!appD.serviceCart[index].isForMe && (appD.serviceCart[index].cust_name.trimmingCharacters(in: .whitespacesAndNewlines)) == ""){
                self.tabBarController?.view.makeToast(message: "Please enter customer details")
                return
            }else if appD.serviceCart[index].isForMe{
                appD.serviceCart[index].cust_phone = UserDefaults.standard.fetchData(forKey: .userPhone)
                appD.serviceCart[index].cust_name = UserDefaults.standard.fetchData(forKey: .userFirstName) + " " + UserDefaults.standard.fetchData(forKey: .userLastName)
                appD.serviceCart[index].cust_email = UserDefaults.standard.fetchData(forKey: .userEmail)
            }
            let therapistStringArr = appD.serviceCart[index].therapists.map { (therapist) -> String in
                return therapist.id
            }
            
            let currentServiceCartData = ["id": appD.serviceCart[index].services[0].serviceID,
                                          "name": appD.serviceCart[index].services[0].name,
                                          "qty": appD.serviceCart[index].qty,
                                          "price": appD.serviceCart[index].services[0].offerPrice.formattedString(),
                                          "subtotal": String((Float(appD.serviceCart[index].services[0].offerPrice) ?? 0) * (Float(appD.serviceCart[index].qty) ?? 0)).formattedString(),
                                          "therapist": "[\(therapistStringArr.joined(separator: ","))]",
                                          "cust_name": appD.serviceCart[index].cust_name,
                                          "cust_email": appD.serviceCart[index].cust_email,
                                          "book_dt": appD.serviceCart[index].bookingDate,
                                          "book_tym": appD.serviceCart[index].bookingTime,
                                          "book_end_tym": appD.serviceCart[index].bookingEndTime,
                                          "notes": appD.serviceCart[index].notes
                ] as [String : Any]
            currentServiceCart.append(currentServiceCartData)
            
            totalAmount += ((Float(appD.serviceCart[index].services[0].offerPrice) ?? 0) * (Float(appD.serviceCart[index].qty) ?? 0))
            finalAmount += ((Float(appD.serviceCart[index].services[0].offerPrice) ?? 0) * (Float(appD.serviceCart[index].qty) ?? 0))
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
        let jsonData = try? JSONSerialization.data(withJSONObject: currentServiceCart, options: [])
        
        var jsonDataString = String(bytes: jsonData!, encoding: .utf8)
        jsonDataString = jsonDataString?.replacingOccurrences(of: "\\", with: "")
        
        let bodyParams = [
            "key": Config.key,
            "salt": Config.salt,
            "cardType": "paydesk",
            "vendor_id": appD.serviceCart[0].vendor_id,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId),
            "fname": UserDefaults.standard.fetchData(forKey: .userFirstName),
            "lname": UserDefaults.standard.fetchData(forKey: .userLastName),
            "email": UserDefaults.standard.fetchData(forKey: .userEmail),
            "phone": UserDefaults.standard.fetchData(forKey: .userPhone),
            "bookdt": appD.serviceCart[0].bookingDate,
            "therapist_id":appD.serviceCart[0].therapists[0].id,
            "timeslot": appD.serviceCart[0].bookingTime,
            "tot_amount": String(totalAmount),
            "final_amount": String(finalAmount),
            "services": jsonDataString ?? "",
            "notes": self.textView.text
        ] as [String: String]
        
        self.activityIndicator.startAnimating()
        
       Alamofire.request( Config.apiEndPoint + "FinalBooking", method: .post, parameters: bodyParams, encoding: JSONEncoding.default)
        .responseJSON { response in
            
            print(response)
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            
            let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
            let status = result["status"] as? String ?? ""
            let message = result["type"] as? String ?? ""
            let txnID = result["orderid"] as? String ?? ""
            
            if status == "Ok"{
                let alert = UIAlertController(title: "Success!", message: "Booking successfully. \n Transaction ID: #\(txnID)", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
                    self.appD.serviceCart.removeAll()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
//                    let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                    destination.isComingFromFinalBooking = true
//                    let navigationController = UINavigationController(rootViewController: destination)
//                    self.appD.window?.rootViewController = navigationController
//                    self.appD.window?.makeKeyAndVisible()
                    
                    let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
                    self.appD.window?.rootViewController = destination
                    self.appD.window?.makeKeyAndVisible()
                }
                alert.addAction(yesAction)
                self.present(alert, animated: false, completion: nil)
            }
            else{
                self.tabBarController?.view.makeToast(message: message)
            }
        }
    }
    func updateCartPrice(){
        
        self.tableView.isHidden = (self.appD.serviceCart.count == 0)
        self.confirmBookingButton.isHidden =  self.tableView.isHidden
        var totalPriceFloat = Float(0.0)
        for index in 0..<self.appD.serviceCart.count{
            let priceString = self.appD.serviceCart[index].final_amount
            let price = Float(priceString) ?? 0.0
            totalPriceFloat += price
        }
        totalPrice = "Grand Total: \(CURRENCY_SYMBOL)\(String(totalPriceFloat).formattedString())"
    }
    
    @objc
    func changeRadioButtons(sender: UIButton){
        if sender == cardPaymentButton{
            payDeskButton.isSelected = false
            cardPaymentButton.isSelected = true
        }else{
            cardPaymentButton.isSelected = false
            payDeskButton.isSelected = true
        }
    }
    
    @objc func deleteButtonAction(sender: UIButton){
        let alert = UIAlertController(title: "Alert!", message: "Are you sure you want to delete the service?", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alertAction) in
            self.appD.serviceCart.remove(at: sender.tag)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
            
            self.tableView.reloadData()
            self.updateCartPrice()
            if self.appD.serviceCart.count == 0{
                self.navigationController?.popViewController(animated: true)
            }
           
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
    
    @objc func editButtonAction(sender: UIButton){
        let destination = SelectDateTimePopUpFinal(nibName: "SelectDateTimePopUpFinal", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        destination.delegate = self
        destination.startingIndex = sender.tag
        destination.currentCartService = self.appD.serviceCart[sender.tag]
        self.present(destination, animated: true, completion: nil)
    }
    
    
    @objc func notesButtonAction(sender: UIButton){
        let destination = NotesPopUp(nibName: "NotesPopUp", bundle: nil)
        destination.notesHeaderText = "Notes for \(self.appD.serviceCart[sender.tag].services.first?.name ?? "service")"
        destination.notesBodyText = self.appD.serviceCart[sender.tag].notes
        destination.index = sender.tag
        destination.delegate = self
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: false, completion: nil)
    }
    
    
    @objc func addMoreServices(){
//        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        let navigationController = UINavigationController(rootViewController: destination)
//        self.appD.window?.rootViewController = navigationController
//        self.appD.window?.makeKeyAndVisible()
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
        self.appD.window?.rootViewController = destination
        self.appD.window?.makeKeyAndVisible()
    }

}

extension FinalBookingViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
            return 2
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return section == 1 ? ((self.appD.serviceCart.count == 0) ? 0 : 1) : (self.appD.serviceCart.count)
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCellFinal", for: indexPath) as! CartTableViewCellFinal
                cell.setData(cartData: appD.serviceCart[indexPath.row])
                cell.deleteButton.tag = indexPath.row
                cell.deleteButton.addTarget(self, action: #selector(deleteButtonAction(sender:)), for: .touchUpInside)
                
                cell.editButton.tag = indexPath.row
                cell.editButton.addTarget(self, action: #selector(editButtonAction(sender:)), for: .touchUpInside)
                cell.notesButton.tag = indexPath.row
                cell.notesButton.addTarget(self, action: #selector(notesButtonAction(sender:)), for: .touchUpInside)
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartLastTableViewCellFinal", for: indexPath) as! CartLastTableViewCellFinal
                cell.grandTotalLabel.text = totalPrice
                self.cardPaymentButton = cell.cardPayment
                self.payDeskButton = cell.payAtDeskButton
                self.cardPaymentButton.isSelected = true
                self.payDeskButton.addTarget(self, action: #selector(changeRadioButtons(sender:)), for: .touchUpInside)
                self.cardPaymentButton.addTarget(self, action: #selector(changeRadioButtons(sender:)), for: .touchUpInside)
                self.textView = cell.textView
                cell.addMore.addTarget(self, action: #selector(addMoreServices), for: .touchUpInside)
                return cell
            }
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            if indexPath.section == 1{
                return 200//470
            }
            else{
                return UITableView.automaticDimension//200
            }
            
        }
}


extension FinalBookingViewController: SelectDateTimePopUpFinalDelegate, NotesPopUpDelegate{
    func selectPopUpData() {
        self.tableView.reloadData()
    }
    func saveButtonAction(textView: UITextView, index: Int) {
        if self.appD.serviceCart.count > index && index > -1{
            self.appD.serviceCart[index].notes = textView.text
            tableView.reloadData()
        }
    }
    func cancelButtonAction(textView: UITextView, index: Int) {
        // No action for now
    }
    
}
