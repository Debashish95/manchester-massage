//
//  ProfileViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImageBackView: UIView!
    @IBOutlet weak var uploadProfileImageButton: UIButton!
    @IBOutlet weak var changeProfileImage: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileNameText: UITextField!
    @IBOutlet weak var profileEmailText: UITextField!
    @IBOutlet weak var profilePhoneNumber: UITextField!
    
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)

        self.setNavigationBar(titleText: "Account", isBackButtonRequired: true, isRightMenuRequired: false)
        logoutButton.addTarget(self, action: #selector(logoutButtonAction), for: .touchUpInside)
        
        logoutButton.layer.cornerRadius = logoutButton.frame.height / 2
        logoutButton.clipsToBounds = true
        
        editButton.layer.cornerRadius = editButton.frame.height / 2
        editButton.clipsToBounds = true
        
        changePasswordButton.layer.cornerRadius = changePasswordButton.frame.height / 2
        changePasswordButton.clipsToBounds = true
        
        changeProfileImage.layer.cornerRadius = changeProfileImage.frame.height / 2
        changeProfileImage.clipsToBounds = true
        
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.clipsToBounds = true
        profileImage.contentMode = .scaleAspectFill
        if let imageURL = URL(string: UserDefaults.standard.fetchData(forKey: .userImage)){
            profileImage.kf.setImage(with: imageURL)
        }else{
            profileImage.image = UIImage(named: "ic_account")
        }
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        uploadProfileImageButton.backgroundColor = ThemeColor.buttonColor

        
        uploadProfileImageButton.setTitle("Upload", for: .normal)
        uploadProfileImageButton.setImage(UIImage(icon: .FAUpload, size: CGSize(width: 20, height: 20), textColor: .white), for: .normal)
        
        uploadProfileImageButton.setTitle("Edit", for: .selected)
        uploadProfileImageButton.setImage(UIImage(icon: .FAPencil, size: CGSize(width: 20, height: 20), textColor: .white), for: .selected)
        
        uploadProfileImageButton.isSelected = (profileImage.image != UIImage(named: "ic_account"))
        
        uploadProfileImageButton.addTarget(self, action: #selector(uploadImageButtonAction(sender:)), for: .touchUpInside)
        
        profileImageBackView.layer.cornerRadius = profileImageBackView.bounds.height / 2
        profileImageBackView.clipsToBounds = true
        
    }
    override func viewDidAppear(_ animated: Bool) {
        profileNameText.addButtomBorder()
        profileEmailText.addButtomBorder()
        profilePhoneNumber.addButtomBorder()
        
        profileNameText.text = UserDefaults.standard.fetchData(forKey: .userFirstName) + " " + UserDefaults.standard.fetchData(forKey: .userLastName)
        profileEmailText.text = UserDefaults.standard.fetchData(forKey: .userEmail)
        profilePhoneNumber.text = UserDefaults.standard.fetchData(forKey: .userPhone)
        
        editButton.setTitleColor(.white, for: .normal)
        editButton.backgroundColor = UIColor(hexString: "#044E75")
        editButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        logoutButton.setTitleColor(.white, for: .normal)
        logoutButton.backgroundColor = .red
        logoutButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        editButton.addTarget(self, action: #selector(editButtonAction), for: .touchUpInside)
        
        changePasswordButton.setTitleColor(.white, for: .normal)
        changePasswordButton.backgroundColor = ThemeColor.buttonColor
        changePasswordButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        changePasswordButton.setTitle("Change Password", for: .normal)
        changePasswordButton.addTarget(self, action: #selector(changePasswordButtonAction), for: .touchUpInside)
        /*if (UserDefaults.standard.string(forKey: "userSocialLoginId") ?? "") != ""{
            changePasswordButton.isEnabled = false
            changePasswordButton.alpha = 0.5
        }else{
            
        }*/
        
        changePasswordButton.isEnabled = true
        changePasswordButton.alpha = 1
        
        changeProfileImage.setTitleColor(.white, for: .normal)
        changeProfileImage.backgroundColor = ThemeColor.buttonColor
        changeProfileImage.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        changeProfileImage.setTitle("Update Profile Picture", for: .normal)
        changeProfileImage.addTarget(self, action: #selector(changeProfileImageButton(sender:)), for: .touchUpInside)
        
    }
    
    @objc func uploadImageButtonAction(sender: UIButton){
        self.imagePicker.present(from: sender)
    }
    
    @objc func changeProfileImageButton(sender: UIButton){
        if !uploadProfileImageButton.isSelected{
            self.tabBarController?.view.makeToast(message: "Please change profile image before upload.")
        }else if self.profileImage.image != UIImage(named: "ic_account"){
            self.uploadProfilePicture(cust_id: UserDefaults.standard.fetchData(forKey: .userId))
        }
        else{
            UserDefaults.standard.deleteData(forKey: .userImage)
            self.tabBarController?.view.makeToast(message: "Profile image has been removed successfully.")
        }
    }
    
    func uploadProfilePicture(cust_id: String) {
        let paramsHead = ["Content-Type": "multipart/form-data"]
        guard let imgData = self.profileImage.image?.pngData() else { return }
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "cust_id": cust_id.toData,
            
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            multipartFormData.append(imgData, withName: "thumbnail", fileName: "\(cust_id)_\(Date().timeIntervalSince1970).png", mimeType: "image/png")
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "ProfileImage" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    let status = result["status"] as? String ?? ""
                    
                    let message = result["type"] as? String ?? ""
                    let info = result["info"] as? NSDictionary ?? NSDictionary()
                    let thumbnail = info["thumbnail"] as? String ?? ""
                    
                    
                    debugPrint("Upload Image::: \(resposeJSON)")
                    UserDefaults.standard.saveData(value: thumbnail, key: .userImage)
                    if status == "success"{
                        self.tabBarController?.view.makeToast(message: "Profile image has been updated successfully.")
                    }
                    else{
                        self.tabBarController?.view.makeToast(message: message)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    @objc func changePasswordButtonAction(){
        if (UserDefaults.standard.string(forKey: "userSocialLoginId") ?? "") != ""{
            self.tabBarController?.view.makeToast(message: "You cannot change password as you have authenticated through social media.")
        }else{
            let destination = ChangePasswordPopUp(nibName: "ChangePasswordPopUp", bundle: nil)
            destination.modalPresentationStyle = .overCurrentContext
            self.definesPresentationContext = true
            destination.delegate = self
            self.definesPresentationContext = true
            self.present(destination, animated: true, completion: nil)
        }
        
    }
    @objc func editButtonAction(){
        let destination = EditProfileViewController(nibName: "EditProfileViewController", bundle: nil)
        destination.delegate = self
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        self.tabBarController?.present(destination, animated: true, completion: nil)
    }
    @objc func logoutButtonAction(){
        let alert = UIAlertController(title: "Alert!", message: "Do you want to logout?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (action) in
            UserDefaults.standard.deleteData(forKey: .userFirstName)
            UserDefaults.standard.deleteData(forKey: .userLastName)
            UserDefaults.standard.deleteData(forKey: .userEmail)
            UserDefaults.standard.deleteData(forKey: .userSocialLogin)
            UserDefaults.standard.deleteData(forKey: .userSocialLoginId)
            UserDefaults.standard.deleteData(forKey: .userId)
            UserDefaults.standard.deleteData(forKey: .userPhone)
            UserDefaults.standard.deleteData(forKey: .userStatus)
            
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
            let navigationController = UINavigationController(rootViewController: destination)
            self.appD.window = UIWindow(frame: UIScreen.main.bounds)
            self.appD.window?.rootViewController = navigationController
            self.appD.window?.makeKeyAndVisible()
        }
        let noAction = UIAlertAction(title: "No", style: .cancel) { (action) in
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: false, completion: nil)
    }
    
    func changePasswordAction(newPassword: String, oldPassword: String){
        
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "salt": Config.salt.toData,
            "key": Config.key.toData,
            "c_email": (UserDefaults.standard.fetchData(forKey: .userEmail)).toData,
            "old_password": oldPassword.toData,
            "new_password": newPassword.toData,
            ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "ChangePass" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let status = resposeJSON["status"] as? String ?? ""
                    let message = resposeJSON["msg"] as? String ?? ""
                    
                    if (status == "OK"){
                        self.tabBarController?.view.makeToast(message: "Password changed successfully.")
                        
                    }
                    else{
                        self.tabBarController?.view.makeToast(message: message)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
}


extension ProfileViewController: EditProfileViewControllerDelegate{
    func updateProfile() {
        self.viewDidAppear(false)
    }
}

extension ProfileViewController: ChangePasswordPopUpDelegate{
    func changePassword(newPassword: String, oldPassword: String) {
        self.changePasswordAction(newPassword: newPassword, oldPassword: oldPassword)
    }
    
    
}

extension ProfileViewController: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        uploadProfileImageButton.isSelected = (image != nil)
        if image == nil{
            self.profileImage.image = UIImage(named: "ic_account")
        }else{
            self.profileImage.image = image
        }
        
    }
}
