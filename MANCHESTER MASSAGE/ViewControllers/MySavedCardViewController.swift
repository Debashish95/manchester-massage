//
//  MySavedCardViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 25/11/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class MySavedCardViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var payNowButton: UIButton!
    
    var delegate: MySavedCardViewControllerDelegate?
    var activityIndicator: NVActivityIndicatorView!
    var savedCardList = [CardModel]()
    var savedCardCVV = ""
    
    var totalPriceFloat = Float(0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBar(titleText: "My Saved Cards", isBackButtonRequired: true, isRightMenuRequired: false)
        
        activityIndicator = self.addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SavedCardTableViewCell", bundle: nil), forCellReuseIdentifier: "SavedCardTableViewCell")
        
        getSavedCardList()
        
        payNowButton.setTitle("Pay \(CURRENCY_SYMBOL)\(String(totalPriceFloat).formattedString())", for: UIControl.State())

    }
    
    override func viewDidAppear(_ animated: Bool) {
        payNowButton.backgroundColor = ThemeColor.buttonColor
        payNowButton.layer.cornerRadius = payNowButton.frame.height / 2
        payNowButton.clipsToBounds = true
        payNowButton.addTarget(self, action: #selector(payNowButtonAction), for: .touchUpInside)
    }
    
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getSavedCardList() {
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/getCardListStripe?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
            if((result["status"] as? Int ?? 0) == 1){
                let finalCardResult = result["result"] as? NSDictionary ?? NSDictionary()
                if let _ = finalCardResult["error"] as? NSDictionary{
                    self.tabBarController?.view.makeToast(message: "Unable to fetch your cards.")
                    self.backButtonAction()
                }else{
                    let cardListData = finalCardResult["data"] as? NSArray ?? NSArray()
                    var cardData = CardModel()
                    for card in cardListData {
                        let cardItem = card as? NSDictionary ?? NSDictionary()
                        cardData.cardID = cardItem["id"] as? String ?? ""
                        let cardObject = cardItem["card"] as? NSDictionary ?? NSDictionary()
                        cardData.brand = cardObject["brand"] as? String ?? ""
                        cardData.expMonth = String(cardObject["exp_month"] as? Int ?? 0)
                        cardData.expYear = String(cardObject["exp_year"] as? Int ?? 0)
                        cardData.last4Digit = cardObject["last4"] as? String ?? ""
                        self.savedCardList.append(cardData)
                    }
                    
                    if self.savedCardList.count == 0 {
                        self.payNowButton.isHidden = true
                        let alert = UIAlertController(title: "Alert!", message: "No Cards Found.", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
                            self.backButtonAction()
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
//                    self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
                }
            }else{
//                self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
            }
            
            self.tableView.reloadData()
            
        }
    }
    
    
    fileprivate func paymentAction() {
        var fourDigitDate = ""
        var exp_month = ""
        var cardNumber = ""
        var cardCVV = ""
        var nameOnCard = ""
        let savedCard = self.savedCardList.first(where: {$0.isSelected}) ?? CardModel()
        fourDigitDate = savedCard.expYear
        exp_month = savedCard.expMonth
        cardNumber = savedCard.last4Digit
        cardCVV = savedCard.cvvText
        
        if cardNumber.isEmpty{
            self.tabBarController?.view.makeToast(message: "Please Enter Card Number")
            return
        }
        if cardCVV.isEmpty {
            self.tabBarController?.view.makeToast(message: "Please Enter Card CVV")
            return
        }
        
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId),
            "amount": "\(totalPriceFloat)",
            "tran_id": "PMP\(String().randomString(length: 12))",
            "card_number": cardNumber,
            "card_exp_month": exp_month,
            "card_exp_year": fourDigitDate,
            "card_cvc": cardCVV,
            "save_card":  "0",
            "new_card": "0",
            "name_on_card": nameOnCard
        ]as [String:String]
        
        Alamofire.request("\(Config.apiEndPoint)stripe_charge_card", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate().responseJSON { (response) in
                
                print(response)
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                self.activityIndicator.stopAnimating()
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    
                    let alert = UIAlertController(title: "Alert!", message: "Sorry! Something went wrong. Please try again!!", preferredStyle: .alert)
                    let okButton = UIAlertAction(title: "Ok", style: .default)
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                    break
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let json = (jSONResponse as? NSDictionary ?? NSDictionary())
                    self.delegate?.response(data: [
                        "id" : json["id"] as? String ?? "",
                        "json": json
                    ], error: nil)
                    self.backButtonAction()
                    break
                }
            }
    }
    
    @objc func payNowButtonAction(){
        
        self.view.endEditing(true)
        
        
        let alert = UIAlertController(title: "PLEASE NOTE",
                                      message: """
When making your payment to Manchester Massage via this site, please be aware that the transaction will appear on your bank statement as PamperTree who act as our 3rd party advertisers.
""", preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "I UNDERSTAND", style: .destructive) { _ in
            self.paymentAction()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension MySavedCardViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCardList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardTableViewCell", for: indexPath) as! SavedCardTableViewCell
        cell.data = self.savedCardList[indexPath.row]
        cell.cvvTextField.delegate = self
        cell.cvvTextField.keyboardType = .numberPad
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for index in 0 ..< self.savedCardList.count{
            if indexPath.row == index{
                if self.savedCardList[index].isSelected{
                    self.savedCardList[index].isSelected = false
//                    useSavedCard = false
                }else{
                    self.savedCardList[index].isSelected = true
//                    useSavedCard = true
                }
            }else{
                self.savedCardList[index].isSelected = false
            }
            self.savedCardList[index].cvvText = ""
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}


protocol MySavedCardViewControllerDelegate{
    func response(data: [String: Any]?, error: [String: String]?)
}

extension MySavedCardViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        for index in 0 ..< self.savedCardList.count{
            if self.savedCardList[index].isSelected{
                self.savedCardList[index].cvvText = textField.text ?? ""
            }
        }
    }
}
