//
//  WalletViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 05/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class WalletViewController: UIViewController {

    @IBOutlet weak var walletImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    var activityIndicator: NVActivityIndicatorView!
    var walletData = [WalletModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "My Wallet", isBackButtonRequired: true)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        tableView.register(UINib(nibName: "WalletTableViewCell", bundle: nil), forCellReuseIdentifier: "WalletTableViewCell")
        
        walletImage.image = walletImage.image?.withRenderingMode(.alwaysTemplate)
        walletImage.tintColor = UIColor.white
        
        let rightButton = UIButton(type: .custom)
        rightButton.setTitle("Add Balance To Wallet" , for: .normal)
        rightButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        rightButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        rightButton.addTarget(self, action: #selector(addBalanceAction), for: .touchUpInside)
        rightButton.setLeftImage(image: .FAPlus, color: ThemeColor.buttonColor, state: .normal, size: CGSize(width: 25, height: 25))
        let rightNavigationBarItem = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightNavigationBarItem
        getWalletData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getWalletData), name: NSNotification.Name("walletStatusChange"), object: nil)
    }
    
    @objc fileprivate func getWalletData(){
        self.activityIndicator.startAnimating()
        walletData.removeAll()
        let requestURL = "\(Config.mainUrl)Data/GetwalletInfo?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        Alamofire.request(requestURL)
            .validate()
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print("Response JSON: \(resposeJSON)")
                
                self.totalAmountLabel.attributedText = self.makeAttributedString(key: "Total Amount: ", value: "\(CURRENCY_SYMBOL)\((resposeJSON["current_balance"] as? String ?? "0").formattedString())")
                    
                
                let result = resposeJSON["result"] as? NSArray ?? NSArray()
                for indx in 0..<result.count{
                    let currentData = result[indx] as? NSDictionary ?? NSDictionary()
                    var walletData = WalletModel()
                    walletData.tran_id = currentData["tran_id"] as? String ?? ""
                    walletData.info = currentData["info"] as? String ?? ""
                    walletData.tran_type = currentData["tran_type"] as? String ?? "D"
                    walletData.amount = currentData["amount"] as? String ?? ""
                    walletData.insert_date = self.convertDate((currentData["insert_date"] as? String ?? "1970-01-01"), from: "yyyy-MM-dd HH:mm:ss", to: "MMM dd, yyyy")
                    self.walletData.append(walletData)
                }
                self.tableView.reloadData()
            }
    }
    
    @objc func addBalanceAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddBalanceViewController") as! AddBalanceViewController
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
    fileprivate func convertDate(_ date: String, from format1: String, to format2: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format1
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = format2
        return  dateFormatter.string(from: date)
    }
    
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 17), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        two.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 20), range: NSMakeRange(0, two.length))
        attributedString.append(two)
        return attributedString
    }
}

extension WalletViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! WalletTableViewCell
        cell.data = walletData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
