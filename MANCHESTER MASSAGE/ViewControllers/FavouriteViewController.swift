//
//  FavouriteViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 15/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class FavouriteViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicator: NVActivityIndicatorView!
    var serviceList = [ServiceModel]()
    var serviceDetailsData: ServiceDetailsModel!
    
    var cartButton: UIButton!
    var rightBarButton: ENMBadgedBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "My Favourites")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "ServiceListTableViewCell", bundle: nil), forCellReuseIdentifier: "ServiceListTableViewCell")
        serviceList.removeAll()
        
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.getfavouriteList), name: NSNotification.Name("getFavList"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to view your favourite services.", preferredStyle: .alert)
            let okButtonAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                self.tabBarController?.selectedIndex = 0
            }
            alert.addAction(okButtonAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            getfavouriteList()
        }
    }
    
    @objc func getfavouriteList(){
        self.serviceList.removeAll()
        self.tableView.reloadData()
        self.activityIndicator.startAnimating()
        let apiURL = Config.mainUrl + "Data/favServiceCategoryList?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        Alamofire.request(apiURL, method: .get).validate().responseJSON { (response) in
            self.activityIndicator.stopAnimating()
            guard response.result.isSuccess else {
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            let results = resposeJSON["result"] as? NSArray ?? NSArray()
            if results.count == 0{
                let alert = UIAlertController(title: "Alert!", message: "No services found in favourite list.", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
//                    self.dismiss(animated: true, completion: nil)
                    self.tabBarController?.selectedIndex = 0
                }
                alert.addAction(yesAction)
                self.present(alert, animated: false, completion: nil)
                return
            }
            
            results.forEach { (result) in
                if let currentResult = result as? NSDictionary{
                    let serviceID = currentResult["cat_id"] as? String ?? ""
                    let serviceName = currentResult["service_title"] as? String ?? ""
                    let serviceImage = currentResult["thumb"] as? String ?? ""
                    let rating = currentResult["rating"] as? String ?? "0"
                    let reviewCount = currentResult["rating_counts"] as? String ?? "0"
                    let sm_id = currentResult["sm_id"] as? String ?? ""
                    
                    let service = ServiceModel(serviceId: serviceID, serviceName: serviceName, serviceImage: serviceImage, serviceDescription: "", servicePrice: "", serviceMaxPrice: "", discount: "", duration: "", cleanUpTime: "", rating: rating, reviewCount: reviewCount, sm_id: sm_id)
                    self.serviceList.append(service)
                }
            }
            self.tableView.reloadData()
        }
        
        
    }
    
    @objc func bookNowButtonAction(sender: UIButton){
        self.setData(catID: serviceList[sender.tag].serviceId)
    }
    
    @objc func detailsButtonAction(sender: UIButton){
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = serviceList[sender.tag].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
    
    func setData(catID:String){
        
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                _ = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? ""
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(smId: (itmDict["sm_id"] as? String ?? ""), serviceID: (itmDict["service_id"] as? String ?? ""), catId: (itmDict["cat_id"] as? String ?? ""), serviceTitle: (itmDict["service_title"] as? String ?? ""), serviceItems: serviceItemsList)
                                    
                                    self.serviceDetailsData = serviceDataTemp
                                }
                                
                                let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
                                destination.modalPresentationStyle = .overCurrentContext
                                self.definesPresentationContext = true
                                destination.delegate = self
                                destination.fromPage = .serviceList
                                destination.serviceData = self.serviceDetailsData
                                self.present(destination, animated: true, completion: nil)
                            }
                            
                            
                          }
        
    }
    
    @objc func removeFavourite(sender: UIButton){
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "typ_id": serviceList[sender.tag].sm_id.toData,
            "typ": "SERVICE_CAT".toData,
            "cust_id": (UserDefaults.standard.string(forKey: "userId") ?? "").toData
            
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "removeFavourite" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    
                    let status = resposeJSON["status"] as? Int ?? 0
                    let message = resposeJSON["msg"] as? String ?? ""
                    
                    if status == 1{
                        self.tabBarController?.view.makeToast(message: "Service has been removed from favourite successfully.")
                        self.serviceList.removeAll()
//                        self.tableView.reloadData()
                        self.getfavouriteList()
                        return
                    }
                    self.tabBarController?.view.makeToast(message: message)
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    
}

extension FavouriteViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceListTableViewCell", for: indexPath) as! ServiceListTableViewCell
        cell.setData(data: serviceList[indexPath.row])
        cell.backView.layer.cornerRadius = 20
        cell.backView.clipsToBounds = true
        cell.bookButton.tag = indexPath.row
        cell.detailsButton.tag = indexPath.row
        cell.discount.isHidden = true
        cell.discountValue.isHidden = true
        cell.descriptionLabel.isHidden = true
        cell.ratingView.settings.fillMode = .precise
        cell.ratingView.rating = Double(serviceList[indexPath.row].rating) ?? 0
        cell.ratingView.text = "\(serviceList[indexPath.row].reviewCount) reviews"
        cell.ratingView.isHidden = false
        cell.bookButton.addTarget(self, action: #selector(self.bookNowButtonAction(sender:)), for: .touchUpInside)
        cell.detailsButton.addTarget(self, action: #selector(self.detailsButtonAction(sender:)), for: .touchUpInside)
        cell.favouriteButton.isHidden = false
        cell.favouriteButton.tag = indexPath.row
        cell.favouriteButton.addTarget(self, action: #selector(self.removeFavourite(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = serviceList[indexPath.row].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}
extension FavouriteViewController: BookNowPopUpDelegate{
    func confirmButtonAction() {
        
    }
    
    func addServiceButtonActions() {
        let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
        self.navigationController?.pushViewController(detination, animated: true)
        
        
    }
}


/*class FavouriteViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var activityIndicator: NVActivityIndicatorView!
    var services = [ServiceModel]()
    var serviceDetailsData: ServiceDetailsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(title: "My Favourites", isBackButtonRequired: true, isRightMenuRequired: false)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UINib(nibName: "HomeServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeServiceCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
        getfavouriteList()
    }
    

    func getfavouriteList(){
        self.services.removeAll()
        self.activityIndicator.startAnimating()
        let apiURL = Config.mainUrl + "Data/favServiceCategoryList?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        Alamofire.request(apiURL, method: .get).validate().responseJSON { (response) in
            self.activityIndicator.stopAnimating()
            guard response.result.isSuccess else {
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            let results = resposeJSON["result"] as? NSArray ?? NSArray()
            if results.count == 0{
                let alert = UIAlertController(title: "Alert!", message: "No services found in favourite list.", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                    self.dismiss(animated: true, completion: nil)
                }
                alert.addAction(yesAction)
                self.present(alert, animated: false, completion: nil)
                return
            }
            
            results.forEach { (result) in
                if let currentResult = result as? NSDictionary{
                    let serviceID = currentResult["cat_id"] as? String ?? ""
                    let serviceName = currentResult["service_title"] as? String ?? ""
                  let serviceImage = "https://www.pampertree.co.uk/api/banner.jpg"
                    let serviceDescription = ""
                    let servicePrice = "100"
                    let serviceMaxPrice = "200"
                    let discount = "50% Off"
                    let duration = "60"
                    let cleanUpTime = "10"
                    
                    let service = ServiceModel(serviceId: serviceID, serviceName: serviceName, serviceImage: serviceImage, serviceDescription: serviceDescription, servicePrice: servicePrice, serviceMaxPrice: serviceMaxPrice, discount: discount, duration: duration, cleanUpTime: cleanUpTime)
                    self.services.append(service)
                }
            }
            self.collectionView.reloadData()
        }
        
        
    }

    @objc func bookNow(sender: UIButton){
        self.setData(catID: services[sender.tag].serviceId)
    }
    
    @objc func serviceDetailsBtnAction(sender: UIButton){
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = services[sender.tag].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func setData(catID:String){
        
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                _ = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? "0"
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(smId: (itmDict["sm_id"] as? String ?? ""), serviceID: (itmDict["service_id"] as? String ?? ""), catId: (itmDict["cat_id"] as? String ?? ""), serviceTitle: (itmDict["service_title"] as? String ?? ""), serviceItems: serviceItemsList)
                                    
                                    self.serviceDetailsData = serviceDataTemp
                                }
                                
                                let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
                                destination.modalPresentationStyle = .overCurrentContext
                                destination.delegate = self
                                destination.fromPage = .home
                                destination.serviceData = self.serviceDetailsData
                                self.present(destination, animated: true, completion: nil)
                            }
                            
                            
                          }
        
    }
}

extension FavouriteViewController: BookNowPopUpDelegate{
    func confirmButtonAction() {
        
    }
    
    func addServiceButtonActions() {
        
        let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        self.navigationController?.pushViewController(detination, animated: true)
        
        
    }
}

extension FavouriteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "HomeServiceCollectionViewCell", for: indexPath) as! HomeServiceCollectionViewCell
        cell.setData(data: services[indexPath.item])
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1.2
        cell.layer.cornerRadius = 10
        cell.discountLabel.layer.cornerRadius = cell.discountLabel.frame.height / 2
        cell.discountLabel.clipsToBounds = true
        cell.bookNowBtn.tag = indexPath.row
        cell.bookNowBtn.addTarget(self, action: #selector(bookNow(sender:)), for: .touchUpInside)
        
        cell.detailsButton.tag = indexPath.row
        cell.detailsButton.addTarget(self, action: #selector(self.serviceDetailsBtnAction(sender:)), for: .touchUpInside)
        
        cell.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = UIScreen.main.bounds.width
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: collectionViewWidth / 2.2, height: collectionViewWidth / 2.0)
        }else{
            return CGSize(width: collectionViewWidth / 2.3, height: collectionViewWidth / 2.0)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}*/
