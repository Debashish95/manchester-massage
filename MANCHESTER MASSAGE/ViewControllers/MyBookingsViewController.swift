//
//  MyBookingsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 27/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class MyBookingsViewController: UIViewController {
    
    @IBOutlet weak var noUpcomingBookings: UIStackView!
    @IBOutlet weak var bookAppointmentButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pastUpcomingSegmentedControl: UISegmentedControl!
    
    var activityIndicator: NVActivityIndicatorView!
    var upcomingbookingsData = [BookingsModel]()
    var pastbookingsData = [BookingsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setHomePageNavBr(title: "Booking History", isMenuRequired: false)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        if #available(iOS 13.0, *) {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: ThemeColor.buttonColor]
        pastUpcomingSegmentedControl.setTitleTextAttributes(titleTextAttributes, for:.normal)

        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
        pastUpcomingSegmentedControl.setTitleTextAttributes(titleTextAttributes1, for:.selected)
            
        }
        self.tableView.dataSource = self
        self.tableView.delegate = self
        tableView.register(UINib(nibName: "BookingTableViewCell", bundle: nil), forCellReuseIdentifier: "BookingTableViewCell")
        tableView.register(UINib(nibName: "BookingCell", bundle: nil), forCellReuseIdentifier: "BookingCell")
        tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        
        
        
        tableView.register(UINib(nibName: "UpComingBookingTableViewCell", bundle: nil), forCellReuseIdentifier: "UpComingBookingTableViewCell")
        
//        getBookingsData()
        
        pastUpcomingSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium)], for: UIControl.State())
        pastUpcomingSegmentedControl.addTarget(self, action: #selector(changeSegmentedValue), for: .valueChanged)
        
        bookAppointmentButton.addTarget(self, action: #selector(bookAppointmentAction), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        bookAppointmentButton.setLeftImage(image: .FAPlus, color: .white, state: .normal, size: CGSize(width: 25, height: 25))
        bookAppointmentButton.layer.cornerRadius = bookAppointmentButton.bounds.height / 2
        bookAppointmentButton.clipsToBounds = true
        noUpcomingBookings.isHidden = true
        
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to view your bookings.", preferredStyle: .alert)
            let okButtonAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                self.tabBarController?.selectedIndex = 0
            }
            alert.addAction(okButtonAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            getBookingsData()
        }
        
//        tableView.reloadData()
    }
    
    @objc func changeSegmentedValue(){
        if pastUpcomingSegmentedControl.selectedSegmentIndex == 1{
            if pastbookingsData.count == 0{
                self.tabBarController?.view.makeToast(message: "No Data Found.")
            }
            tableView.isHidden = false
            noUpcomingBookings.isHidden = true
        }else{
            if upcomingbookingsData.count == 0{
                self.tabBarController?.view.makeToast(message: "No Data Found.")
            }
            tableView.isHidden = (upcomingbookingsData.count == 0)
            noUpcomingBookings.isHidden = !(upcomingbookingsData.count == 0)
        }
        tableView.reloadData()
    }
    
    @objc func bookAppointmentAction(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
        destination.serviceList = self.appD.serviceData
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getBookingsData(){
        
        self.activityIndicator.startAnimating()
        self.upcomingbookingsData.removeAll()
        self.pastbookingsData.removeAll()
        tableView.reloadData()
        let reuestURL = Config.mainUrl + "Data/GetBookingList?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                let results = resposeJSON["result"] as? NSArray ?? NSArray()
                
                for result in results{
                    let currentBooking = result as? NSDictionary ?? NSDictionary()
                    
                    
                    
                    let orderid = currentBooking["orderid"] as? String ?? ""
                    let share_url = currentBooking["share_url"] as? String ?? ""
                    let status = currentBooking["status"] as? String ?? "0"
                    let cust_id = currentBooking["cust_id"] as? String ?? ""
                    let firstName = currentBooking["cust_fname"] as? String ?? ""
                    let lastName = currentBooking["cust_lname"] as? String ?? ""
                    let customerEmail = currentBooking["cust_email"] as? String ?? ""
                    let cust_phone = currentBooking["cust_phone"] as? String ?? ""
                    let therapist_id = currentBooking["therapist_id"] as? String ?? ""
                    let bookdt = currentBooking["bookdt"] as? String ?? ""
                    let timeslot = currentBooking["timeslot"] as? String ?? ""
                    let notes = currentBooking["notes"] as? String ?? ""
                    
                    let salon_id = currentBooking["salon_id"] as? String ?? ""
                    let salon = currentBooking["salon"] as? String ?? ""
                    let transaction_type = currentBooking["transaction_type"] as? String ?? ""
                    let currentService = currentBooking["services"] as? NSArray ?? NSArray()
                    let is_review = currentBooking["is_review"] as? String ?? "0"
                    let booking_id = currentBooking["booking_id"] as? String ?? "0"
                    let booked_on = self.convertDateFormater2(currentBooking["booked_on"] as? String ?? "0")
                        
                    
                    
                    var reviewData: ReviewModel?
                    if let reviewInfo = currentBooking["review_info"] as? NSDictionary{
                        reviewData = ReviewModel(reviewer_name: reviewInfo["r_name"] as? String ?? "",
                                    reviewer_email: reviewInfo["r_email"] as? String ?? "",
                                    review: reviewInfo["r_review"] as? String ?? "",
                                    service_review: reviewInfo["rt_service"] as? String ?? "",
                                    staff_review: reviewInfo["rt_staff"] as? String ?? "",
                                    cleanliness_review: reviewInfo["rt_clean"] as? String ?? "",
                                    overall_review: reviewInfo["rt_overall"] as? String ?? "",
                                    created_on: reviewInfo["created_on"] as? String ?? "")
                        
                    }
                    
                    var services = [servicelist]()
                    if currentService.count > 0{
                        
                        
                        for index in 0..<currentService.count{
                            let currentServiceData = currentService[index] as? NSDictionary ?? NSDictionary ()
                            let serviceId = currentServiceData["service_id"] as? String ?? ""
                            let cart_id = currentServiceData["cat_id"] as? String ?? ""
                            let serviceName = currentServiceData["service_name"] as? String ?? ""
                            let therapistNumber = currentServiceData["therapist"] as? String ?? "0"
                            let bookingDate = currentServiceData["book_dt"] as? String ?? ""
                            let bookingTime = currentServiceData["book_tym"] as? String ?? ""
                            let bookEndTime = currentServiceData["book_end_tym"] as? String ?? ""
                            let cat_thumb = currentServiceData["cat_thumb"] as? String ?? ""
                            let custName = currentServiceData["cust_name"] as? String ?? ""
                            let therapistArr = currentServiceData["therapist_arr"] as? String ?? ""
                            let therapistNm = currentServiceData["therepist_nm"] as? String ?? ""
                            let qty = currentServiceData["qty"] as? String ?? "0"
                            let basePrice = currentServiceData["base_price"] as? String ?? "0"
                            let subTotal = currentServiceData["subtotal"] as? String ?? "0"
                            let duration = currentServiceData["duration"] as? String ?? "0"
                            let cleanUpTime = currentServiceData["cleanup_time"] as? String ?? "0"
                            let notes = currentServiceData["notes"] as? String ?? ""
                            let sub_orderid = currentServiceData["sub_orderid"] as? String ?? ""
                            let bookingStatus = currentServiceData["status"] as? String ?? ""
                            let service = servicelist(serviceId: serviceId, sub_orderid: sub_orderid, serviceName: serviceName, therapistName: therapistNumber, bookingDate: bookingDate, bookingTime: bookingTime, bookEndTime: bookEndTime, custName: custName, therapistArr: therapistArr, therepistNm: therapistNm, qty: qty, basePrice: basePrice, subTotal: subTotal, duration: duration,cart_id: cart_id, cat_thumb: cat_thumb, cleanup_time: cleanUpTime, notes: notes, bookingStatus: bookingStatus)
                            services.append(service)
                        }
                        
                        
                    }
                    
                    let currentBookingData = BookingsModel(status: status, orderid: orderid, cust_id: cust_id, firstName: firstName, lastName: lastName, customerEmail: customerEmail, serviceType: "", servicePrice: "", cust_phone: cust_phone, therapist_id: therapist_id, salon_id: salon_id, salon: salon,services: services, bookdt: bookdt, timeslot: timeslot, notes: notes, transaction_type: transaction_type, is_review: is_review, booking_id: booking_id, reviews: reviewData, share_url: share_url, booked_on: booked_on, amount: "0")
                    
                    if currentBookingData.services.count > 0{
                        if(self.compareDate(expiryDate: currentBooking["bookdt"] as? String ?? "", expiryTime: currentBooking["timeslot"] as? String ?? "") ){
                            self.upcomingbookingsData.append(currentBookingData)
                        }
                        else{
                            self.pastbookingsData.append(currentBookingData)
                        }
                    }
                    
                    
                }
                self.tableView.reloadData()
                if self.upcomingbookingsData.count == 0 && self.pastUpcomingSegmentedControl.selectedSegmentIndex == 0{
                    self.noUpcomingBookings.isHidden = false
                }else{
                    self.noUpcomingBookings.isHidden = true
                }
                
        }
    }
    
    @objc func gotoManageBookingPage(sender: UIButton){
        if sender.accessibilityIdentifier == "manage"{
            let destination = UIStoryboard(name: "Bookings", bundle:  nil).instantiateViewController(withIdentifier: "ManageBookingViewController") as! ManageBookingViewController
            destination.bookingData = upcomingbookingsData[sender.tag]
            destination.isComingFromPastBooking = false
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
    }
    
    @objc func gotoViewDetailsPage(sender: UIButton){
        if sender.accessibilityIdentifier == "view"{
            let destination = UIStoryboard(name: "Bookings", bundle:  nil).instantiateViewController(withIdentifier: "ManageBookingViewController") as! ManageBookingViewController
            destination.bookingData = pastbookingsData[sender.tag]
            destination.isComingFromPastBooking = true
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView

        let destination = UIStoryboard(name: "Bookings", bundle:  nil).instantiateViewController(withIdentifier: "ManageBookingViewController") as! ManageBookingViewController
        destination.bookingData = pastbookingsData[tappedImage.tag]
        destination.isComingFromPastBooking = true
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func imageTappedNew(tapGestureRecognizer: UITapGestureRecognizer)
       {
           let tappedImage = tapGestureRecognizer.view as! UIImageView

           let destination = UIStoryboard(name: "Bookings", bundle:  nil).instantiateViewController(withIdentifier: "ManageBookingViewController") as! ManageBookingViewController
           destination.bookingData = upcomingbookingsData[tappedImage.tag]
           destination.isComingFromPastBooking = false
           self.navigationController?.pushViewController(destination, animated: true)
       }
    
    func convertDateFormater1(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "EEEE\ndd MMM yyyy"
        return  dateFormatter.string(from: date)
    }
    func convertDateFormaterNew(_ date: String) -> String
    {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "EEEE, dd MMM yyyy"
        return  dateFormatter.string(from: date)
        
    }
    
    func convertDateFormater2(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "hh:mm a, EEEE, dd MMM yyyy"
        return  dateFormatter.string(from: date)
    }
    
    func compareDate(expiryDate:String, expiryTime:String) -> Bool {
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let finalDateString = expiryDate + " " + expiryTime
        if Date() <= dateFormatter.date(from: finalDateString) ?? Date() {
            return true
        } else {
            return false
        }
    }
    
    func convertDuration(duartion: String, cleanUp: String) -> (String, String){
        let durationInt = (Int(duartion) ?? 0) * 60
        let cleanUpInt = (Int(cleanUp) ?? 0) * 60
        let finalTime = (durationInt - cleanUpInt)
        return (finalTime.secondsToTime(), cleanUpInt.secondsToTime())
    }
}
extension MyBookingsViewController: UITableViewDataSource, UITableViewDelegate{
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (pastUpcomingSegmentedControl.selectedSegmentIndex == 1) ? pastbookingsData.count :  self.upcomingbookingsData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  (pastUpcomingSegmentedControl.selectedSegmentIndex == 1) ? self.pastbookingsData[section].services.count + 2 :  self.upcomingbookingsData[section].services.count + 2
         
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if pastUpcomingSegmentedControl.selectedSegmentIndex == 1{
            
            if(indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "UpComingBookingTableViewCell", for: indexPath) as! UpComingBookingTableViewCell
                
                cell.massageDate.text = convertDateFormater1(self.pastbookingsData[indexPath.section].services.first?.bookingDate ?? "")
                cell.massageTime.text = self.pastbookingsData[indexPath.section].services.first?.bookingTime ?? ""
                cell.cancelView.isHidden = true
                
                cell.topImage.kf.setImage(with: URL(string: self.pastbookingsData[indexPath.section].services[0].cat_thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                    cell.topImage.contentMode = .scaleAspectFill
                }
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.topImage.isUserInteractionEnabled = true
                cell.topImage.tag = indexPath.section
                cell.topImage.addGestureRecognizer(tapGestureRecognizer)
                
                return cell
            }
                if(indexPath.row == self.pastbookingsData[indexPath.section].services.count + 1)
             {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
                cell.manageButton.setTitle("View Details", for: UIControl.State())
                cell.manageButton.layer.cornerRadius = 5
                cell.manageButton.layer.masksToBounds = true
                cell.manageButton.backgroundColor = ThemeColor.bookNowButtonColor
                cell.manageButton.accessibilityIdentifier = "view"
                cell.manageButton.tag = indexPath.section
                cell.manageButton.addTarget(self, action: #selector(gotoViewDetailsPage(sender:)), for: .touchUpInside)
                return cell
            }
                
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingCell", for: indexPath) as! BookingCell
                cell.massageName.attributedText = setAttributedText(first: "Service: ", second: self.pastbookingsData[indexPath.section].services[indexPath.row - 1].serviceName)
                
                cell.therapistName.attributedText = setAttributedText(first: "Therapist: ", second: "With " + self.pastbookingsData[indexPath.section].services[indexPath.row - 1].therepistNm)
                
                cell.appointmentLabel.attributedText = setAttributedText(first: "Day/Time: ", second: "At " + self.pastbookingsData[indexPath.section].services[indexPath.row - 1].bookingTime + ", " + convertDateFormaterNew(self.pastbookingsData[indexPath.section].services[indexPath.row - 1].bookingDate))
                cell.appointmentLabel.textColor = .black
                cell.durationTxt.attributedText = setAttributedText(first: "Booked On: ", second: self.pastbookingsData[indexPath.section].booked_on)

                cell.paymentLabel.attributedText = setAttributedText(first: "Payment: ", second: (self.pastbookingsData[indexPath.section].transaction_type == "paydesk" ? "Pay At Desk" : "Prepaid by customer \(CURRENCY_SYMBOL)\(self.pastbookingsData[indexPath.section].services[indexPath.row - 1].subTotal.formattedString())"))
//                 by customer \(CURRENCY_SYMBOL)\(self.pastbookingsData[indexPath.section].services[indexPath.row - 1].subTotal)
                let duration = convertDuration(duartion: (self.pastbookingsData[indexPath.section].services[indexPath.row - 1].duration), cleanUp: (self.pastbookingsData[indexPath.section].services[indexPath.row - 1].cleanup_time))
                cell.durationLabel.attributedText = setAttributedText(first: "Duration: ", second: " \(duration.0), \(duration.1) cleanup")
                return cell
            }
            
        }
        else{
            
            
            if(indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "UpComingBookingTableViewCell", for: indexPath) as! UpComingBookingTableViewCell
                cell.massageDate.text = convertDateFormater1(self.upcomingbookingsData[indexPath.section].services[0].bookingDate)
                cell.massageTime.text = self.upcomingbookingsData[indexPath.section].services[0].bookingTime
                cell.cancelView.isHidden = !(self.upcomingbookingsData[indexPath.section].status == "2")
                
                cell.topImage.kf.setImage(with: URL(string: self.upcomingbookingsData[indexPath.section].services[0].cat_thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                                   cell.topImage.contentMode = .scaleAspectFill
                               }
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedNew(tapGestureRecognizer:)))
                cell.topImage.isUserInteractionEnabled = true
                cell.topImage.tag = indexPath.section
                cell.topImage.addGestureRecognizer(tapGestureRecognizer)
                
                return cell
            }
            if(indexPath.row == self.upcomingbookingsData[indexPath.section].services.count + 1)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
                cell.manageButton.setTitle("Manage Booking", for: UIControl.State())
                cell.manageButton.layer.cornerRadius = 5
                cell.manageButton.layer.masksToBounds = true
                cell.manageButton.backgroundColor = ThemeColor.bookNowButtonColor
                cell.manageButton.accessibilityIdentifier = "manage"
                cell.manageButton.tag = indexPath.section
                cell.manageButton.addTarget(self, action: #selector(gotoManageBookingPage(sender:)), for: .touchUpInside)
                return cell
            }
                
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingCell", for: indexPath) as! BookingCell
                 cell.massageName.attributedText = setAttributedText(first: "Service: ", second: self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].serviceName)
                 
                 cell.therapistName.attributedText = setAttributedText(first: "Therapist: ", second: "With " + self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].therepistNm)
                
                cell.appointmentLabel.attributedText = setAttributedText(first: "Day/Time: ", second: "At " + self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].bookingTime + ", " + convertDateFormaterNew(self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].bookingDate))
                cell.appointmentLabel.textColor = .black
                 cell.durationTxt.attributedText = setAttributedText(first: "Booked On: ", second: "\(self.upcomingbookingsData[indexPath.section].booked_on)")

                cell.paymentLabel.attributedText = setAttributedText(first: "Payment: ", second: (self.upcomingbookingsData[indexPath.section].transaction_type == "paydesk" ? "Pay At Desk" : "Prepaid by customer \(CURRENCY_SYMBOL)\(self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].subTotal.formattedString())"))
 //                 by customer \(CURRENCY_SYMBOL)\(self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].subTotal)
                 let duration = convertDuration(duartion: (self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].duration), cleanUp: (self.upcomingbookingsData[indexPath.section].services[indexPath.row - 1].cleanup_time))
                 cell.durationLabel.attributedText = setAttributedText(first: "Duration: ", second: " \(duration.0), \(duration.1) cleanup")
                return cell
            }
            
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.row == 0){
            return 250
        }else{
            //return 50
            if pastUpcomingSegmentedControl.selectedSegmentIndex == 1{
                if indexPath.row == self.pastbookingsData[indexPath.section].services.count + 1{
                    return 50
                }else{
                    return 180
                }
            }else{
                if indexPath.row == self.upcomingbookingsData[indexPath.section].services.count + 1{
                    return 50
                }else{
                    return 180
                }
            }
        }
    }
    
    func setAttributedText(first: String, second: String) -> NSMutableAttributedString{
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .bold)
        ]
        let firstAttributedText = NSMutableAttributedString(string: first, attributes: attributes)
        let secondAttributedText = NSMutableAttributedString(string: second)
        firstAttributedText.append(secondAttributedText)
        return firstAttributedText
    }
}
