//
//  AddBalanceViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 06/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class AddBalanceViewController: UIViewController {

    @IBOutlet weak var proceedToPayButton: UIButton!
    @IBOutlet weak var infoTextField: UITextView!
    @IBOutlet weak var amountTextField: UITextField!
    
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationBar(titleText: "Add Balance To Wallet", isBackButtonRequired: true)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        infoTextField.text = ""
        amountTextField.text = ""
        
        proceedToPayButton.addTarget(self, action: #selector(proceedToPayButtonAction), for: .touchUpInside)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.amountTextField.tintColor = ThemeColor.buttonColor
        self.amountTextField.setPlaceholder(placeholder: "Enter Amount to add to wallet", color: UIColor.gray)
        self.amountTextField.addButtomBorder()
        self.amountTextField.setTextFieldImage(image: FAType.FAGbp, direction: .left)
        self.amountTextField.keyboardType = .numberPad
        
        self.infoTextField.tintColor = ThemeColor.buttonColor
        self.infoTextField.layer.cornerRadius = 3
        self.infoTextField.layer.borderWidth = 1.0
        self.infoTextField.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.infoTextField.clipsToBounds = true
        self.infoTextField.tintColor = ThemeColor.buttonColor
    }
    
    @objc func proceedToPayButtonAction(){
        
        let amount = Int(self.amountTextField.text ?? "0")
        guard let finalAmount = amount, finalAmount > 0 else{
            self.tabBarController?.view.makeToast(message: "Please enter a valid amount")
            return
        }
        
        if infoTextField.text.isBlank{
            self.tabBarController?.view.makeToast(message: "Please Enter Description")
            return
        }
        
        let destination = GenericPaymentViewController(nibName: "GenericViewController", bundle: nil)
        destination.delegate = self
        destination.totalPriceFloat = Float(finalAmount)
        self.navigationController?.pushViewController(destination, animated: true)
    }
    

    fileprivate func addBalanceToWallet(paymentID: String){
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "amount": amountTextField.text ?? "0",
            "pg_transaction_id": paymentID,
            "info": infoTextField.text ?? "",
        ] as [String : String]
        Alamofire.request(Config.apiEndPoint + "walletEntry", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "walletStatusChange"), object: nil)
                        
                        let alertController = UIAlertController(title: "Success", message: "\(CURRENCY_SYMBOL)\(self.amountTextField.text ?? "0") has been credit your wallet successfully.", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                    
                }
            }
    }

}


extension AddBalanceViewController: GenericPaymentViewControllerDelegate{
    func onPaymentSuccess(paymentID: String, paymentResponse: NSDictionary?) {
        addBalanceToWallet(paymentID: paymentID)
    }
    
    func onPaymentFailure() {
        print("Payment failure")
    }
    
    func onPaymentCancelled() {
        print("Payment Cancelled")
    }
    
    
}
