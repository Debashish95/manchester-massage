//
//  ProductCategoryViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 16/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
class ProductCategoryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var cartButton: UIButton!
    var rightBarButton: ENMBadgedBarButtonItem!
    
    var wishButton: UIButton!
    var rightBarButton2: ENMBadgedBarButtonItem!
    
    var activityIndicator: NVActivityIndicatorView!
    
    var data = [ProductCategoryModel]()
    var isComingFromSamePage = false
    var pageName = ""
    
    var isFromFinalOrder = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if isFromFinalOrder{
            isFromFinalOrder = false
            let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "MyOrderListViewController") as! MyOrderListViewController
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ProductCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCategoryCollectionViewCell")
        
        
        cartButton = UIButton(type: .custom)
        cartButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        cartButton.addTarget(self, action: #selector(self.productCartButtonAction), for: .touchUpInside)
        cartButton.setLeftImage(image: FAType.FAShoppingCart, color: .black, state: .normal)
        rightBarButton = ENMBadgedBarButtonItem(customView: cartButton, value: "0")
        rightBarButton.badgeBackgroundColor = .red
        rightBarButton.badgeTextColor = .white
        rightBarButton.badgeValue = self.appD.itemsInCart
        
        wishButton = UIButton(type: .custom)
        wishButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        wishButton.addTarget(self, action: #selector(self.wishListButtonAction), for: .touchUpInside)
        wishButton.setLeftImage(image: FAType.FAHeart, color: .black, state: .normal)
        rightBarButton2 = ENMBadgedBarButtonItem(customView: wishButton, value: "0")
        rightBarButton2.badgeBackgroundColor = .red
        rightBarButton2.badgeTextColor = .white
        rightBarButton2.badgeValue = ""
//        self.navigationItem.rightBarButtonItems = [rightBarButton, rightBarButton2]
        
        if !isComingFromSamePage{
            getProductCategory()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeValue), name: NSNotification.Name("changePhysicalCart"), object: nil)
        getCartList()
        getWishist()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isComingFromSamePage{
            self.setNavigationBar(titleText: pageName, isBackButtonRequired: true, isRightMenuRequired: true)
        }else{
            self.setHomePageNavBr(title: "Product Category", isMenuRequired: true)
//            self.setNavigationBar(titleText: "Product Category", isBackButtonRequired: false, isRightMenuRequired: true)
        }
    }
    
    override func menuButtonAction() {
        let destination = LeftViewController(nibName: "LeftViewController", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        destination.parentVC = self
        self.definesPresentationContext = true
        self.topViewController?.present(destination, animated: false, completion: nil)
    }
    
    @objc func updateBadgeValue(){
        rightBarButton.badgeValue = self.appD.itemsInCart
        rightBarButton2.badgeValue = ""
    }
    
    fileprivate func getProductCategory(){
        data.removeAll()
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetProductcat?key=\(Config.key)&salt=\(Config.salt)"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<result.count{
                        let currentResult = result[index] as? NSDictionary ?? NSDictionary()
                        let cat_id = currentResult["cat_id"] as? String ?? ""
                        let parent_cat_id = currentResult["parent_cat_id"] as? String ?? ""
                        let cat_nm = currentResult["cat_nm"] as? String ?? ""
                        let cat_url = currentResult["cat_url"] as? String ?? ""
                        let thumb = currentResult["thumb"] as? String ?? ""
                        
                        let subcats = currentResult["subcats"] as? NSArray ?? NSArray()
                        var finalSubCats = [ProductCategoryModel]()
                        for index1 in 0..<subcats.count{
                            let currentResult1 = subcats[index1] as? NSDictionary ?? NSDictionary()
                            let cat_id1 = currentResult1["cat_id"] as? String ?? ""
                            let parent_cat_id1 = currentResult1["parent_cat_id"] as? String ?? ""
                            let cat_nm1 = currentResult1["cat_nm"] as? String ?? ""
                            let cat_url1 = currentResult1["cat_url"] as? String ?? ""
                            let thumb1 = currentResult1["thumb"] as? String ?? ""
                            
                            let currentSubCategory = ProductCategoryModel(cat_id: cat_id1, parent_cat_id: parent_cat_id1, cat_nm: cat_nm1, cat_url: cat_url1, thumb: thumb1)
                            finalSubCats.append(currentSubCategory)
                        }
                        
                        let finalData = ProductCategoryModel(cat_id: cat_id, parent_cat_id: parent_cat_id, cat_nm: cat_nm, cat_url: cat_url, thumb: thumb, subcats: finalSubCats)
                        self.data.append(finalData)
                       
                    }
                }
                
                self.collectionView.reloadData()
                
                if self.data.count == 0{
                    let alert = UIAlertController(title: "Alert!", message: "No product category found!!", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .default) { (_) in
                        self.backButtonAction()
                    }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        
    }
    
    func getCartList(){
        self.activityIndicator.startAnimating()
        self.appD.cartList.removeAll()
        let reuestURL = Config.mainUrl + "Data/Getcart?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                self.activityIndicator.stopAnimating()
                guard response.result.isSuccess else {
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    var itemsInCart = 0
                    for index in 0..<result.count{
                        let currentResult = result[index] as? NSDictionary ?? NSDictionary()

                        let prod_id = currentResult["prod_id"] as? String ?? ""
                        let prod_name = currentResult["prod_name"] as? String ?? ""
                        let unit_price = currentResult["unit_price"] as? String ?? "0"
                        let price = currentResult["price"] as? String ?? "0"
                        let qnty = currentResult["qnty"] as? String ?? "0"
                        let thumb = currentResult["thumb"] as? String ?? ""
                        let base_price = currentResult["base_price"] as? String ?? ""
                        let discount_price = currentResult["discount_price"] as? String ?? ""
                        itemsInCart += (Int(qnty) ?? 0)
                        let finalCartProduct = ProductCartModel(prod_id: prod_id, prod_name: prod_name, unit_price: unit_price, price: price, base_price: base_price, discount_price: discount_price, qnty: qnty, thumb: thumb)
                        self.appD.cartList.append(finalCartProduct)
                        
                    }
                    self.appD.itemsInCart = "\(itemsInCart)"
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changePhysicalCart"), object: nil)
            }
    }
    
    @objc func getWishist(){
        self.appD.wishList.removeAll()
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getwishlist?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<result.count{
                        let currentProduct = result[index] as? NSDictionary ?? NSDictionary()
                        let prod_id = currentProduct["prod_id"] as? String ?? ""
                        let p_name = currentProduct["p_name"] as? String ?? ""
                        let is_featured = currentProduct["is_featured"] as? String ?? ""
                        let cat_id = currentProduct["cat_id"] as? String ?? ""
                        let brand_id = currentProduct["brand_id"] as? String ?? ""
                        let p_url = currentProduct["p_url"] as? String ?? ""
                        let base_price = currentProduct["base_price"] as? String ?? "0.0"
                        let discount_price = currentProduct["discount_price"] as? String ?? ""
                        let thumb = currentProduct["thumb"] as? String ?? ""
                        var finalData = ProductListModel(
                            prod_id: prod_id, p_name: p_name, is_featured: is_featured, cat_id: cat_id, brand_id: brand_id, p_url: p_url, base_price: base_price, discount_price: discount_price, thumb: thumb
                        )
                        finalData.isInCart = self.appD.cartList.contains(where: {$0.prod_id == prod_id})
                        if finalData.isInCart{
                            finalData.cartQuantity = self.appD.cartList.first(where: {$0.prod_id == prod_id})?.qnty ?? "0"
                        }
                        self.appD.wishList.append(finalData)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changePhysicalCart"), object: nil)
                }
            }
    }
    
    override func backButtonAction() {
        if isComingFromSamePage{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

extension ProductCategoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCategoryCollectionViewCell", for: indexPath) as! ProductCategoryCollectionViewCell
        cell.data = data[indexPath.item]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItems = UIDevice.current.userInterfaceIdiom == .pad ? 3 : 2
        let cellWidth = (collectionView.bounds.width / CGFloat(numberOfItems))
        return CGSize(width: cellWidth - 10, height: cellWidth * 1.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.data[indexPath.item].subcats.count > 0{
            let destination = ProductCategoryViewController(nibName: "ProductCategoryViewController", bundle: nil)
            destination.data = self.data[indexPath.item].subcats
            destination.pageName = self.data[indexPath.item].cat_nm
            destination.isComingFromSamePage = true
            self.navigationController?.pushViewController(destination, animated: true)
        }else{
            let destination = ProductListViewController(nibName: "ProductListViewController", bundle: nil)
            destination.category_id = self.data[indexPath.item].cat_id
            destination.pageTitle = self.data[indexPath.item].cat_nm
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
    }
}
