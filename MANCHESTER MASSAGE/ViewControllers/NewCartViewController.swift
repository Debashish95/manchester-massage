//
//  NewCartViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 18/02/22.
//  Copyright © 2022 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class NewCartViewController: UIViewController {

    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicator: NVActivityIndicatorView!
    
    var totalPrice = "\(CURRENCY_SYMBOL)0" {
        didSet{
            tableView.reloadData()
            updateButtonStatus()
        }
    }
    
    var serviceDetailsData: ServiceDetailsModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "Cart Summary", isBackButtonRequired: true, isRightMenuRequired: false)
        
        tableView.register(UINib(nibName: "NewCartTableViewCell", bundle: nil), forCellReuseIdentifier: "NewCartTableViewCell")
        tableView.register(UINib(nibName: "CartLastTableViewCell", bundle: nil), forCellReuseIdentifier: "CartLastTableViewCell")
        tableView.estimatedRowHeight = 200
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = true
        bottomHeight.constant = (self.tabBarController?.tabBar.bounds.height ?? 0) + 5
        
        confirmButton.addTarget(self, action: #selector(confirmPaymentButtonAction), for: .touchUpInside)
        
        if appD.serviceCart.count == 0 {
            showNoDataAlert()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateCartPrice()
        confirmButton.layer.cornerRadius = confirmButton.bounds.height / 2
        confirmButton.clipsToBounds = true
    }
    
    fileprivate func showNoDataAlert() {
        let alert = UIAlertController(title: "Alert!", message: "Your cart is empty.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            self.backButtonAction()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func updateCartPrice(){
        self.tableView.isHidden = (self.appD.serviceCart.count == 0)
        self.confirmButton.isHidden =  self.tableView.isHidden
        self.confirmButton.backgroundColor = ThemeColor.buttonColor
        var totalPriceFloat = Float(0.0)
        for index in 0..<self.appD.serviceCart.count{
            let priceString = self.appD.serviceCart[index].final_amount
            let price = Float(priceString) ?? 0.0
            totalPriceFloat += price
        }
        totalPrice = "Grand Total: \(CURRENCY_SYMBOL)\(String(totalPriceFloat).formattedString())"
    }

    @objc func addMoreServices(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
        self.appD.window?.rootViewController = destination
        self.appD.window?.makeKeyAndVisible()
    }
    
    fileprivate func resetTimeSlots(_ sender: UIButton) {
        appD.serviceCart[sender.tag].bookingTime = ""
        appD.serviceCart[sender.tag].bookingDate = ""
        appD.serviceCart[sender.tag].therapists.removeAll()
    }
    
    @objc func yesButtionAction(sender: UIButton){
        if appD.serviceCart[sender.tag].isForMe { return }
        appD.serviceCart[sender.tag].isForMe = true
        resetTimeSlots(sender)
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
        updateButtonStatus()
    }
    
    @objc func noButtionAction(sender: UIButton){
        if !appD.serviceCart[sender.tag].isForMe { return }
        appD.serviceCart[sender.tag].isForMe = false
        resetTimeSlots(sender)
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
        updateButtonStatus()
    }
    
    fileprivate func updateButtonStatus() {
        confirmButton.enable()
        appD.serviceCart.forEach { service in
            if service.therapists.isEmpty || service.bookingDate.isEmpty || service.bookingTime.isEmpty {
                confirmButton.disable()
                return
            }
        }
    }
    
    @objc func toolButtonActions(sender: UIButton) {
        switch sender.accessibilityLabel {
        case "edit":
            editButtonAction(index: sender.tag)
            break
        case "remove":
            removeButtonAction(index: sender.tag)
            break
        case "note":
            noteButtonAction(index: sender.tag)
            break
        case "timeslot":
            openTimeSlotPopUp(index: sender.tag)
            break
        default:
            break
        }
    }
    
    @objc func confirmPaymentButtonAction() {
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Alert!", message: "Please review your booking date, time and therapist(s) before proceed to payment.", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Proceed", style: .destructive) { _ in
                let destination = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                self.navigationController?.pushViewController(destination, animated: true)
            }
            
            let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(okButton)
            alertController.addAction(cancelButton)
            self.present(alertController, animated: false, completion: nil)
        }
    }
    
    fileprivate func removeButtonAction(index: Int) {
        let alert = UIAlertController(title: "Alert!", message: "Are you sure you want to delete the service?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (alertAction) in
            self.appD.serviceCart.remove(at: index)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
            
            self.tableView.reloadData()
            self.updateButtonStatus()
            self.updateCartPrice()
            if self.appD.serviceCart.count == 0{
                self.showNoDataAlert()
            }
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    fileprivate func editButtonAction(index: Int) {
        let catId = self.appD.serviceCart[index].cat_id
        self.setData(catID: catId, index: index)
    }
    
    func setData(catID:String, index: Int){
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                _ = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? "0"
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(smId: (itmDict["sm_id"] as? String ?? ""), serviceID: (itmDict["service_id"] as? String ?? ""), catId: (itmDict["cat_id"] as? String ?? ""), serviceTitle: (itmDict["service_title"] as? String ?? ""), serviceItems: serviceItemsList)
                                    
                                    self.serviceDetailsData = serviceDataTemp
                                }
                                
                                let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
                                destination.modalPresentationStyle = .overCurrentContext
                                self.definesPresentationContext = true
                                destination.delegate = self
                                destination.fromPage = .cartPage
                                destination.serviceData = self.serviceDetailsData
                                destination.cartItemIndex = index
                                destination.selectedIndex = self.serviceDetailsData.serviceItems.firstIndex(where: { $0.serviceID == (self.appD.serviceCart[index].services.first?.serviceID ?? "")}) ?? 0
                                self.present(destination, animated: true, completion: nil)
                            }
                            
                            
        }
        
    }
    
    fileprivate func noteButtonAction(index: Int) {
        let destination = NotesPopUp(nibName: "NotesPopUp", bundle: nil)
        destination.notesHeaderText = "Notes for \(self.appD.serviceCart[index].services.first?.name ?? "service")"
        destination.notesBodyText = self.appD.serviceCart[index].notes
        destination.index = index
        destination.delegate = self
        destination.modalPresentationStyle = .overFullScreen
        self.present(destination, animated: false, completion: nil)
    }
    
    fileprivate func openTimeSlotPopUp(index: Int) {
        let destination = NewTimeSlotPopUp(nibName: "NewTimeSlotPopUp", bundle: nil)
        destination.currentService = appD.serviceCart[index]
        destination.delegate = self
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        present(destination, animated: true, completion: nil)
    }
}

extension NewCartViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        section == 0 ? appD.serviceCart.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartLastTableViewCell", for: indexPath) as! CartLastTableViewCell
            cell.grandTotalLabel.text = totalPrice
            cell.addMore.addTarget(self, action: #selector(addMoreServices), for: .touchUpInside)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewCartTableViewCell", for: indexPath) as! NewCartTableViewCell
        cell.delegate = self
        cell.cartData = appD.serviceCart[indexPath.row]
        
        cell.yesButton.tag = indexPath.row
        cell.yesButton.addTarget(self, action: #selector(yesButtionAction(sender:)), for: .touchUpInside)
        
        cell.noButton.tag = indexPath.row
        cell.noButton.addTarget(self, action: #selector(noButtionAction(sender:)), for: .touchUpInside)
        
        cell.selectTimeSlotButton.layer.cornerRadius = 5
        cell.selectTimeSlotButton.clipsToBounds = true
        cell.selectTimeSlotButton.accessibilityLabel = "timeslot"
        cell.selectTimeSlotButton.tag = indexPath.row
        cell.selectTimeSlotButton.addTarget(self, action: #selector(toolButtonActions(sender:)), for: .touchUpInside)
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.accessibilityLabel = "remove"
        cell.removeButton.addTarget(self, action: #selector(toolButtonActions(sender:)), for: .touchUpInside)
        
        cell.editButton.tag = indexPath.row
        cell.editButton.accessibilityLabel = "edit"
        cell.editButton.addTarget(self, action: #selector(toolButtonActions(sender:)), for: .touchUpInside)
        
        cell.notesButton.tag = indexPath.row
        cell.notesButton.accessibilityLabel = "note"
        cell.notesButton.addTarget(self, action: #selector(toolButtonActions(sender:)), for: .touchUpInside)
        
        cell.nameTextField.tag = indexPath.row
        cell.emailTextField.tag = indexPath.row
        
        cell.selectTimeSlotButton.setTitle("Select Date/Time", for: .normal)
        cell.selectTimeSlotButton.setTitle("Change Date/Time", for: .selected)
        cell.selectTimeSlotButton.isSelected = !cell.cartData.bookingTime.isEmpty
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 360
        }
        return UITableView.automaticDimension
    }
}

extension NewCartViewController: NotesPopUpDelegate {
    func cancelButtonAction(textView: UITextView, index: Int) {
        
    }
    func saveButtonAction(textView: UITextView, index: Int) {
        if self.appD.serviceCart.count > index && index > -1{
            self.appD.serviceCart[index].notes = textView.text
            tableView.reloadData()
            updateButtonStatus()
        }
    }
}

extension NewCartViewController: NewCartTableViewCellDelegate {
    func getTextFieldValue(textField: UITextField) {
        let index = textField.tag
        if textField.accessibilityLabel == "name" {
            appD.serviceCart[index].cust_name = textField.text ?? ""
        } else if textField.accessibilityLabel == "email" {
            appD.serviceCart[index].cust_email = textField.text ?? ""
        }
        print("Name: \(appD.serviceCart[index].cust_name) || \(appD.serviceCart[index].cust_email)")
    }
}

extension NewCartViewController: NewTimeSlotPopUpDelegate {
    func getTimeSlot(service: CartModel,
                     date: String,
                     time: String,
                     therapists: [EmployeeModel]) {
        
        if let index = appD.serviceCart.firstIndex(where: {$0 == service}) {
            appD.serviceCart[index].bookingDate = date
            appD.serviceCart[index].bookingTime = time
            appD.serviceCart[index].therapists = therapists
           
            
            let df = DateFormatter()
            df.dateFormat = "hh:mm a"
            if let bookingStartTime = df.date(from: time),
               let bookingDuration = Int(appD.serviceCart[index].services.first?.duration ?? "0"){
                if let bookingEndTime = Calendar.current.date(
                    byAdding: .minute,
                    value: bookingDuration,
                    to: bookingStartTime) {
                    appD.serviceCart[index].bookingEndTime = df.string(from: bookingEndTime)
                }
            }
            
            
            tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
            updateButtonStatus()
        }
    }
}

extension NewCartViewController: BookNowPopUpDelegate {
    func addServiceButtonActions() {
        updateCartPrice()
    }
}
