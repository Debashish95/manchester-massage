//
//  ServiceListViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 19/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ServiceListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicator: NVActivityIndicatorView!
    var serviceList = [ServiceModel]()
    var serviceDetailsData: ServiceDetailsModel!
    
    var cartButton: UIButton!
    var rightBarButton: ENMBadgedBarButtonItem!
    
    var parentCategoryId = ""
    var isBeautyService = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        if !isBeautyService{
            self.setNavigationBar(titleText: "Massage Services", isBackButtonRequired: false, isRightMenuRequired: true)
        }else{
            self.setNavigationBar(titleText: "Beauty Treatments", isBackButtonRequired: false, isRightMenuRequired: true)
        }
        
        self.navigationItem.leftBarButtonItem = nil
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "ServiceListTableViewCell", bundle: nil), forCellReuseIdentifier: "ServiceListTableViewCell")
        serviceList.removeAll()
        
        if isBeautyService{
            parentCategoryId = appD.parentCategoryData.first(where: {$0.cat_nm.lowercased().contains("beauty")})?.cat_id ?? ""
        }else{
            parentCategoryId = appD.selectedParentCategory.cat_id
        }
        getServiceData()
        
        cartButton = UIButton(type: .custom)
        cartButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        cartButton.addTarget(self, action: #selector(self.cartButtonAction), for: .touchUpInside)
        cartButton.setLeftImage(image: FAType.FAShoppingCart, color: .black, state: .normal)
        rightBarButton = ENMBadgedBarButtonItem(customView: cartButton, value: "0")
        rightBarButton.badgeBackgroundColor = .red
        rightBarButton.badgeTextColor = .white
        rightBarButton.badgeValue = "\(self.appD.serviceCart.count)"
        self.navigationItem.rightBarButtonItem = rightBarButton

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeValue), name: NSNotification.Name("changeCart"), object: nil)
    }
    
    @objc func updateBadgeValue(){
        rightBarButton.badgeValue = "\(self.appD.serviceCart.count)"
    }
    
    @objc func bookNowButtonAction(sender: UIButton){
        self.setData(catID: serviceList[sender.tag].serviceId)
    }
    
    @objc func detailsButtonAction(sender: UIButton){
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = serviceList[sender.tag].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getServiceData(){
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetSubCats?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&parent_cat_id=\(parentCategoryId)"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            
            let results = resposeJSON["result"] as? NSArray ?? NSArray()
            for result in results{
                let currentResult = result as? NSDictionary ?? NSDictionary()
                _ = currentResult["parent_cat_id"] as? String ?? ""
                let cat_id = currentResult["cat_id"] as? String ?? ""
                let cat_nm = currentResult["cat_nm"] as? String ?? ""
                let thumb = currentResult["thumb"] as? String ?? ""
                _ = currentResult["banner"] as? String ?? ""
                let desc = currentResult["intro_txt"] as? String ?? ""
                let cleanup_time = currentResult["cleanup_time"] as? String ?? ""
                let duration = currentResult["duration"] as? String ?? ""
                let review_count = currentResult["review_count"] as? String ?? "0"
                let review_avrg = currentResult["review_avrg"] as? String ?? "0"
               
                let cleanUpInt = (Int(cleanup_time) ?? 0) * 60
                let durationInt = (Int(duration) ?? 0) * 60
                let finalDuration = durationInt - cleanUpInt
                
                let maxPrice = currentResult["max_price"] as? String ?? "0"
                let price = currentResult["min_offer"] as? String ?? "0"
                
                let discountFloat = ((Double(maxPrice) ?? 0) - (Double(price) ?? 0)) / (Double(maxPrice) ?? .leastNonzeroMagnitude)
                var discount = 0
                if discountFloat.isFinite{
                    discount = Int(discountFloat * 100)
                }
                
                let discountString = "\(discount)% off"
                
                let serviceData = ServiceModel(serviceId: cat_id, serviceName: cat_nm, serviceImage: thumb, serviceDescription: desc, servicePrice: price, serviceMaxPrice: maxPrice , discount: discountString , duration: finalDuration.secondsToTime(), cleanUpTime: cleanUpInt.secondsToTime(), rating: review_avrg ,reviewCount: review_count)
                self.serviceList.append(serviceData)
            }
            
            
            
            
            self.tableView.reloadData()
        }
    }
    
    
    func setData(catID:String){
        
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                _ = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? ""
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(smId: (itmDict["sm_id"] as? String ?? ""), serviceID: (itmDict["service_id"] as? String ?? ""), catId: (itmDict["cat_id"] as? String ?? ""), serviceTitle: (itmDict["service_title"] as? String ?? ""), serviceItems: serviceItemsList)
                                    
                                    self.serviceDetailsData = serviceDataTemp
                                }
                                
                                let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
                                destination.modalPresentationStyle = .overCurrentContext
                                self.definesPresentationContext = true
                                destination.delegate = self
                                destination.fromPage = .serviceList
                                destination.serviceData = self.serviceDetailsData
                                self.present(destination, animated: true, completion: nil)
                            }
                            
                            
                          }
        
    }
    
    
}

extension ServiceListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceListTableViewCell", for: indexPath) as! ServiceListTableViewCell
        cell.setData(data: serviceList[indexPath.row], isBeauty: self.isBeautyService)
        cell.backView.layer.cornerRadius = 20
        cell.backView.clipsToBounds = true
        cell.bookButton.tag = indexPath.row
        cell.detailsButton.tag = indexPath.row
        cell.bookButton.addTarget(self, action: #selector(self.bookNowButtonAction(sender:)), for: .touchUpInside)
        cell.detailsButton.addTarget(self, action: #selector(self.detailsButtonAction(sender:)), for: .touchUpInside)
        cell.ratingView.isHidden = false
        cell.ratingView.settings.fillMode = .precise
        cell.ratingView.settings.textFont = .boldSystemFont(ofSize: 14)
        cell.ratingView.rating = Double(serviceList[indexPath.row].rating) ?? 0
        cell.ratingView.text = "(\(serviceList[indexPath.row].reviewCount) reviews)"
        cell.discountValue.layer.cornerRadius = 10//5
        cell.discountValue.clipsToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = serviceList[indexPath.row].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}
extension ServiceListViewController: BookNowPopUpDelegate{
    func confirmButtonAction() {
        
    }
    
    func addServiceButtonActions() {
//        if UserDefaults.standard.fetchData(forKey: .skipLogin){
//            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
//            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
//                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
//                destination.isFromLeftMenu = true
//                let navigationController = UINavigationController(rootViewController: destination)
//                navigationController.modalPresentationStyle = .fullScreen
//                self.present(navigationController, animated: false, completion: nil)
//            }
//            alert.addAction(yesAction)
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//
//            }
//            alert.addAction(cancelAction)
//
//            self.present(alert, animated: false, completion: nil)
//        }else{
            let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
            self.navigationController?.pushViewController(detination, animated: true)
       // }
        
    }
}
