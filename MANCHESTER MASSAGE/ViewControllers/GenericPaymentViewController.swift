//
//  GenericViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 23/05/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Stripe
import CreditCardForm
import NVActivityIndicatorView
import Alamofire

class GenericPaymentViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var mySavedCardButton: UIButton!
    @IBOutlet weak var mySavedCardLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var saveCardButton: UIButton!
    @IBOutlet weak var cardNameTextField: UITextField!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var creditCardForm: CreditCardFormView!
    
    var activityIndicator: NVActivityIndicatorView!
    let stripeBackendURL = "https://manchester-massage-test.herokuapp.com/"
    var totalPriceFloat = Float(0.0)
    var paymentTextField = STPPaymentCardTextField()
    var delegate: GenericPaymentViewControllerDelegate?
    
    var savedCardList = [CardModel]()
    var savedCardCVV = ""
    var useSavedCard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        self.setNavigationBar(titleText: "Payment", isBackButtonRequired: true, isRightMenuRequired: false)
        
        payNowButton.setTitle("Pay \(CURRENCY_SYMBOL)\(String(totalPriceFloat).formattedString())", for: .normal)
        payNowButton.addTarget(self, action: #selector(payNowButtonAction), for: .touchUpInside)
        setupCardView()
        
//        bottomHeight.constant = (self.tabBarController?.tabBar.bounds.height ?? 0) + 5
        paymentTextField.frame = self.cardView.bounds
        paymentTextField.delegate = self
        paymentTextField.postalCodeEntryEnabled = false
        paymentTextField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cardView.addSubview(paymentTextField)
        
        creditCardForm.paymentCardTextFieldDidChange(cardNumber: paymentTextField.cardNumber ?? "", expirationYear: UInt(paymentTextField.expirationYear) , expirationMonth: UInt(paymentTextField.expirationMonth), cvc: paymentTextField.cvc)
        
        
        saveCardButton.setImage(UIImage(icon: .FASquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        saveCardButton.setImage(UIImage(icon: .FACheckSquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .selected)
        saveCardButton.setTitle("Save this card for future use", for: .normal)
        saveCardButton.setTitleColor(UIColor.black, for: .normal)
      
        saveCardButton.addTarget(self, action: #selector(self.saveCardButtonAction), for: .touchUpInside)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SavedCardTableViewCell", bundle: nil), forCellReuseIdentifier: "SavedCardTableViewCell")
        
        mySavedCardButton.addTarget(self, action: #selector(self.mySavedCardButtonAction), for: .touchUpInside)
//        self.getSavedCardList()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        cardNameTextField.addBottomBorderWithColor(color: .gray, width: 1)
    }
    
    func setupCardView(){
        cardNameTextField.borderStyle = .none
        cardNameTextField.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        cardNameTextField.setPlaceholder(placeholder: "Card Holder Name", color: .gray)
        cardNameTextField.setRightViewFAIcon(icon: .FAUser, textColor: .gray)
        cardNameTextField.delegate = self
        cardNameTextField.text = "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))"
        cardNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        
        creditCardForm.cardHolderString = "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))"
        creditCardForm.cardNumberFont = UIFont.boldSystemFont(ofSize: 18)
        creditCardForm.cardPlaceholdersFont = UIFont.systemFont(ofSize: 14, weight: .medium)
        creditCardForm.cardTextFont = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func mySavedCardButtonAction(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MySavedCardViewController") as! MySavedCardViewController
        destination.delegate = self
        destination.totalPriceFloat = self.totalPriceFloat
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    fileprivate func paymentAction() {
        var fourDigitDate = ""
        var exp_month = ""
        var cardNumber = ""
        var cardCVV = ""
        var nameOnCard = ""
        if useSavedCard{
            let savedCard = self.savedCardList.first(where: {$0.isSelected}) ?? CardModel()
            fourDigitDate = savedCard.expYear
            exp_month = savedCard.expMonth
            cardNumber = savedCard.last4Digit
            cardCVV = savedCard.cvvText
        }else{
            cardNumber = paymentTextField.cardNumber ?? ""
            exp_month = "\(paymentTextField.expirationMonth)"
            let df = DateFormatter()
            df.dateFormat = "yy"
            guard let dt = df.date(from: paymentTextField.formattedExpirationYear ?? "") else { return }
            df.dateFormat = "yyyy"
            fourDigitDate = df.string(from: dt)
            cardCVV = paymentTextField.cvc ?? ""
            nameOnCard = cardNameTextField.text ?? ""
        }
        
        if cardNumber.isEmpty{
            self.tabBarController?.view.makeToast(message: "Please Enter Card Number")
            return
        }
        if cardCVV.isEmpty {
            self.tabBarController?.view.makeToast(message: "Please Enter Card CVV")
            return
        }
        
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId),
            "amount": "\(totalPriceFloat)",
            "tran_id": "PMP\(String().randomString(length: 12))",
            "card_number": cardNumber,
            "card_exp_month": exp_month,
            "card_exp_year": fourDigitDate,
            "card_cvc": cardCVV,
            "save_card": saveCardButton.isSelected ? "1" : "0",
            "new_card": useSavedCard ? "0" : "1",
            "name_on_card": nameOnCard
        ]as [String:String]
        
        Alamofire.request("\(Config.apiEndPoint)stripe_charge_card", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate().responseJSON { (response) in
                
                print(response)
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                self.activityIndicator.stopAnimating()
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    self.delegate?.onPaymentFailure()
                    self.navigationController?.popViewController(animated: true)
                    break
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let json = (jSONResponse as? NSDictionary ?? NSDictionary())
                    self.delegate?.onPaymentSuccess(paymentID: json["id"] as? String ?? "", paymentResponse: json)
                    self.navigationController?.popViewController(animated: true)
                    break
                }
            }
    }
    
    @objc func payNowButtonAction(){
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "PLEASE NOTE",
                                      message: """
When making your payment to Manchester Massage via this site, please be aware that the transaction will appear on your bank statement as PamperTree who act as our 3rd party advertisers.
""", preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "I UNDERSTAND", style: .destructive) { _ in
            self.paymentAction()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func saveCardButtonAction(){
        saveCardButton.isSelected = !saveCardButton.isSelected
    }
    
    func getSavedCardList() {
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/getCardListStripe?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
            if((result["status"] as? Int ?? 0) == 1){
                let finalCardResult = result["result"] as? NSDictionary ?? NSDictionary()
                if let _ = finalCardResult["error"] as? NSDictionary{
                    self.tabBarController?.view.makeToast(message: "Unable to fetch your cards.")
                }else{
                    let cardListData = finalCardResult["data"] as? NSArray ?? NSArray()
                    var cardData = CardModel()
                    for card in cardListData {
                        let cardItem = card as? NSDictionary ?? NSDictionary()
                        cardData.cardID = cardItem["id"] as? String ?? ""
                        let cardObject = cardItem["card"] as? NSDictionary ?? NSDictionary()
                        cardData.brand = cardObject["brand"] as? String ?? ""
                        cardData.expMonth = String(cardObject["exp_month"] as? Int ?? 0)
                        cardData.expYear = String(cardObject["exp_year"] as? Int ?? 0)
                        cardData.last4Digit = cardObject["last4"] as? String ?? ""
                        self.savedCardList.append(cardData)
                    }
                    
                    self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
                }
            }else{
                self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
            }
            
            self.tableView.reloadData()
            
        }
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if useSavedCard{
            for index in 0..<savedCardList.count{
                savedCardList[index].isSelected = false
            }
            useSavedCard = false
            self.tableView.reloadData()
        }
        
        creditCardForm.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: UInt(textField.expirationYear), expirationMonth: UInt(textField.expirationMonth), cvc: textField.cvc)
    }
    
    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidEndEditingExpiration(expirationYear: UInt(textField.expirationYear))
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidBeginEditingCVC()
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidEndEditingCVC()
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        for index in 0 ..< self.savedCardList.count{
            if self.savedCardList[index].isSelected{
                self.savedCardList[index].cvvText = textField.text ?? ""
            }
        }
        
    }

}

extension GenericPaymentViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCardList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardTableViewCell", for: indexPath) as! SavedCardTableViewCell
        cell.data = self.savedCardList[indexPath.row]
        cell.cvvTextField.delegate = self
        cell.cvvTextField.keyboardType = .numberPad
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for index in 0 ..< self.savedCardList.count{
            if indexPath.row == index{
                if self.savedCardList[index].isSelected{
                    self.savedCardList[index].isSelected = false
                    useSavedCard = false
                }else{
                    self.savedCardList[index].isSelected = true
                    useSavedCard = true
                }
            }else{
                self.savedCardList[index].isSelected = false
            }
            self.savedCardList[index].cvvText = ""
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension GenericPaymentViewController: STPAuthenticationContext{
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}

extension GenericPaymentViewController: UITextFieldDelegate{
    @objc func textFieldDidChange(_ textField: UITextField) {
        creditCardForm.cardHolderString = textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cardNameTextField {
            textField.resignFirstResponder()
            paymentTextField.becomeFirstResponder()
        } else if textField == paymentTextField  {
            textField.resignFirstResponder()
        }
        return true
    }
}


protocol GenericPaymentViewControllerDelegate {
    func onPaymentSuccess(paymentID: String, paymentResponse: NSDictionary?)
    func onPaymentFailure()
    func onPaymentCancelled()
}

extension GenericPaymentViewController: MySavedCardViewControllerDelegate{
    func response(data: [String : Any]?, error: [String : String]?) {
        if data != nil,
            let paymentID = data?["id"] as? String,
           let json = data?["json"] as? NSDictionary{
            self.delegate?.onPaymentSuccess(paymentID: paymentID, paymentResponse: json)
            self.navigationController?.popViewController(animated: true)
        }else{
            let alert = UIAlertController(title: "Alert!", message: "Sorry! Something went wrong. Please try again!!", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
