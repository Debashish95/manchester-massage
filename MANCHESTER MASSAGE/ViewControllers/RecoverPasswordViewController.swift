//
//  ViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 13/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class RecoverPasswordViewController: UIViewController {
    
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var verifyOTPView: UIView!
    
    @IBOutlet weak var newPasswordText: UITextField!
    @IBOutlet weak var reEnterText: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var verifyOTPButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    
    var emailText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Recover Password", isBackButtonRequired: true)
        newPasswordView.isHidden = true
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.newPasswordText.isSecureTextEntry = true
        self.reEnterText.isSecureTextEntry = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let destination = OTPViewController(nibName: "OTPViewController", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        destination.delegate = self
        self.present(destination, animated: false, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        
        
        self.newPasswordText.setPlaceholder(placeholder: "Enter your new password", color: .gray)
        self.newPasswordText.layer.cornerRadius = self.newPasswordText.frame.height / 2
        self.newPasswordText.layer.borderColor = UIColor.gray.cgColor
        self.newPasswordText.layer.borderWidth = 1.0
        self.newPasswordText.clipsToBounds = true
        self.newPasswordText.enablePasswordEye()
        
        
        self.reEnterText.setPlaceholder(placeholder: "Re-enter new password", color: .gray)
        self.reEnterText.layer.cornerRadius = self.reEnterText.frame.height / 2
        self.reEnterText.layer.borderColor = UIColor.gray.cgColor
        self.reEnterText.layer.borderWidth = 1.0
        self.reEnterText.clipsToBounds = true
        self.reEnterText.enablePasswordEye()
        
        self.otpTextField.keyboardType = .numberPad
        self.otpTextField.setPlaceholder(placeholder: "OTP", color: .gray)
        self.otpTextField.layer.cornerRadius = self.otpTextField.frame.height / 2
        self.otpTextField.layer.borderColor = UIColor.gray.cgColor
        self.otpTextField.layer.borderWidth = 1.0
        self.otpTextField.clipsToBounds = true
        
        self.changePasswordButton.backgroundColor = ThemeColor.buttonColor
        self.changePasswordButton.layer.cornerRadius = self.changePasswordButton.frame.height / 2
        self.changePasswordButton.clipsToBounds = true
        self.changePasswordButton.setTitleColor(.white, for: .normal)
        self.changePasswordButton.setTitle("Change Password", for: .normal)
        self.changePasswordButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        self.changePasswordButton.addTarget(self, action: #selector(self.changePasswordButtonAction), for: .touchUpInside)
        
        self.verifyOTPButton.backgroundColor = ThemeColor.buttonColor
        self.verifyOTPButton.layer.cornerRadius = self.changePasswordButton.frame.height / 2
        self.verifyOTPButton.clipsToBounds = true
        self.verifyOTPButton.setTitleColor(.white, for: .normal)
        self.verifyOTPButton.setTitle("Next", for: .normal)
        self.verifyOTPButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        self.verifyOTPButton.addTarget(self, action: #selector(self.verifyOTPButtonAction), for: .touchUpInside)
    }
    override func backButtonAction(){
        if !verifyOTPView.isHidden{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.verifyOTPView.isHidden = false
            self.newPasswordView.isHidden = true
        }
        
    }
    
    @objc func verifyOTPButtonAction(){
        self.verifyOTPView.isHidden = true
        self.newPasswordView.isHidden = false
    }
    
    @objc func changePasswordButtonAction(){
        //        backButtonAction()
        if (self.otpTextField.text?.isBlank ?? false){
            self.view.makeToast(message: "Please enter the OTP sent to your registered email addresss.")
            return
        }
        
        if (self.newPasswordText.text?.isBlank ?? false){
            self.view.makeToast(message: "Please enter the new password.")
            return
        }
        
        if (self.reEnterText.text?.isBlank ?? false){
            self.view.makeToast(message: "Please enter the new password again.")
            return
        }
        
        if newPasswordText.text != reEnterText.text{
            self.view.makeToast(message: "Both passwords do not match.")
            return
        }
        
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "email": emailText.toData,
            "method": "otp".toData,
            "otp": self.otpTextField.text!.toData,
            "new_pass": newPasswordText.text!.toData
            ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + Config.api.changePassword , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    let status = result["status"] as? String ?? ""
                    let message = result["status"] as? String ?? ""
                    
                    if status == "Ok"{
                       let alert = UIAlertController(title: "Success!", message: "Password Changed successfully.", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        self.navigationController?.popViewController(animated: true)
                       }))
                       self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    else{
                        self.view.makeToast(message: message)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    
}

extension RecoverPasswordViewController: OTPViewControllerDelegate{
    func getEmailAddress(email: String) {
        self.emailText = email
    }
    
    
}
