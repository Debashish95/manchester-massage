//
//  ManageBookingViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 02/02/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import MapKit
import Contacts
import CoreLocation
import MessageUI

class ManageBookingViewController: UIViewController, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var shareCardImage: UIImageView!
    @IBOutlet weak var shareCardButtonImage: UIImageView!
    @IBOutlet weak var shareCardButton: UIButton!
    @IBOutlet weak var shareCardView: UIView!
    
    @IBOutlet weak var rebookImage: UIImageView!
    @IBOutlet weak var rebookButtonImage: UIImageView!
    @IBOutlet weak var rebookButton: UIButton!
    @IBOutlet weak var rebookView: UIView!
    
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var bookingCancelLabel: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var addressSnapView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cancellationPolicyLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var recieptButton: UIButton!
    @IBOutlet weak var callVenueButton: UIButton!
    @IBOutlet weak var getDirectionButton: UIButton!
    
    @IBOutlet weak var cancelImage: UIImageView!
    @IBOutlet weak var recieptImage: UIImageView!
    @IBOutlet weak var callImage: UIImageView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var weatherView: UIView!
    @IBOutlet weak var receiptView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var reviewImage: UIImageView!
    
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var reviewButtonImage: UIImageView!
    @IBOutlet weak var reviewButton: UIButton!
    
    @IBOutlet weak var checkWeatherButton: UIButton!
    @IBOutlet weak var weatherImage: UIImageView!
    var activityIndicator: NVActivityIndicatorView!
    var bookingData: BookingsModel!
    var currentReview: ReviewModel!
    
    var orderID = ""
    var isFirstTimeLoad = true
    var isComingFromPastBooking = false
    var currentRescheduleIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
//        cancellationPolicyLabel.text = "\u{2022} If you are unable to attend your booking you can cancel it but please give the venue as much as warning as possible."
//        reschedulePolicyLabel.text = "\u{2022} You can reschedule your booking up to 1 hour(s) before your appointment. So do not worry if your plans change between now and then."
        self.orderID = bookingData.orderid
        bookingCancelLabel.isHidden = !(bookingData.status == "2")
        self.setNavigationBar(titleText: "Manage Booking", isBackButtonRequired: true, isRightMenuRequired: false)
        scrollView.frame = UIScreen.main.bounds
        
        tableView.register(UINib(nibName: "BookSummaryTableViewCell", bundle: nil), forCellReuseIdentifier: "BookSummaryTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableViewHeight.constant = CGFloat(150 * bookingData.services.count)
        
        notesLabel.text = ""
        
//        if bookingData.notes.count > 0{
//            notesLabel.numberOfLines = 0
//            let notesHeading: NSMutableAttributedString =  NSMutableAttributedString(string: "Notes")
//            notesHeading.addAttribute(NSAttributedString.Key.foregroundColor, value: ThemeColor.buttonColor, range: NSMakeRange(0, notesHeading.length))
//            notesHeading.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 17, weight: .semibold), range: NSMakeRange(0, notesHeading.length))
//            let notesBody = NSMutableAttributedString(string: bookingData.notes)
//            notesBody.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15, weight: .regular), range: NSMakeRange(0, notesBody.length))
//            notesHeading.append(NSAttributedString(string: "\n"))
//            notesHeading.append(notesBody)
//            notesLabel.attributedText = notesHeading
//        }
        
        getDirectionButton.addTarget(self, action: #selector(addressButtonAction(sender:)), for: .touchUpInside)
        getDirectionButton.accessibilityValue = addressLabel.text
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeReview), name: NSNotification.Name("hideReviewButton"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        if isFirstTimeLoad{
            isFirstTimeLoad = false
            setupView()
        }
        
    }
    
    @objc func removeReview(notification: NSNotification){
//        self.reviewView.isHidden = true
        self.reviewView.isHidden = !(isComingFromPastBooking)
        self.reviewButton.isSelected = true
        let reviewInfo = (notification.userInfo! as NSDictionary)
        bookingData.reviews = ReviewModel(reviewer_name: reviewInfo["r_name"] as? String ?? "",
                                          reviewer_email: reviewInfo["r_email"] as? String ?? "",
                                          review: reviewInfo["review_text"] as? String ?? "",
                                          service_review: reviewInfo["service_review"] as? String ?? "0",
                                          staff_review: reviewInfo["staff_review"] as? String ?? "0",
                                          cleanliness_review: reviewInfo["cleanliness_review"] as? String ?? "",
                                          overall_review: reviewInfo["overall_review"] as? String ?? "0",
                                          created_on: reviewInfo["created_on"] as? String ?? "")
    }
    @objc func gotoMapView(){
        let accessValue = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = accessValue
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            
            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    
    func setupView(){
        
        cancelButton.setTitle("Cancel Booking", for: .normal)
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        cancelImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        cancelButton.setTitleColor(.black, for: .normal)
        cancelButton.contentHorizontalAlignment = .left
        cancelButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        cancelButton.addTarget(self, action: #selector(self.cancelButtonAction), for: .touchUpInside)
        cancelView.isHidden = (isComingFromPastBooking || bookingData.status == "2")
       // cancelImage.isHidden = cancelButton.isHidden
        heightConstraint.constant = (bookingData.status == "2") ? 50 : 0
        recieptButton.setTitle("Reciept", for: .normal)
        recieptButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        recieptImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        recieptButton.setTitleColor(.black, for: .normal)
        recieptButton.contentHorizontalAlignment = .left
        recieptButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        recieptButton.addTarget(self, action: #selector(self.receiptButtonAction), for: .touchUpInside)
        
        callVenueButton.setTitle("Call or SMS", for: .normal)
        callVenueButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        callImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        callVenueButton.setTitleColor(.black, for: .normal)
        callVenueButton.contentHorizontalAlignment = .left
        callVenueButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        callVenueButton.addTarget(self, action: #selector(callButtonAction), for: .touchUpInside)
        
        checkWeatherButton.setTitle("Check Weather", for: .normal)
        checkWeatherButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        weatherImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        checkWeatherButton.setTitleColor(.black, for: .normal)
        checkWeatherButton.contentHorizontalAlignment = .left
        checkWeatherButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        checkWeatherButton.addTarget(self, action: #selector(checkWeatherAction), for: .touchUpInside)
        
        reviewButton.setTitle("Add Review", for: .normal)
        reviewButton.setTitle("View Review", for: .selected)
        reviewButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        reviewButtonImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        reviewButton.setTitleColor(.black, for: .normal)
        reviewButton.contentHorizontalAlignment = .left
        reviewButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        reviewImage.image = UIImage(icon: .FAStarO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor)
        reviewButton.addTarget(self, action: #selector(self.goToReviewPage), for: .touchUpInside)
        
//        reviewView.isHidden = !(isComingFromPastBooking && bookingData.is_review == "0")
        
         reviewView.isHidden = !(isComingFromPastBooking)
         reviewButton.isSelected = (bookingData.is_review == "1")
         
        if bookingData.status == "2"{
            reviewView.isHidden = true
        }
        
        
        rebookButton.setTitle("Rebook", for: .normal)
        rebookButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        rebookButtonImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        rebookButton.setTitleColor(.black, for: .normal)
        rebookButton.contentHorizontalAlignment = .left
        rebookButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        rebookImage.image = UIImage(icon: .FAUndo, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor)
        rebookButton.addTarget(self, action: #selector(self.rebookAction), for: .touchUpInside)
        
        rebookView.isHidden = !(bookingData.status == "2" || isComingFromPastBooking)
        
        shareCardButton.setTitle("Share Booking", for: .normal)
        shareCardButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        shareCardButtonImage.image = UIImage(icon: .FAAngleRight, size: CGSize(width: 30, height: 30))
        shareCardButton.setTitleColor(.black, for: .normal)
        shareCardButton.contentHorizontalAlignment = .left
        shareCardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        shareCardImage.image = UIImage(icon: .FAShareAlt, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor)
        shareCardButton.addTarget(self, action: #selector(self.shareCardAction(sender:)), for: .touchUpInside)
        
        cancelView.dropShadowView()
        receiptView.dropShadowView()
        callView.dropShadowView()
        weatherView.dropShadowView()
        reviewView.dropShadowView()
        rebookView.dropShadowView()
        shareCardView.dropShadowView()
        
        
        
        addressLabel.text = "Manchester Massage \n28a Swan street, Manchester Northern Quarters, M4 5JQ, UK"
        addressLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        addressLabel.numberOfLines = 3
        
        let addressGesture = UITapGestureRecognizer(target: self, action: #selector(addressButtonAction(sender:)))
        addressGesture.numberOfTapsRequired = 1
        addressLabel.isUserInteractionEnabled = true
        addressLabel.accessibilityValue = addressLabel.text
        addressLabel.addGestureRecognizer(addressGesture)
        getDirectionButton.setSupportButton(image: .FALocationArrow, title: "Get Direction")
        getDirectionButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        getDirectionButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        getDirectionButton.setFATitleColor(color: ThemeColor.buttonColor, forState: .normal)
        getDirectionButton.setLeftImage(image: .FALocationArrow, color: ThemeColor.buttonColor, state: .normal)
        getDirectionButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        getDirectionButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        getDirectionButton.addTarget(self, action: #selector(addressButtonAction(sender:)), for: .touchUpInside)
        getDirectionButton.accessibilityValue = addressLabel.text
        
        
        shareButton.setSupportButton(image: .FAShareAlt, title: "Share")
        shareButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        shareButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        shareButton.setFATitleColor(color: ThemeColor.buttonColor, forState: .normal)
        shareButton.setLeftImage(image: .FAShareAlt, color: ThemeColor.buttonColor, state: .normal)
        shareButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        shareButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        shareButton.addTarget(self, action: #selector(shareButtonAction(sender:)), for: .touchUpInside)
        
        
        let latitude = Double(self.appD.vanue_latitude) ?? 0.0
        let longitude = Double(self.appD.vanue_longitude) ?? 0.0
        let mapSnapshotOptions = MKMapSnapshotter.Options()

        // Set the region of the map that is rendered.
        let location = CLLocationCoordinate2DMake(latitude, longitude)
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapSnapshotOptions.region = region

        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale

        // Set the size of the image output.
        mapSnapshotOptions.size = addressSnapView.frame.size

        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true

        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        snapShotter.start { (snapshot:MKMapSnapshotter.Snapshot?, error: Error?) in
            guard let image = snapshot?.image else{
                return
            }
            self.addressSnapView.image = image
            self.addressSnapView.contentMode = .scaleAspectFill
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.gotoMapView))
            self.addressSnapView.isUserInteractionEnabled = true
            self.addressSnapView.addGestureRecognizer(tapGesture)
        }
        
    }
    
    @objc func shareCardAction(sender: UIButton){
        
        guard let url = URL(string: bookingData.share_url) else {
            return
        }
        let shareAll = [url]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func convertDateFormater1(_ date: String) -> String
    {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "E, dd MMM yyyy"
        return  dateFormatter.string(from: date)

    }
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func rebookAction(){
        self.appD.serviceCart.removeAll()
        for index in 0..<bookingData.services.count{
            let currentService = bookingData.services[index]
            let serviceItem = ServiceItemsModel(serviceID: currentService.serviceId, name: currentService.serviceName, duration: currentService.duration, cleanupTime: currentService.cleanup_time, main_price: currentService.basePrice, offerPrice: currentService.subTotal, therapistNumber: "\(currentService.therapistArr.components(separatedBy: ",").count)")
            
            var cartData =  CartModel(itemId: (self.appD.serviceCart.count + 1), services: [serviceItem], cat_id: currentService.cart_id, vendor_id: "4", tot_amount: currentService.basePrice, final_amount: currentService.subTotal)
            cartData.serviceID = currentService.serviceId
            cartData.therapists.removeAll()
            cartData.isForMe = currentService.custName == ("\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))")
            cartData.cust_name = cartData.isForMe ? "" : currentService.custName
            self.appD.serviceCart.append(cartData)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
        
        let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
        self.navigationController?.pushViewController(detination, animated: true)

    }
    @objc func callButtonAction(){

        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "0161 834 9213 - Land Line", style: .default) { action -> Void in

             if let urls = URL(string: "tel://01618349213"), UIApplication.shared.canOpenURL(urls){
                                  UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                              }
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "07415 831969 - Mobile", style: .default) { action -> Void in
          self.callButtonActionNew()
        
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }

        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)


        // present an actionSheet...
        // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad

        actionSheetController.popoverPresentationController?.sourceView = self.callVenueButton // works for both iPhone & iPad

        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
        
        
        
    }
    
    @objc func goToReviewPage(){
        let destination = PostReviewViewController(nibName: "PostReviewViewController", bundle: nil)
        destination.bookingID = self.bookingData.booking_id
        destination.reviewData = self.bookingData.reviews
        self.navigationController?.pushViewController(destination, animated: true)
    }
    @objc func callButtonActionNew(){

        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Call", style: .default) { action -> Void in

                    if let urls = URL(string: "tel://07415831969"), UIApplication.shared.canOpenURL(urls){
                       UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                   }
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "SMS", style: .default) { action -> Void in

            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Enter a message details here";
            messageVC.recipients = ["07415831969"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }

        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)


        // present an actionSheet...
        // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad

        actionSheetController.popoverPresentationController?.sourceView = self.callVenueButton // works for both iPhone & iPad

        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
        
        
        
    }
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
        case .failed:
            print("Message failed")
        case .sent:
            print("Message was sent")
        default:
            return
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func addressButtonAction(sender: Any){

        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }

            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    
    @objc func cancelButtonAction(){
        if isChangeBookingPossible(bookingDate: bookingData.bookdt, bookingTime: bookingData.timeslot){
            let destination = CancelPopUpViewController(nibName: "CancelPopUpViewController", bundle: nil)
            destination.delegate = self
            destination.modalPresentationStyle = .overCurrentContext
            self.definesPresentationContext = true
            self.present(destination, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Alert!", message: "Booking can't be cancelled in last 24 Hours.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
            }
            alert.addAction(yesAction)
            self.present(alert, animated: false, completion: nil)
        }
        
    }
    
    func cancelBookingWebService(){
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "orderid": (orderID).toData,
            "cust_id": (UserDefaults.standard.fetchData(forKey: .userId)).toData
            ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "CancelBooking" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    
                    let status = resposeJSON["status"] as? String ?? ""
                    let message = resposeJSON["message"] as? String ?? ""
                    
                    if status == "Y"{
                        let alert = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
//                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                            let navigationController = UINavigationController(rootViewController: destination)
//                            self.appD.window?.rootViewController = navigationController
//                            self.appD.window?.makeKeyAndVisible()
                            self.navigationController?.popViewController(animated: true)
                        }
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else{
                        self.tabBarController?.view.makeToast(message: message)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    @objc func bookAgainAction(sender: UIButton){
        
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = bookingData.services[sender.tag].cart_id
        destination.isFromBookingPage = true
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func rescheduleAction(sender: UIButton){
        
        if !isChangeBookingPossible(bookingDate: bookingData.services[sender.tag].bookingDate, bookingTime: bookingData.services[sender.tag].bookingTime){
            let alert = UIAlertController(title: "Alert!", message: "Booking can't be rescheduled in last 24 Hours.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
            }
            alert.addAction(yesAction)
            self.present(alert, animated: false, completion: nil)
            return
        }
        
        let destination = RescheduleBooking(nibName: "RescheduleBooking", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        var currentCart = [CartModel]()
        bookingData.services.forEach { (currentService) in
            
            let currentServiceData = ServiceItemsModel(serviceID: currentService.serviceId, name: currentService.serviceName, duration: currentService.duration, cleanupTime: currentService.cleanup_time, main_price: currentService.basePrice, offerPrice: currentService.subTotal, therapistNumber: "\(currentService.therapistArr.components(separatedBy: ",").count)")
            
            let isForMe = false
                
//                (currentService.custName == "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \((UserDefaults.standard.fetchData(forKey: .userLastName)))")
            
            var therapistIDs = bookingData.services[sender.tag].therapistArr.components(separatedBy: ",").map({$0.replacingOccurrences(of: "]", with: "")})
            
            therapistIDs = therapistIDs.map({$0.replacingOccurrences(of: "[", with: "")})
            var finalTherapists = self.appD.employeeDetails.filter { (emp) -> Bool in
                return therapistIDs.contains(where: {$0 == emp.id})
            }
            if finalTherapists.count == 0{
                finalTherapists = self.getMyTherapists(data: bookingData.services[sender.tag].therapistArr)
            }
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            df.timeZone = .current
            let dateData = df.date(from: currentService.bookingDate) ?? Date()
            df.dateFormat = "EEEE"
            let finalDate = df.string(from: dateData)
            
            let tempCart = CartModel(itemId: (currentCart.count + 1), services: [currentServiceData], cat_id: currentService.cart_id, vendor_id: bookingData.salon_id, tot_amount: bookingData.servicePrice, final_amount: currentService.subTotal, isForMe: isForMe, bookingTime: currentService.bookingTime, bookingEndTime: currentService.bookEndTime, bookingDay: finalDate, bookingDate: currentService.bookingDate, cust_name: currentService.custName, cust_email: bookingData.customerEmail, cust_phone: bookingData.cust_phone, therapists: finalTherapists, serviceID: currentService.serviceId, qty: currentService.qty, other_name: currentService.custName)
            currentCart.append(tempCart)
           
        }
        if currentCart.count > 0{
            destination.bookedServices = currentCart
            destination.currentCartService = currentCart[sender.tag]
            destination.startingIndex = sender.tag
            destination.delegate = self
            destination.delegate = self
            self.currentRescheduleIndex = sender.tag
    //        destination.servicesArray = bookingData.services.map({$0.serviceId})
            self.present(destination, animated: true, completion: nil)
        }else{
            self.tabBarController?.view.makeToast(message: "No Services found to reschedule.")
        }
        
    }
    
}

extension ManageBookingViewController: CancelPopUpViewControllerDelegate{
    func confirmCancellationAction() {
//        let alert = UIAlertController(title: "Alert!", message: "Are you sure you want to cancel this booking?", preferredStyle: .alert)
//        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (action) in
//            self.cancelBookingWebService()
//        }
//        let noAction = UIAlertAction(title: "No", style: .cancel) { (action) in
//        }
//        alert.addAction(yesAction)
//        alert.addAction(noAction)
//        self.present(alert, animated: false, completion: nil)
        self.cancelBookingWebService()
    }
    
    
    @objc func shareButtonAction(sender: UIButton){
        let text = "Manchester Massage 28a Swan St, Manchester M4 5JQ, United Kingdom https://maps.app.goo.gl/MzoBQbszekTKRdAx8"
        let shareAll = [text]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func checkWeatherAction(){
        
        if let urls = URL(string: "https://www.accuweather.com/en/gb/manchester/m4-5/weather-forecast/329260"), UIApplication.shared.canOpenURL(urls){
            UIApplication.shared.open(urls, options: [:], completionHandler: nil)
        }
    }
    
    @objc func receiptButtonAction(){
        if let urls = URL(string: "https://booking.manchestermassage.co.uk/receipt/\(bookingData.orderid)"), UIApplication.shared.canOpenURL(urls){
            UIApplication.shared.open(urls, options: [:], completionHandler: nil)
        }
    }
    
    func isChangeBookingPossible(bookingDate: String, bookingTime: String) -> Bool{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        df.timeZone = .current
        let dateString = "\(bookingDate) \(bookingTime)"
        let bookingDate = df.date(from: dateString) ?? Date()
        let bookingChangePossibleDate = Calendar.current.date(byAdding: .hour, value: -24, to: bookingDate) ?? Date()
        return Date()<=bookingChangePossibleDate
    }
}


extension ManageBookingViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookingData.services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookSummaryTableViewCell", for: indexPath) as! BookSummaryTableViewCell
        cell.data = [
            "serviceName": bookingData.services[indexPath.row].serviceName,
            "userName": "For " + bookingData.services[indexPath.row].custName,
            "therapistArr": bookingData.services[indexPath.row].therapistArr,
            "time": bookingData.services[indexPath.row].bookingTime + " " + convertDateFormater1(bookingData.services[indexPath.row].bookingDate),
            "rescheduleTitle": (isComingFromPastBooking ? "Book Again" : "Reschedule"),
            "notes": bookingData.services[indexPath.row].notes
        ]
        cell.rescheduleButton.isHidden = ((bookingData.status == "2") && !isComingFromPastBooking)
        cell.rescheduleButton.tag = indexPath.row
        if isComingFromPastBooking{
            cell.rescheduleButton.addTarget(self, action: #selector(self.bookAgainAction(sender:)), for: .touchUpInside)
        }else{
            cell.rescheduleButton.addTarget(self, action: #selector(self.rescheduleAction(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}



extension ManageBookingViewController: RescheduleBookingDelegate{
    func selectPopUpData(data: [CartModel]) {
        
        
        
        var currentServiceCart = [[String: Any]()]
        currentServiceCart.removeAll()
        let requestURL = Config.apiEndPoint + "ResheduleBooking"
        if currentRescheduleIndex >= 0 && currentRescheduleIndex < data.count{
            for index in 0..<currentRescheduleIndex{
                let currentService =  bookingData.services[index]
                var therapistIDs = currentService.therapistArr.components(separatedBy: ",").map({$0.replacingOccurrences(of: "]", with: "")})
                therapistIDs = therapistIDs.map({$0.replacingOccurrences(of: "[", with: "")})
                if therapistIDs.count == 0{
                    therapistIDs = self.getMyTherapists(data: currentService.therapistArr).map({$0.id})
                }
                let serviceData = [
                    "id": currentService.serviceId,
                    "name": currentService.serviceName,
                    "qty": "1",
                    "price": currentService.basePrice,
                    "subtotal": currentService.subTotal,
                    "therapist": therapistIDs.joined(separator: "|").replacingOccurrences(of: "\"", with: ""),
                    "cust_name": currentService.custName,
                    "book_dt": currentService.bookingDate,
                    "book_tym": currentService.bookingTime,
                    "book_end_tym": currentService.bookEndTime,
                ]
                currentServiceCart.append(serviceData)
            }
            
            let selectedTherapist = data[currentRescheduleIndex].therapists.map({$0.id})
            let currentService =  data[currentRescheduleIndex].services.first!
            let serviceData11 = [
                "id": currentService.serviceID,
                "name": currentService.name,
                "qty": "1",
                "price": currentService.main_price,
                "subtotal": currentService.offerPrice,
                "therapist": selectedTherapist.joined(separator: "|"),
                "cust_name": data[currentRescheduleIndex].cust_name,
                "book_dt": data[currentRescheduleIndex].bookingDate,
                "book_tym": data[currentRescheduleIndex].bookingTime,
                "book_end_tym":data[currentRescheduleIndex].bookingEndTime,
            ]
            currentServiceCart.append(serviceData11)
            if (currentRescheduleIndex + 1) < bookingData.services.count{
                for index in (currentRescheduleIndex + 1)..<bookingData.services.count{
                    let currentService =  bookingData.services[index]
                    var therapistIDs = currentService.therapistArr.components(separatedBy: ",").map({$0.replacingOccurrences(of: "]", with: "")})
                    therapistIDs = therapistIDs.map({$0.replacingOccurrences(of: "[", with: "")})
                    if therapistIDs.count == 0{
                        therapistIDs = self.getMyTherapists(data: currentService.therapistArr).map({$0.id})
                    }
                    let serviceData = [
                        "id": currentService.serviceId,
                        "name": currentService.serviceName,
                        "qty": "1",
                        "price": currentService.basePrice,
                        "subtotal": currentService.subTotal,
                        "therapist": therapistIDs.joined(separator: "|").replacingOccurrences(of: "\"", with: ""),
                        "cust_name": currentService.custName,
                        "book_dt": currentService.bookingDate,
                        "book_tym": currentService.bookingTime,
                        "book_end_tym": currentService.bookEndTime,
                    ]
                    currentServiceCart.append(serviceData)
                }
            }
            
            do{
                let jsonData = try? JSONSerialization.data(withJSONObject: currentServiceCart, options: [])
                
                var jsonDataString = String(bytes: jsonData!, encoding: .utf8)
                jsonDataString = jsonDataString?.replacingOccurrences(of: "\\", with: "")
                
                let parameter = [
                    "orderid": bookingData.orderid.toData,
                    "cust_id": bookingData.cust_id.toData,
                    "bookdt": bookingData.bookdt.toData,
                    "timeslot": bookingData.timeslot.toData,
                    "services": jsonDataString?.toData ?? Data(),
                    "key": Config.key.toData,
                    "salt": Config.salt.toData
                ]as [String : Data]
                
                //=========>
                let paramsHead = ["Content-Type": "multipart/form-data"]
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameter{
                        multipartFormData.append(value, withName: key)
                    }
                }, usingThreshold: UInt64.init(), to: requestURL , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
                    
                    switch encodingResult{
                    case .failure( _):
                        self.activityIndicator.stopAnimating()
                        break;
                    case .success(let request, true, _):
                        request.responseJSON(completionHandler: { (response) in
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            
                            let status = resposeJSON["status"] as? String ?? ""
                            let message = resposeJSON["message"] as? String ?? ""
                            
                            if status == "Y"{
                                let alert = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
                                let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    //                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                    //                            let navigationController = UINavigationController(rootViewController: destination)
                                    //                            self.appD.window?.rootViewController = navigationController
                                    //                            self.appD.window?.makeKeyAndVisible()
                                    self.navigationController?.popViewController(animated: true)
                                }
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                self.tabBarController?.view.makeToast(message: message)
                            }
                        })
                    default:
                        self.activityIndicator.stopAnimating()
                        self.tabBarController?.view.makeToast(message: Message.apiError)
                        break;
                    }})
                //=========>
            }catch{
                
            }
            
            
        }
    }
    
    func getMyTherapists(data: String)-> [EmployeeModel]{
        var employeeDataFinal = [EmployeeModel]()
        if let data = data.data(using: .utf8){
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String]
                {
                    jsonArray.forEach { (empDataStr) in
                        if let empData = self.appD.employeeDetails.filter({$0.id == empDataStr}).first{
                            employeeDataFinal.append(empData)
                        }
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
        return employeeDataFinal
    }
}
