//
//  ProductReviewViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 19/05/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Cosmos

class ProductReviewViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    
    var products = ProductCartModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setNavigationBar(titleText: "Post Review", isBackButtonRequired: true)
        
        
        self.productNameLabel.text = products.prod_name
        self.ratingView.rating = 0
        self.ratingView.settings.fillMode = .precise
        self.reviewTextView.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.productImageView.kf.setImage(with: URL(string: products.thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
            self.productImageView.contentMode = .scaleAspectFill
        }
        self.productImageView.layer.cornerRadius = self.productImageView.bounds.height / 2
        self.productImageView.layer.borderWidth = 1.0
        self.productImageView.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.productImageView.clipsToBounds = true
        
        self.reviewTextView.layer.cornerRadius = 3
        self.reviewTextView.layer.borderWidth = 1.0
        self.reviewTextView.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.reviewTextView.clipsToBounds = true
        self.reviewTextView.text = ""
        self.reviewTextView.tintColor = ThemeColor.buttonColor
    }

}
