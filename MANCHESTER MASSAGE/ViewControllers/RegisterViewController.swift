//
//  RegisterViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 14/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import FBSDKLoginKit
import Firebase
import GoogleSignIn
//import TwitterKit
import AuthenticationServices

class RegisterViewController: UIViewController, UITextFieldDelegate, UNUserNotificationCenterDelegate {

    @IBOutlet weak var appleSignInView: UIView!
    @IBOutlet weak var profileImageBackView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var uploadButton: UIButton!
    
    
    @IBOutlet weak var term2Button: UIButton!
    @IBOutlet weak var passwordInfoLabel: UILabel!
    
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var referencePicker: APJTextPickerView!
    @IBOutlet weak var invalidReferalCode: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var orLabel: UILabel!
    var activityIndicator: NVActivityIndicatorView!
    
    var referenceData = [ReferenceModel]()
    var imagePicker: ImagePicker!
    
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var referralCodeText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Registration", isBackButtonRequired: true)
        self.signinButton.addTarget(self, action: #selector(self.signinButtonAction), for: .touchUpInside)
        
        self.forgotPasswordButton.addTarget(self, action: #selector(self.forgotPasswordButtonAction), for: .touchUpInside)
        self.facebookButton.addTarget(self, action: #selector(self.facebookButtonAction), for: .touchUpInside)
        self.googleButton.addTarget(self, action: #selector(self.googleButtonAction), for: .touchUpInside)
//        self.twitterButton.addTarget(self, action: #selector(self.twitterLoginaction), for: .touchUpInside)
        
        self.twitterButton.isHidden = true
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.termsButton.setLeftImage(image: FAType.FASquareO, color: .gray, state: .normal)
        self.termsButton.setLeftImage(image: FAType.FACheckSquareO, color: .gray, state: .selected)
        
        self.termsButton.setTitle("I accept to", for: .normal)
        self.termsButton.setTitle("I accept to", for: .selected)
        
        self.term2Button.setTitle("terms and conditions", for: .normal)
        self.term2Button.setTitle("terms and conditions", for: .selected)
        
        self.termsButton.setTitleColor(UIColor.gray, for: [.normal, .selected])
        self.term2Button.setTitleColor(UIColor.gray, for: [.normal, .selected])
        
        self.signinButton.backgroundColor = ThemeColor.buttonColor
        self.signinButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.signinButton.clipsToBounds = true
        self.signinButton.setTitleColor(.white, for: .normal)
        self.signinButton.setTitle("REGISTER", for: .normal)
        
        self.loginButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        self.loginButton.setTitle("Already have an account? Login", for: .normal)
        self.loginButton.addTarget(self, action: #selector(self.loginButtonAction), for: .touchUpInside)
        
        self.forgotPasswordButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        self.forgotPasswordButton.setTitle("Forgot password?", for: .normal)
        
        self.facebookButton.backgroundColor = ThemeColor.facebookButtonColor
        self.facebookButton.layer.cornerRadius = self.facebookButton.frame.height / 2
        self.facebookButton.clipsToBounds = true
        self.facebookButton.setTitleColor(.white, for: .normal)
        self.facebookButton.setTitle("", for: .normal)
        self.facebookButton.setFAIcon(icon: .FAFacebook, iconSize: 30, forState: .normal)
        
        self.googleButton.backgroundColor = ThemeColor.googleButtonColor
        self.googleButton.layer.cornerRadius = self.facebookButton.frame.height / 2
        self.googleButton.clipsToBounds = true
        self.googleButton.setTitleColor(.white, for: .normal)
        self.googleButton.setTitle("", for: .normal)
        self.googleButton.setFAIcon(icon: .FAGoogle, iconSize: 30, forState: .normal)
        
        self.twitterButton.backgroundColor = ThemeColor.twitterButtonColor
        self.twitterButton.layer.cornerRadius = self.facebookButton.frame.height / 2
        self.twitterButton.clipsToBounds = true
        self.twitterButton.setTitleColor(.white, for: .normal)
        self.twitterButton.setTitle("", for: .normal)
        self.twitterButton.setFAIcon(icon: .FATwitter, iconSize: 30, forState: .normal)
        
        self.instagramButton.backgroundColor = ThemeColor.instagramButtonColor
        self.instagramButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.instagramButton.clipsToBounds = true
        self.instagramButton.setTitleColor(.white, for: .normal)
        self.instagramButton.setTitle("Sign up with Instagram", for: .normal)
        self.instagramButton.setLeftImage(image: .FAInstagram)
        self.instagramButton.isHidden = true
        self.passwordInfoLabel.textColor = .red
        self.invalidReferalCode.textColor = .red
        signupReference()
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        uploadButton.backgroundColor = ThemeColor.buttonColor
        profileImageBackView.layer.cornerRadius = profileImageBackView.bounds.height / 2
        profileImageBackView.clipsToBounds = true
        
        profileImageView.layer.cornerRadius = profileImageView.bounds.height / 2
        profileImageView.clipsToBounds = true
        profileImageView.contentMode = .scaleAspectFill
        
        uploadButton.setTitle("Upload", for: .normal)
        uploadButton.setImage(UIImage(icon: .FAUpload, size: CGSize(width: 20, height: 20), textColor: .white), for: .normal)
        
        uploadButton.setTitle("Edit", for: .selected)
        uploadButton.setImage(UIImage(icon: .FAPencil, size: CGSize(width: 20, height: 20), textColor: .white), for: .selected)
        
        uploadButton.isSelected = (profileImageView.image != UIImage(named: "ic_account"))
        
        uploadButton.addTarget(self, action: #selector(uploadImageButtonAction(sender:)), for: .touchUpInside)
        
        referralCodeText.isHidden = true
        setUpSignInAppleButton()
        
        self.fullNameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        self.referralCodeText.text = ""
        self.phoneNumberTextField.text = ""
        
        self.invalidReferalCode.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func setUpSignInAppleButton() {
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
            authorizationButton.cornerRadius = appleSignInView.frame.height / 2
            authorizationButton.frame = appleSignInView.bounds
            self.appleSignInView.addSubview(authorizationButton)
        } else {
            // Fallback on earlier versions
        }
    }
    
    fileprivate func initialSetUp() {
        self.fullNameTextField.addButtomBorder()
        self.phoneNumberTextField.addButtomBorder()
        self.emailTextField.addButtomBorder()
        self.passwordTextField.addButtomBorder()
        self.referencePicker.addButtomBorder()
        self.referralCodeText.addButtomBorder()
        
        self.fullNameTextField.setTextFieldImage(image: FAType.FAUser, direction: .right)
        self.fullNameTextField.keyboardType = .namePhonePad
        self.phoneNumberTextField.setTextFieldImage(image: FAType.FAPhone, direction: .right)
        self.phoneNumberTextField.keyboardType = .phonePad
        self.emailTextField.setTextFieldImage(image: FAType.FAEnvelope, direction: .right)
        self.emailTextField.keyboardType = .emailAddress
        self.referralCodeText.keyboardType = .namePhonePad
        
        self.passwordTextField.isSecureTextEntry = true
        self.passwordTextField.enablePasswordToggle()
        
        self.passwordTextField.delegate = self
        self.referralCodeText.delegate = self
        
        
        //        self.passwordTextField.setTextFieldImage(image: FAType.FALock, direction: .right)
        //        self.passwordTextField.isSecureTextEntry = true
        self.fullNameTextField.font = .systemFont(ofSize: 17)
        self.fullNameTextField.setPlaceholder(placeholder: "Full Name", color: UIColor.gray)
        self.phoneNumberTextField.font = .systemFont(ofSize: 17)
        self.phoneNumberTextField.setPlaceholder(placeholder: "Phone No.", color: UIColor.gray)
        self.emailTextField.font = .systemFont(ofSize: 17)
        self.emailTextField.setPlaceholder(placeholder: "Email ID", color: UIColor.gray)
        self.passwordTextField.font = .systemFont(ofSize: 17)
        self.passwordTextField.setPlaceholder(placeholder: "Password", color: UIColor.gray)
        
        self.referralCodeText.font = .systemFont(ofSize: 17)
        self.referralCodeText.setPlaceholder(placeholder: "Referral Code", color: UIColor.gray)
        
        self.termsButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.term2Button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.termsButton.addTarget(self, action: #selector(self.termsAction(sender:)), for: .touchUpInside)
        term2Button.addTarget(self, action: #selector(openTermsLink), for: .touchUpInside)
        
        referencePicker.setRightViewFAIcon(icon: .FAAngleDown, textColor: .gray, size: CGSize(width: 30, height: 30))
        referencePicker.setPlaceholder(placeholder: "Where did you first hear about us?", color: UIColor.gray)
        referencePicker.type = .strings
        referencePicker.font = .systemFont(ofSize: 17)
        referencePicker.dataSource = self
        referencePicker.pickerDelegate = self
        
        
        femaleButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        femaleButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        femaleButton.setTitle("Female", for: UIControl.State())
        femaleButton.setTitleColor(.darkGray, for: .normal)
        femaleButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        femaleButton.tag = 101
        femaleButton.addTarget(self, action: #selector(genderSelection(sender:)), for: .touchUpInside)
        femaleButton.isSelected = true
        
        maleButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        maleButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        maleButton.setTitle("Male", for: UIControl.State())
        maleButton.setTitleColor(.darkGray, for: .normal)
        maleButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        maleButton.tag = 102
        maleButton.addTarget(self, action: #selector(genderSelection(sender:)), for: .touchUpInside)
        
        yesButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        yesButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        yesButton.setTitle("Yes", for: UIControl.State())
        yesButton.setTitleColor(.darkGray, for: .normal)
        yesButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        yesButton.addTarget(self, action: #selector(referralButtonAction(sender:)), for: .touchUpInside)
        yesButton.isSelected = false
        
        noButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        noButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        noButton.setTitle("No", for: UIControl.State())
        noButton.setTitleColor(.darkGray, for: .normal)
        noButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        noButton.addTarget(self, action: #selector(referralButtonAction(sender:)), for: .touchUpInside)
        noButton.isSelected = true
        
        self.fullNameTextField.tintColor = ThemeColor.buttonColor
        self.phoneNumberTextField.tintColor = ThemeColor.buttonColor
        self.emailTextField.tintColor = ThemeColor.buttonColor
        self.passwordTextField.tintColor = ThemeColor.buttonColor
        self.referralCodeText.tintColor = ThemeColor.buttonColor
        
        // FIXME: Hide this gender option to pass AppStore Review Process (V1.8)
        femaleButton.isHidden = true
        maleButton.isHidden = true

    }
    
    override func viewDidAppear(_ animated: Bool) {
        initialSetUp()
    }
    
    @objc func loginButtonAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navBar = UINavigationController(rootViewController: destination)
        navBar.modalPresentationStyle = .fullScreen
        self.present(navBar, animated: true, completion: nil)
    }
    
    @objc func referralButtonAction(sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender == noButton{
            yesButton.isSelected = false
        }else{
            noButton.isSelected = false
        }
        referralCodeText.isHidden = noButton.isSelected
    }
    
    @objc func genderSelection(sender: UIButton){
        femaleButton.isSelected = (sender.tag == 101)
        maleButton.isSelected = !(sender.tag == 101)
    }
    
    @objc func forgotPasswordButtonAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecoverPasswordViewController") as! RecoverPasswordViewController
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func openTermsLink(){
        let destnation = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        destnation.pageTitle = "Terms & Conditions"
        destnation.pageContents = appD.termsStaticString
        self.navigationController?.pushViewController(destnation, animated: true)
    }
    
    @objc func termsAction(sender: UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    @objc func signinButtonAction(){
        
        //https://developer.pampertree.co.uk/Data/GetRefid?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&ref_id=4ZFHGKVE
        let referenceID = referenceData.first(where: {$0.isSelected})?.id ?? ""
        if (self.fullNameTextField.text?.isBlank ?? true){
            self.view.makeToast(message: "Please Enter Name")
        }
        else if (self.fullNameTextField.text?.isValidName ?? true){
            self.view.makeToast(message: "Invalid Name")
        }
        else if (self.phoneNumberTextField.text?.isBlank ?? true){
            self.view.makeToast(message: "Please Enter Phone Number")
        }
        else if !(self.phoneNumberTextField.text?.isPhoneNumber ?? true){
            self.view.makeToast(message: "Invalid Phone Number")
        }
        else if (self.emailTextField.text?.isBlank ?? true){
            self.view.makeToast(message: "Please Enter Email ID")
        }
        else if !(self.emailTextField.text?.isValidEmail ?? true){
            self.view.makeToast(message: "Invalid Email ID")
        }
        
        else if (self.passwordTextField.text?.isBlank ?? true){
            self.view.makeToast(message: "Please Enter Password")
        }
        else if !(self.passwordTextField.text?.isValidPassword ?? true){
            passwordInfoLabel.isHidden = false
        }
        else if !(self.termsButton.isSelected){
            self.view.makeToast(message: "Please check the terms and conditions")
        }
        else if referenceID == ""{
            self.view.makeToast(message: "Please choose where did you first hear about us")
        }else if ((referralCodeText.text?.isBlank ?? true) && yesButton.isSelected){
            self.view.makeToast(message: "Please enter the referral code")
        }
        else{
            var genderText = "Female"
            if maleButton.isSelected{
                genderText = "Male"
            }
            
            let names = (self.fullNameTextField.text ?? "").components(separatedBy: ",")
            let firstName = (names.count > 0) ? names[0] : ""
            let lastName = (names.count > 1) ? names[1] : ""
            let paramsHead = ["Content-Type": "multipart/form-data"]
            let bodyParams = [
                "typ": Config.api.register.toData,
                "key": Config.key.toData,
                "salt": Config.salt.toData,
                "email": (self.emailTextField.text ?? "").toData,
                "pass": (self.passwordTextField.text ?? "").toData,
                "first_name": firstName.toData,
                "last_name": lastName.toData,
                "mobile": (self.phoneNumberTextField.text ?? "").toData,
                "gender": genderText.toData,
                "reference": referenceID.toData,
                "fcm_token": self.appD.fcmTokenToRegister.toData,
                "ref_id": (self.referralCodeText.text ?? "").toData
                ] as [String : Data]
            self.activityIndicator.startAnimating()
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in bodyParams{
                    multipartFormData.append(value, withName: key)
                }
                
            }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "guestcheck" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
                
                switch encodingResult{
                case .failure( _):
                    self.activityIndicator.stopAnimating()
                    break;
                case .success(let request, true, _):
                    request.responseJSON(completionHandler: { (response) in
                        
                        DispatchQueue.main.async {
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                            let status = result["status"] as? String ?? ""
                            let message = result["type"] as? String ?? ""
                            let referalMessage = result["referal_msg"] as? String ?? ""
                            
                            if status == "Ok" && !message.contains("existing"){
                                let info = result["info"] as? NSDictionary ?? NSDictionary()
                                let c_id = info["c_id"] as? String ?? ""
                                let social_login = info["social_login"] as? String ?? ""
                                let social_login_id = info["social_login_id"] as? String ?? ""
                                let c_fname = info["c_fname"] as? String ?? ""
                                let c_lname = info["c_lname"] as? String ?? ""
                                let c_email = info["c_email"] as? String ?? ""
                                let c_phone = info["c_phone"] as? String ?? ""
                                //                            let c_password = info["c_password"] as? String ?? ""
                                let status = info["status"] as? String ?? ""
                                
                                let my_referal_code = info["my_referal_code"] as? String ?? ""
                                let my_referal_link = info["my_referal_link"] as? String ?? ""
                                
                                UserDefaults.standard.saveData(value: my_referal_code, key: .referalCode)
                                UserDefaults.standard.saveData(value: my_referal_link, key: .referalLink)
                                
                                UserDefaults.standard.saveData(value: c_id, key: .userId)
                                UserDefaults.standard.saveData(value: social_login, key: .userSocialLogin)
                                UserDefaults.standard.saveData(value: social_login_id, key: .userSocialLoginId)
                                UserDefaults.standard.saveData(value: c_fname, key: .userFirstName)
                                UserDefaults.standard.saveData(value: c_lname, key: .userLastName)
                                UserDefaults.standard.saveData(value: c_email, key: .userEmail)
                                UserDefaults.standard.saveData(value: c_phone, key: .userPhone)
                                UserDefaults.standard.saveData(value: status, key: .userStatus)
                                UserDefaults.standard.deleteData(forKey: .skipLogin)
                                
                                if !(self.referralCodeText.text?.isBlank ?? true){
                                    if #available(iOS 13.0, *) {
                                        self.showReferalNotofication(notificactionMessage: referalMessage)
                                    } else {
                                        
                                    }
                                }
                                
                                if self.profileImageView.image != UIImage(named: "ic_account"){
                                    self.uploadProfilePicture(cust_id: c_id)
                                }else{
                                    self.goToHome()
                                }
                                
                                
                            }
                            else{
                                if message.contains("existing_user") {
                                    self.view.makeToast(message: "You have already registered with us.")
                                }else{
                                    self.view.makeToast(message: message)
                                }
                                
                            }
                        }
                    })
                default:
                    self.activityIndicator.stopAnimating()
                    self.view.makeToast(message: Message.apiError)
                    break;
                }})
        }
    }
    
    func goToHome() {
//        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        let navigationController = UINavigationController(rootViewController: destination)
//        self.appD.window?.rootViewController = navigationController
//        self.appD.window?.makeKeyAndVisible()
//        let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
//        let navigationController = UINavigationController(rootViewController: destination)
//        self.appD.window?.rootViewController = navigationController
//        self.appD.window?.makeKeyAndVisible()
        
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
        self.appD.window?.rootViewController = destination
        self.appD.window?.makeKeyAndVisible()
    }
    
    @objc func facebookButtonAction(){
        socialLoginAction = .facebook
        let fbLoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            
            if error != nil
            {
                print("error occured with login \(String(describing: error?.localizedDescription))")
            }
                
            else if (result?.isCancelled)!
            {
                print("login canceled")
            }
                
            else
            {
                let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields": "name,email,id"])
                graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                    if ((error) != nil)
                    {
                        // Process error
                        print("Error: \(String(describing: error))")
                    }
                    else
                    {
                        print(result as Any)
                        self.activityIndicator.startAnimating()
                        if let facebookResult = result as? NSDictionary{
                            let name = facebookResult["name"] as? String ?? ""
                            let email = facebookResult["email"] as? String ?? ""
                            let id = facebookResult["id"] as? String ?? ""
                            let profile_picture = "http://graph.facebook.com/\(id)/picture?type=large"
                            self.socialMediaLogin(userId: id, firstName: name, lastName: "", email: email, profileImage: profile_picture, password: "Facebook_" + "\(email)", socialType: "F")
                        }

                        
//                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                        let navigationController = UINavigationController(rootViewController: destination)
//                        self.appD.window?.rootViewController = navigationController
//                        self.appD.window?.makeKeyAndVisible()
//                        let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
//                        let navigationController = UINavigationController(rootViewController: destination)
//                        self.appD.window?.rootViewController = navigationController
//                        self.appD.window?.makeKeyAndVisible()
//                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
//                        self.appD.window?.rootViewController = destination
//                        self.appD.window?.makeKeyAndVisible()
                    }
                })
            }
        }
        
        
    }
    
    @objc func signupReference(){
        self.referenceData.removeAll()
        let reuestURL = Config.mainUrl +  "Data/signupRef?key=\(Config.key)&salt=\(Config.salt)"
        print("Request URL: \(reuestURL)")
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                if((resposeJSON["status"] as? Int ?? 0) == 1){
                    let results = resposeJSON["result"] as? NSArray ?? NSArray()
                    results.forEach { (result) in

                        let currentResult = result as? NSDictionary ?? NSDictionary()
                        self.referenceData.append(ReferenceModel(id: currentResult["id"] as? String ?? "", name: currentResult["name"] as? String ?? ""))
                    }
                }
                
                
            }
    }
    
    @objc func googleButtonAction(){
        socialLoginAction = .google
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @available(iOS 13.0, *)
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
//    @objc func twitterLoginaction(){
//        socialLoginAction = .twitter
//        let store = TWTRTwitter.sharedInstance().sessionStore
//
//        if let userID = store.session()?.userID {
//            store.logOutUserID(userID)
//        }
//        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
//            if (session != nil) {
//
//                let client = TWTRAPIClient.withCurrentUser()
//
//                client.requestEmail { email, error in
//                    if let session = session, let email = email{
//                        let twitterClient = TWTRAPIClient(userID: session.userID)
//                        twitterClient.loadUser(withID: session.userID) { (user, error) in
//
//                            var profileURL = ""
//                            if error == nil{
//                                profileURL = user?.profileImageLargeURL ?? ""
//                            }
//                            self.socialMediaLogin(userId: session.userID, firstName: session.userName, lastName: "", email: email, profileImage: profileURL, password: "Twitter_\(email)", socialType: "T")
//
//                        }
//                    } else {
//                        print("error: \(error!.localizedDescription)");
//                    }
//                }
//            } else {
//
//            }
//        })
//    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == passwordTextField{
            passwordInfoLabel.isHidden = self.passwordTextField.text?.isValidPassword ?? true
        } else if textField == referralCodeText {
            self.invalidReferalCode.isHidden = textField.text?.isBlank ?? true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == referralCodeText && !(textField.text?.isBlank ?? false){
            checkReferalCode(textField.text ?? "")
        }
    }
    
    fileprivate func socialMediaLogin(userId: String, firstName: String, lastName: String, email: String, profileImage: String = "", password: String, socialType: String){
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "typ": Config.api.register.toData,
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "email": email.toData,
            "pass": password.toData,
            "first_name": firstName.toData,
            "last_name": lastName.toData,
            "social_login": socialType.toData,
            "social_login_id": userId.toData,
            "fcm_token": self.appD.fcmTokenToRegister.toData,
            
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            if profileImage != ""{
                
//                multipartFormData.append(value, withName: "thumbnail")
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "guestcheck" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    print("Response: \(resposeJSON)")
                    
                    let type = result["type"] as? String ?? ""
                    if (type.starts(with: "existing_user") || type == "Sign Up Complete"){
                        let info = result["info"] as? NSDictionary ?? NSDictionary()
                        let c_id = info["c_id"] as? String ?? ""
                        //                            let social_login = info["social_login"] as? String ?? ""
                        //                            let social_login_id = info["social_login_id"] as? String ?? ""
                        let c_fname = info["c_fname"] as? String ?? ""
                        let c_lname = info["c_lname"] as? String ?? ""
                        let c_email = info["c_email"] as? String ?? ""
                        let c_phone = info["c_phone"] as? String ?? ""
                        
                        let status = info["status"] as? String ?? ""
                        
                        let my_referal_code = info["my_referal_code"] as? String ?? ""
                        let my_referal_link = info["my_referal_link"] as? String ?? ""
                        
                        UserDefaults.standard.saveData(value: my_referal_code, key: .referalCode)
                        UserDefaults.standard.saveData(value: my_referal_link, key: .referalLink)
                        
                        UserDefaults.standard.saveData(value: c_id, key: .userId)
                        UserDefaults.standard.saveData(value: socialType, key: .userSocialLogin)
                        UserDefaults.standard.saveData(value: userId, key: .userSocialLoginId)
                        UserDefaults.standard.saveData(value: c_fname, key: .userFirstName)
                        UserDefaults.standard.saveData(value: c_lname, key: .userLastName)
                        UserDefaults.standard.saveData(value: c_email, key: .userEmail)
                        UserDefaults.standard.saveData(value: c_phone, key: .userPhone)
                        UserDefaults.standard.saveData(value: status, key: .userStatus)
                        UserDefaults.standard.saveData(value: profileImage, key: .userImage)
                        
                        UserDefaults.standard.deleteData(forKey: .skipLogin)
                        
                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
                        self.appD.window?.rootViewController = destination
                        self.appD.window?.makeKeyAndVisible()
                    }else{
                        self.view.makeToast(message: "Something Went Wrong. Please try again.")
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    @objc func uploadImageButtonAction(sender: UIButton){
            self.imagePicker.present(from: sender)
        }
        
        
        func uploadProfilePicture(cust_id: String) {
            let paramsHead = ["Content-Type": "multipart/form-data"]
            guard let imgData = self.profileImageView.image?.pngData() else { return }
            let bodyParams = [
                "key": Config.key.toData,
                "salt": Config.salt.toData,
                "cust_id": cust_id.toData,
                
            ] as [String : Data]
            self.activityIndicator.startAnimating()
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in bodyParams{
                    multipartFormData.append(value, withName: key)
                }
                multipartFormData.append(imgData, withName: "thumbnail", fileName: "\(cust_id)_\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                
            }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "ProfileImage" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
                
                switch encodingResult{
                case .failure( _):
                    self.activityIndicator.stopAnimating()
                    break;
                case .success(let request, true, _):
                    request.responseJSON(completionHandler: { (response) in
                        
                        let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                        self.activityIndicator.stopAnimating()
                        
                        let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                        let status = result["status"] as? String ?? ""
                        
                        let message = result["type"] as? String ?? ""
                        let info = result["info"] as? NSDictionary ?? NSDictionary()
                        let thumbnail = info["thumbnail"] as? String ?? ""
                        
                        
                        debugPrint("Upload Image::: \(resposeJSON)")
                        UserDefaults.standard.saveData(value: thumbnail, key: .userImage)
                        if status == "success"{
                            self.goToHome()
                        }
                        else{
                            self.view.makeToast(message: message)
                        }
                    })
                default:
                    self.activityIndicator.stopAnimating()
                    self.view.makeToast(message: Message.apiError)
                    break;
                }})
        }
    
    
    func checkReferalCode(_ code: String) {
        let reuestURL = Config.mainUrl +  "Data/GetRefid?key=\(Config.key)&salt=\(Config.salt)&ref_id=\(code)"
        print("Request URL: \(reuestURL)")
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                if(resposeJSON["status"] as? Bool ?? false) {
                    
                }
            }
    }

}

extension RegisterViewController: GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
        } else {
            
            guard let user = user else {
                self.view.makeToast(message: "Something went wrong.")
                return
            }
            let userId = user.userID               // For client-side use only!
            let fullName = user.profile.name
            let email = user.profile.email
            let dimension = round(100 * UIScreen.main.scale)
            let profileImage = user.profile.imageURL(withDimension: UInt(dimension))?.absoluteString
            
            self.socialMediaLogin(userId: userId ?? "", firstName: fullName ?? "", lastName: "", email: email ?? "", profileImage: profileImage ?? "", password: "Google_\(email ?? "")", socialType: "G")
           
        }
    }
    
    
}


extension RegisterViewController: APJTextPickerViewDataSource, APJTextPickerViewDelegate{
    func numberOfRows(in pickerView: APJTextPickerView) -> Int {
        return self.referenceData.count
    }
    
    func textPickerView(_ textPickerView: APJTextPickerView, titleForRow row: Int) -> String? {
        return referenceData[row].name
    }
    
    func textPickerView(_ textPickerView: APJTextPickerView, didSelectString row: Int) {
        for index in 0..<referenceData.count{
            referenceData[index].isSelected = (referenceData[index].id == referenceData[row].id)
        }
    }
    
    
}

extension RegisterViewController: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        uploadButton.isSelected = (image != nil)
        if image == nil{
            self.profileImageView.image = UIImage(named: "ic_account")
        }else{
            self.profileImageView.image = image
        }
        
    }
}

struct ReferenceModel{
    var id: String
    var name: String
    var isSelected: Bool = false
}

@available(iOS 13.0, *)
extension RegisterViewController: ASAuthorizationControllerDelegate{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Something Went Wrong \(error.localizedDescription)")
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            self.activityIndicator.stopAnimating()
            
            if let givenName = appleIDCredential.fullName?.givenName{
                let familyName = appleIDCredential.fullName?.familyName ?? ""
                let user_id = appleIDCredential.user
                let email = appleIDCredential.email ?? ""
                
                UserDefaults.standard.setValue(email, forKey: "apple_email")
                UserDefaults.standard.setValue(user_id, forKey: "apple_user_id")
                UserDefaults.standard.setValue(givenName, forKey: "apple_givenName")
                UserDefaults.standard.setValue(familyName, forKey: "apple_familyName")
                UserDefaults.standard.synchronize()
                
                self.socialMediaLogin(userId: user_id, firstName: givenName, lastName: familyName, email: email, password: "Apple_" + email, socialType: "A")
                
            }
            else{

                let givenName = UserDefaults.standard.string(forKey: "apple_givenName") ?? ""
                let familyName = UserDefaults.standard.string(forKey: "apple_familyName") ?? ""
                let user_id = UserDefaults.standard.string(forKey: "apple_user_id") ?? ""
                let email = UserDefaults.standard.string(forKey: "apple_email") ?? ""
                
                if email != ""{
                    self.socialMediaLogin(userId: user_id, firstName: givenName, lastName: familyName, email: email, password: "Apple_" + email, socialType: "A")
                }else{
                    let alert = UIAlertController(title: "Aler!", message: "You have already hidden the email sharing option. Kindly delete the certificate manually and select email sharing option to use apple sign in. Please refer to the following steps. Settings -> Account Login -> Password & Security -> Apps using Your AppID -> Certificate -> Stop Using Apple ID.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            
            break
            
        case let passwordCredential as ASPasswordCredential:
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                self.showPasswordCredentialAlert(username: username, password: password)
            }
            break
        default:
            break
            
        }
    }
    
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        let alertController = UIAlertController(title: "Keychain Credential Received",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    func showReferalNotofication(notificactionMessage: String){
        
        let content = UNMutableNotificationContent()
        content.title = "Congrats !! You have got referal bonus"
        content.body = notificactionMessage
        content.sound = UNNotificationSound.default
        content.threadIdentifier = "referal-\(Date().timeIntervalSince1970)"
        content.summaryArgument  = "referal"
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 2.0, repeats: false)
        let request = UNNotificationRequest(identifier:"downloadsuccess\(content.threadIdentifier)", content: content, trigger: trigger)
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().add(request){(error) in
            
            if (error != nil){
                
                print(error?.localizedDescription as Any)
            }
        }

        
        /*let content = UNMutableNotificationContent()
        let categoryIdentifire = "Referal Notification" + "\(Date().timeIntervalSince1970)"
        content.sound = UNNotificationSound.default
        content.body = notificactionMessage
        content.badge = 1
        content.categoryIdentifier = categoryIdentifire
        
        let imageName = "refergift"
        guard let imageURL = Bundle.main.url(forResource: imageName, withExtension: "png") else { return }
        let attachment = try! UNNotificationAttachment(identifier: imageName, url: imageURL, options: .none)
        content.attachments = [attachment]
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        let identifier = "Local Notification" + categoryIdentifire
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }*/
    }
    
}
