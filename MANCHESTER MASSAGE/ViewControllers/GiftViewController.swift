//
//  GiftViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 20/05/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class GiftViewController: UIViewController {

    @IBOutlet weak var saveGiftCardView: CardView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var giftCardNumber: UITextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var activityIndicator: NVActivityIndicatorView!
    var receivedGiftCard = [GiftModel]()
    var sentGiftCard = [GiftModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationBar(titleText: "Gift Cards", isBackButtonRequired: true, isRightMenuRequired: false)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "GiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GiftCollectionViewCell")
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: collectionView.bounds.width/2 - 10, height: (collectionView.bounds.width/2) * 0.6)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionView.collectionViewLayout = layout
        
        if #available(iOS 13.0, *) {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: ThemeColor.buttonColor]
            segmentedControl.setTitleTextAttributes(titleTextAttributes, for:.normal)
            
            let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
            segmentedControl.setTitleTextAttributes(titleTextAttributes1, for:.selected)
            
        }

        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium)], for: UIControl.State())
        segmentedControl.addTarget(self, action: #selector(getGiftCards), for: .valueChanged)
        
        getGiftCards()
        
        let createButton = UIButton(type: .custom)
        createButton.setTitle("Create Gift Card", for: .normal)
        createButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        createButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        createButton.setLeftImage(image: .FAPlus, color: ThemeColor.buttonColor, state: .normal, size: CGSize(width: 20, height: 20))
        createButton.addTarget(self, action: #selector(createGiftCardButtonAction), for: .touchUpInside)
        let rightButtonItem = UIBarButtonItem(customView: createButton)
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
        saveGiftCardView.isHidden = true
        saveButton.layer.cornerRadius = 5
        saveButton.addTarget(self, action: #selector(saveGiftCardAction), for: .touchUpInside)
        
        saveGiftCardView.isHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getGiftCards), name: NSNotification.Name("giftCardStatusChange"), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.giftCardNumber.font = .systemFont(ofSize: 17)
        self.giftCardNumber.setPlaceholder(placeholder: "Enter Gift Card Code", color: UIColor.gray)
        self.giftCardNumber.setTextFieldImage(image: FAType.FAGift, direction: .left)
        self.giftCardNumber.tintColor = ThemeColor.buttonColor
        
    }
    
    @objc func createGiftCardButtonAction(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CreateGiftCardViewController") as! CreateGiftCardViewController
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func getGiftCards(){
        saveGiftCardView.isHidden = (segmentedControl.selectedSegmentIndex == 1)
        getGiftCards(sent: (segmentedControl.selectedSegmentIndex == 1))
    }
    
    fileprivate func getGiftCards(sent: Bool = false){
        sentGiftCard.removeAll()
        receivedGiftCard.removeAll()
        collectionView.reloadData()
        self.activityIndicator.startAnimating()
        var requestURL = Config.mainUrl + "Data/GiftCardReceived?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        if sent{
            requestURL = Config.mainUrl + "Data/GiftCardSent?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        }
        let urlConvertible = URL(string: requestURL)!
        
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                if(resposeJSON["status"] as? Bool ?? false){
                    let giftCardReceived = resposeJSON["result"] as? NSArray ?? NSArray()
                    if giftCardReceived.count > 0{
                        for index in 0..<giftCardReceived.count{
                            let currentGiftCard = giftCardReceived[index] as? NSDictionary ?? NSDictionary()
                            var giftCard = GiftModel()
                            giftCard.category = currentGiftCard["giftcard"] as? String ?? ""
                            giftCard.card_code = (currentGiftCard["card_code"] as? String ?? "")
                            if sent{
                                giftCard.card_code = giftCard.card_code.uppercased()
                            }else{
                                giftCard.card_code = giftCard.card_code.makeMaskedString(maskedText: "-", repeatAfter: 4).uppercased()
                            }
                            giftCard.amount = "\(CURRENCY_SYMBOL)\((currentGiftCard["amount"] as? String ?? "0").formattedString())"
                            giftCard.send_via_post = currentGiftCard["send_via_post"] as? String ?? ""
                            giftCard.valid_till = self.convertDate((currentGiftCard["valid_till"] as? String ?? "1970-01-01"), from: "yyyy-MM-dd HH:mm:ss", to: "MMM dd, yyyy")
                            giftCard.created_on = self.convertDate((currentGiftCard["created_on"] as? String ?? "1970-01-01"), from: "yyyy-MM-dd HH:mm:ss", to: "MMM dd, yyyy")
                            
                            giftCard.receiver_address = currentGiftCard["receiver_address"] as? String ?? ""
                            giftCard.receiver_email = currentGiftCard["receiver_email"] as? String ?? ""
                            giftCard.status = currentGiftCard["status"] as? String ?? ""
                            giftCard.Status_info = currentGiftCard["Status_info"] as? String ?? ""
                            if sent{
                                self.sentGiftCard.append(giftCard)
                            }else{
                                self.receivedGiftCard.append(giftCard)
                            }
                        }
                        self.collectionView.reloadData()
                    }
                }else{
                    self.tabBarController?.tabBar.makeToast(message: "Something went wrong. Please try again.")
                }
                
                
            }
        
    }
    
    fileprivate func convertDate(_ date: String, from format1: String, to format2: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format1
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = format2
        return  dateFormatter.string(from: date)
    }
    
    @objc func saveGiftCardAction(){
        self.view.endEditing(true)
        if giftCardNumber.text?.isEmpty ?? true{
            self.tabBarController?.view.makeToast(message: "Enter Gift Card Code")
            return
        }
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "cust_email": UserDefaults.standard.string(forKey: "userId") ?? "",
            "card_code": giftCardNumber.text ?? ""
        ] as [String : String]
        Alamofire.request(Config.apiEndPoint + "Giftcardsave", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                case .success(let jSONResponse):
                    print(jSONResponse)
                    self.activityIndicator.stopAnimating()
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftCardStatusChange"), object: nil)
                        
                        var giftCardSuccessMessage = "Gift Card has been created successfully."
                        if (Int(parameters["quantity"] ?? "0") ?? 0) > 1 {
                            giftCardSuccessMessage = "Gift Cards have been created successfully."
                        }
                        let alertController = UIAlertController(title: "Success", message: giftCardSuccessMessage, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
//                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                    
                }
            }
        
    }
    
    @objc func redeemButtonAction(sender: UIButton){
        self.view.endEditing(true)
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "cust_email": UserDefaults.standard.string(forKey: "userEmail") ?? "",
            "card_code": receivedGiftCard[sender.tag].card_code.replacingOccurrences(of: "-", with: "")
        ] as [String : String]
        Alamofire.request(Config.apiEndPoint + "GiftcardRedeem", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                case .success(let jSONResponse):
                    print(jSONResponse)
                    self.activityIndicator.stopAnimating()
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftCardStatusChange"), object: nil)
                        
                        let alertController = UIAlertController(title: "Success", message: result["msg"] as? String ?? "", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
                            
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                    
                }
            }
        
    }
}

extension GiftViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return segmentedControl.selectedSegmentIndex == 1 ? sentGiftCard.count : receivedGiftCard.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GiftCollectionViewCell", for: indexPath) as! GiftCollectionViewCell
        cell.redeemButton.layer.cornerRadius = cell.redeemButton.bounds.height / 2
        cell.redeemButton.clipsToBounds = true
        cell.isSent = segmentedControl.selectedSegmentIndex == 1
        cell.data = segmentedControl.selectedSegmentIndex == 1 ? self.sentGiftCard[indexPath.item] : self.receivedGiftCard[indexPath.item]
        cell.giftOuterView.addShadow(to: [.top, .bottom, .left, .right], radius: 5.0)
        if segmentedControl.selectedSegmentIndex == 0 {
            cell.redeemButton.tag = indexPath.item
            cell.redeemButton.addTarget(self, action: #selector(redeemButtonAction(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: collectionView.bounds.width/2 - 10, height: (collectionView.bounds.width/2) * 0.6)
        if !isIPad{
            size = CGSize(width: collectionView.bounds.width - 10, height: (collectionView.bounds.width) * 0.5)
        }
        return size
    }
    
}
