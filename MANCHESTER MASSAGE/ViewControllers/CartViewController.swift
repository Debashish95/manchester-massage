//
//  CartViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 22/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import MapKit
class CartViewController: UIViewController {
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmBookingButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: NVActivityIndicatorView!
    var totalPrice = ""
    var window: UIWindow?
    var serviceDetailsData: ServiceDetailsModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "Cart Summary", isBackButtonRequired: true, isRightMenuRequired: false)
        tableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "CartTableViewCell")
        tableView.register(UINib(nibName: "CartLastTableViewCell", bundle: nil), forCellReuseIdentifier: "CartLastTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        bottomHeight.constant = (self.tabBarController?.tabBar.bounds.height ?? 0) + 5
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCart), name: NSNotification.Name("changeCart"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         if self.appD.serviceCart.count == 0{
                   confirmBookingButton.isHidden = true
                   let alert = UIAlertController(title: "Alert!", message: "No services found in cart.", preferredStyle: .alert)
                   let yesAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                       self.appD.serviceCart.removeAll()
                       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
                       self.navigationController?.popViewController(animated: true)
                   }
                   alert.addAction(yesAction)
                   self.present(alert, animated: false, completion: nil)
               }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        confirmBookingButton.backgroundColor = ThemeColor.buttonColor
        
        confirmBookingButton.layer.cornerRadius = confirmBookingButton.frame.height / 2
        confirmBookingButton.clipsToBounds = true
        confirmBookingButton.addTarget(self, action: #selector(confirmBookingButtonAction), for: .touchUpInside)
        tableView.reloadData()
        updateCartPrice()
    }
    
    override func viewDidLayoutSubviews() {
        tableView.reloadData()
    }
    func updateCartPrice(){
        var totalPriceFloat = Float(0.0)
        for index in 0..<self.appD.serviceCart.count{
            let priceString = self.appD.serviceCart[index].final_amount
            let price = Float(priceString) ?? 0.0
            totalPriceFloat += price
        }
        
        totalPrice = "Grand Total: \(CURRENCY_SYMBOL)\(String(totalPriceFloat).formattedString())"
    }
    @objc func confirmBookingButtonAction(){
        
        
        /*if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
        }else{*/
            let destination = SelectDateTimePopUp(nibName: "SelectDateTimePopUp", bundle: nil)
            destination.modalPresentationStyle = .overCurrentContext
            self.definesPresentationContext = true
            destination.cartViewController = self
            self.present(destination, animated: true, completion: nil)
//        }
       
    }
    
    @objc func yesButtionAction(sender: UIButton){
        appD.serviceCart[sender.tag].isForMe = true
        tableView.reloadData()
    }
    
    @objc func noButtionAction(sender: UIButton){
        appD.serviceCart[sender.tag].isForMe = false
        tableView.reloadData()
    }
    
    
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func updateCart(){
        self.tableView.reloadData()
        self.updateCartPrice()
    }
    @objc func deleteButtonAction(sender: UIButton){
        
       let alertPopUp = UIAlertController(title: "Alert!", message: "Are you sure you want to delete the service?", preferredStyle: .alert)
        
        let alertActionYes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            if self.appD.serviceCart.count > sender.tag{
                self.appD.serviceCart.remove(at: sender.tag)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
                if self.appD.serviceCart.count == 0{
                    let alert = UIAlertController(title: "Alert!", message: "Cart is empty.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    self.tabBarController?.view.makeToast(message: "Service removed from cart successfully.")
                }
                self.updateCartPrice()
            }else{
                self.tabBarController?.view.makeToast(message: "Unable to delete the service")
            }
        }
        
        let alertActionNo = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        alertPopUp.addAction(alertActionNo)
        alertPopUp.addAction(alertActionYes)
        alertPopUp.popoverPresentationController?.sourceView = sender
        self.present(alertPopUp, animated: true, completion: nil)
        

    }
    
    @objc func addMoreServices(){
//        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        let navigationController = UINavigationController(rootViewController: destination)
//        self.appD.window?.rootViewController = navigationController
//        self.appD.window?.makeKeyAndVisible()
//        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
//        destination.serviceList = self.appD.serviceData
//        self.navigationController?.pushViewController(destination, animated: true)
        
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
        self.appD.window?.rootViewController = destination
        self.appD.window?.makeKeyAndVisible()
    }
    
    @objc func editButtonAction(sender: UIButton){
        let catId = self.appD.serviceCart[sender.tag].cat_id
        self.setData(catID: catId, index: sender.tag)
    }
    
    func setData(catID:String, index: Int){
        
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                _ = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? "0"
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(smId: (itmDict["sm_id"] as? String ?? ""), serviceID: (itmDict["service_id"] as? String ?? ""), catId: (itmDict["cat_id"] as? String ?? ""), serviceTitle: (itmDict["service_title"] as? String ?? ""), serviceItems: serviceItemsList)
                                    
                                    self.serviceDetailsData = serviceDataTemp
                                }
                                
                                let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
                                destination.modalPresentationStyle = .overCurrentContext
                                self.definesPresentationContext = true
                                destination.delegate = self
                                destination.fromPage = .cartPage
                                destination.serviceData = self.serviceDetailsData
                                destination.cartItemIndex = index
                                destination.selectedIndex = self.serviceDetailsData.serviceItems.firstIndex(where: { $0.serviceID == (self.appD.serviceCart[index].services.first?.serviceID ?? "")}) ?? 0
                                self.present(destination, animated: true, completion: nil)
                            }
                            
                            
        }
        
    }
}


extension CartViewController: BookNowPopUpDelegate{
    func confirmButtonAction() {
        
    }
    
    func addServiceButtonActions() {
//        if UserDefaults.standard.fetchData(forKey: .skipLogin){
//            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
//            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
//                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
//                destination.isFromLeftMenu = true
//                let navigationController = UINavigationController(rootViewController: destination)
//                navigationController.modalPresentationStyle = .fullScreen
//                self.present(navigationController, animated: false, completion: nil)
//            }
//            alert.addAction(yesAction)
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//
//            }
//            alert.addAction(cancelAction)
//
//            self.present(alert, animated: false, completion: nil)
//        }else{
//            let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
//            self.navigationController?.pushViewController(detination, animated: true)
       // }
        
        tableView.reloadData()
        updateCartPrice()
        
    }
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 1 ? ((self.appD.serviceCart.count == 0) ? 0 : 1) : (self.appD.serviceCart.count)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
            cell.setData(cartData: appD.serviceCart[indexPath.row])
            
            cell.yesButton.tag = indexPath.row
            cell.yesButton.addTarget(self, action: #selector(yesButtionAction(sender:)), for: .touchUpInside)
            
            cell.noButton.tag = indexPath.row
            cell.noButton.addTarget(self, action: #selector(noButtionAction(sender:)), for: .touchUpInside)
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteButtonAction(sender:)), for: .touchUpInside)
            cell.reloadView(tag: indexPath.row)
            cell.stackView.isHidden = appD.serviceCart[indexPath.row].isForMe
            cell.nameTextField.setPlaceholder(placeholder: "Full Name", color: .gray)
            cell.emailTextField.setPlaceholder(placeholder: "Email ID", color: .gray)
            cell.phoneTextField.setPlaceholder(placeholder: "Phone Number", color: .gray)
            
            cell.nameTextField.setTextFieldImage(image: FAType.FAUser, direction: .right)
            cell.emailTextField.setTextFieldImage(image: FAType.FAEnvelope, direction: .right)
            cell.phoneTextField.setTextFieldImage(image: FAType.FAPhone, direction: .right)
            
//            cell.nameTextField.delegate = self
//            cell.emailTextField.delegate = self
//            cell.phoneTextField.delegate = self
            
            cell.nameTextField.tag = indexPath.row
            cell.emailTextField.tag = indexPath.row
            cell.phoneTextField.tag = indexPath.row
            
            cell.nameTextField.accessibilityLabel = "name"
            cell.emailTextField.accessibilityLabel = "email"
            cell.phoneTextField.accessibilityLabel = "phone"
            
            if(appD.serviceCart[indexPath.row].isForMe){
                
                cell.nameTextField.isHidden = true
            }
            else{
                
                cell.nameTextField.isHidden = false
                cell.nameTextField.text = appD.serviceCart[indexPath.row].cust_name
            }
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(editButtonAction(sender:)), for: .touchUpInside)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartLastTableViewCell", for: indexPath) as! CartLastTableViewCell
            cell.grandTotalLabel.text = totalPrice
            cell.addMore.addTarget(self, action: #selector(addMoreServices), for: .touchUpInside)
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1{
            return 360
        }
        else{
            if appD.serviceCart[indexPath.row].isForMe{
                return 130
            }
            return 250
        }
        
    }
    
}

//extension CartViewController:UITextFieldDelegate{
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField.accessibilityLabel == "name"{
//            self.appD.serviceCart[textField.tag].cust_name = textField.text ?? ""
//        }
//        if textField.accessibilityLabel == "email"{
//            self.appD.serviceCart[textField.tag].cust_email = textField.text ?? ""
//        }
//        if textField.accessibilityLabel == "phone"{
//            self.appD.serviceCart[textField.tag].cust_phone = textField.text ?? ""
//        }
//    }
//}
