//
//  SupportViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 20/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import MapKit
import Contacts
import CoreLocation
import Alamofire
import MapKit
import MessageUI

class SupportViewController: UIViewController {
    @IBOutlet weak var phoneNumberOneButton: UIButton!
    @IBOutlet weak var phoneNumberTwoButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!

    
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var aboutusButton: UIButton!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapBtn: UIButton!
    
    var cancelStaticString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBar(titleText: "Support & Care", isBackButtonRequired: true, isRightMenuRequired: false)
        
        setupView()
    }

    func setupView(){
        self.phoneNumberOneButton.setSupportButton(image: .FAPhone, title: "07415 831969")
        self.phoneNumberOneButton.addTarget(self, action: #selector(phoneNumberOneButtonAction), for: .touchUpInside)
        self.phoneNumberTwoButton.setSupportButton(image: .FAPhone, title: "0161 834 9213")
        self.phoneNumberTwoButton.addTarget(self, action: #selector(phoneNumberTwoButtonAction), for: .touchUpInside)
        self.emailButton.setSupportButton(image: .FAEnvelope, title: "info@manchestermassage.co.uk")
        self.emailButton.addTarget(self, action: #selector(emailButtonAction), for: .touchUpInside)
        
        //self.addressButton.setSupportButton(image: .FALocationArrow, title: "28a Swan street, Manchester Northern Quarters, M4 5JQ")
        //self.addressButton.titleLabel?.numberOfLines = 2
        //self.addressButton.titleLabel?.adjustsFontSizeToFitWidth = true
        //self.addressButton.addTarget(self, action: #selector(addressButtonAction(sender:)), for: .touchUpInside)
        
        self.mapBtn.titleLabel?.numberOfLines = 3
        self.mapBtn.setSupportButton(image: .FALocationArrow, title: "28a Swan street, Manchester Northern Quarters, M4 5JQ, UK", color: .black)
        self.mapBtn.addTarget(self, action: #selector(addressButtonAction(sender:)), for: .touchUpInside)
        
        let artwork = Artwork(
            title: "MANCHESTER MASSAGE",
            locationName: "",
            discipline: "",
            coordinate: CLLocationCoordinate2D(latitude: 53.4853806, longitude:  -2.2340228))
        
        
        let oahuCenter = CLLocation(latitude: 53.4853806, longitude:  -2.2340228)
        let region = MKCoordinateRegion(
            center: oahuCenter.coordinate,
            latitudinalMeters: 0,
            longitudinalMeters: 0)
        if #available(iOS 13.0, *) {
            mapView.setCameraBoundary(
                MKMapView.CameraBoundary(coordinateRegion: region),
                animated: true)
            let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 1000)
            mapView.setCameraZoomRange(zoomRange, animated: true)
        } else {
            // Fallback on earlier versions
        }
        mapView.addAnnotation(artwork)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.gotoMapView))
        self.mapView.isUserInteractionEnabled = true
        self.mapView.addGestureRecognizer(tapGesture)
        self.termsButton.setSupportFooterButton(title: "Terms & Conditions")
        self.termsButton.addTarget(self, action: #selector(termsButtonAction), for: .primaryActionTriggered)
        self.privacyPolicyButton.setSupportFooterButton(title: "Privacy Policy")
        self.privacyPolicyButton.addTarget(self, action: #selector(privacyButtonAction), for: .primaryActionTriggered)
        self.aboutusButton.setSupportFooterButton(title: "About Us")
        self.aboutusButton.addTarget(self, action: #selector(aboutUsButtonAction), for: .primaryActionTriggered)
        
        getStaticPageDetails()
    }
    
    @objc func gotoMapView(){
        let accessValue = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = accessValue
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            
            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    
    @objc func phoneNumberOneButtonAction(){
        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Call", style: .default) { action -> Void in

             if let urls = URL(string: "tel://07415831969"), UIApplication.shared.canOpenURL(urls){
                                  UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                              }
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "SMS", style: .default) { action -> Void in

            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Enter a message details here";
            messageVC.recipients = ["07415831969"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }

        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)


        // present an actionSheet...
        // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad

        actionSheetController.popoverPresentationController?.sourceView = self.phoneNumberTwoButton // works for both iPhone & iPad

        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
    }
    
    @objc func phoneNumberTwoButtonAction(){
        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Call", style: .default) { action -> Void in

             if let urls = URL(string: "tel://01618349213"), UIApplication.shared.canOpenURL(urls){
                                  UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                              }
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "SMS", style: .default) { action -> Void in

            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Enter a message details here";
            messageVC.recipients = ["01618349213"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }

        // add actions
        actionSheetController.addAction(firstAction)
//        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)


        // present an actionSheet...
        // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad

        actionSheetController.popoverPresentationController?.sourceView = self.phoneNumberTwoButton // works for both iPhone & iPad

        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
    }
    
    @objc func emailButtonAction(){
        if let urls = URL(string: "mailto:info@manchestermassage.co.uk"), UIApplication.shared.canOpenURL(urls){
           UIApplication.shared.open(urls, options: [:], completionHandler: nil)
       }
    }
    
    @objc func aboutUsButtonAction(){
        let destnation = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        destnation.pageTitle = "About Us"
        destnation.pageContents = appD.aboutStaticString
        self.navigationController?.pushViewController(destnation, animated: true)
    }
    @objc func termsButtonAction(){
        let destnation = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        destnation.pageTitle = "Terms & Conditions"
        destnation.pageContents = appD.termsStaticString
        self.navigationController?.pushViewController(destnation, animated: true)
    }
    @objc func privacyButtonAction(){
        let destnation = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
        destnation.pageTitle = "Privacy Policy"
        destnation.pageContents = appD.privacyStaticString
        self.navigationController?.pushViewController(destnation, animated: true)
    }
    
    @objc func addressButtonAction(sender: UIButton){

        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = "Manchester Massage \n28a Swan St, Manchester M4 5JQ, UK"
//            sender.currentTitle ?? ""
        let search = MKLocalSearch(request: searchRequest)
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }

            for item in response.mapItems {
                item.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
                break;
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.phoneNumberOneButton.dropShadow()
        self.phoneNumberTwoButton.dropShadow()
        self.emailButton.dropShadow()
        self.aboutusButton.dropShadow()
        self.termsButton.dropShadow()
        self.privacyPolicyButton.dropShadow()
    }
    
    fileprivate func getStaticPageDetails(){
        let apiURL = Config.mainUrl + "Data/PageContent?key=\(Config.key)&salt=\(Config.salt)"
        Alamofire.request(apiURL, method: .get).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.appD.aboutStaticString = resposeJSON["about_us"] as? String ?? ""
            self.appD.termsStaticString = resposeJSON["terms"] as? String ?? ""
            self.appD.privacyStaticString = resposeJSON["privacy"] as? String ?? ""
        }
        
    }
  
}

extension SupportViewController: MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
        case .failed:
            print("Message failed")
        case .sent:
            print("Message was sent")
        default:
            return
        }
        dismiss(animated: true, completion: nil)
    }
    
    
}
