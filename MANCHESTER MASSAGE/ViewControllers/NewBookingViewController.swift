//
//  NewBookingViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 30/11/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import CoreML

class NewBookingViewController: UIViewController {

    @IBOutlet weak var noUpcomingBookings: UIStackView!
    @IBOutlet weak var bookAppointmentButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pastUpcomingSegmentedControl: UISegmentedControl!
    
    var activityIndicator: NVActivityIndicatorView!
    
    var upcomingbookingsData = [BookingsModel]()
    var pastbookingsData = [BookingsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setHomePageNavBr(title: "Booking History", isMenuRequired: false)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NewBookingTableViewCell", bundle: nil), forCellReuseIdentifier: "NewBookingTableViewCell")
        
        pastUpcomingSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium)], for: UIControl.State())
        pastUpcomingSegmentedControl.addTarget(self, action: #selector(changeSegmentedValue), for: .valueChanged)
        
        bookAppointmentButton.addTarget(self, action: #selector(bookAppointmentAction), for: .touchUpInside)
        
        if #available(iOS 13.0, *) {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: ThemeColor.buttonColor]
        pastUpcomingSegmentedControl.setTitleTextAttributes(titleTextAttributes, for:.normal)

        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
        pastUpcomingSegmentedControl.setTitleTextAttributes(titleTextAttributes1, for:.selected)
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        bookAppointmentButton.setLeftImage(image: .FAPlus, color: .white, state: .normal, size: CGSize(width: 25, height: 25))
        bookAppointmentButton.layer.cornerRadius = bookAppointmentButton.bounds.height / 2
        bookAppointmentButton.clipsToBounds = true
        noUpcomingBookings.isHidden = true
        
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to view your bookings.", preferredStyle: .alert)
            let okButtonAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                self.tabBarController?.selectedIndex = 0
            }
            alert.addAction(okButtonAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            getBookingsData()
        }
    }
    
    @objc func bookAppointmentAction(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
        destination.serviceList = self.appD.serviceData
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    fileprivate func getBookingsData(){
        
        self.activityIndicator.startAnimating()
        self.upcomingbookingsData.removeAll()
        self.pastbookingsData.removeAll()
        tableView.reloadData()
        let reuestURL = Config.mainUrl + "Data/GetBookingList?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        self.tabBarController?.view.makeToast(message: Message.apiError)
                        self.activityIndicator.stopAnimating()
                        return
                    }
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    let results = resposeJSON["result"] as? NSArray ?? NSArray()
                    
                    for result in results{
                        let currentBooking = result as? NSDictionary ?? NSDictionary()
                        
                        
                        let final_amount = (currentBooking["final_amount"] as? String ?? "0").formattedString()
                        let orderid = currentBooking["orderid"] as? String ?? ""
                        let share_url = currentBooking["share_url"] as? String ?? ""
                        let status = currentBooking["status"] as? String ?? "0"
                        let cust_id = currentBooking["cust_id"] as? String ?? ""
                        let firstName = currentBooking["cust_fname"] as? String ?? ""
                        let lastName = currentBooking["cust_lname"] as? String ?? ""
                        let customerEmail = currentBooking["cust_email"] as? String ?? ""
                        let cust_phone = currentBooking["cust_phone"] as? String ?? ""
                        let therapist_id = currentBooking["therapist_id"] as? String ?? ""
                        let bookdt = currentBooking["bookdt"] as? String ?? ""
                        let timeslot = currentBooking["timeslot"] as? String ?? ""
                        let notes = currentBooking["notes"] as? String ?? ""
                        
                        let salon_id = currentBooking["salon_id"] as? String ?? ""
                        let salon = currentBooking["salon"] as? String ?? ""
                        let transaction_type = currentBooking["transaction_type"] as? String ?? ""
                        let currentService = currentBooking["services"] as? NSArray ?? NSArray()
                        let is_review = currentBooking["is_review"] as? String ?? "0"
                        let booking_id = currentBooking["booking_id"] as? String ?? "0"
                        let booked_on = self.convertDateFormater2(currentBooking["booked_on"] as? String ?? "0")
                        
                        
                        
                        var reviewData: ReviewModel?
                        if let reviewInfo = currentBooking["review_info"] as? NSDictionary{
                            reviewData = ReviewModel(reviewer_name: reviewInfo["r_name"] as? String ?? "",
                                                     reviewer_email: reviewInfo["r_email"] as? String ?? "",
                                                     review: reviewInfo["r_review"] as? String ?? "",
                                                     service_review: reviewInfo["rt_service"] as? String ?? "",
                                                     staff_review: reviewInfo["rt_staff"] as? String ?? "",
                                                     cleanliness_review: reviewInfo["rt_clean"] as? String ?? "",
                                                     overall_review: reviewInfo["rt_overall"] as? String ?? "",
                                                     created_on: reviewInfo["created_on"] as? String ?? "")
                            
                        }
                        
                        var services = [servicelist]()
                        if currentService.count > 0{
                            
                            
                            for index in 0..<currentService.count{
                                let currentServiceData = currentService[index] as? NSDictionary ?? NSDictionary ()
                                let serviceId = currentServiceData["service_id"] as? String ?? ""
                                let cart_id = currentServiceData["cat_id"] as? String ?? ""
                                let serviceName = currentServiceData["service_name"] as? String ?? ""
                                let therapistNumber = currentServiceData["therapist"] as? String ?? "0"
                                let bookingDate = currentServiceData["book_dt"] as? String ?? ""
                                let bookingTime = currentServiceData["book_tym"] as? String ?? ""
                                let bookEndTime = currentServiceData["book_end_tym"] as? String ?? ""
                                let cat_thumb = currentServiceData["cat_thumb"] as? String ?? ""
                                let custName = currentServiceData["cust_name"] as? String ?? ""
                                let therapistArr = currentServiceData["therapist_arr"] as? String ?? ""
                                let therapistNm = currentServiceData["therepist_nm"] as? String ?? ""
                                let qty = currentServiceData["qty"] as? String ?? "0"
                                let basePrice = currentServiceData["base_price"] as? String ?? "0"
                                let subTotal = currentServiceData["subtotal"] as? String ?? "0"
                                let duration = currentServiceData["duration"] as? String ?? "0"
                                let cleanUpTime = currentServiceData["cleanup_time"] as? String ?? "0"
                                let notes = currentServiceData["notes"] as? String ?? ""
                                let sub_orderid = currentServiceData["sub_orderid"] as? String ?? ""
                                let bookingStatus = currentServiceData["status"] as? String ?? ""
                                let service = servicelist(serviceId: serviceId, sub_orderid: sub_orderid, serviceName: serviceName, therapistName: therapistNumber, bookingDate: bookingDate, bookingTime: bookingTime, bookEndTime: bookEndTime, custName: custName, therapistArr: therapistArr, therepistNm: therapistNm, qty: qty, basePrice: basePrice, subTotal: subTotal, duration: duration,cart_id: cart_id, cat_thumb: cat_thumb, cleanup_time: cleanUpTime, notes: notes, bookingStatus: bookingStatus)
                                services.append(service)
                            }
                            
                            
                        }
                        
                        var currentBookingData = BookingsModel(status: status, orderid: orderid, cust_id: cust_id, firstName: firstName, lastName: lastName, customerEmail: customerEmail, serviceType: "", servicePrice: "", cust_phone: cust_phone, therapist_id: therapist_id, salon_id: salon_id, salon: salon,services: services, bookdt: bookdt, timeslot: timeslot, notes: notes, transaction_type: transaction_type, is_review: is_review, booking_id: booking_id, reviews: reviewData, share_url: share_url, booked_on: booked_on, amount: final_amount)
                        
                        if currentBookingData.status == "2" {
                            currentBookingData.cancellationStatus = .full
                        } else {
                            currentBookingData.services.forEach { service in
                                if service.bookingStatus == "2" {
                                    currentBookingData.cancellationStatus = .partial
                                }
                            }
                        }
                        if currentBookingData.services.count > 0{
                            if(self.compareDate(expiryDate: currentBooking["bookdt"] as? String ?? "", expiryTime: currentBooking["timeslot"] as? String ?? "") ){
                                self.upcomingbookingsData.append(currentBookingData)
                            }
                            else{
                                self.pastbookingsData.append(currentBookingData)
                            }
                        }
                        
                        
                    }
                    self.tableView.reloadData()
                    if self.upcomingbookingsData.count == 0 && self.pastUpcomingSegmentedControl.selectedSegmentIndex == 0{
                        self.noUpcomingBookings.isHidden = false
                    }else{
                        self.noUpcomingBookings.isHidden = true
                    }
                    
                }
                                
        }
    }
    
    @objc func changeSegmentedValue(){
        if pastUpcomingSegmentedControl.selectedSegmentIndex == 1{
            if pastbookingsData.count == 0{
                self.tabBarController?.view.makeToast(message: "No Data Found.")
            }
            tableView.isHidden = false
            noUpcomingBookings.isHidden = true
        }else{
            if upcomingbookingsData.count == 0{
                self.tabBarController?.view.makeToast(message: "No Data Found.")
            }
            tableView.isHidden = (upcomingbookingsData.count == 0)
            noUpcomingBookings.isHidden = !(upcomingbookingsData.count == 0)
        }
        tableView.reloadData()
    }
    
    func convertDateFormater2(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "hh:mm a, EEEE, dd MMM yyyy"
        return  dateFormatter.string(from: date)
    }
    
    func compareDate(expiryDate:String, expiryTime:String) -> Bool {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let finalDateString = expiryDate + " " + expiryTime
        if Date() <= dateFormatter.date(from: finalDateString) ?? Date() {
            return true
        } else {
            return false
        }
    }

}

extension NewBookingViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return pastUpcomingSegmentedControl.selectedSegmentIndex == 0 ? upcomingbookingsData.count : pastbookingsData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pastUpcomingSegmentedControl.selectedSegmentIndex == 0 ? upcomingbookingsData[section].services.count : pastbookingsData[section].services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewBookingTableViewCell", for: indexPath) as! NewBookingTableViewCell
        let data = pastUpcomingSegmentedControl.selectedSegmentIndex == 0 ? upcomingbookingsData[indexPath.section] : pastbookingsData[indexPath.section]
        cell.data = data
        cell.serviceData = data.services[indexPath.row]
        if pastUpcomingSegmentedControl.selectedSegmentIndex == 0{
            cell.manageBooking.setTitle("Manage Booking", for: .normal)
        }else{
            cell.manageBooking.setTitle("View Booking", for: .normal)
        }
        cell.manageBooking.isUserInteractionEnabled = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "ManageBookingsViewController") as! ManageBookingsViewController
        let data = pastUpcomingSegmentedControl.selectedSegmentIndex == 0 ? upcomingbookingsData[indexPath.section] : pastbookingsData[indexPath.section]
        destination.currentBooking = data
        destination.orderID = data.orderid
        destination.isComingFromPastBooking = !(pastUpcomingSegmentedControl.selectedSegmentIndex == 0)
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
