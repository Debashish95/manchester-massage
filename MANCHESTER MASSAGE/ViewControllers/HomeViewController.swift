//
//  HomeViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 15/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    
    var offerData = [OfferModel]()
    var serviceDescription = ""
    var activityIndicator: NVActivityIndicatorView!
    
    var cartButton: UIButton!
    var rightBarButton: ENMBadgedBarButtonItem!
    
    var backButton: UIButton!
    
    var bannerString = [String]()
    var serviceDetailsData: ServiceDetailsModel!
    var isComingFromFinalBooking = false
    var sharingCategory = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        serviceDescription = ""
        
        self.setHomePageNavBr(title: "MANCHESTER MASSAGE", isMenuRequired: true)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "HomeHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeHeaderTableViewCell")
         self.tableView.register(UINib(nibName: "HomeServiceListTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeServiceListTableViewCell")
        self.tableView.register(UINib(nibName: "HomeOffersTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeOffersTableViewCell")
        
        cartButton = UIButton(type: .custom)
        cartButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        cartButton.addTarget(self, action: #selector(self.cartButtonAction), for: .touchUpInside)
        cartButton.setLeftImage(image: FAType.FAShoppingCart, color: .black, state: .normal)
        rightBarButton = ENMBadgedBarButtonItem(customView: cartButton, value: "0")
        rightBarButton.badgeBackgroundColor = .red
        rightBarButton.badgeTextColor = .white
        rightBarButton.badgeValue = "\(self.appD.serviceCart.count)"
        self.navigationItem.rightBarButtonItem = rightBarButton

        /**
         backButton = UIButton(type: .custom)
         backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
         backButton.addTarget(self, action: #selector(self.gotoCategoryPage), for: .touchUpInside)
         backButton.setLeftImage(image: FAType.FALongArrowLeft, color: .black, state: .normal)
         let backBarButton = UIBarButtonItem(customView: backButton)
         self.navigationItem.rightBarButtonItems = [rightBarButton, backBarButton]
         
         */
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeValue), name: NSNotification.Name("changeCart"), object: nil)
        
        if sharingCategory != ""{
            let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
            destination.catID = sharingCategory
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
        getHomeData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isComingFromFinalBooking{
            isComingFromFinalBooking = false
            let destination = UIStoryboard(name: "Bookings", bundle: nil).instantiateViewController(withIdentifier: "MyBookingsViewController") as! MyBookingsViewController
            let navigationController = UINavigationController(rootViewController: destination)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: false, completion: nil)
        }
    }
    @objc func updateBadgeValue(){
        rightBarButton.badgeValue = "\(self.appD.serviceCart.count)"
    }
    
    override func menuButtonAction() {
        let destination = LeftViewController(nibName: "LeftViewController", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        destination.parentVC = self
        self.topViewController?.present(destination, animated: false, completion: nil)
    }
    
    @objc func seeAllButtonAction(isBeauty: Bool){
        self.tabBarController?.selectedIndex = isBeauty ? 3 : 2
//        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ServiceListViewController") as! ServiceListViewController
//        destination.isBeautyService = isBeauty
//        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getHomeData(){
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetAppversion?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&parent_cat_id=\(self.appD.selectedParentCategory.cat_id)"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            
            let home_slide = resposeJSON["home_slide"] as? NSArray ?? NSArray()
            self.bannerString.removeAll()
            for slides in home_slide{
                let slideImage = slides as? String ?? ""
                self.bannerString.append(slideImage)
            }
            
            let top_service = resposeJSON["top_service"] as? NSArray ?? NSArray()
            self.appD.serviceData.removeAll()
            for service in top_service{
                let currentService = service as? NSDictionary ?? NSDictionary()
                let cat_id = currentService["cat_id"] as? String ?? ""
                let cat_nm = currentService["cat_nm"] as? String ?? ""
                let thumb = currentService["thumb"] as? String ?? ""
                let maxPrice = currentService["max_price"] as? String ?? "0"
                let review_count = currentService["review_count"] as? String ?? "0"
                let review_avrg = currentService["review_avrg"] as? String ?? "0"
                let price = currentService["min_offer"] as? String ?? "0"
                let duration = currentService["duration"] as? String ?? "0"
                let cleanUpTime = currentService["cleanup_time"] as? String ?? "0"
                let durationInt = Int(duration) ?? 0
                let cleanUpInt = Int(cleanUpTime) ?? 0
                let durationToHour = (durationInt - cleanUpInt) * 60
                
                let discountFloat = ((Double(maxPrice) ?? 0) - (Double(price) ?? 0)) / (Double(maxPrice) ?? .leastNonzeroMagnitude)
                let discount = Int(discountFloat * 100)
                let discountString = "\(discount)% off"
                let services = ServiceModel(serviceId: cat_id, serviceName: cat_nm, serviceImage: thumb, servicePrice: price, serviceMaxPrice: maxPrice, discount: discountString, duration: durationToHour.secondsToTime(), cleanUpTime: cleanUpTime, rating: review_avrg, reviewCount: review_count)
                self.appD.serviceData.append(services)
            }
            
            
            let beauty_service = resposeJSON["beauty_service"] as? NSArray ?? NSArray()
            self.appD.beautyServiceData.removeAll()
            for service in beauty_service{
                let currentService = service as? NSDictionary ?? NSDictionary()
                let cat_id = currentService["cat_id"] as? String ?? ""
                let cat_nm = currentService["cat_nm"] as? String ?? ""
                let thumb = currentService["thumb"] as? String ?? ""
                let maxPrice = currentService["max_price"] as? String ?? "0"
                let review_count = currentService["review_count"] as? String ?? "0"
                let review_avrg = currentService["review_avrg"] as? String ?? "0"
                let price = currentService["min_offer"] as? String ?? "0"
                let duration = currentService["duration"] as? String ?? "0"
                let cleanUpTime = currentService["cleanup_time"] as? String ?? "0"
                let durationInt = Int(duration) ?? 0
                let cleanUpInt = Int(cleanUpTime) ?? 0
                let durationToHour = (durationInt - cleanUpInt) * 60
                
                let discountFloat = ((Double(maxPrice) ?? 0) - (Double(price) ?? 0)) / (Double(maxPrice) ?? .leastNonzeroMagnitude)
                let discount = Int(discountFloat * 100)
                let discountString = "\(discount)% off"
                let services = ServiceModel(serviceId: cat_id, serviceName: cat_nm, serviceImage: thumb, servicePrice: price, serviceMaxPrice: maxPrice, discount: discountString, duration: durationToHour.secondsToTime(), cleanUpTime: cleanUpTime, rating: review_avrg, reviewCount: review_count)
                self.appD.beautyServiceData.append(services)
            }
            
            let top_offer = resposeJSON["top_offer"] as? NSArray ?? NSArray()
            self.offerData.removeAll()
            for offer in top_offer{
                let currentOffer = offer as? NSDictionary ?? NSDictionary()
                let sdtl_id = currentOffer["sdtl_id"] as? String ?? ""
                let cart_id = currentOffer["cat_id"] as? String ?? ""
                let isBeauty = (currentOffer["parent_cat_name"] as? String ?? "").lowercased().contains("beauty")
                let name = currentOffer["name"] as? String ?? ""
                let duration = currentOffer["duration"] as? String ?? "0"
                let cleanUpTime = currentOffer["cleanup_time"] as? String ?? "0"
                let main_price = currentOffer["main_price"] as? String ?? "0"
                let offer_price = currentOffer["offer_price"] as? String ?? "0"
                let offer_text = currentOffer["offer_text"] as? String ?? "0"
                let pic = currentOffer["pic"] as? String ?? ""
                
                let durationInt = Int(duration) ?? 0
                let cleanUpInt = Int(cleanUpTime) ?? 0
                let discountamount = Int(currentOffer["offer_percent"] as? String ?? "0") ?? 0
                
                let durationToHour = (durationInt - cleanUpInt) * 60
                
                let descriptionText = "Was £\(main_price), Now £\(offer_price) for \(durationToHour.secondsToTime()) Massage"
                
                let offers = OfferModel(offerId: sdtl_id, offerName: name, offerImage: pic, offerDiscount: "\(discountamount)% Off",cartId: cart_id,main_price:main_price ,offer_price:offer_price ,duration: durationToHour.secondsToTime(), offerText: descriptionText, cleanUpTime: cleanUpTime, isBeauty: isBeauty)
                self.offerData.append(offers)
                
            }
            
            
            self.tableView.reloadData()
            self.getVanueWebService()
            self.getTimeSlots()
        }
    }
    
    func getVanueWebService(){
        let reuestURL = Config.mainUrl + "Data/GetVanue?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
//                self.tabBarController?.view.makeToast(message: Message.apiError)
//                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            let resultJSON = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
            let employeeArray = resultJSON["employee"] as? NSArray ?? NSArray()
            let vanue = resultJSON["vanue"] as? NSDictionary ?? NSDictionary()
            self.appD.vanue_latitude = vanue["vanue_lat"] as? String ?? ""
            self.appD.vanue_longitude = vanue["vanue_lon"] as? String ?? ""
            self.appD.employeeDetails.removeAll()
            for index in 0..<employeeArray.count{
                let currentEmployee = employeeArray[index] as? NSDictionary ?? NSDictionary()
                let id = currentEmployee["id"] as? String ?? ""
                let user_name = currentEmployee["user_name"] as? String ?? ""
                let user_lname = currentEmployee["user_lname"] as? String ?? ""
                let user_email = currentEmployee["user_email"] as? String ?? ""
                let phone = currentEmployee["phone"] as? String ?? ""
                
                let employeeData = EmployeeModel(id: id, user_name: user_name, user_lname: user_lname, user_email: user_email, phone: phone)
                self.appD.employeeDetails.append(employeeData)
            }

        }
    }
    
    func getTimeSlots(){
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let currentData = dateFormatterGet.string(from: Date())
        
        let reuestURL = Config.mainUrl + "Data/GetTimeSlot?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&vendor_id=4&date=" + currentData
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            let timeSlotArray = resposeJSON["result"] as? NSArray ?? NSArray()
                            
                            self.appD.timeSlots.removeAll()
                            for index in 0..<timeSlotArray.count{
                                let currentTime = timeSlotArray[index] as? String ?? ""
                                self.appD.timeSlots.append(currentTime)
                            }
                            
        }
    }
    
    @objc func gotoCategoryPage(){
        let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
        let navigationController = UINavigationController(rootViewController: destination)
        self.appD.window?.rootViewController = navigationController
        self.appD.window?.makeKeyAndVisible()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath, isBeauty: Bool) {
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = isBeauty ? self.appD.beautyServiceData[indexPath.item].serviceId : self.appD.serviceData[indexPath.item].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
    }
}


extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 1 ? offerData.count : 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderTableViewCell", for: indexPath) as! HomeHeaderTableViewCell
            cell.setData(bannerData: bannerString)
            cell.bannerView.layer.cornerRadius = 15
            cell.bannerView.clipsToBounds = true
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeOffersTableViewCell", for: indexPath) as! HomeOffersTableViewCell
            cell.setData(data: offerData[indexPath.row], from: self)
            cell.offerDiscount.layer.cornerRadius = cell.offerDiscount.frame.height / 2
            cell.offerDiscount.clipsToBounds = true
            cell.backgroundColor = UIColor(hexString: "#f1f1f1")
            cell.offerImage.layer.cornerRadius = cell.offerImage.frame.height / 2
            cell.offerImage.clipsToBounds = true
            
            cell.detailsBtn.tag = indexPath.row
            cell.detailsBtn.addTarget(self, action: #selector(detailsBtnAction(sender:)), for: .touchUpInside)
            
            cell.bookNow.tag = indexPath.row
            cell.bookNow.addTarget(self, action: #selector(bookNowBtnAction(sender:)), for: .touchUpInside)
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeServiceListTableViewCell", for: indexPath) as! HomeServiceListTableViewCell
            cell.isBeautyService = false
            cell.setData(data: self.appD.serviceData, from: self)
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeServiceListTableViewCell", for: indexPath) as! HomeServiceListTableViewCell
            cell.isBeautyService = true
            cell.setData(data: self.appD.beautyServiceData, from: self)
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    @objc func serviceDetailsBtnAction(sender:UIButton, isBeauty: Bool){
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = isBeauty ? self.appD.beautyServiceData[sender.tag].serviceId : self.appD.serviceData[sender.tag].serviceId
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
    @objc func detailsBtnAction(sender:UIButton){
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = offerData[sender.tag].cartId
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
    @objc func bookNowBtnAction(sender:UIButton){
        self.setData(catID: offerData[sender.tag].cartId)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1{
            let offerHeaderView = HomeOfferHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
            offerHeaderView.backgroundColor = UIColor(hexString: "#f1f1f1")
            return offerHeaderView
        }
        else{
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return (UIScreen.main.bounds.width * 0.48)
        case 1:
            return 160
        case 2, 3:
            if indexPath.section == 3 && self.appD.beautyServiceData.count == 0{
                return 0
            }
            if indexPath.section == 2 && self.appD.serviceData.count == 0{
                return 0
            }
            if UIDevice.current.userInterfaceIdiom == .pad{
                return (UIScreen.main.bounds.width / 1.7) * 2 + 20
            }else{
                return (UIScreen.main.bounds.width / 1.5) * 2 + 20
            }
        default:
            return .zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 50 : 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
            destination.catID = self.offerData[indexPath.row].cartId
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    
    func setData(catID:String){
        
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                _ = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? "0"
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(smId: (itmDict["sm_id"] as? String ?? ""), serviceID: (itmDict["service_id"] as? String ?? ""), catId: (itmDict["cat_id"] as? String ?? ""), serviceTitle: (itmDict["service_title"] as? String ?? ""), serviceItems: serviceItemsList)
                                    
                                    self.serviceDetailsData = serviceDataTemp
                                }
                                
                                let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
                                destination.modalPresentationStyle = .overCurrentContext
                                self.definesPresentationContext = true
                                destination.delegate = self
                                destination.fromPage = .home
                                destination.serviceData = self.serviceDetailsData
                                self.present(destination, animated: true, completion: nil)
                            }
                            
                            
        }
        
    }
    
    
    
    
}
extension HomeViewController: BookNowPopUpDelegate{
    func confirmButtonAction() {
        
    }
    
    func addServiceButtonActions() {
//        if UserDefaults.standard.fetchData(forKey: .skipLogin){
//            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
//            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
//                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
//                destination.isFromLeftMenu = true
//                let navigationController = UINavigationController(rootViewController: destination)
//                navigationController.modalPresentationStyle = .fullScreen
//                self.present(navigationController, animated: false, completion: nil)
//            }
//            alert.addAction(yesAction)
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//
//            }
//            alert.addAction(cancelAction)
//
//            self.present(alert, animated: false, completion: nil)
//        }else{
            let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
            self.navigationController?.pushViewController(detination, animated: true)
       // }
        
    }
}
extension Int {

    func secondsToTime() -> String {
        
        let (h,m,s) = (self / 3600, (self % 3600) / 60, (self % 3600) % 60)
        
        let h_string = h < 10 ? "0\(h)" : "\(h)"
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        _ =  s < 10 ? "0\(s)" : "\(s)"
        
        if(h_string == "00"){
            return "\(m_string) min"
        }else if(m_string == "00" && h_string != "00"){
            if((Int(h_string) ?? 0) > 1) {
                return "\(h_string) hours"
            } else {
                return "\(h_string) hour"
            }
        }else{
            if((Int(h_string) ?? 0) > 1) {
                return "\(h_string) hours \(m_string) min"
            } else {
                return "\(h_string) hour \(m_string) min"
            }
        }
    }
}
