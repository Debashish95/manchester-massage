//
//  SettingsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 08/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var menuItems = [MenuModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Settings")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        
        setupSettingsMenu()
    }
    
    func setupSettingsMenu(){
        
        let menu2 = MenuModel(menuName: "PROFILE", menuImage: .FAUser, isEnabled: !UserDefaults.standard.fetchData(forKey: .skipLogin))
        menuItems.append(menu2)
        
        let menuSupport = MenuModel(menuName: "SUPPORT", menuImage: .FASupport)
        menuItems.append(menuSupport)

        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let menu10 = MenuModel(menuName: "LOGIN", menuImage: .FASignIn)
            menuItems.append(menu10)
        }else{
            let menu9 = MenuModel(menuName: "LOGOUT", menuImage: .FASignOut)
            menuItems.append(menu9)
        }
    }

}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.setData(data: menuItems[indexPath.row])
        cell.backView.backgroundColor = UIColor(hexString: "#B0FFFFFF",alpha: 0.8)
        cell.menuNameLabel.textColor = UIColor.black
        cell.menuImage.tintColor = ThemeColor.buttonColor
        cell.bottomDividerView.isHidden = (indexPath.row == menuItems.count - 1)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
