//
//  PhysicalCartViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 19/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class PhysicalCartViewController: UIViewController {
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var addProductToCart: UIButton!
    @IBOutlet weak var noProductView: UIStackView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var footerView: UIStackView!
    
    var totalPrice = 0.0
    
    var activityIndicator: NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        self.setNavigationBar(titleText: "My Cart", isBackButtonRequired: true, isRightMenuRequired: false)
        
        checkOutButton.layer.cornerRadius = (checkOutButton.bounds.height / 2)
        checkOutButton.clipsToBounds = true
        checkOutButton.addTarget(self, action: #selector(checkOutButtonAction), for: .touchUpInside)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "PhysicalCartTableViewCell", bundle: nil), forCellReuseIdentifier: "PhysicalCartTableViewCell")
        totalLabel.text = ""
        
        addProductToCart.addTarget(self, action: #selector(addProductToCartAction), for: .touchUpInside)
        bottomHeight.constant = (self.tabBarController?.tabBar.bounds.height ?? 0)
    }
    override func viewDidAppear(_ animated: Bool) {
        addProductToCart.setLeftImage(image: .FAPlus, color: .white, state: .normal, size: CGSize(width: 25, height: 25))
        addProductToCart.layer.cornerRadius = addProductToCart.bounds.height / 2
        addProductToCart.clipsToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        updatePrice()
        tableView.reloadData()
    }
    
    @objc func addProductToCartAction(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func checkOutButtonAction(){
        let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
        destination.isFromCartPage = true
        destination.totalPriceFloat = Float(totalPrice)
        self.navigationController?.pushViewController(destination, animated: true)
//        let destination = PhysicalCartPaymentViewController(nibName: "PhysicalCartPaymentViewController", bundle: nil)
//        destination.totalPriceFloat = Float(totalPrice)
//        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func updatePrice(){
        totalPrice = 0.0
        for index in 0..<self.appD.cartList.count{
            let currentItem = self.appD.cartList[index]
            self.totalPrice += (Double(currentItem.price) ?? 0)
        }
        
        totalLabel.setFAText(
            prefixText: "",
            icon: .FAShoppingCart,
            postfixText: " \(self.appD.itemsInCart) Item(s) | \(CURRENCY_SYMBOL)\(String(totalPrice).formattedString())",
            size: 20)
        totalLabel.textColor = .white
        
        footerView.isHidden = (totalPrice == 0.0)
        
        noProductView.isHidden = !(totalPrice == 0.0)
    }
    
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCartList(){
        self.activityIndicator.startAnimating()
        self.appD.cartList.removeAll()
        let reuestURL = Config.mainUrl + "Data/Getcart?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                self.activityIndicator.stopAnimating()
                guard response.result.isSuccess else {
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    var itemsInCart = 0
                    for index in 0..<result.count{
                        let currentResult = result[index] as? NSDictionary ?? NSDictionary()

                        let prod_id = currentResult["prod_id"] as? String ?? ""
                        let prod_name = (currentResult["prod_name"] as? String ?? "")
                        let unit_price = currentResult["unit_price"] as? String ?? "0"
                        let price = currentResult["price"] as? String ?? "0"
                        let qnty = currentResult["qnty"] as? String ?? "0"
                        let thumb = currentResult["thumb"] as? String ?? ""
                        let base_price = currentResult["base_price"] as? String ?? ""
                        let discount_price = currentResult["discount_price"] as? String ?? ""
                        itemsInCart += (Int(qnty) ?? 0)
                        let finalCartProduct = ProductCartModel(prod_id: prod_id, prod_name: prod_name, unit_price: unit_price, price: price, base_price: base_price, discount_price: discount_price, qnty: qnty, thumb: thumb)
                        self.appD.cartList.append(finalCartProduct)
                        
                    }
                    self.appD.itemsInCart = "\(itemsInCart)"
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changePhysicalCart"), object: nil)
                self.updatePrice()
                self.tableView.reloadData()
            }
    }
    
    @objc func addToCartButtonAction(sender: UIButton){
        if self.appD.cartList.count > 0{
            addRemoveToCartAction(apiName: "addtocart", product_id: self.appD.cartList[sender.tag].prod_id)
        }
    }
    @objc func removeFromCartButtonAction(sender: UIButton){
        if self.appD.cartList.count > 0{
            addRemoveToCartAction(apiName: "removecart", product_id: self.appD.cartList[sender.tag].prod_id)
        }
    }
    
    @objc func deleteFromCartButtonAction(sender: UIButton){
        if self.appD.cartList.count > 0{
            let alert = UIAlertController(title: "Alert!", message: "Are you sure you want to delete the item from the cart?", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (_) in
                self.addRemoveToCartAction(
                    apiName: "removecart",
                    product_id: self.appD.cartList[sender.tag].prod_id,
                    quantity: self.appD.cartList[sender.tag].qnty
                )
            }
            let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: false, completion: nil)
            
        }
        
    }
    
    func addRemoveToCartAction(apiName: String, product_id: String, quantity: String = "1"){
        
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId).toData,
            "prod_count": quantity.toData,
            "prod_id": product_id.toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + apiName , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let status = resposeJSON["status"] as? Bool ?? false
                    let msg = resposeJSON["msg"] as? String ?? ""
                    if status{
                        self.getCartList()
                        
                        NotificationCenter.default.post(name: NSNotification.Name("getProductDetails"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name("getProductList"), object: nil)
                        if apiName == "addtocart"{
                            self.tabBarController?.view.makeToast(message: "Product has been added to cart successfully.")
                        }else{
                            self.tabBarController?.view.makeToast(message: "Product has been removed from cart successfully.")
                        }
                    }else{
                        self.tabBarController?.view.makeToast(message: msg)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})

    }

}

extension PhysicalCartViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appD.cartList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhysicalCartTableViewCell", for: indexPath) as! PhysicalCartTableViewCell
        cell.data = self.appD.cartList[indexPath.row]
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteFromCartButtonAction(sender:)), for: .touchUpInside)
        
        cell.productQuantity.rightButton.tag = indexPath.row
        cell.productQuantity.rightButton.addTarget(self, action: #selector(addToCartButtonAction(sender:)), for: .touchUpInside)
        cell.productQuantity.leftButton.tag = indexPath.row
        cell.productQuantity.leftButton.addTarget(self, action: #selector(removeFromCartButtonAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let destination = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
        destination.productID = self.appD.cartList[indexPath.row].prod_id
        destination.pageTitle = self.appD.cartList[indexPath.row].prod_name
        
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
}
