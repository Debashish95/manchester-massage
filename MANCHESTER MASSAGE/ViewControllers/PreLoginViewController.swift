//
//  PreLoginViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 14/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit

class PreLoginViewController: UIViewController {

    @IBOutlet weak var skipLoginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    var isFromLeftMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loginButton.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        self.registrationButton.addTarget(self, action: #selector(registerButtonAction), for: .touchUpInside)
        self.skipLoginButton.addTarget(self, action: #selector(self.skipLoginButtonAction), for: .touchUpInside)
        self.skipLoginButton.setTitleColor(ThemeColor.buttonColor, for: UIControl.State())
        self.skipLoginButton.isHidden = isFromLeftMenu
        self.setNavigationBar(titleText: "", isBackButtonRequired: isFromLeftMenu, isRightMenuRequired: false)
        
    }
    
    override func viewDidLayoutSubviews() {
        registrationButton.layer.borderColor = ThemeColor.buttonColor.cgColor
        registrationButton.layer.borderWidth = 1.0
        registrationButton.layer.cornerRadius = 10
        registrationButton.clipsToBounds = true
        
        loginButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        subHeadingLabel.textColor = ThemeColor.buttonColor
    }
    
    @objc func skipLoginButtonAction(){
        UserDefaults.standard.saveData(value: true, key: .skipLogin)
//        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//        let navigationController = UINavigationController(rootViewController: destination)
//        self.appD.window?.rootViewController = navigationController
//        self.appD.window?.makeKeyAndVisible()
        
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
        self.appD.window?.rootViewController = destination
        self.appD.window?.makeKeyAndVisible()
//        let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
//        let navigationController = UINavigationController(rootViewController: destination)
//        self.appD.window?.rootViewController = navigationController
//        self.appD.window?.makeKeyAndVisible()
        
    }

    @objc func loginButtonAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navBar = UINavigationController(rootViewController: destination)
        navBar.modalPresentationStyle = .fullScreen
        self.present(navBar, animated: true, completion: nil)
    }
    
    @objc func registerButtonAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        let navBar = UINavigationController(rootViewController: destination)
        navBar.modalPresentationStyle = .fullScreen
        self.present(navBar, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
