//
//  ParentCategoryViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 14/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class ParentCategoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: NVActivityIndicatorView!
    
    var parentCategoryData = [ParentCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        self.setNavigationBar(titleText: "Choose Category", isBackButtonRequired: false)
        
        tableView.register(UINib(nibName: "ParentCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "ParentCategoryTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        getParentCategoryList()
    }


    fileprivate func getParentCategoryList(){
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetAllCats?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                let results = resposeJSON["result"] as? NSArray ?? NSArray()
                for index in 0..<results.count{
                    let currentObject = results[index] as? NSDictionary ?? NSDictionary()
                    let cat_id = currentObject["cat_id"] as? String ?? ""
                    let cat_nm = currentObject["cat_nm"] as? String ?? ""
                    let thumb = currentObject["thumb"] as? String ?? ""
                    let banner = currentObject["thumb"] as? String ?? ""
                    let currentCategory = ParentCategory(cat_id: cat_id, cat_nm: cat_nm, thumb: thumb, banner: banner)
                    self.parentCategoryData.append(currentCategory)
                }
                self.tableView.reloadData()
                
            }
    }

}

extension ParentCategoryViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parentCategoryData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParentCategoryTableViewCell", for: indexPath) as! ParentCategoryTableViewCell
        cell.data = parentCategoryData[indexPath.row]
        cell.backView.cornnerRadius = 10
        cell.backView.clipsToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.appD.selectedParentCategory = parentCategoryData[indexPath.row]
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigationController = UINavigationController(rootViewController: destination)
        self.appD.window?.rootViewController = navigationController
        self.appD.window?.makeKeyAndVisible()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.width * (9/16)
    }
}

//900/500

