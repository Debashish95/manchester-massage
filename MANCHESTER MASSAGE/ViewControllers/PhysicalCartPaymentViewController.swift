//
//  PhysicalCartPaymentViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 31/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import NVActivityIndicatorView
import CreditCardForm

class PhysicalCartPaymentViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var mySavedCardButton: UIButton!
    @IBOutlet weak var cardNameTextField: UITextField!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var creditCardForm: CreditCardFormView!
    
    @IBOutlet weak var mySavedCardLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveCardButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    let stripeBackendURL = "https://manchester-massage-test.herokuapp.com/"
    var totalPriceFloat = Float(0.0)
    var paymentTextField = STPPaymentCardTextField()
    var buyAgainItems = [ProductCartModel]()
    var addressID = "0"
    var addressData = [AddressModel]()
    
    var savedCardList = [CardModel]()
    var savedCardCVV = ""
    var useSavedCard = false

    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Payment", isBackButtonRequired: true, isRightMenuRequired: false)
        
        
        payNowButton.addTarget(self, action: #selector(preOrder), for: .touchUpInside)
        setupCardView()
//        bottomHeight.constant = (self.tabBarController?.tabBar.bounds.height ?? 0) + 5
        paymentTextField.frame = self.cardView.bounds
        paymentTextField.delegate = self
        paymentTextField.postalCodeEntryEnabled = false
        paymentTextField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cardView.addSubview(paymentTextField)
        
        creditCardForm.paymentCardTextFieldDidChange(cardNumber: paymentTextField.cardNumber ?? "", expirationYear: UInt(paymentTextField.expirationYear) , expirationMonth: UInt(paymentTextField.expirationMonth), cvc: paymentTextField.cvc)
        
        saveCardButton.setImage(UIImage(icon: .FASquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        saveCardButton.setImage(UIImage(icon: .FACheckSquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .selected)
        saveCardButton.setTitle("Save this card for future use", for: .normal)
        saveCardButton.setTitleColor(UIColor.black, for: .normal)
      
        saveCardButton.addTarget(self, action: #selector(self.saveCardButtonAction), for: .touchUpInside)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SavedCardTableViewCell", bundle: nil), forCellReuseIdentifier: "SavedCardTableViewCell")
        
//        self.getSavedCardList()
        mySavedCardButton.addTarget(self, action: #selector(self.mySavedCardButtonAction), for: .touchUpInside)
    }
    override func viewDidAppear(_ animated: Bool) {
        cardNameTextField.addBottomBorderWithColor(color: .gray, width: 1)
    }
    func setupCardView(){
        cardNameTextField.borderStyle = .none
        cardNameTextField.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        cardNameTextField.setPlaceholder(placeholder: "Card Holder Name", color: .gray)
        cardNameTextField.setRightViewFAIcon(icon: .FAUser, textColor: .gray)
        cardNameTextField.delegate = self
        cardNameTextField.text = "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))"
        cardNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        
        creditCardForm.cardHolderString = "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))"
        creditCardForm.cardNumberFont = UIFont.boldSystemFont(ofSize: 18)
        creditCardForm.cardPlaceholdersFont = UIFont.systemFont(ofSize: 14, weight: .medium)
        creditCardForm.cardTextFont = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func saveCardButtonAction(){
        saveCardButton.isSelected = !saveCardButton.isSelected
    }
    
    func getSavedCardList() {
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/getCardListStripe?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
            if((result["status"] as? Int ?? 0) == 1){
                let finalCardResult = result["result"] as? NSDictionary ?? NSDictionary()
                if let _ = finalCardResult["error"] as? NSDictionary{
                    self.tabBarController?.view.makeToast(message: "Unable to fetch your cards.")
                }else{
                    let cardListData = finalCardResult["data"] as? NSArray ?? NSArray()
                    var cardData = CardModel()
                    for card in cardListData {
                        let cardItem = card as? NSDictionary ?? NSDictionary()
                        cardData.cardID = cardItem["id"] as? String ?? ""
                        let cardObject = cardItem["card"] as? NSDictionary ?? NSDictionary()
                        cardData.brand = cardObject["brand"] as? String ?? ""
                        cardData.expMonth = String(cardObject["exp_month"] as? Int ?? 0)
                        cardData.expYear = String(cardObject["exp_year"] as? Int ?? 0)
                        cardData.last4Digit = cardObject["last4"] as? String ?? ""
                        self.savedCardList.append(cardData)
                    }
                    
                    self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
                }
            }else{
                self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
            }
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func preOrder(){
        self.view.endEditing(true)
        var fourDigitDate = ""
        var exp_month = ""
        var cardNumber = ""
        var cardCVV = ""
        var nameOnCard = ""
        if useSavedCard{
            let savedCard = self.savedCardList.first(where: {$0.isSelected}) ?? CardModel()
            fourDigitDate = savedCard.expYear
            exp_month = savedCard.expMonth
            cardNumber = savedCard.last4Digit
            cardCVV = savedCard.cvvText
        }else{
            cardNumber = paymentTextField.cardNumber ?? ""
            exp_month = "\(paymentTextField.expirationMonth)"
            let df = DateFormatter()
            df.dateFormat = "yy"
            guard let dt = df.date(from: paymentTextField.formattedExpirationYear ?? "") else { return }
            df.dateFormat = "yyyy"
            fourDigitDate = df.string(from: dt)
            cardCVV = paymentTextField.cvc ?? ""
            nameOnCard = cardNameTextField.text ?? ""
        }
        
        if cardNumber.isEmpty{
            self.tabBarController?.view.makeToast(message: "Please Enter Card Number")
            return
        }
        if cardCVV.isEmpty {
            self.tabBarController?.view.makeToast(message: "Please Enter Card CVV")
            return
        }
        self.activityIndicator.startAnimating()
        
        /**
         [{"prod_id":"23","qnty":"3","unit_price":"100","t_price":"300"},{"prod_id":"24","qnty":"2","unit_price":"120","t_price":"240"}]
         
         */
        var currentServiceCart = [[String: Any]()]
        currentServiceCart.removeAll()
        let tempCart = (buyAgainItems.count > 0) ? buyAgainItems : self.appD.cartList
        
        for index in 0..<tempCart.count{
            let currentServiceCartData =
                ["prod_id": tempCart[index].prod_id,
                 "qnty": tempCart[index].qnty,
                 "unit_price": tempCart[index].unit_price,
                 "t_price": tempCart[index].price,
                ] as [String : Any]
            currentServiceCart.append(currentServiceCartData)
        }
        
        let jsonData = try? JSONSerialization.data(withJSONObject: currentServiceCart, options: [])
        let cartString = String(bytes: jsonData!, encoding: .utf8) ?? ""
        
        let parameters = [
            "amount": String(Int(self.totalPriceFloat)),
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "cart": cartString,
            "address_id": addressID,
            "source": "ios_capp"
            ] as [String : String]
        Alamofire.request(Config.apiEndPoint + "preorder", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        self.payNowButtonAction(order_id: result["order_id"] as? String ?? "")
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                    
                }
            }
        
    }

    
    @objc func finalOrder(json: NSDictionary, payment_id: String, order_id: String){
        self.activityIndicator.startAnimating()
        let jsonData = try? JSONSerialization.data(withJSONObject: json, options: [])
        let cartString = String(bytes: jsonData!, encoding: .utf8) ?? ""
        
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "order_id": order_id,
            "payment_id": payment_id,
            "payment_info": cartString
            ] as [String : Any]
        let url = URL(string: "https://developer.pampertree.co.uk/api/postdata/finalorder")!
        Alamofire.request(url, method: .post, parameters: parameters)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        let txnID = result["order_id"] as? String ?? ""
                        let msg = result["msg"] as? String ?? ""
                        let alert = UIAlertController(title: "Success!", message: "\(msg) \n Transaction ID: #\(txnID)", preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
                            self.appD.cartList.removeAll()
                            self.appD.itemsInCart = ""
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changePhysicalCart"), object: nil)
//                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                            let navigationController = UINavigationController(rootViewController: destination)
//                            self.appD.window?.rootViewController = navigationController
//                            self.appD.window?.makeKeyAndVisible()
                            
                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
                            destination.isComingFromProductPage = true
                            self.appD.window?.rootViewController = destination
                            self.appD.window?.makeKeyAndVisible()
                        }
                        alert.addAction(yesAction)
                        self.present(alert, animated: false, completion: nil)
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                }
            }
        
    }
    
    @objc func mySavedCardButtonAction(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MySavedCardViewController") as! MySavedCardViewController
        destination.delegate = self
        destination.totalPriceFloat = self.totalPriceFloat
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func payNowButtonAction(order_id: String){

        self.view.endEditing(true)
        var fourDigitDate = ""
        var exp_month = ""
        var cardNumber = ""
        var cardCVV = ""
        var nameOnCard = ""
        if useSavedCard{
            let savedCard = self.savedCardList.first(where: {$0.isSelected}) ?? CardModel()
            fourDigitDate = savedCard.expYear
            exp_month = savedCard.expMonth
            cardNumber = savedCard.last4Digit
            cardCVV = savedCard.cvvText
        }else{
            cardNumber = paymentTextField.cardNumber ?? ""
            exp_month = "\(paymentTextField.expirationMonth)"
            let df = DateFormatter()
            df.dateFormat = "yy"
            guard let dt = df.date(from: paymentTextField.formattedExpirationYear ?? "") else { return }
            df.dateFormat = "yyyy"
            fourDigitDate = df.string(from: dt)
            cardCVV = paymentTextField.cvc ?? ""
            nameOnCard = cardNameTextField.text ?? ""
        }
        
        if cardNumber.isEmpty{
            self.tabBarController?.view.makeToast(message: "Please Enter Card Number")
            return
        }
        if cardCVV.isEmpty {
            self.tabBarController?.view.makeToast(message: "Please Enter Card CVV")
            return
        }
        
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId),
            "amount": "\(totalPriceFloat)",
            "tran_id": "PMP\(String().randomString(length: 12))",
            "card_number": cardNumber,
            "card_exp_month": exp_month,
            "card_exp_year": fourDigitDate,
            "card_cvc": cardCVV,
            "save_card": saveCardButton.isSelected ? "1" : "0",
            "new_card": useSavedCard ? "0" : "1",
            "name_on_card": nameOnCard
        ]as [String:String]

        Alamofire.request("\(Config.apiEndPoint)stripe_charge_card", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate().responseJSON { (response) in
                
                print(response)
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                self.activityIndicator.stopAnimating()
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    self.navigationController?.popViewController(animated: true)
                    break
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let json = (jSONResponse as? NSDictionary ?? NSDictionary())
                    self.tabBarController?.view.makeToast(message: "Payment Successful.")
                    self.finalOrder(json: json, payment_id: json["id"] as? String ?? "", order_id: order_id)
                    self.navigationController?.popViewController(animated: true)
                    break
                }
        }
    }
    
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if useSavedCard{
            for index in 0..<savedCardList.count{
                savedCardList[index].isSelected = false
            }
            useSavedCard = false
            self.tableView.reloadData()
        }
        creditCardForm.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: UInt(textField.expirationYear), expirationMonth: UInt(textField.expirationMonth), cvc: textField.cvc)
    }
    
    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidEndEditingExpiration(expirationYear: UInt(textField.expirationYear))
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidBeginEditingCVC()
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidEndEditingCVC()
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        for index in 0 ..< self.savedCardList.count{
            if self.savedCardList[index].isSelected{
                self.savedCardList[index].cvvText = textField.text ?? ""
            }
        }
        
    }
    
}

extension PhysicalCartPaymentViewController: STPAuthenticationContext{
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}

extension PhysicalCartPaymentViewController: UITextFieldDelegate{
    @objc func textFieldDidChange(_ textField: UITextField) {
        creditCardForm.cardHolderString = textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cardNameTextField {
            textField.resignFirstResponder()
            paymentTextField.becomeFirstResponder()
        } else if textField == paymentTextField  {
            textField.resignFirstResponder()
        }
        return true
    }
}


extension PhysicalCartPaymentViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCardList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardTableViewCell", for: indexPath) as! SavedCardTableViewCell
        cell.data = self.savedCardList[indexPath.row]
        cell.cvvTextField.delegate = self
        cell.cvvTextField.keyboardType = .numberPad
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for index in 0 ..< self.savedCardList.count{
            if indexPath.row == index{
                if self.savedCardList[index].isSelected{
                    self.savedCardList[index].isSelected = false
                    useSavedCard = false
                }else{
                    self.savedCardList[index].isSelected = true
                    useSavedCard = true
                }
            }else{
                self.savedCardList[index].isSelected = false
            }
            self.savedCardList[index].cvvText = ""
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}


extension PhysicalCartPaymentViewController: MySavedCardViewControllerDelegate{
    func response(data: [String : Any]?, error: [String : String]?) {
        if data != nil, let id = data?["id"] as? String, let json = data?["json"] as? NSDictionary{
            self.finalOrder(json: json, payment_id: id, order_id: "")
        }else{
            let alert = UIAlertController(title: "Alert!", message: "Sorry! Something went wrong. Please try again!!", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
