//
//  ReviewListViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 18/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import Cosmos

class ReviewListViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var totalReviewAVG: CosmosView!
    @IBOutlet weak var totalReviewsCountLabel: UILabel!
    @IBOutlet weak var serviceReview: CosmosView!
    @IBOutlet weak var staffReview: CosmosView!
    @IBOutlet weak var cleanReview: CosmosView!
    @IBOutlet weak var overallReview: CosmosView!
    
    var catId = ""
    var allReviews = [ReviewModel]()
    
    var totalReview = 0
    var serviceName = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationBar(titleText: serviceName, isBackButtonRequired: true)
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        ratingLabel.textColor = UIColor(red: 1, green: 149/255, blue: 0, alpha: 1)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ReviewListTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewListTableViewCell")
        self.topView.isHidden = true
        
        totalReviewAVG.isUserInteractionEnabled = false
        serviceReview.isUserInteractionEnabled = false
        staffReview.isUserInteractionEnabled = false
        cleanReview.isUserInteractionEnabled = false
        overallReview.isUserInteractionEnabled = false
        
        getServiceCatReviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        topView.dropShadowView()
    }
    
    func getServiceCatReviews(){
        self.activityIndicator.startAnimating()
        let requestURL = (Config.mainUrl + "Data/getServiceCatReviews?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&service_cat=" + self.catId)
        let urlConvertible = URL(string: requestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            print(resposeJSON)
                            
                            var totalServiceReview = 0.0
                            var totalStaffReview = 0.0
                            var totalCleanReview = 0.0
                            var totalOverallReview = 0.0
                            
                            
                            if (resposeJSON["status"] as? Bool ?? false){
                                let results = resposeJSON["result"] as? NSArray ?? NSArray()
                                results.forEach { (result) in
                                    let currentResult = result as? NSDictionary ?? NSDictionary()
                                    
                                    totalServiceReview += (Double(currentResult["service_review"] as? String ?? "0") ?? 0)
                                    totalStaffReview += (Double(currentResult["staff_review"] as? String ?? "0") ?? 0)
                                    totalCleanReview += (Double(currentResult["cleanliness_review"] as? String ?? "0") ?? 0)
                                    totalOverallReview += (Double(currentResult["overall_review"] as? String ?? "0") ?? 0)
                                    
                                    let reviewObject = ReviewModel(
                                        reviewer_name: currentResult["reviewer_name"] as? String ?? "",
                                        reviewer_email: currentResult["reviewer_email"] as? String ?? "",
                                        review: currentResult["review"] as? String ?? "",
                                        service_review: currentResult["service_review"] as? String ?? "0",
                                        staff_review: currentResult["staff_review"] as? String ?? "0",
                                        cleanliness_review: currentResult["cleanliness_review"] as? String ?? "0",
                                        overall_review: currentResult["overall_review"] as? String ?? "0",
                                        created_on: currentResult["created_on"] as? String ?? "")
                                    self.allReviews.append(reviewObject)
                                }
                                
                            }else{
                                self.tabBarController?.view.makeToast(message: resposeJSON["Message"] as? String ?? "")
                            }
                            
                            if self.allReviews.count > 0{
                                self.topView.isHidden = false
                                self.totalReview = self.allReviews.count
                                self.totalReviewsCountLabel.text = "Based on \(self.totalReview) review(s)"
                                self.totalReviewAVG.rating = ((totalServiceReview +
                                                                totalStaffReview +
                                                                totalCleanReview +
                                                                totalOverallReview) / 4) / Double(self.totalReview)
                                self.ratingLabel.text = "\(Double(round(10*self.totalReviewAVG.rating)/10))"
                                
                                self.staffReview.rating = ((totalStaffReview) / Double(self.totalReview))
                                self.serviceReview.rating = ((totalServiceReview) / Double(self.totalReview))
                                self.cleanReview.rating = ((totalCleanReview) / Double(self.totalReview))
                                self.overallReview.rating = ((totalOverallReview) / Double(self.totalReview))
                                self.tableView.reloadData()
                            }else{
                                let alert = UIAlertController(title: "Alert!", message: "No reviews found.", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    self.navigationController?.popViewController(animated: true)
                                }
                                alert.addAction(okAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            
                          }
    }

}

extension ReviewListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewListTableViewCell", for: indexPath) as! ReviewListTableViewCell
        cell.ratingView.rating = Double(self.allReviews[indexPath.row].overall_review) ?? 0.0
        cell.reviewLabel.text = self.allReviews[indexPath.row].review
        cell.nameLabel.text = self.allReviews[indexPath.row].reviewer_name
        let date1 = self.allReviews[indexPath.row].created_on.convertDate(format: "yyyy-MM-dd HH:mm:ss") ?? Date()
        let timeDiff = Date().offset(from: date1)
        cell.dateLabel.text = "Posted \(timeDiff) ago"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

struct ReviewModel{
    var reviewer_name: String
    var reviewer_email: String
    var review: String
    var service_review: String = "0"
//    var valueformoney_review: String
    var staff_review: String = "0"
    var cleanliness_review: String = "0"
    var overall_review: String = "0"
    var created_on: String
    var review_org: String = ""
}
