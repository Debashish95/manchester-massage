//
//  ManageBookingsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 03/12/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import MessageUI

class ManageBookingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicator: NVActivityIndicatorView!
    var currentBooking: BookingsModel!
    var orderID = ""
    
    var bookingPageMenu = [BookingMenuModel]()
    var isComingFromPastBooking = false
    var currentRescheduleIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("Order ID: \(orderID) => \(currentBooking.cancellationStatus.rawValue)")
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        self.setNavigationBar(titleText: "Manage Booking", isBackButtonRequired: true, isRightMenuRequired: false)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "UpComingBookingTableViewCell", bundle: nil), forCellReuseIdentifier: "UpComingBookingTableViewCell")
        
        tableView.estimatedRowHeight = 200
        let cellsToRegister = ["UpComingBookingTableViewCell", "ManageBookingServiceTableViewCell", "ManageBookingFooterTableViewCell", "ManageBookingMapTableViewCell"]
        cellsToRegister.forEach { cellName in
            tableView.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeReview), name: NSNotification.Name("hideReviewButton"), object: nil)
        
        createBookingPageMenu()
    }
    
    fileprivate func createBookingPageMenu(){
        
        bookingPageMenu.removeAll()
        
        if !isComingFromPastBooking && currentBooking.services.filter({$0.bookingStatus != "2"}).count > 1{
            let menu1 = BookingMenuModel(image: UIImage(named: "camcel_booking"), menuName: "Cancel All Bookings", accessibilityName: "cancelbooking")
            bookingPageMenu.append(menu1)
        }
        
        let menu2 = BookingMenuModel(image: UIImage(named: "reciept"), menuName: "Receipt", accessibilityName: "receipt")
        bookingPageMenu.append(menu2)
        
        let menu3 = BookingMenuModel(image: UIImage(named: "weather"), menuName: "Check Weather", accessibilityName: "weather")
        bookingPageMenu.append(menu3)
        
        let menu4 = BookingMenuModel(image: UIImage(named: "call"), menuName: "Call or SMS", accessibilityName: "call")
        bookingPageMenu.append(menu4)
        
        if (isComingFromPastBooking) && currentBooking.status != "2"{
            if currentBooking.is_review == "1"{
                let menu6 = BookingMenuModel(image: nil, menuName: "View Review", accessibilityName: "view_review")
                bookingPageMenu.append(menu6)
            }else{
                let menu6 = BookingMenuModel(image: nil, menuName: "Add Review", accessibilityName: "add_review")
                bookingPageMenu.append(menu6)
            }
            
        }
        
        if (currentBooking.status == "2" || isComingFromPastBooking){
            let menu6 = BookingMenuModel(image: nil, menuName: "Rebook", accessibilityName: "rebook")
            bookingPageMenu.append(menu6)
        }

        let menu5 = BookingMenuModel(image: nil, menuName: "Share Booking", accessibilityName: "share")
        bookingPageMenu.append(menu5)
    }
    
    
    @objc func removeReview(notification: NSNotification){
//        self.reviewView.isHidden = true
        let reviewInfo = (notification.userInfo! as NSDictionary)
        currentBooking.is_review = "1"
        currentBooking.reviews = ReviewModel(reviewer_name: reviewInfo["r_name"] as? String ?? "",
                                          reviewer_email: reviewInfo["r_email"] as? String ?? "",
                                          review: reviewInfo["review_text"] as? String ?? "",
                                          service_review: reviewInfo["service_review"] as? String ?? "0",
                                          staff_review: reviewInfo["staff_review"] as? String ?? "0",
                                          cleanliness_review: reviewInfo["cleanliness_review"] as? String ?? "",
                                          overall_review: reviewInfo["overall_review"] as? String ?? "0",
                                          created_on: reviewInfo["created_on"] as? String ?? "")
        createBookingPageMenu()
        tableView.reloadSections(IndexSet(integer: currentBooking.services.count), with: .none)
    }
    
    func convertDateFormater1(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "EEEE\ndd MMM yyyy"
        return  dateFormatter.string(from: date)
    }
    
    
    @objc fileprivate func cancelAllBookings(){
        if isChangeBookingPossible(bookingDate: currentBooking.bookdt, bookingTime: currentBooking.timeslot){
            
            let alert = UIAlertController(title: "Cancellation Policy", message: "If you are unable to attend your booking you can cancel it but please give the venue as much as warning as possible.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "REQUEST CANCELLATION", style: .destructive) { _ in
                self.cancelBookingAPI()
            }
            let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            self.showAlert(title: "Sorry!", message: "Booking can't be cancelled in last 24 Hours.")
        }
    }
    
    
    @objc fileprivate func singleCancel(sender: UIButton){
        if isChangeBookingPossible(bookingDate: currentBooking.services[sender.tag].bookingDate, bookingTime: currentBooking.services[sender.tag].bookingTime){
            
            let alert = UIAlertController(title: "Cancellation Policy", message: "If you are unable to attend your booking you can cancel it but please give the venue as much as warning as possible.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "REQUEST CANCELLATION", style: .destructive) { _ in
                self.cancelSingleServiceAPI(subOrderId: self.currentBooking.services[sender.tag].sub_orderid)
            }
            let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(noAction)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            self.showAlert(title: "Sorry!", message: "Booking can't be cancelled in last 24 Hours.")
        }
    }
    
    @objc fileprivate func rescheduleButtonAction(sender: UIButton){
        if sender.accessibilityLabel == "book_again"{
            //Book Again Action
            bookAgainAction(sender: sender)
        }else{
            //reschedule Action
            rescheduleAction(sender: sender)
        }
    }
    
    func bookAgainAction(sender: UIButton){
        let destination = UIStoryboard(name: "Home", bundle:  nil).instantiateViewController(withIdentifier: "ServiceDetailsViewController") as! ServiceDetailsViewController
        destination.catID = currentBooking.services[sender.tag].cart_id
        destination.isFromBookingPage = true
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func rescheduleAction(sender: UIButton){
        
        if !isChangeBookingPossible(bookingDate: currentBooking.services[sender.tag].bookingDate, bookingTime: currentBooking.services[sender.tag].bookingTime){
            let alert = UIAlertController(title: "Alert!", message: "Booking can't be rescheduled in last 24 Hours.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
            }
            alert.addAction(yesAction)
            self.present(alert, animated: false, completion: nil)
            return
        }
        
        let destination = RescheduleBooking(nibName: "RescheduleBooking", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        var currentCart = [CartModel]()
        currentBooking.services.forEach { (currentService) in
            
            let currentServiceData = ServiceItemsModel(serviceID: currentService.serviceId, name: currentService.serviceName, duration: currentService.duration, cleanupTime: currentService.cleanup_time, main_price: currentService.basePrice, offerPrice: currentService.subTotal, therapistNumber: "\(currentService.therapistArr.components(separatedBy: ",").count)")
            
            let isForMe = false
                
//                (currentService.custName == "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \((UserDefaults.standard.fetchData(forKey: .userLastName)))")
            
            var therapistIDs = currentBooking.services[sender.tag].therapistArr.components(separatedBy: ",").map({$0.replacingOccurrences(of: "]", with: "")})
            
            therapistIDs = therapistIDs.map({$0.replacingOccurrences(of: "[", with: "")})
            var finalTherapists = self.appD.employeeDetails.filter { (emp) -> Bool in
                return therapistIDs.contains(where: {$0 == emp.id})
            }
            if finalTherapists.count == 0{
                finalTherapists = self.getMyTherapists(data: currentBooking.services[sender.tag].therapistArr)
            }
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            df.timeZone = .current
            let dateData = df.date(from: currentService.bookingDate) ?? Date()
            df.dateFormat = "EEEE"
            let finalDate = df.string(from: dateData)
            
            let tempCart = CartModel(itemId: (currentCart.count + 1), services: [currentServiceData], cat_id: currentService.cart_id, vendor_id: currentBooking.salon_id, tot_amount: currentBooking.servicePrice, final_amount: currentService.subTotal, isForMe: isForMe, bookingTime: currentService.bookingTime, bookingEndTime: currentService.bookEndTime, bookingDay: finalDate, bookingDate: currentService.bookingDate, cust_name: currentService.custName, cust_email: currentBooking.customerEmail, cust_phone: currentBooking.cust_phone, therapists: finalTherapists, serviceID: currentService.serviceId, qty: currentService.qty, other_name: currentService.custName)
            currentCart.append(tempCart)
           
        }
        if currentCart.count > 0{
            destination.bookedServices = currentCart
            destination.currentCartService = currentCart[sender.tag]
            destination.startingIndex = sender.tag
            destination.delegate = self
            destination.delegate = self
            self.currentRescheduleIndex = sender.tag
    //        destination.servicesArray = currentBooking.services.map({$0.serviceId})
            self.present(destination, animated: true, completion: nil)
        }else{
            self.tabBarController?.view.makeToast(message: "No Services found to reschedule.")
        }
        
    }
    
    func isChangeBookingPossible(bookingDate: String, bookingTime: String) -> Bool{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        df.timeZone = .current
        let dateString = "\(bookingDate) \(bookingTime)"
        let bookingDate = df.date(from: dateString) ?? Date()
        let bookingChangePossibleDate = Calendar.current.date(byAdding: .hour, value: -24, to: bookingDate) ?? Date()
        return Date()<=bookingChangePossibleDate
    }
    
    @objc func rebookAction(){
        self.appD.serviceCart.removeAll()
        for index in 0..<currentBooking.services.count{
            let currentService = currentBooking.services[index]
            let serviceItem = ServiceItemsModel(serviceID: currentService.serviceId, name: currentService.serviceName, duration: currentService.duration, cleanupTime: currentService.cleanup_time, main_price: currentService.basePrice, offerPrice: currentService.subTotal, therapistNumber: "\(currentService.therapistArr.components(separatedBy: ",").count)")
            
            var cartData =  CartModel(itemId: (self.appD.serviceCart.count + 1), services: [serviceItem], cat_id: currentService.cart_id, vendor_id: "4", tot_amount: currentService.basePrice, final_amount: currentService.subTotal)
            cartData.serviceID = currentService.serviceId
            cartData.therapists.removeAll()
            cartData.isForMe = currentService.custName == ("\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))")
            cartData.cust_name = cartData.isForMe ? "" : currentService.custName
            self.appD.serviceCart.append(cartData)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
        
        let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
        self.navigationController?.pushViewController(detination, animated: true)

    }
}

extension ManageBookingsViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentBooking.services.count + 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == currentBooking.services.count {
            return bookingPageMenu.count
        }
        if section == currentBooking.services.count + 1{
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == currentBooking.services.count + 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ManageBookingMapTableViewCell", for: indexPath) as! ManageBookingMapTableViewCell
            cell.setUpCell()
            return cell
        }
        if indexPath.section == currentBooking.services.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ManageBookingFooterTableViewCell", for: indexPath) as! ManageBookingFooterTableViewCell
            cell.data = bookingPageMenu[indexPath.row]
            return cell
        }
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpComingBookingTableViewCell", for: indexPath) as! UpComingBookingTableViewCell
            cell.topImage.kf.setImage(with: URL(string: self.currentBooking.services[indexPath.section].cat_thumb), placeholder: UIImage(named: "doodle.png"), options: nil, progressBlock: nil) { (result) in
                cell.topImage.contentMode = .scaleAspectFill
            }
            
            cell.cancelView.isHidden = (isComingFromPastBooking || currentBooking.services[indexPath.section].bookingStatus == "2")
                    
            cell.massageDate.text = convertDateFormater1(self.currentBooking.services[indexPath.section].bookingDate)
            cell.massageTime.text = self.currentBooking.services[indexPath.section].bookingTime
            cell.cancelView.isHidden = !(self.currentBooking.services[indexPath.section].bookingStatus == "2")
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ManageBookingServiceTableViewCell", for: indexPath) as! ManageBookingServiceTableViewCell
            cell.data = currentBooking.services[indexPath.section]
            cell.cancelButton.tag = indexPath.section
            cell.rescheduleButton.tag = indexPath.section
            let rescheduleText = (isComingFromPastBooking || self.currentBooking.services[indexPath.section].bookingStatus == "2") ? "Book Again" : "Reschedule"
            cell.rescheduleButton.accessibilityLabel = (self.currentBooking.services[indexPath.section].bookingStatus == "2") || (isComingFromPastBooking) ? "book_again" : "reschedule"
            cell.rescheduleButton.setTitle(rescheduleText, for: .normal)
            cell.cancelButton.isHidden = isComingFromPastBooking || self.currentBooking.services[indexPath.section].bookingStatus == "2"
            cell.cancelButton.addTarget(self, action: #selector(singleCancel(sender:)), for: .touchUpInside)
            cell.rescheduleButton.addTarget(self, action: #selector(rescheduleButtonAction(sender:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == currentBooking.services.count{
            return 70
        }
        if indexPath.section == currentBooking.services.count + 1{
            return 350
        }
        if indexPath.row == 0{
            return 210
        }else{
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = tableView.cellForRow(at: indexPath)
        
        if indexPath.section == currentBooking.services.count {
            
            switch bookingPageMenu[indexPath.row].accessibilityName {
            case "weather":
                if let urls = URL(string: "https://www.accuweather.com/en/gb/manchester/m4-5/weather-forecast/329260"), UIApplication.shared.canOpenURL(urls){
                    UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                }
                break
            case "cancelbooking":
                cancelAllBookings()
                break
            case "receipt":
                if let urls = URL(string: "https://booking.manchestermassage.co.uk/receipt/\(currentBooking.orderid)"), UIApplication.shared.canOpenURL(urls){
                    UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                }
                break
                
            case "share":
                guard let url = URL(string: currentBooking.share_url) else {
                    return
                }
                let shareAll = [url]
                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = currentCell
                self.present(activityViewController, animated: true, completion: nil)
                break
                
            case "call":

                let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                // create an action
                let firstAction: UIAlertAction = UIAlertAction(title: "0161 834 9213 - Land Line", style: .default) { action -> Void in
                    
                    if let urls = URL(string: "tel://01618349213"), UIApplication.shared.canOpenURL(urls){
                        UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                    }
                }
                
                let secondAction: UIAlertAction = UIAlertAction(title: "07415 831969 - Mobile", style: .default) { action -> Void in
                    self.callButtonActionNew(cell: currentCell)
                    
                }
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                
                // add actions
                actionSheetController.addAction(firstAction)
                actionSheetController.addAction(secondAction)
                actionSheetController.addAction(cancelAction)
                
                
                // present an actionSheet...
                // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
                
                actionSheetController.popoverPresentationController?.sourceView = currentCell // works for both iPhone & iPad
                
                present(actionSheetController, animated: true) {
                    print("option menu presented")
                }
                
                break
            case "rebook":
                rebookAction()
                break
            case "add_review":
                let destination = PostReviewViewController(nibName: "PostReviewViewController", bundle: nil)
                destination.bookingID = self.currentBooking.booking_id
                destination.reviewData = self.currentBooking.reviews
                self.navigationController?.pushViewController(destination, animated: true)
                break
            case "view_review":
                let destination = PostReviewViewController(nibName: "PostReviewViewController", bundle: nil)
                destination.bookingID = self.currentBooking.booking_id
                destination.reviewData = self.currentBooking.reviews
                self.navigationController?.pushViewController(destination, animated: true)
                break
            default:
                break
            }
            
            
            
        }
    }
    
    
    @objc func callButtonActionNew(cell: UITableViewCell?){
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Call", style: .default) { action -> Void in

                    if let urls = URL(string: "tel://07415831969"), UIApplication.shared.canOpenURL(urls){
                       UIApplication.shared.open(urls, options: [:], completionHandler: nil)
                   }
        }

        let secondAction: UIAlertAction = UIAlertAction(title: "SMS", style: .default) { action -> Void in

            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Enter a message details here";
            messageVC.recipients = ["07415831969"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }

        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction)


        // present an actionSheet...
        // present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad

        actionSheetController.popoverPresentationController?.sourceView = cell // works for both iPhone & iPad

        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
        
        
        
    }
}


// MARK:- API Calls
extension ManageBookingsViewController{
    
    func cancelBookingAPI(){
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "orderid": (orderID).toData,
            "cust_id": (UserDefaults.standard.fetchData(forKey: .userId)).toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "CancelBooking" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    
                    let status = resposeJSON["status"] as? String ?? ""
                    let message = resposeJSON["message"] as? String ?? ""
                    
                    if status == "Y"{
                        let alert = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                            //                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            //                            let navigationController = UINavigationController(rootViewController: destination)
                            //                            self.appD.window?.rootViewController = navigationController
                            //                            self.appD.window?.makeKeyAndVisible()
                            self.navigationController?.popViewController(animated: true)
                        }
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else{
                        self.tabBarController?.view.makeToast(message: message)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    
    func cancelSingleServiceAPI(subOrderId: String){
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "orderid": (orderID).toData,
            "sub_orderid": subOrderId.toData,
            "cust_id": (UserDefaults.standard.fetchData(forKey: .userId)).toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "CancelSingleService" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    
                    let status = resposeJSON["status"] as? String ?? ""
                    let message = resposeJSON["message"] as? String ?? ""
                    
                    if status == "Y"{
                        let alert = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else{
                        self.tabBarController?.view.makeToast(message: message)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
}


struct BookingMenuModel{
    var image: UIImage?
    var menuName: String
    var accessibilityName: String
}


extension ManageBookingsViewController: MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
//            self.showAlert(title: "Sorry!", message: "Message has been cancelled.")
            print("Message was cancelled")
        case .failed:
//            self.showAlert(title: "Sorry!", message: "Failed to send message.")
            print("Message failed")
        case .sent:
//            self.showAlert(title: "Success!", message: "Message has been sent successfully.")
            print("Message was sent")
        default:
            return
        }
        dismiss(animated: true, completion: nil)
    }
    
    
}


extension ManageBookingsViewController{
    func getMyTherapists(data: String)-> [EmployeeModel]{
        var employeeDataFinal = [EmployeeModel]()
        if let data = data.data(using: .utf8){
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String]
                {
                    jsonArray.forEach { (empDataStr) in
                        if let empData = self.appD.employeeDetails.filter({$0.id == empDataStr}).first{
                            employeeDataFinal.append(empData)
                        }
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
        return employeeDataFinal
    }
}

extension ManageBookingsViewController: RescheduleBookingDelegate{
    func selectPopUpData(data: [CartModel]) {
        
        
        
        var currentServiceCart = [[String: Any]()]
        currentServiceCart.removeAll()
        let requestURL = Config.apiEndPoint + "ResheduleBooking"
        if currentRescheduleIndex >= 0 && currentRescheduleIndex < data.count{
            for index in 0..<currentRescheduleIndex{
                let currentService =  currentBooking.services[index]
                var therapistIDs = currentService.therapistArr.components(separatedBy: ",").map({$0.replacingOccurrences(of: "]", with: "")})
                therapistIDs = therapistIDs.map({$0.replacingOccurrences(of: "[", with: "")})
                if therapistIDs.count == 0{
                    therapistIDs = self.getMyTherapists(data: currentService.therapistArr).map({$0.id})
                }
                let serviceData = [
                    "id": currentService.serviceId,
                    "name": currentService.serviceName,
                    "qty": "1",
                    "price": currentService.basePrice,
                    "subtotal": currentService.subTotal,
                    "therapist": therapistIDs.joined(separator: "|").replacingOccurrences(of: "\"", with: ""),
                    "cust_name": currentService.custName,
                    "book_dt": currentService.bookingDate,
                    "book_tym": currentService.bookingTime,
                    "book_end_tym": currentService.bookEndTime,
                    "status": currentService.bookingStatus
                ]
                currentServiceCart.append(serviceData)
                
            }
            
            let selectedTherapist = data[currentRescheduleIndex].therapists.map({$0.id})
            let currentService =  data[currentRescheduleIndex].services.first!
            let serviceData11 = [
                "id": currentService.serviceID,
                "name": currentService.name,
                "qty": "1",
                "price": currentService.main_price,
                "subtotal": currentService.offerPrice,
                "therapist": selectedTherapist.joined(separator: "|"),
                "cust_name": data[currentRescheduleIndex].cust_name,
                "book_dt": data[currentRescheduleIndex].bookingDate,
                "book_tym": data[currentRescheduleIndex].bookingTime,
                "book_end_tym":data[currentRescheduleIndex].bookingEndTime,
            ]
            currentServiceCart.append(serviceData11)
            if (currentRescheduleIndex + 1) < currentBooking.services.count{
                for index in (currentRescheduleIndex + 1)..<currentBooking.services.count{
                    let currentService =  currentBooking.services[index]
                    var therapistIDs = currentService.therapistArr.components(separatedBy: ",").map({$0.replacingOccurrences(of: "]", with: "")})
                    therapistIDs = therapistIDs.map({$0.replacingOccurrences(of: "[", with: "")})
                    if therapistIDs.count == 0{
                        therapistIDs = self.getMyTherapists(data: currentService.therapistArr).map({$0.id})
                    }
                    let serviceData = [
                        "id": currentService.serviceId,
                        "name": currentService.serviceName,
                        "qty": "1",
                        "price": currentService.basePrice,
                        "subtotal": currentService.subTotal,
                        "therapist": therapistIDs.joined(separator: "|").replacingOccurrences(of: "\"", with: ""),
                        "cust_name": currentService.custName,
                        "book_dt": currentService.bookingDate,
                        "book_tym": currentService.bookingTime,
                        "book_end_tym": currentService.bookEndTime,
                    ]
                    currentServiceCart.append(serviceData)
                }
            }
            
            do{
                let jsonData = try? JSONSerialization.data(withJSONObject: currentServiceCart, options: [])
                
                var jsonDataString = String(bytes: jsonData!, encoding: .utf8)
                jsonDataString = jsonDataString?.replacingOccurrences(of: "\\", with: "")
                
                let parameter = [
                    "orderid": currentBooking.orderid.toData,
                    "cust_id": currentBooking.cust_id.toData,
                    "bookdt": currentBooking.bookdt.toData,
                    "timeslot": currentBooking.timeslot.toData,
                    "services": jsonDataString?.toData ?? Data(),
                    "key": Config.key.toData,
                    "salt": Config.salt.toData
                ]as [String : Data]
                
                self.activityIndicator.startAnimating()
                //=========>
                let paramsHead = ["Content-Type": "multipart/form-data"]
                Alamofire.upload(multipartFormData: { (multipartFormData) in
                    
                    for (key, value) in parameter{
                        multipartFormData.append(value, withName: key)
                    }
                }, usingThreshold: UInt64.init(), to: requestURL , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
                    
                    switch encodingResult{
                    case .failure( _):
                        self.activityIndicator.stopAnimating()
                        break;
                    case .success(let request, true, _):
                        request.responseJSON(completionHandler: { (response) in
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            
                            let status = resposeJSON["status"] as? String ?? ""
                            let message = resposeJSON["message"] as? String ?? ""
                            
                            if status == "Y"{
                                let alert = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
                                let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                                    //                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                    //                            let navigationController = UINavigationController(rootViewController: destination)
                                    //                            self.appD.window?.rootViewController = navigationController
                                    //                            self.appD.window?.makeKeyAndVisible()
                                    self.navigationController?.popViewController(animated: true)
                                }
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            else{
                                self.tabBarController?.view.makeToast(message: message)
                            }
                        })
                    default:
                        self.activityIndicator.stopAnimating()
                        self.tabBarController?.view.makeToast(message: Message.apiError)
                        break;
                    }})
                //=========>
            }catch{
                
            }
            
            
        }
    }
}
