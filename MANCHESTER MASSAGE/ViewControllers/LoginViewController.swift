//
//  LoginViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 14/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import FBSDKLoginKit
import Firebase
import GoogleSignIn
import AuthenticationServices
//import TwitterKit

var socialLoginAction: SocialLoginAction?

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var appleSignInView: UIView!
    
    @IBOutlet weak var rememberPassword: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var pass_info: UILabel!
    
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Sign in", isBackButtonRequired: true)
        
        self.forgotPasswordButton.addTarget(self, action: #selector(self.forgotPasswordButtonAction), for: .touchUpInside)
        self.facebookButton.addTarget(self, action: #selector(self.facebookButtonAction), for: .touchUpInside)
        self.facebookButton.isHidden = false
        
        self.googleButton.addTarget(self, action: #selector(self.googleButtonAction), for: .touchUpInside)
        self.googleButton.isHidden = false
        
//        self.twitterButton.addTarget(self, action: #selector(self.twitterLoginaction), for: .touchUpInside)
        self.twitterButton.isHidden = true
        
        self.signinButton.addTarget(self, action: #selector(self.signinButtonAction), for: .touchUpInside)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.signinButton.backgroundColor = ThemeColor.buttonColor
        self.signinButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.signinButton.clipsToBounds = true
        self.signinButton.setTitleColor(.white, for: .normal)
        self.signinButton.setTitle("SIGN IN", for: .normal)
        
        self.forgotPasswordButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
        self.forgotPasswordButton.setTitle("Forgot password?", for: .normal)
        
        self.facebookButton.backgroundColor = ThemeColor.facebookButtonColor
        self.facebookButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.facebookButton.clipsToBounds = true
        self.facebookButton.setTitleColor(.white, for: .normal)
        self.facebookButton.setTitle("", for: .normal)
        self.facebookButton.setFAIcon(icon: .FAFacebook, iconSize: 30, forState: .normal)
        
        self.googleButton.backgroundColor = ThemeColor.googleButtonColor
        self.googleButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.googleButton.clipsToBounds = true
        self.googleButton.setTitleColor(.white, for: .normal)
        self.googleButton.setTitle("", for: .normal)
        self.googleButton.setFAIcon(icon: .FAGoogle, iconSize: 30, forState: .normal)
        
        self.twitterButton.backgroundColor = ThemeColor.twitterButtonColor
        self.twitterButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.twitterButton.clipsToBounds = true
        self.twitterButton.setTitleColor(.white, for: .normal)
        self.twitterButton.setTitle("", for: .normal)
        self.twitterButton.setFAIcon(icon: .FATwitter, iconSize: 30, forState: .normal)
        
        self.instagramButton.backgroundColor = ThemeColor.instagramButtonColor
        self.instagramButton.layer.cornerRadius = self.signinButton.frame.height / 2
        self.instagramButton.clipsToBounds = true
        self.instagramButton.setTitleColor(.white, for: .normal)
        self.instagramButton.setTitle("", for: .normal)
        self.instagramButton.setFAIcon(icon: .FAInstagram, iconSize: 30, forState: .normal)
        self.instagramButton.isHidden = true
        
        if UserDefaults.standard.fetchData(forKey: ._isRememberMe){
            let fetchCredential = UserDefaults.standard.fetchUserCredential()
            self.emailTextField.text = fetchCredential.email
            self.passwordTextField.text = fetchCredential.password
            self.passwordTextField.isSelected = true
            self.rememberPassword.isSelected = true
        }
        
        
        self.rememberPassword.setLeftImage(image: FAType.FASquareO, color: .gray, state: .normal)
        self.rememberPassword.setLeftImage(image: FAType.FACheckSquareO, color: .gray, state: .selected)
        
        self.rememberPassword.setTitle("Remember Me", for: .normal)
        self.rememberPassword.setTitle("Remember Me", for: .selected)
        self.rememberPassword.addTarget(self, action: #selector(rememberPasswordAction), for: .touchUpInside)
        
        setUpSignInAppleButton()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pass_info.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.emailTextField.addButtomBorder()
        self.passwordTextField.addButtomBorder()
        
        self.emailTextField.setTextFieldImage(image: FAType.FAEnvelope, direction: .right)
        self.emailTextField.keyboardType = .emailAddress
       
        self.passwordTextField.isSecureTextEntry = true
        self.passwordTextField.enablePasswordToggle()
        self.passwordTextField.delegate = self
        
        
        
        self.emailTextField.setPlaceholder(placeholder: "Email ID", color: UIColor.gray)
        self.passwordTextField.setPlaceholder(placeholder: "Password", color: UIColor.gray)
    }
    
    @objc func rememberPasswordAction(){
        self.rememberPassword.isSelected = !self.rememberPassword.isSelected
    }
   
    @objc func forgotPasswordButtonAction(){
        let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecoverPasswordViewController") as! RecoverPasswordViewController
        self.navigationController?.pushViewController(destination, animated: true)
    }
    @objc func signinButtonAction(){
        if (self.emailTextField.text?.isBlank ?? true){
            self.view.makeToast(message: "Please Enter Email ID")
        }
        else if (self.passwordTextField.text?.isBlank ?? true){
            self.view.makeToast(message: "Please Enter Password")
        }
        else{
            let paramsHead = ["Content-Type": "multipart/form-data"]
            let bodyParams = [
                "typ": Config.api.login.toData,
                "key": Config.key.toData,
                "salt": Config.salt.toData,
                "email": (self.emailTextField.text ?? "").toData,
                "pass": (self.passwordTextField.text ?? "").toData,
                "fcm_token": self.appD.fcmTokenToRegister.toData
            ] as [String : Data]
            self.activityIndicator.startAnimating()
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in bodyParams{
                    multipartFormData.append(value, withName: key)
                }                
            }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "guestcheck" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
                
                switch encodingResult{
                case .failure( _):
                    self.activityIndicator.stopAnimating()
                    break;
                case .success(let request, true, _):
                    request.responseJSON(completionHandler: { (response) in
                        
                        let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                        self.activityIndicator.stopAnimating()
                       
                        let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                        let status = result["status"] as? String ?? ""
                        let message = result["type"] as? String ?? ""
                        
                        if status == "Ok"{
                            let info = result["info"] as? NSDictionary ?? NSDictionary()
                            let c_id = info["c_id"] as? String ?? ""
                            let social_login = info["social_login"] as? String ?? ""
                            let social_login_id = info["social_login_id"] as? String ?? ""
                            let c_fname = info["c_fname"] as? String ?? ""
                            let c_lname = info["c_lname"] as? String ?? ""
                            let c_email = info["c_email"] as? String ?? ""
                            let c_phone = info["c_phone"] as? String ?? ""
                            let thumbnail = info["thumbnail"] as? String ?? ""
//                            let c_password = info["c_password"] as? String ?? ""
                            let status = info["status"] as? String ?? ""
                            
                            let my_referal_code = info["my_referal_code"] as? String ?? ""
                            let my_referal_link = info["my_referal_link"] as? String ?? ""
                            
                            UserDefaults.standard.saveData(value: my_referal_code, key: .referalCode)
                            UserDefaults.standard.saveData(value: my_referal_link, key: .referalLink)
                            
                            UserDefaults.standard.saveData(value: c_id, key: .userId)
                            UserDefaults.standard.saveData(value: social_login, key: .userSocialLogin)
                            UserDefaults.standard.saveData(value: social_login_id, key: .userSocialLoginId)
                            UserDefaults.standard.saveData(value: c_fname, key: .userFirstName)
                            UserDefaults.standard.saveData(value: c_lname, key: .userLastName)
                            UserDefaults.standard.saveData(value: c_email, key: .userEmail)
                            UserDefaults.standard.saveData(value: c_phone, key: .userPhone)
                            UserDefaults.standard.saveData(value: status, key: .userStatus)
                            UserDefaults.standard.deleteData(forKey: .skipLogin)
                            UserDefaults.standard.saveData(value: thumbnail, key: .userImage)
                            
                            if self.rememberPassword.isSelected{
                                UserDefaults.standard.saveUserCredential(email: c_email, password: self.passwordTextField.text ?? "")
                            }else{
                                UserDefaults.standard.deleteUserCredential()
                            }
                            UserDefaults.standard.saveUserCredential(email: c_email, password: (self.passwordTextField.text ?? ""))
                            
//                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                            let navigationController = UINavigationController(rootViewController: destination)
//                            self.appD.window?.rootViewController = navigationController
//                            self.appD.window?.makeKeyAndVisible()
//                            let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
//                            let navigationController = UINavigationController(rootViewController: destination)
//                            self.appD.window?.rootViewController = navigationController
//                            self.appD.window?.makeKeyAndVisible()
                            
                            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
                            self.appD.window?.rootViewController = destination
                            self.appD.window?.makeKeyAndVisible()
                            
                            
                        }
                        else{
                            self.view.makeToast(message: message)
                        }
                    })
                default:
                    self.activityIndicator.stopAnimating()
                    self.view.makeToast(message: Message.apiError)
                    break;
                }})
        }
    }
    
    @objc func facebookButtonAction(){
        self.activityIndicator.startAnimating()
        socialLoginAction = .facebook
        let fbLoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            
            if error != nil
            {
                print("error occured with login \(String(describing: error?.localizedDescription))")
            }
                
            else if (result?.isCancelled)!
            {
                print("login canceled")
            }
                
            else
            {
                let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields": "name,email,id"])
                graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                    if ((error) != nil)
                    {
                        // Process error
                        print("Error: \(String(describing: error))")
                    }
                    else
                    {
                        print(result as Any)
                        self.activityIndicator.startAnimating()
                        if let facebookResult = result as? NSDictionary{
                            let name = facebookResult["name"] as? String ?? ""
                            let email = facebookResult["email"] as? String ?? ""
                            let id = facebookResult["id"] as? String ?? ""
                            let profile_picture = "http://graph.facebook.com/\(id)/picture?type=large"
                            self.socialMediaLogin(userId: id, firstName: name, lastName: "", email: email, profileImage: profile_picture, password: "Facebook_" + "\(email)", socialType: "F")
                        }

                        
//                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                        let navigationController = UINavigationController(rootViewController: destination)
//                        self.appD.window?.rootViewController = navigationController
//                        self.appD.window?.makeKeyAndVisible()
//                        let destination = ParentCategoryViewController(nibName: "ParentCategoryViewController", bundle: nil)
//                        let navigationController = UINavigationController(rootViewController: destination)
//                        self.appD.window?.rootViewController = navigationController
//                        self.appD.window?.makeKeyAndVisible()
//                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
//                        self.appD.window?.rootViewController = destination
//                        self.appD.window?.makeKeyAndVisible()
                    }
                })
            }
        }
        
    }
    
    @objc func googleButtonAction(){
        socialLoginAction = .google
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func setUpSignInAppleButton() {
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
            authorizationButton.cornerRadius = appleSignInView.frame.height / 2
            authorizationButton.frame = appleSignInView.bounds
            self.appleSignInView.addSubview(authorizationButton)
        } else {
            // Fallback on earlier versions
        }
        
      //Add button on some view or stack
//      self.signInButtonStack.addArrangedSubview(authorizationButton)
    }
    
    @available(iOS 13.0, *)
    @objc func handleAppleIdRequest() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
//    @objc func twitterLoginaction(){
//        socialLoginAction = .twitter
//        let store = TWTRTwitter.sharedInstance().sessionStore
//
//        if let userID = store.session()?.userID {
//            store.logOutUserID(userID)
//        }
//        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
//            if (session != nil) {
//
//                let client = TWTRAPIClient.withCurrentUser()
//
//                client.requestEmail { email, error in
//                    if let session = session, let email = email{
//                        let twitterClient = TWTRAPIClient(userID: session.userID)
//                        twitterClient.loadUser(withID: session.userID) { (user, error) in
//
//                            var profileURL = ""
//                            if error == nil{
//                                profileURL = user?.profileImageLargeURL ?? ""
//                            }
//                            self.socialMediaLogin(userId: session.userID, firstName: session.userName, lastName: "", email: email, profileImage: profileURL, password: "Twitter_\(email)", socialType: "T")
//
//                        }
//                    } else {
//                        print("error: \(error!.localizedDescription)");
//                    }
//                }
//            } else {
//
//            }
//        })
//    }
    
    
    fileprivate func socialMediaLogin(userId: String, firstName: String, lastName: String, email: String, profileImage: String = "", password: String, socialType: String){
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "typ": Config.api.register.toData,
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "email": email.toData,
            "pass": password.toData,
            "first_name": firstName.toData,
            "last_name": lastName.toData,
            "social_login": socialType.toData,
            "social_login_id": userId.toData,
            "fcm_token": self.appD.fcmTokenToRegister.toData,
            
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            if profileImage != ""{
                
//                multipartFormData.append(value, withName: "thumbnail")
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + "guestcheck" , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    print("Response: \(resposeJSON)")
                    
                    let type = result["type"] as? String ?? ""
                    if (type.starts(with: "existing_user") || type == "Sign Up Complete"){
                        let info = result["info"] as? NSDictionary ?? NSDictionary()
                        let c_id = info["c_id"] as? String ?? ""
                        //                            let social_login = info["social_login"] as? String ?? ""
                        //                            let social_login_id = info["social_login_id"] as? String ?? ""
                        let c_fname = info["c_fname"] as? String ?? ""
                        let c_lname = info["c_lname"] as? String ?? ""
                        let c_email = info["c_email"] as? String ?? ""
                        let c_phone = info["c_phone"] as? String ?? ""
                        
                        let status = info["status"] as? String ?? ""
                        
                        let my_referal_code = info["my_referal_code"] as? String ?? ""
                        let my_referal_link = info["my_referal_link"] as? String ?? ""
                        
                        UserDefaults.standard.saveData(value: my_referal_code, key: .referalCode)
                        UserDefaults.standard.saveData(value: my_referal_link, key: .referalLink)
                        
                        UserDefaults.standard.saveData(value: c_id, key: .userId)
                        UserDefaults.standard.saveData(value: socialType, key: .userSocialLogin)
                        UserDefaults.standard.saveData(value: userId, key: .userSocialLoginId)
                        UserDefaults.standard.saveData(value: c_fname, key: .userFirstName)
                        UserDefaults.standard.saveData(value: c_lname, key: .userLastName)
                        UserDefaults.standard.saveData(value: c_email, key: .userEmail)
                        UserDefaults.standard.saveData(value: c_phone, key: .userPhone)
                        UserDefaults.standard.saveData(value: status, key: .userStatus)
                        UserDefaults.standard.saveData(value: profileImage, key: .userImage)
                        
                        UserDefaults.standard.deleteData(forKey: .skipLogin)
                        
                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
                        self.appD.window?.rootViewController = destination
                        self.appD.window?.makeKeyAndVisible()
                    }else{
                        self.view.makeToast(message: "Something Went Wrong. Please try again.")
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
}


extension LoginViewController: GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
        } else {
            
            guard let user = user else {
                self.view.makeToast(message: "Something went wrong.")
                return
            }
            let userId = user.userID               // For client-side use only!
            let fullName = user.profile.name
            let email = user.profile.email
            let dimension = round(100 * UIScreen.main.scale)
            let profileImage = user.profile.imageURL(withDimension: UInt(dimension))?.absoluteString
            
            self.socialMediaLogin(userId: userId ?? "", firstName: fullName ?? "", lastName: "", email: email ?? "", profileImage: profileImage ?? "", password: "Google_\(email ?? "")", socialType: "G")
           
        }
    }
    
    
}

enum SocialLoginAction{
    case google, facebook, twitter, instagram
}

extension UITextField {
    fileprivate func setPasswordToggleImage(_ button: UIButton) {
        if(isSecureTextEntry){
            button.setImage(UIImage(named: "eye_close"), for: .normal)
        }else{
            button.setImage(UIImage(named: "eye"), for: .normal)
            
        }
    }
    
    func enablePasswordToggle(){
        let button = UIButton(type: .custom)
        setPasswordToggleImage(button)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
        self.rightView = button
        self.rightViewMode = .always
    }
    @IBAction func togglePasswordView(_ sender: Any) {
        self.isSecureTextEntry = !self.isSecureTextEntry
        setPasswordToggleImage(sender as! UIButton)
    }
}

@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Something Went Wrong \(error.localizedDescription)")
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            self.activityIndicator.stopAnimating()
            
            if let givenName = appleIDCredential.fullName?.givenName{
                let familyName = appleIDCredential.fullName?.familyName ?? ""
                let user_id = appleIDCredential.user
                let email = appleIDCredential.email ?? ""
                
                UserDefaults.standard.setValue(email, forKey: "apple_email")
                UserDefaults.standard.setValue(user_id, forKey: "apple_user_id")
                UserDefaults.standard.setValue(givenName, forKey: "apple_givenName")
                UserDefaults.standard.setValue(familyName, forKey: "apple_familyName")
                UserDefaults.standard.synchronize()
                
                self.socialMediaLogin(userId: user_id, firstName: givenName, lastName: familyName, email: email, password: "Apple_" + email, socialType: "A")
                
            }
            else{

                let givenName = UserDefaults.standard.string(forKey: "apple_givenName") ?? ""
                let familyName = UserDefaults.standard.string(forKey: "apple_familyName") ?? ""
                let user_id = UserDefaults.standard.string(forKey: "apple_user_id") ?? ""
                let email = UserDefaults.standard.string(forKey: "apple_email") ?? ""
                
                if email != ""{
                    self.socialMediaLogin(userId: user_id, firstName: givenName, lastName: familyName, email: email, password: "Apple_" + email, socialType: "A")
                }else{
                    let alert = UIAlertController(title: "Aler!", message: "You have already hidden the email sharing option. Kindly delete the certificate manually and select email sharing option to use apple sign in. Please refer to the following steps. Settings -> Account Login -> Password & Security -> Apps using Your AppID -> Certificate -> Stop Using Apple ID.", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            
            break
            
        case let passwordCredential as ASPasswordCredential:
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                self.showPasswordCredentialAlert(username: username, password: password)
            }
            break
        default:
            break
            
        }
    }
    
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        let alertController = UIAlertController(title: "Keychain Credential Received",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    
}

extension LoginViewController: AuthUIDelegate{
    
}
