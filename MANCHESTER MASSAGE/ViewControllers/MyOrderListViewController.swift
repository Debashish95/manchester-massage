//
//  MyOrderListViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 15/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class MyOrderListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: NVActivityIndicatorView!
    var orderListData = [OrderListModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "My Orders", isBackButtonRequired: true, isRightMenuRequired: false)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "OrderListTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderListTableViewCell")
        
        getOrderList()
    }
    
    func getOrderList(){
        orderListData.removeAll()
        //        https://pampertree.co.uk/Data/GetOrderList?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=148
        activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetOrderList?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                let currentResult = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                let results = currentResult["result"] as? NSArray ?? NSArray()
                var currentOrder = OrderListModel()
                for result in results{
                    let currentBooking = result as? NSDictionary ?? NSDictionary()
                    currentOrder.vendor_id = currentBooking["vendor_id"] as? String ?? ""
                    currentOrder.order_id = currentBooking["order_id"] as? String ?? ""
                    currentOrder.amount = "\(String(currentBooking["amount"] as? String ?? "0").formattedString())"
                    currentOrder.created_on = self.convertDate(currentBooking["created_on"] as? String ?? "1970-01-01 00:00:00", from: "yyyy-MM-dd HH:mm:ss", to: "dd MMM YYYY, hh:mm a")
                    
                    let shipp_Address = currentBooking["address"] as? NSDictionary ?? NSDictionary()
                    var shippingAddress = AddressModel()
                    shippingAddress.addressId = shipp_Address["address_id"] as? String ?? ""
                    shippingAddress.fullName = shipp_Address["name"] as? String ?? ""
                    shippingAddress.phone = shipp_Address["phone"] as? String ?? ""
                    shippingAddress.addressLine1 = shipp_Address["address_one"] as? String ?? ""
                    shippingAddress.addressLine2 = shipp_Address["address_two"] as? String ?? ""
                    shippingAddress.town = shipp_Address["city"] as? String ?? ""
                    shippingAddress.postalCode = shipp_Address["zip"] as? String ?? ""
                    shippingAddress.country = shipp_Address["country"] as? String ?? ""
                    shippingAddress.security = shipp_Address["security_code"] as? String ?? ""
                    shippingAddress.weekendDelivery = shipp_Address["weekend_delivery"] as? String ?? ""
                    currentOrder.address = shippingAddress
                    currentOrder.invoiceURL = "https://booking.manchestermassage.co.uk/orderslip/\(currentOrder.order_id)"
                    
                    let products = currentBooking["products"] as? NSArray ?? NSArray()
                    var totalOrderedProducts = [ProductCartModel]()
                    for product in products{
                        let currentProduct = product as? NSDictionary ?? NSDictionary()
                        var productItem = ProductCartModel()
                        productItem.prod_id = currentProduct["prod_id"] as? String ?? ""
                        productItem.prod_name = currentProduct["p_name"] as? String ?? ""
                        productItem.qnty = currentProduct["qnty"] as? String ?? ""
                        productItem.unit_price = "\(String(currentProduct["unit_price"] as? String ?? "0").formattedString())"
                        productItem.price = "\(String(currentProduct["t_price"] as? String ?? "0").formattedString())"
                        productItem.thumb = currentProduct["thumb"] as? String ?? ""
                        productItem.small_description = currentProduct["small_description"] as? String ?? ""
                        totalOrderedProducts.append(productItem)
                    }
                    currentOrder.products = totalOrderedProducts
                    
                    let tracks = currentBooking["track"] as? NSArray ?? NSArray()
                    var trackArray = [TrackModel]()
                    for track in tracks{
                        let currentShippingStatus = track as? NSDictionary ?? NSDictionary()
                        var shipData = TrackModel()
                        shipData.ecom_order_id = currentShippingStatus["ecom_order_id"] as? String ?? ""
                        shipData.track_status_id = currentShippingStatus["track_status_id"] as? String ?? ""
                        shipData.track_status_name = currentShippingStatus["track_status_name"] as? String ?? ""
                        shipData.msg = currentShippingStatus["msg"] as? String ?? ""
                        shipData.timestamp = self.convertDate(currentShippingStatus["timestamp"] as? String ?? "1970-01-01 00:00:00", from: "yyyy-MM-dd HH:mm:ss", to: "dd MMM YYYY, hh:mm a")
                        shipData.track_status_icon = currentShippingStatus["track_status_icon"] as? String ?? ""
                        trackArray.append(shipData)
                    }
                    currentOrder.track = trackArray
                    self.orderListData.append(currentOrder)
                }
                self.tableView.reloadData()
            }
    }

    fileprivate func convertDate(_ date: String, from format1: String, to format2: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format1
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = format2
        return  dateFormatter.string(from: date)
    }
}


extension MyOrderListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderListData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderListTableViewCell", for: indexPath) as! OrderListTableViewCell
        cell.data = orderListData[indexPath.row]
        cell.productImageView.layer.cornerRadius = cell.productImageView.bounds.height / 2
        cell.productImageView.clipsToBounds = true
        cell.productImageView.layer.borderWidth = 1.0
        cell.productImageView.layer.borderColor = ThemeColor.buttonColor.cgColor
        cell.productImageView.backgroundColor = .darkGray
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        destination.orderDetails = orderListData[indexPath.row]
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
