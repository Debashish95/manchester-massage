//
//  ReviewsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 24/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire

class ReviewsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicator: NVActivityIndicatorView!
    var allReviews = [ExternalReviewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "Reviews", isBackButtonRequired: true)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ReviewListTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewListTableViewCell")
        tableView.register(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTableViewCell")
        
//        tableView.register(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ReviewsTableViewCell")
        
        getAllReviews()
        
    }
    
    func getAllReviews(){
        allReviews.removeAll()
        self.activityIndicator.startAnimating()
        let requestURL = (Config.mainUrl + "Data/GetExternalReview?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq")
        let urlConvertible = URL(string: requestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            print(resposeJSON)
                            
                            
                            if (resposeJSON["status"] as? Bool ?? false){
                                let results = resposeJSON["result"] as? NSArray ?? NSArray()
                                results.forEach { (result) in
                                    let currentResult = result as? NSDictionary ?? NSDictionary()
                                    
                                    if let orgName = currentResult["review_org"] as? String{
                                        let review_org_icon = currentResult["review_org_icon"] as? String ?? ""
                                        let reviewObject = ReviewModel(
                                            reviewer_name: currentResult["reviewer_name"] as? String ?? "",
                                            reviewer_email: currentResult["reviewer_email"] as? String ?? "",
                                            review: currentResult["review_text"] as? String ?? "",
                                            overall_review: currentResult["rating"] as? String ?? "0",
                                            created_on: currentResult["review_date"] as? String ?? "",
                                            review_org: currentResult["review_org"] as? String ?? "")
                                        
                                        if let index = self.allReviews.firstIndex(where: {$0.review_org.lowercased() == orgName.lowercased()}){
                                            self.allReviews[index].reviews.append(reviewObject)
                                        }else{
                                            let profile_url = currentResult["profile_url"] as? String ?? ""
                                            self.allReviews.append(ExternalReviewModel(review_org: orgName, reviews: [reviewObject], profileURL: profile_url, image: review_org_icon))
                                        }
                                    }
                                }
                                
                            }else{
                                self.tabBarController?.view.makeToast(message: resposeJSON["Message"] as? String ?? "")
                            }
                            if self.allReviews.count > 0{
                                self.allReviews[self.allReviews.count - 1].isOpen = true
                            }
                            self.tableView.reloadData()
                            
                          }
    }
    
    @objc func reloadReviewSections(sender: UIButton){
        for index in 0..<allReviews.count{
            if sender.tag == index{
                allReviews[index].isOpen = !allReviews[index].isOpen
            }
        }
        tableView.reloadData()
    }
    @objc func reloadReviewSectionsImage(sender: UITapGestureRecognizer){
        for index in 0..<allReviews.count{
            if sender.view?.tag == index{
                allReviews[index].isOpen = !allReviews[index].isOpen
            }
        }
        tableView.reloadData()
    }
    
    @objc func openThirdPartyReviews(sender: UIButton){
        if let finalURL = URL(string: allReviews[sender.tag].profileURL){
            let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
            destination.pageURL = finalURL
            destination.pageTitle = "\(allReviews[sender.tag].review_org) Reviews"
            destination.isFromReviews = true
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
}


extension ReviewsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.allReviews.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allReviews[section].isOpen ? self.allReviews[section].reviews.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewListTableViewCell", for: indexPath) as! ReviewListTableViewCell
        cell.ratingView.rating = Double(self.allReviews[indexPath.section].reviews[indexPath.row].overall_review) ?? 0.0
        cell.reviewLabel.text = self.allReviews[indexPath.section].reviews[indexPath.row].review
        cell.nameLabel.text = self.allReviews[indexPath.section].reviews[indexPath.row].reviewer_name
        let date1 = self.allReviews[indexPath.section].reviews[indexPath.row].created_on.convertDate(format: "yyyy-MM-dd HH:mm:ss") ?? Date()
        let timeDiff = Date().offset(from: date1)
        cell.dateLabel.text = "Posted \(timeDiff) ago"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewsTableViewCell") as! ReviewsTableViewCell
        cell.headerLabel.setTitle(self.allReviews[section].review_org, for: .normal)
        cell.headerLabel.setTitleColor(.black, for: .normal)
        cell.headerLabel.titleLabel?.font = .boldSystemFont(ofSize: 18)
        
        cell.reviewImageView.contentMode = .scaleAspectFit
        cell.reviewImageView.kf.setImage(with: URL(string: self.allReviews[section].image))
        
        if allReviews[section].isOpen {
            cell.viewMoreButton.setLeftImage(image: .FAAngleDown, color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
        }else{
            cell.viewMoreButton.setLeftImage(image: .FAAngleRight, color: UIColor.darkGray, size: CGSize(width: 50, height: 50))
        }
        cell.reviewImageView.isUserInteractionEnabled = true
        cell.reviewImageView.tag = section
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.reloadReviewSectionsImage(sender:)))
        tapGesture.numberOfTapsRequired = 1
        cell.reviewImageView.addGestureRecognizer(tapGesture)
        cell.headerLabel.tag = section
        cell.headerLabel.addTarget(self, action: #selector(reloadReviewSections(sender:)), for: .touchUpInside)
        cell.viewMoreButton.tag = section
        cell.viewMoreButton.addTarget(self, action: #selector(reloadReviewSections(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0,
                                        y: 0,
                                        width: tableView.bounds.width - 10,
                                        height: 50))
        view.backgroundColor = .white
        let button = UIButton(type: .custom)
        button.frame = view.bounds.inset(by: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15))
        button.backgroundColor = .white
        button.setTitleColor(ThemeColor.buttonColor, for: .normal)
        button.setTitle("Read More Reviews", for: .normal)
        button.contentHorizontalAlignment = .right
        button.titleLabel?.font = .boldSystemFont(ofSize: 15)
        button.tag = section
        button.addTarget(self, action: #selector(openThirdPartyReviews(sender:)), for: .touchUpInside)
        view.addSubview(button)
        return view
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return allReviews[section].isOpen ? 50 : 0
    }
    
}


struct ExternalReviewModel{
    var review_org: String
    var reviews: [ReviewModel]
    var profileURL: String
    var isOpen: Bool = false
    var image: String
}
