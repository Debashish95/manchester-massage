//
//  CreateGiftCardViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 21/05/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class CreateGiftCardViewController: UIViewController {

    @IBOutlet weak var chooseAmountPicker: APJTextPickerView!
    @IBOutlet weak var chooseQuantityPicker: APJTextPickerView!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var emailRadioButton: UIButton!
    @IBOutlet weak var postRadioButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var deliveryAddress: UITextView!
    
    var activityIndicator: NVActivityIndicatorView!

    var totalPriceFloat = Float(0.0)
    var amountOptions = ["1", "10", "15", "20", "50", "100", "200", "500"]
    var quantityOptions = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "20", "25", "30", "35", "40", "45", "50"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Create Gift Cards", isBackButtonRequired: true, isRightMenuRequired: false)
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.emailRadioButton.addTarget(self, action: #selector(self.chooseDeliveryMethodAction(sender:)), for: .touchUpInside)
        self.postRadioButton.addTarget(self, action: #selector(self.chooseDeliveryMethodAction(sender:)), for: .touchUpInside)
        self.emailRadioButton.isSelected = true
        self.messageTextView.text = ""
        self.checkOutButton.addTarget(self, action: #selector(checkOutButtonAction), for: .touchUpInside)
        
        self.chooseAmountPicker.dataSource = self
        self.chooseAmountPicker.type = .strings
        self.chooseAmountPicker.pickerDelegate = self
        
        self.chooseQuantityPicker.dataSource = self
        self.chooseQuantityPicker.type = .strings
        self.chooseQuantityPicker.pickerDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.chooseAmountPicker.font = .systemFont(ofSize: 17)
        self.chooseAmountPicker.setPlaceholder(placeholder: "Select Gift Card Price", color: UIColor.gray)
        self.chooseAmountPicker.tintColor = ThemeColor.buttonColor
        
        self.chooseQuantityPicker.font = .systemFont(ofSize: 17)
        self.chooseQuantityPicker.setPlaceholder(placeholder: "Select Gift Card Quantity", color: UIColor.gray)
        self.chooseQuantityPicker.tintColor = ThemeColor.buttonColor
        
        self.emailTextField.font = .systemFont(ofSize: 17)
        self.emailTextField.setTextFieldImage(image: .FAEnvelopeO, direction: .right)
        self.emailTextField.tintColor = ThemeColor.buttonColor
        
        self.messageTextView.layer.cornerRadius = 3
        self.messageTextView.layer.borderWidth = 1.0
        self.messageTextView.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.messageTextView.clipsToBounds = true
        self.messageTextView.tintColor = ThemeColor.buttonColor
        
        self.deliveryAddress.layer.cornerRadius = 3
        self.deliveryAddress.layer.borderWidth = 1.0
        self.deliveryAddress.layer.borderColor = ThemeColor.buttonColor.cgColor
        self.deliveryAddress.clipsToBounds = true
        self.deliveryAddress.tintColor = ThemeColor.buttonColor
        
        self.emailRadioButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        self.emailRadioButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        
        self.postRadioButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        self.postRadioButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        
        self.checkOutButton.layer.cornerRadius = 3
        self.checkOutButton.clipsToBounds = true
        
    }
    
    @objc func chooseDeliveryMethodAction(sender: UIButton){
        if sender == emailRadioButton{
            emailRadioButton.isSelected = true
            postRadioButton.isSelected = false
        }else{
            emailRadioButton.isSelected = false
            postRadioButton.isSelected = true
        }
        
        emailStackView.isHidden = !emailRadioButton.isSelected
        addressStackView.isHidden = !postRadioButton.isSelected
    }
    
    @objc func checkOutButtonAction(){
        self.view.endEditing(true)
        if chooseAmountPicker.text?.isBlank ?? false{
            self.tabBarController?.view.makeToast(message: "Choose Gift Card Amount")
            return
        }
        if chooseQuantityPicker.text?.isBlank ?? false{
            self.tabBarController?.view.makeToast(message: "Choose Gift Card Quantity")
            return
        }
        if !(emailTextField.text?.isValidEmail ?? true) && emailRadioButton.isSelected{
            self.tabBarController?.view.makeToast(message: "Invalid Email address. Please try again.")
            return
        }
        
        if (deliveryAddress.text?.isBlank ?? false) && postRadioButton.isSelected{
            self.tabBarController?.view.makeToast(message: "Please Enter Delivery Address.")
            return
        }
        
        let amount = Int(amountOptions[chooseAmountPicker.currentIndexSelected ?? 0]) ?? 0
        let quantity = Int(quantityOptions[chooseQuantityPicker.currentIndexSelected ?? 0]) ?? 0
        totalPriceFloat = Float(amount * quantity)
        let destination = GenericPaymentViewController(nibName: "GenericViewController", bundle: nil)
        destination.delegate = self
        destination.totalPriceFloat = totalPriceFloat
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    fileprivate func giftCardEntry(paymentID: String){
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "amount": amountOptions[chooseAmountPicker.currentIndexSelected ?? 0],
            "pg_transaction_id": paymentID,
            "send_via_post": postRadioButton.isSelected ? "Y" : "N",
            "receiver_email": postRadioButton.isSelected ? "" : (emailTextField.text ?? ""),
            "quantity": quantityOptions[chooseQuantityPicker.currentIndexSelected ?? 0],
            "info": messageTextView.text ?? "",
            "receiver_address": !postRadioButton.isSelected ? "" : (deliveryAddress.text ?? "")
        ] as [String : String]
        self.activityIndicator.startAnimating()
        Alamofire.request(Config.apiEndPoint + "GiftcardEntry", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftCardStatusChange"), object: nil)
                        
                        var giftCardSuccessMessage = "Gift Card has been created successfully."
                        if (Int(parameters["quantity"] ?? "0") ?? 0) > 1 {
                            giftCardSuccessMessage = "Gift Cards have been created successfully."
                        }
                        let alertController = UIAlertController(title: "Success", message: giftCardSuccessMessage, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                    
                }
            }
    }

}

extension CreateGiftCardViewController: GenericPaymentViewControllerDelegate{
    func onPaymentSuccess(paymentID: String, paymentResponse: NSDictionary?) {
        print("Payment ID: \(paymentID)")
        self.giftCardEntry(paymentID: paymentID)
    }
    
    func onPaymentFailure() {
        print("Payment Failure")
    }
    
    func onPaymentCancelled() {
        print("Payment Cancelled")
    }
    
    
}



extension CreateGiftCardViewController: APJTextPickerViewDataSource, APJTextPickerViewDelegate{
    func numberOfRows(in pickerView: APJTextPickerView) -> Int {
        if pickerView == chooseAmountPicker{
            return amountOptions.count
        }else{
            return quantityOptions.count
        }
    }
    
    func textPickerView(_ textPickerView: APJTextPickerView, titleForRow row: Int) -> String? {
        if textPickerView == chooseAmountPicker{
            return "\(CURRENCY_SYMBOL)\(amountOptions[row])"
        }else{
            return quantityOptions[row]
        }
    }
    
}

extension CreateGiftCardViewController {
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    func createRequestBodyWith(boundary:String, fileName:String?, image: UIImage?, parameters:[String : String]) -> Data{
        
        var body = Data()
        for (key, value) in parameters {
            body.append("--\(boundary)\r\n".toData)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".toData)
            body.append("\(value)\r\n".toData)
        }
        body.append("--\(boundary)\r\n".toData)
        let mimetype = "image/jpg"
        if let image = image, let fileName = fileName{
            let imageData = image.jpegData(compressionQuality: 0)
            body.append("--\(boundary)\r\n".toData)
            body.append("Content-Disposition: form-data; name=\"\(fileName)\"; filename=\"\(Date().timeIntervalSince1970).jpg\"\r\n".toData)
            body.append("Content-Type: \(mimetype)\r\n\r\n".toData)
            body.append(imageData ?? Data())
            body.append("\r\n".toData)
        }
        body.append("--\(boundary)\r\n".toData)
        return body as Data
        
    }
}
