//
//  GalleryViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 23/02/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class GalleryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var activityIndicator: NVActivityIndicatorView!
    
    var galleryImages = [String]()
    
    var cellSize = CGSize.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "Gallery", isBackButtonRequired: true)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCollectionViewCell")
        
        getGalleryImages()
    }
    
    func getGalleryImages(){
        self.galleryImages.removeAll()
        let reuestURL = Config.mainUrl +  "Data/GetVenueGallery?key=\(Config.key)&salt=\(Config.salt)"
        print("Request URL: \(reuestURL)")
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                if((resposeJSON["status"] as? Int ?? 0) == 1){
                    let results = resposeJSON["result"] as? NSArray ?? NSArray()
                    results.forEach { (result) in
                        
                        let currentResult = result as? NSDictionary ?? NSDictionary()
                        self.galleryImages.append(currentResult["media"] as? String ?? "")
                    }
                }
                if UIDevice.current.userInterfaceIdiom == .pad{
                    let totalWidth = (self.collectionView.bounds.width / 2) - 5
                    let totalHeight = totalWidth * (2/3)
                    self.cellSize = CGSize(width: totalWidth, height: totalHeight)
                    
                }else{
                    let totalWidth = (self.collectionView.bounds.width / 2) - 5
                    let totalHeight = totalWidth * (2/3)
                    self.cellSize = CGSize(width: totalWidth, height: totalHeight)
                }
                self.collectionView.reloadData()
                
            }
    }

}

extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        cell.data = galleryImages[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let destination = ImageViewerViewController(nibName: "ImageViewerViewController", bundle: nil)
        destination.modalPresentationStyle = .overFullScreen
        destination.modalTransitionStyle = .crossDissolve
        destination.imageURL = self.galleryImages[indexPath.item]
        self.present(destination, animated: false, completion: nil)
    }
}
