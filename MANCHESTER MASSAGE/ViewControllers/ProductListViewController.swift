//
//  ProductListViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 17/03/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
class ProductListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var activityIndicator: NVActivityIndicatorView!
    var cartButton: UIButton!
    var wishButton: UIButton!
    
    var rightBarButton: ENMBadgedBarButtonItem!
    var rightBarButton2: ENMBadgedBarButtonItem!
    
    var category_id = ""
    var pageTitle = ""
    var data = [ProductListModel]()
    var isFromWishList = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: pageTitle, isBackButtonRequired: true, isRightMenuRequired: true)
        
        cartButton = UIButton(type: .custom)
        cartButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        cartButton.addTarget(self, action: #selector(self.productCartButtonAction), for: .touchUpInside)
        cartButton.setLeftImage(image: FAType.FAShoppingCart, color: .black, state: .normal)
        rightBarButton = ENMBadgedBarButtonItem(customView: cartButton, value: "0")
        rightBarButton.badgeBackgroundColor = .red
        rightBarButton.badgeTextColor = .white
        rightBarButton.badgeValue = self.appD.itemsInCart
        
        wishButton = UIButton(type: .custom)
        wishButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        wishButton.addTarget(self, action: #selector(self.wishListButtonAction), for: .touchUpInside)
        wishButton.setLeftImage(image: FAType.FAHeart, color: .black, state: .normal)
        rightBarButton2 = ENMBadgedBarButtonItem(customView: wishButton, value: "0")
        rightBarButton2.badgeBackgroundColor = .red
        rightBarButton2.badgeTextColor = .white
        rightBarButton2.badgeValue = ""
//        if isFromWishList{
//            self.navigationItem.rightBarButtonItems = [rightBarButton]
//        }else{
//            self.navigationItem.rightBarButtonItems = [rightBarButton, rightBarButton2]
//        }
        
        
        
        
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(UINib(nibName: "ProductListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductListCollectionViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeValue), name: NSNotification.Name("changePhysicalCart"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProductList), name: NSNotification.Name("getProductList"), object: nil)
        if !isFromWishList{
//            self.getCartList()
            NotificationCenter.default.addObserver(self, selector: #selector(self.getProductList), name: NSNotification.Name("wishListChanged"), object: nil)
            self.getProductList()
        }else{
            NotificationCenter.default.addObserver(self, selector: #selector(self.getWishist), name: NSNotification.Name("getUpdatedWishList"), object: nil)
            self.getWishist()
        }
        
    }
    override func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func updateBadgeValue(){
        rightBarButton.badgeValue = self.appD.itemsInCart
        rightBarButton2.badgeValue = ""
    }
    
    @objc
    fileprivate func getProductList(){
        data.removeAll()
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetProducts?key=\(Config.key)&salt=\(Config.salt)&cat_id=\(category_id)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<result.count{
                        let currentProduct = result[index] as? NSDictionary ?? NSDictionary()
                        let prod_id = currentProduct["prod_id"] as? String ?? ""
                        let p_name = currentProduct["p_name"] as? String ?? ""
                        let is_featured = currentProduct["is_featured"] as? String ?? ""
                        let cat_id = currentProduct["cat_id"] as? String ?? ""
                        let brand_id = currentProduct["brand_id"] as? String ?? ""
                        let p_url = currentProduct["p_url"] as? String ?? ""
                        let base_price = currentProduct["base_price"] as? String ?? "0.0"
                        let discount_price = currentProduct["discount_price"] as? String ?? ""
                        let thumb = currentProduct["thumb"] as? String ?? ""
                        let big_description = currentProduct["big_description"] as? String ?? ""
                       
                        let galleyImage = currentProduct["gal_img"] as? NSArray ?? NSArray()
                        var galleryImages = [GalleryImage]()
                        for index2 in 0..<galleyImage.count{
                            let currentGallery = galleyImage[index2] as? NSDictionary ?? NSDictionary()
                            let m_id = currentGallery["m_id"] as? String ?? ""
                            let typ_id = currentGallery["typ_id"] as? String ?? ""
                            let media_url = currentGallery["media_url"] as? String ?? ""
                            let galleyObj = GalleryImage(m_id: m_id, typ_id: typ_id, media_url: media_url)
                            galleryImages.append(galleyObj)
                            
                        }
                        var finalData = ProductListModel(
                            prod_id: prod_id, p_name: p_name, is_featured: is_featured, cat_id: cat_id, brand_id: brand_id, p_url: p_url, base_price: base_price, discount_price: discount_price, thumb: thumb, big_description: big_description, galleyImage: galleryImages
                        )
                        finalData.isInWishList =  currentProduct["iswish"] as? Bool ?? false
                        finalData.isInCart = currentProduct["iscart"] as? Bool ?? false
                        finalData.cartQuantity = currentProduct["cart_count"] as? String ?? "0"
                        self.data.append(finalData)
                    }
                }
                
                self.collectionView.reloadData()
                
                if self.data.count == 0{
                    let alert = UIAlertController(title: "Alert!", message: "No products found!!", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .default) { (_) in
                        self.backButtonAction()
                    }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
    }
    
    func addRemoveToCartAction(apiName: String, product_id: String, quantity: String = "1"){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
            return
        }
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId).toData,
            "prod_count": quantity.toData,
            "prod_id": product_id.toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + apiName , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let status = resposeJSON["status"] as? Bool ?? false
                    let msg = resposeJSON["msg"] as? String ?? ""
                    if status{
                        self.getProductList()
//                        getCartList()
                        self.appD.getCartList()
                        if apiName == "addtocart"{
                            self.tabBarController?.view.makeToast(message: "Product has been added to cart successfully.")
                        }else{
                            self.tabBarController?.view.makeToast(message: "Product has been removed from cart successfully.")
                        }
                    }else{
                        self.tabBarController?.view.makeToast(message: msg)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})

    }
    
    @objc func addToCartButtonAction(sender: UIButton){
        addRemoveToCartAction(apiName: "addtocart", product_id: self.data[sender.tag].prod_id)
    }
    @objc func removeFromCartButtonAction(sender: UIButton){
        addRemoveToCartAction(apiName: "removecart", product_id: self.data[sender.tag].prod_id)
    }
    
    @objc func addRemoveWishList(sender: UIButton){
        if UserDefaults.standard.fetchData(forKey: .skipLogin){
            let alert = UIAlertController(title: "Alert!", message: "Please login first to proceed further.", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
                destination.isFromLeftMenu = true
                let navigationController = UINavigationController(rootViewController: destination)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: false, completion: nil)
            }
            alert.addAction(yesAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            
            self.present(alert, animated: false, completion: nil)
            return
        }
        let product_id = self.data[sender.tag].prod_id
        var apiName = "addtowishlist"
        if sender.isSelected{
            apiName = "removewishlist"
        }
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId).toData,
            "prod_id": product_id.toData
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + apiName , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    let status = resposeJSON["status"] as? Bool ?? false
                    let msg = resposeJSON["msg"] as? String ?? ""
                    if status{
                        
                        if !self.isFromWishList{
                            self.data[sender.tag].isInWishList = !self.data[sender.tag].isInWishList
                            self.collectionView.reloadData()
                            self.appD.getCartList()
                        }else{
                            self.getWishist()
                            NotificationCenter.default.post(name: NSNotification.Name("wishListChanged"), object: nil)
                        }
                        
                        self.tabBarController?.view.makeToast(message: msg)
                        
                    }else{
                        self.tabBarController?.view.makeToast(message: msg)
                    }
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
        
    }
    
    @objc func getWishist(){
        self.appD.wishList.removeAll()
        if isFromWishList{
            self.data.removeAll()
        }
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/Getwishlist?key=\(Config.key)&salt=\(Config.salt)&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate()
            .responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print(resposeJSON)
                
                if resposeJSON["status"] as? Bool ?? false{
                    let result = resposeJSON["result"] as? NSArray ?? NSArray()
                    for index in 0..<result.count{
                        let currentProduct = result[index] as? NSDictionary ?? NSDictionary()
                        let prod_id = currentProduct["prod_id"] as? String ?? ""
                        let p_name = currentProduct["prod_name"] as? String ?? ""
                        let is_featured = currentProduct["is_featured"] as? String ?? ""
                        let cat_id = currentProduct["cat_id"] as? String ?? ""
                        let brand_id = currentProduct["brand_id"] as? String ?? ""
                        let p_url = currentProduct["p_url"] as? String ?? ""
                        let base_price = currentProduct["base_price"] as? String ?? "0.0"
                        let discount_price = currentProduct["discount_price"] as? String ?? ""
                        let thumb = currentProduct["thumb"] as? String ?? ""
                        var finalData = ProductListModel(
                            prod_id: prod_id, p_name: p_name, is_featured: is_featured, cat_id: cat_id, brand_id: brand_id, p_url: p_url, base_price: base_price, discount_price: discount_price, thumb: thumb, isInWishList: true
                        )
                        finalData.isInCart = self.appD.cartList.contains(where: {$0.prod_id == prod_id})
                        if finalData.isInCart{
                            finalData.cartQuantity = self.appD.cartList.first(where: {$0.prod_id == prod_id})?.qnty ?? "0"
                        }
                        if self.isFromWishList{
                            self.data.append(finalData)
                        }
                        self.appD.wishList.append(finalData)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changePhysicalCart"), object: nil)
                    
                    if self.isFromWishList{
                        if self.data.count == 0{
                            let alert = UIAlertController(title: "Alert!", message: "No products found!!", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "OK", style: .default) { (_) in
                                self.backButtonAction()
                            }
                            alert.addAction(alertAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        self.collectionView.reloadData()
                    }
                }
            }
    }
    
    
}

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCollectionViewCell", for: indexPath) as! ProductListCollectionViewCell
        cell.data = data[indexPath.item]
        
//        cell.addToCardButton.tag = indexPath.row
//        cell.addToCardButton.layer.cornerRadius = cell.addToCardButton.bounds.height / 2
//        cell.addToCardButton.clipsToBounds = true
        cell.addToCardButton.addTarget(self, action: #selector(self.addToCartButtonAction(sender:)), for: .touchUpInside)
        cell.addToCardButton.layoutIfNeeded()
        
        
        cell.wishButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        cell.wishButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        cell.wishButton.layer.shadowOpacity = 1.0
        cell.wishButton.layer.shadowRadius = 0.0
        cell.wishButton.layer.masksToBounds = false
        cell.wishButton.layer.cornerRadius = cell.wishButton.bounds.height / 2
        cell.wishButton.clipsToBounds = true
        cell.wishButton.tag = indexPath.row
        cell.wishButton.addTarget(self, action: #selector(addRemoveWishList(sender:)), for: .touchUpInside)
        
        
        if isFromWishList{
            cell.addToCardButton.isHidden = true
            cell.addToCartStepper.isHidden = true
            cell.wishButton.isSelected = true
            cell.cartButton.isHidden = true
        }else{
            cell.wishButton.isSelected = data[indexPath.item].isInWishList
            if data[indexPath.item].isInCart{
                cell.addToCardButton.isHidden = true
                cell.addToCartStepper.isHidden = false
                cell.cartButton.isHidden = false
            }else{
                cell.addToCardButton.isHidden = false
                cell.addToCartStepper.isHidden = true
                cell.cartButton.isHidden = true
            }
        }
        cell.stepperStack.isHidden = cell.addToCartStepper.isHidden
//        cell.addToCartStepper.layer.borderWidth = 1.0
//        cell.addToCartStepper.layer.borderColor = ThemeColor.buttonColor.cgColor
//        cell.addToCartStepper.layer.cornerRadius = cell.addToCartStepper.bounds.height / 2
//        cell.addToCartStepper.clipsToBounds = true
        
        cell.addToCartStepper.rightButton.tag = indexPath.row
        cell.addToCartStepper.rightButton.addTarget(self, action: #selector(addToCartButtonAction(sender:)), for: .touchUpInside)
        cell.addToCartStepper.leftButton.tag = indexPath.row
        cell.addToCartStepper.leftButton.addTarget(self, action: #selector(removeFromCartButtonAction(sender:)), for: .touchUpInside)
        cell.addToCartStepper.layoutIfNeeded()
        
        cell.cartButton.roundCorners(corners: [.topLeft, .bottomRight], radius: 8)
        cell.cartButton.backgroundColor = ThemeColor.buttonColor
        cell.cartButton.setFAIcon(icon: .FAShoppingCart, iconSize: 25, forState: .normal)
        cell.cartButton.addTarget(self, action: #selector(self.productCartButtonAction), for: .touchUpInside)
        cell.cartButton.tintColor = .white
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItems = UIDevice.current.userInterfaceIdiom == .pad ? 3 : 2
        let cellWidth = (collectionView.bounds.width / CGFloat(numberOfItems))
        return CGSize(width: cellWidth - 10, height: cellWidth * 1.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let destination = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
        destination.productID = self.data[indexPath.item].prod_id
        destination.data = self.data[indexPath.item]
        destination.pageTitle = self.data[indexPath.item].p_name
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
 
