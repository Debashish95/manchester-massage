//
//  OrderDetailsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 17/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import TimelineTableViewCell

class OrderDetailsViewController: UIViewController {

    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var orderDetails = OrderListModel()
    
    var timeLineData = [(TimelinePoint, UIColor, String, String, String?, [String]?, String?)]()
    var shouldShowTrackView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "Order Details", isBackButtonRequired: true, isRightMenuRequired: false)
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(UINib(nibName: "OrderListTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderListTableViewCell")
        tableView.register(UINib(nibName: "OrderDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderDetailsTableViewCell")
        tableView.register(UINib(nibName: "TrackingHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "TrackingHeaderTableViewCell")
        orderIDLabel.text = "Order ID: #\(orderDetails.order_id)"
        
        let bundle = Bundle(for: TimelineTableViewCell.self)
        let nibUrl = bundle.url(forResource: "TimelineTableViewCell", withExtension: "bundle")
        let timelineTableViewCellNib = UINib(nibName: "TimelineTableViewCell",
            bundle: Bundle(url: nibUrl!)!)
        tableView.register(timelineTableViewCellNib, forCellReuseIdentifier: "TimelineTableViewCell")

        setupTrackingData()
    }
    
    @objc func viewInvoiceAction(){
        if let urls = URL(string: orderDetails.invoiceURL), UIApplication.shared.canOpenURL(urls){
            UIApplication.shared.open(urls, options: [:], completionHandler: nil)
        }
    }
    
    func setupTrackingData(){
        
        let timeLinePoint = TimelinePoint(diameter: 10, color: ThemeColor.bookNowButtonColor, filled: true)
        timeLineData.append((timeLinePoint, ThemeColor.buttonColor, orderDetails.created_on, "Order has been placed succesfully", "Order placed", nil, nil))
        
        for index in 0..<orderDetails.track.count{
            let currentStatus = orderDetails.track[index]
            let timeLinePoint = TimelinePoint(diameter: 10, color: ThemeColor.bookNowButtonColor, filled: true)
            if ((orderDetails.track.count - 1) == index){
                timeLineData.append((timeLinePoint, ThemeColor.buttonColor, currentStatus.timestamp, currentStatus.msg, currentStatus.track_status_name, nil, currentStatus.track_status_icon))
            }else{
                timeLineData.append((timeLinePoint, ThemeColor.buttonColor, currentStatus.timestamp, currentStatus.msg, currentStatus.track_status_name, nil, currentStatus.track_status_icon))
            }
        }
        timeLineData.append((TimelinePoint(diameter: 10, color: ThemeColor.bookNowButtonColor, filled: true), UIColor.clear, "", "", "", ["435", "1101"], nil))
    }
    
    @objc func buyAgainButtonAction(){
        let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "AddressListViewController") as! AddressListViewController
        destination.isFromCartPage = true
        destination.buyAgainItems = orderDetails.products
        destination.totalPriceFloat = Float(orderDetails.amount) ?? 0
        self.navigationController?.pushViewController(destination, animated: true)
        
//        let destination = PhysicalCartPaymentViewController(nibName: "PhysicalCartPaymentViewController", bundle: nil)
//        destination.addressID = orderDetails.address.addressId
//        destination.buyAgainItems = orderDetails.products
//        destination.totalPriceFloat = Float(orderDetails.amount) ?? 0
//        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func showHideTrackingView(){
        shouldShowTrackView = !shouldShowTrackView
        tableView.reloadData()
    }
    @objc func writeReviewButton(sender: UIButton){
        let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "ProductReviewViewController") as! ProductReviewViewController
        destination.products = orderDetails.products[sender.tag]
        self.navigationController?.pushViewController(destination, animated: true)
    }
}

extension OrderDetailsViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return orderDetails.products.count
        case 1:
            return shouldShowTrackView ? timeLineData.count : 0
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackingHeaderTableViewCell") as! TrackingHeaderTableViewCell
        if shouldShowTrackView{
            cell.trackingStatusImage.setFAIconWithName(icon: .FAAngleDown, textColor: .darkGray)
        }else{
            cell.trackingStatusImage.setFAIconWithName(icon: .FAAngleRight, textColor: .darkGray)
        }
        
        cell.showHideTrackingStatus.addTarget(self, action: #selector(showHideTrackingView), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineTableViewCell",
                                                     for: indexPath) as! TimelineTableViewCell
            // Configure the cell...
            let (timelinePoint, timelineBackColor, title, description, lineInfo, _, illustration) = timeLineData[indexPath.row]
            var timelineFrontColor = UIColor.clear
            if (indexPath.row > 0) {
                timelineFrontColor = timeLineData[indexPath.row - 1].1
            }
            
            cell.timelinePoint = timelinePoint
            cell.timeline.frontColor = timelineFrontColor
            cell.timeline.backColor = timelineBackColor
            cell.titleLabel.text = title
            cell.descriptionLabel.text = description
            cell.lineInfoLabel.text = lineInfo
            
            cell.viewsInStackView = []
            if indexPath.row == 0{
                cell.illustrationImageView.image = UIImage(named: "delivery-cart")
                let templateImage = cell.illustrationImageView.image?.withRenderingMode(.alwaysTemplate)
                cell.illustrationImageView.image = templateImage
                cell.illustrationImageView.tintColor = ThemeColor.buttonColor
                
            }else if let illustration = illustration {
                
                cell.illustrationImageView.kf.setImage(with: URL(string: illustration)!, placeholder: UIImage(), options: nil, progressBlock: nil) { (result) in
                    
                    cell.illustrationImageView.contentMode = .scaleAspectFit
                    let templateImage = cell.illustrationImageView.image?.withRenderingMode(.alwaysTemplate)
                    cell.illustrationImageView.image = templateImage
                    cell.illustrationImageView.tintColor = ThemeColor.buttonColor
                }
            }
            else {
                cell.illustrationImageView.image = nil
            }
            cell.bubbleEnabled = !(indexPath.row == timeLineData.count - 1)
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsTableViewCell", for: indexPath) as! OrderDetailsTableViewCell
            cell.addressNameLabel.text = orderDetails.address.fullName
            
            var addressArray = [String]()
            addressArray.append(orderDetails.address.addressLine1)
            if orderDetails.address.addressLine2 != ""{
                addressArray.append(orderDetails.address.addressLine2)
            }
            addressArray.append(orderDetails.address.town + " \(orderDetails.address.postalCode)")
            addressArray.append(orderDetails.address.country)
            cell.addressTextLabel.attributedText = makeAttributedString(key: "Address: ", value: addressArray.joined(separator: ","))
            cell.phoneLabel.attributedText = makeAttributedString(key: "Phone: ", value: orderDetails.address.phone)
            if orderDetails.address.security == ""{
                cell.securityCodeLabel.attributedText = makeAttributedString(key: "Security Code: ", value: "N/A")
            }else{
                cell.securityCodeLabel.attributedText = makeAttributedString(key: "Security Code: ", value: orderDetails.address.security)
            }
            
            cell.viewInvoiceButton.addTarget(self, action: #selector(viewInvoiceAction), for: .touchUpInside)
            cell.buyNowButton.addTarget(self, action: #selector(buyAgainButtonAction), for: .touchUpInside)
//            cell.orderDateLabel.text = "(\(orderDetails.orderDate))"
//            cell.deliveryDateLabel.text = "(\(orderDetails.deliveryDate))"
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderListTableViewCell", for: indexPath) as! OrderListTableViewCell
            cell.productData = orderDetails.products[indexPath.row]
            cell.productImageView.layer.cornerRadius = cell.productImageView.bounds.height / 2
            cell.productImageView.clipsToBounds = true
            cell.productImageView.layer.borderWidth = 1.0
            cell.productImageView.layer.borderColor = ThemeColor.buttonColor.cgColor
            cell.productImageView.backgroundColor = .darkGray
            cell.writeReviewButton.layer.cornerRadius = 3
            cell.writeReviewButton.clipsToBounds = true
            cell.writeReviewButton.tag = indexPath.row
            cell.writeReviewButton.isHidden = true
            cell.writeReviewButton.addTarget(self, action: #selector(writeReviewButton(sender:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let destination = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            destination.productID = self.orderDetails.products[indexPath.item].prod_id
            destination.pageTitle = self.orderDetails.products[indexPath.row].prod_name
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return 55
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return UIDevice.current.userInterfaceIdiom == .pad ? 150 : 180
        }else{
            return UITableView.automaticDimension
        }
    }
}

extension OrderDetailsViewController{
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 15), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        attributedString.append(two)
        return attributedString
    }
}
