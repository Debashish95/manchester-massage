//
//  StaticPagesViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 21/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class StaticPagesViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
//    @IBOutlet weak var staticContentLabel: UILabel!
    var pageTitle = ""
    var pageContents = ""
    var isFromLeftMenu = false
    var isFromReviews = false
    var pageURL: URL!
    
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationBar(titleText: pageTitle, isBackButtonRequired: true, isRightMenuRequired: false)
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        webView.backgroundColor = .white
        
        if isFromReviews{
            let request = URLRequest(url: pageURL)
            webView.navigationDelegate = self
            activityIndicator.startAnimating()
            webView.load(request)
        }else{
            let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
            webView.loadHTMLString(headerString + pageContents, baseURL: nil)
        }
        
        
    }
    override func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
        if isFromLeftMenu{
            self.dismiss(animated: true, completion: nil)
        }
    }

}


extension StaticPagesViewController: WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
    }
}
