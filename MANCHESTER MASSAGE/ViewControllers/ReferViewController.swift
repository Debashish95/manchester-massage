//
//  ReferViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 12/06/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ReferViewController: UIViewController {
    
    
    @IBOutlet weak var rightCardConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftCardConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topHeaderView: UIView!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var referrallabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var copyButton: UIButton!
    
    var activityIndicator: NVActivityIndicatorView!
    var walletData = [WalletModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: "REFER and EARN", isBackButtonRequired: true)
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        
        if isIPad{
            leftCardConstraint.constant = 80
            rightCardConstraint.constant = leftCardConstraint.constant
        }
        
        referrallabel.text = "Earn up to \(CURRENCY_SYMBOL)50 by inviting your friends, family and relatives to join Manchester Massage."
        
        inviteButton.layer.cornerRadius = 5
        inviteButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
        copyButton.backgroundColor = ThemeColor.bookNowButtonColor
        copyButton.layer.cornerRadius = 5
        copyButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        copyButton.setTitle("Copy your referal code \(UserDefaults.standard.fetchData(forKey: .referalCode))", for: .normal)
        
        copyButton.addTarget(self, action: #selector(copyButtonAction), for: .touchUpInside)
        
        totalAmountLabel.text = ""
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ReferalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ReferalCollectionViewCell")
        
        inviteButton.addTarget(self, action: #selector(inviteButtonAction(sender:)), for: .touchUpInside)
        
        getWalletData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.applyCurvedPath(givenView: self.topHeaderView,curvedPercent: 0.3)
    }
    func applyCurvedPath(givenView: UIView,curvedPercent:CGFloat) {
        guard curvedPercent <= 1 && curvedPercent >= 0 else{
            return
        }
        
        let shapeLayer = CAShapeLayer(layer: givenView.layer)
        shapeLayer.path = self.pathCurvedForView(givenView: givenView,curvedPercent: curvedPercent).cgPath
        shapeLayer.frame = givenView.bounds
        shapeLayer.masksToBounds = true
        givenView.layer.mask = shapeLayer
    }
    
    func pathCurvedForView(givenView: UIView, curvedPercent:CGFloat) ->UIBezierPath
    {
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:givenView.bounds.size.height - (givenView.bounds.size.height*curvedPercent)))
        arrowPath.addQuadCurve(to: CGPoint(x:0, y:givenView.bounds.size.height - (givenView.bounds.size.height*curvedPercent)), controlPoint: CGPoint(x:givenView.bounds.size.width/2, y:givenView.bounds.size.height))
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()
        
        return arrowPath
    }
    
    
    @objc fileprivate func getWalletData(){
        self.activityIndicator.startAnimating()
        walletData.removeAll()
        let requestURL = "\(Config.mainUrl)Data/GetwalletInfo?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId) + "&typ=referal"
        Alamofire.request(requestURL)
            .validate()
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print("Response JSON: \(resposeJSON)")
                
                self.totalAmountLabel.attributedText = self.makeAttributedString(key: "Total Amount: ", value: "\(CURRENCY_SYMBOL)\((resposeJSON["referal_earn"] as? String ?? "0").formattedString())")
                
                
                let result = resposeJSON["result"] as? NSArray ?? NSArray()
                for indx in 0..<result.count{
                    let currentData = result[indx] as? NSDictionary ?? NSDictionary()
                    var walletData = WalletModel()
                    walletData.tran_id = currentData["tran_id"] as? String ?? ""
                    walletData.info = currentData["info"] as? String ?? ""
                    walletData.tran_type = currentData["tran_type"] as? String ?? "D"
                    walletData.amount = currentData["amount"] as? String ?? ""
                    walletData.insert_date = self.convertDate((currentData["insert_date"] as? String ?? "1970-01-01"), from: "yyyy-MM-dd HH:mm:ss", to: "MMM dd, yyyy")
                    self.walletData.append(walletData)
                }
                self.collectionView.reloadData()
            }
    }
    
    fileprivate func convertDate(_ date: String, from format1: String, to format2: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format1
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = format2
        return  dateFormatter.string(from: date)
    }
    
    func makeAttributedString(key: String, value: String) -> NSMutableAttributedString{
        let attributedString: NSMutableAttributedString =  NSMutableAttributedString(string: key)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 17), range: NSMakeRange(0, attributedString.length))
        let two = NSMutableAttributedString(string: value)
        two.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 20), range: NSMakeRange(0, two.length))
        attributedString.append(two)
        return attributedString
    }
    
    @objc func copyButtonAction(){
        self.tabBarController?.view.makeToast(message: "Referal code saved to clipboard successfully.")
        UIPasteboard.general.string =
            UserDefaults.standard.fetchData(forKey: .referalCode)
    }
    @objc func inviteButtonAction(sender: UIButton){
        // Setting description
        let firstActivityItem = "Hello There! Please use my referal code \(UserDefaults.standard.fetchData(forKey: .referalCode)) while signing up in Manchester Massage to earn upto \(CURRENCY_SYMBOL)50"
        
        var activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem], applicationActivities: nil)
        // Setting url
        if let appLink = NSURL(string: "https://apps.apple.com/us/app/manchester-massage/id1498013848") {
        activityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, appLink], applicationActivities: nil)
        }
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = sender.bounds
        
        // Pre-configuring activity items
        if #available(iOS 13.0, *) {
            activityViewController.activityItemsConfiguration = [
                UIActivity.ActivityType.message
            ] as? UIActivityItemsConfigurationReading
        } else {
            // Fallback on earlier versions
        }
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo,
            UIActivity.ActivityType.postToFacebook
        ]
        
        if #available(iOS 13.0, *) {
            activityViewController.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
        self.present(activityViewController, animated: true, completion: nil)
    }
}


extension ReferViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return walletData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReferalCollectionViewCell", for: indexPath) as! ReferalCollectionViewCell
        cell.data = walletData[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = (collectionView.bounds.width / 2) - 5
        if isIPad{
            width = (self.collectionView.bounds.width / 3) - 10
        }
        return CGSize(width: width, height: width)
    }
}
