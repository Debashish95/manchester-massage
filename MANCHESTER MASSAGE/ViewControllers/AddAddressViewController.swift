//
//  AddAddressViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 12/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class AddAddressViewController: UIViewController {

    @IBOutlet weak var addressNameTextField: UITextField!
    @IBOutlet weak var setPrimaryAddressButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var addressLine1TextField: UITextField!
    @IBOutlet weak var addressLine2TextField: UITextField!
    
    @IBOutlet weak var townCityTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var securityCode: UITextField!
    
    @IBOutlet weak var weekendPickUpPicker: APJTextPickerView!
    
    var activityIndicator: NVActivityIndicatorView!
    
    var addressData: AddressModel?
    var pageTitle = ""
    var shouldEdit = false
    
    var weekendDelivery = ["N/A", "Saturday", "Sunday", "Both Saturday and Sunday"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNavigationBar(titleText: pageTitle, isBackButtonRequired: true, isRightMenuRequired: false)
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        weekendPickUpPicker.type = .strings
        weekendPickUpPicker.pickerDelegate = self
        weekendPickUpPicker.dataSource = self
        submitButton.setTitle((shouldEdit ? "Update Address" : "Add Address"), for: .normal)
        submitButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        
//        updateAddressList
        
//        weekendPickUpPicker.currentIndexSelected = (weekendDelivery.count - 1)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.addressNameTextField.setPlaceholder(placeholder: "Address Name (Example: Home, Office etc..)", color: UIColor.gray)
        
        self.fullNameTextField.setPlaceholder(placeholder: "Full Name", color: UIColor.gray)
        self.phoneNumberTextField.setPlaceholder(placeholder: "Phone Number", color: UIColor.gray)
        self.postalCodeTextField.setPlaceholder(placeholder: "Postal Code", color: UIColor.gray)
        self.addressLine1TextField.setPlaceholder(placeholder: "Address Line 1", color: UIColor.gray)
        self.addressLine2TextField.setPlaceholder(placeholder: "Address Line 2 (Optional)", color: UIColor.gray)
        
        self.townCityTextField.setPlaceholder(placeholder: "Town / City", color: UIColor.gray)
        self.countryTextField.setPlaceholder(placeholder: "Country", color: UIColor.gray)
        self.securityCode.setPlaceholder(placeholder: "Security Code/Call Box number to access this building", color: UIColor.gray)
        self.weekendPickUpPicker.setPlaceholder(placeholder: "Weekend Delivery", color: UIColor.gray)
        
        self.addressNameTextField.setTextFieldImage(image: FAType.FAAddressBookO, direction: .right)
        self.fullNameTextField.setTextFieldImage(image: FAType.FAUser, direction: .right)
        self.phoneNumberTextField.setTextFieldImage(image: FAType.FAPhone, direction: .right)
        self.postalCodeTextField.setTextFieldImage(image: FAType.FABuildingO, direction: .right)
        self.addressLine1TextField.setTextFieldImage(image: FAType.FAMapMarker, direction: .right)
        self.addressLine2TextField.setTextFieldImage(image: FAType.FAMapMarker, direction: .right)
        
        self.townCityTextField.setTextFieldImage(image: FAType.FAMapMarker, direction: .right)
        self.countryTextField.setTextFieldImage(image: FAType.FAGlobe, direction: .right)
        self.securityCode.setTextFieldImage(image: FAType.FALock, direction: .right)
        self.weekendPickUpPicker.setTextFieldImage(image: FAType.FAAngleDown, direction: .right)
        
        self.setPrimaryAddressButton.setLeftImage(image: .FACheckSquareO, color: UIColor.gray, state: .selected, size: CGSize(width: 20, height: 20))
        self.setPrimaryAddressButton.setLeftImage(image: .FASquareO, color: UIColor.gray, state: .normal, size: CGSize(width: 20, height: 20))
        self.setPrimaryAddressButton.setTitle(" Set As Primary Addess", for: UIControl.State())
        self.setPrimaryAddressButton.addTarget(self, action: #selector(togglePrimaryAddress), for: .touchUpInside)
        
        self.addressNameTextField.addButtomBorder()
        self.fullNameTextField.addButtomBorder()
        self.phoneNumberTextField.addButtomBorder()
        self.postalCodeTextField.addButtomBorder()
        self.addressLine1TextField.addButtomBorder()
        self.addressLine2TextField.addButtomBorder()
        
        self.townCityTextField.addButtomBorder()
        self.countryTextField.addButtomBorder()
        self.securityCode.addButtomBorder()
        self.weekendPickUpPicker.addButtomBorder()
        
        if let data = addressData{
            addressNameTextField.text = data.addressName
            fullNameTextField.text = data.fullName
            phoneNumberTextField.text = data.phone
            postalCodeTextField.text = data.postalCode
            addressLine1TextField.text = data.addressLine1
            addressLine2TextField.text = data.addressLine2
            
            townCityTextField.text = data.town
            countryTextField.text = data.country
            securityCode.text = data.security
            
            weekendPickUpPicker.currentIndexSelected = Int(data.weekendDelivery) ?? 0
            weekendPickUpPicker.text = weekendDelivery[Int(data.weekendDelivery) ?? 0]
            setPrimaryAddressButton.isSelected = data.isPrimary
        }
    }
    
    @objc func togglePrimaryAddress(){
        self.setPrimaryAddressButton.isSelected = !self.setPrimaryAddressButton.isSelected
    }
    
    @objc func submitButtonAction(){
        
        /**
         else if !(postalCodeTextField.text?.isValidPostalCode ?? true){
             self.tabBarController?.tabBar.makeToast(message: "Invalid Postal Code")
         }
         */
        if (addressNameTextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Address Name")
        }else if (fullNameTextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Full Name")
        }else if (phoneNumberTextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Phone Number")
        }else if !(phoneNumberTextField.text?.isPhoneNumber ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Invalid Phone Number")
        }else if (postalCodeTextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Postal Code")
        }else if (addressLine1TextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Address Line 1")
        }else if (townCityTextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Town/City")
        }else if (countryTextField.text?.isBlank ?? true){
            self.tabBarController?.tabBar.makeToast(message: "Enter Country")
        }else{

            self.activityIndicator.startAnimating()
            let apiEndPoint = shouldEdit ? "CustAddressEdit" : "CustAddressAdd"
            var parameters = [
                "key"               : Config.key,
                "salt"              : Config.salt,
                "cust_id"           : UserDefaults.standard.string(forKey: "userId") ?? "",
                "address_name"      : self.addressNameTextField.text ?? "",
                "name"              : self.fullNameTextField.text ?? "",
                "phone"             : self.phoneNumberTextField.text ?? "",
                "address_one"       : self.addressLine1TextField.text ?? "",
                "address_two"       : self.addressLine2TextField.text ?? "",
                "city"              : self.townCityTextField.text ?? "",
                "country"           : self.countryTextField.text ?? "",
                "zip"               : self.postalCodeTextField.text ?? "",
                "security_code"     : self.securityCode.text ?? "",
                "weekend_delivery"  : "\(self.weekendDelivery.firstIndex(of: self.weekendPickUpPicker.text ?? "") ?? 0)",
                "is_primary"        : self.setPrimaryAddressButton.isSelected ? "1" : "0"
            ] as [String : String]
            
            if shouldEdit{
                parameters["address_id"] = addressData?.addressId ?? ""
            }
            
//            address_id
            
            Alamofire.request(Config.apiEndPoint + apiEndPoint, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .validate()
                .responseJSON { (response) in
                    switch response.result{
                    case .failure(let error):
                        self.activityIndicator.stopAnimating()
                        print(error.localizedDescription)
                    case .success(let jSONResponse):
                        print(jSONResponse)
                        let result = jSONResponse as? NSDictionary ?? NSDictionary()
                        self.activityIndicator.stopAnimating()
                        let results = result["result"] as? NSDictionary ?? NSDictionary()
                        let status = results["status"] as? String ?? ""
                        if (status == "success"){
                            let alertController = UIAlertController(title: "Success!", message: results["msg"] as? String ?? "", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
                                self.navigationController?.popViewController(animated: true)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateAddressList"), object: nil)
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: false, completion: nil)
                            
                        }else{
                            self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                        }
                        
                    }
                }
        }
    }

}


extension AddAddressViewController: APJTextPickerViewDelegate, APJTextPickerViewDataSource{
    func numberOfRows(in pickerView: APJTextPickerView) -> Int {
        return weekendDelivery.count
    }
    func textPickerView(_ textPickerView: APJTextPickerView, titleForRow row: Int) -> String? {
        return weekendDelivery[row]
    }
}
