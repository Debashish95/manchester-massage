//
//  PaymentViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 16/09/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import NVActivityIndicatorView
import CreditCardForm

class PaymentViewController: UIViewController, STPPaymentCardTextFieldDelegate {

    @IBOutlet weak var walletCheckBox: UIButton!
    @IBOutlet weak var walletImageView: UIImageView!
    @IBOutlet weak var giftCardStackView: UIStackView!
    @IBOutlet weak var giftCardNoButton: UIButton!
    @IBOutlet weak var giftCardYesButton: UIButton!
    @IBOutlet weak var haveGiftCardLabel: UILabel!
    @IBOutlet weak var giftCardTextBox: UITextField!
    @IBOutlet weak var reedemGiftCardButton: UIButton!
    
    
    
    
    @IBOutlet weak var walletBalanceButton: UIButton!
    /*@IBOutlet weak var mySavedCardLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!*/
    @IBOutlet weak var saveCardButton: UIButton!
    
    @IBOutlet weak var mySavedCardButton: UIButton!
    @IBOutlet weak var cardNameTextField: UITextField!
    @IBOutlet var payNowButton: UIButton!
    @IBOutlet weak var stripeCardView: CardView!
    @IBOutlet weak var creditCardForm: CreditCardFormView!
    /*@IBOutlet weak var grandTotalAmount: UILabel!
    @IBOutlet weak var massageTime: UILabel!
    @IBOutlet weak var massageDate: UILabel!
    @IBOutlet weak var massageSelected: UILabel!
    @IBOutlet weak var receiptLabel: UILabel!*/
    var notes = ""
    
    var activityIndicator: NVActivityIndicatorView!
    
    var totalPriceFloat = Float(0.0)
    var amountToPay = Float(0.0)
    var paymentTextField = STPPaymentCardTextField()
    
    var savedCardList = [CardModel]()
    var savedCardCVV = ""
    var useSavedCard = false
    var walletBalance = Float(0.0)
    var walletBalanceString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "Payment", isBackButtonRequired: true, isRightMenuRequired: false)
        
        updateCartPrice()
        // Do any additional setup after loading the view.
//        self.massageSelected.text = "\(self.appD.serviceCart.count)"
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let currentDate = df.date(from: self.appD.serviceCart[0].bookingDate)
        if let _ = currentDate {
            df.dateFormat = "dd-MM-yyyy"
//            self.massageDate.text = self.appD.serviceCart[0].bookingDay + ", " +  df.string(from: currentDate!)
        }else{
//             self.massageDate.text = self.appD.serviceCart[0].bookingDay + ", " + self.appD.serviceCart[0].bookingDate
        }
        
//        self.massageTime.text = self.appD.serviceCart[0].bookingTime
        
        paymentTextField.frame = self.stripeCardView.bounds
        paymentTextField.postalCodeEntryEnabled = false
        paymentTextField.delegate = self
        paymentTextField.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.stripeCardView.addSubview(paymentTextField)
        
        setupCardView()
        
        saveCardButton.setImage(UIImage(icon: .FASquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        saveCardButton.setImage(UIImage(icon: .FACheckSquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .selected)
        saveCardButton.setTitle("Save this card for future use", for: .normal)
        saveCardButton.setTitleColor(UIColor.black, for: .normal)
      
        saveCardButton.addTarget(self, action: #selector(self.saveCardButtonAction), for: .touchUpInside)
        
        mySavedCardButton.addTarget(self, action: #selector(self.mySavedCardButtonAction), for: .touchUpInside)
        
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.register(UINib(nibName: "SavedCardTableViewCell", bundle: nil), forCellReuseIdentifier: "SavedCardTableViewCell")
        
//        self.getSavedCardList()
        walletBalanceButton.isHidden = true
        walletCheckBox.setImage(UIImage(icon: .FASquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .normal)
        walletCheckBox.setImage(UIImage(icon: .FACheckSquareO, size: CGSize(width: 30, height: 30), textColor: .darkGray), for: .selected)
        walletBalanceButton.setTitle("Wallet Balance", for: .normal)
        walletBalanceButton.setTitleColor(UIColor.black, for: .normal)
        walletCheckBox.addTarget(self, action: #selector(self.walletBalanceButtonAction), for: .touchUpInside)
        walletBalanceButton.addTarget(self, action: #selector(self.walletBalanceButtonAction), for: .touchUpInside)
        
        getWalletData()
        
        setupViewForGiftCard()
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        payNowButton.setTitle("Pay \(CURRENCY_SYMBOL)\(String(amountToPay).formattedString())", for: UIControl.State())
        payNowButton.backgroundColor = ThemeColor.buttonColor
        payNowButton.layer.cornerRadius = payNowButton.frame.height / 2
        payNowButton.clipsToBounds = true
        payNowButton.addTarget(self, action: #selector(payNowButtonAction), for: .touchUpInside)
        cardNameTextField.addBottomBorderWithColor(color: .gray, width: 1)
    }

    func setupCardView(){
        cardNameTextField.borderStyle = .none
        cardNameTextField.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        cardNameTextField.setPlaceholder(placeholder: "Card Holder Name", color: .gray)
        cardNameTextField.setRightViewFAIcon(icon: .FAUser, textColor: .gray)
        cardNameTextField.delegate = self
        cardNameTextField.text = "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))"
        cardNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        
        creditCardForm.cardHolderString = "\(UserDefaults.standard.fetchData(forKey: .userFirstName)) \(UserDefaults.standard.fetchData(forKey: .userLastName))"
        creditCardForm.cardNumberFont = UIFont.boldSystemFont(ofSize: 18)
        creditCardForm.cardPlaceholdersFont = UIFont.systemFont(ofSize: 14, weight: .medium)
        creditCardForm.cardTextFont = UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    @objc func mySavedCardButtonAction(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MySavedCardViewController") as! MySavedCardViewController
        destination.delegate = self
        destination.totalPriceFloat = self.amountToPay
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func saveCardButtonAction(){
        saveCardButton.isSelected = !saveCardButton.isSelected
    }
    
    @objc func walletBalanceButtonAction () {
        walletBalanceButton.isSelected = !walletBalanceButton.isSelected
        walletCheckBox.isSelected = !walletCheckBox.isSelected
        
        if walletCheckBox.isSelected {
            if totalPriceFloat.isLessThanOrEqualTo(walletBalance) {
                amountToPay = 0
            }else {
                amountToPay = totalPriceFloat - walletBalance
            }
        }else {
            amountToPay = totalPriceFloat
        }
        
        if amountToPay.isEqual(to: .zero) {
            mySavedCardButton.isEnabled = false
//            self.grandTotalAmount.text = "Book Now"
            payNowButton.setTitle("Book Now", for: UIControl.State())
        }else{
            mySavedCardButton.isEnabled = true
//            self.grandTotalAmount.text = "Grand Total: \(CURRENCY_SYMBOL)\(String(amountToPay).formattedString())"
            payNowButton.setTitle("Pay \(CURRENCY_SYMBOL)\(String(amountToPay).formattedString())", for: UIControl.State())
        }
        
    }
    
    
    fileprivate func setupViewForGiftCard() {
        walletImageView.image = UIImage(named: "wallet")
        walletImageView.image = walletImageView.image?.withRenderingMode(.alwaysTemplate)
        walletImageView.tintColor = ThemeColor.buttonColor
        
        haveGiftCardLabel.text = "Have Gift Card?"
        haveGiftCardLabel.textColor = .black
        haveGiftCardLabel.font = .systemFont(ofSize: 16, weight: .medium)
        
        giftCardYesButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .normal)
        giftCardYesButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        giftCardYesButton.setTitle("Yes", for: .normal)
        giftCardYesButton.setTitleColor(.darkGray, for: .normal)
        giftCardYesButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        giftCardYesButton.addTarget(self, action: #selector(giftCardButtonAction(sender:)), for: .touchUpInside)
        
        giftCardNoButton.setImage(UIImage(icon: .FACircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .normal)
        giftCardNoButton.setImage(UIImage(icon: .FADotCircleO, size: CGSize(width: 30, height: 30), textColor: ThemeColor.buttonColor), for: .selected)
        giftCardNoButton.setTitle("No", for: .normal)
        giftCardNoButton.setTitleColor(.darkGray, for: .normal)
        giftCardNoButton.setTitleColor(ThemeColor.buttonColor, for: .selected)
        giftCardNoButton.addTarget(self, action: #selector(giftCardButtonAction(sender:)), for: .touchUpInside)
        giftCardNoButton.isSelected = true
        
        giftCardTextBox.text = ""
        giftCardTextBox.tintColor = ThemeColor.buttonColor
        giftCardTextBox.textColor = .black
        giftCardTextBox.delegate = self
        giftCardTextBox.setPlaceholder(placeholder: "Enter Gift Card Code", color: UIColor.gray)
        giftCardTextBox.setTextFieldImage(image: FAType.FAGift, direction: .left)
        giftCardTextBox.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        giftCardStackView.isHidden = true
        
        reedemGiftCardButton.backgroundColor = ThemeColor.buttonColor
        reedemGiftCardButton.setTitle("Reedem", for: .normal)
        reedemGiftCardButton.setTitleColor(.white, for: .normal)
        reedemGiftCardButton.layer.cornerRadius = 4
        reedemGiftCardButton.addTarget(self, action: #selector(giftCardButtonAction(sender:)), for: .touchUpInside)
    }
    
    @objc func giftCardButtonAction(sender: UIButton) {
        switch sender {
        case reedemGiftCardButton:
            view.endEditing(true)
            if giftCardTextBox.text?.isBlank ?? false {
                self.showAlert(message: "Please enter gift card number")
                return
            }
            reedemGiftCard()
            break
        case giftCardYesButton:
            giftCardYesButton.isSelected = true
            giftCardNoButton.isSelected = false
            giftCardStackView.isHidden = false
            break
        case giftCardNoButton:
            giftCardYesButton.isSelected = false
            giftCardNoButton.isSelected = true
            giftCardTextBox.text = ""
            giftCardStackView.isHidden = true
            break
        default:
            break
        }
    }
    
    fileprivate func reedemGiftCard() {
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "cust_email": UserDefaults.standard.string(forKey: "userEmail") ?? "",
            "card_code": giftCardTextBox.text?.replacingOccurrences(of: "-", with: "") ?? ""
        ] as [String : String]
        Alamofire.request(Config.apiEndPoint + "GiftcardRedeem", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    self.activityIndicator.stopAnimating()
                case .success(let jSONResponse):
                    print(jSONResponse)
                    self.activityIndicator.stopAnimating()
                    let result = jSONResponse as? NSDictionary ?? NSDictionary()
                    if (result["status"] as? Bool ?? false){
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftCardStatusChange"), object: nil)
                        
                        let alertController = UIAlertController(title: "Success", message: result["msg"] as? String ?? "", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
                            self.getWalletData()
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                    }else{
                        self.tabBarController?.view.makeToast(message: "Something went wrong. Please try again.")
                    }
                    
                }
            }
    }
    
    func getSavedCardList() {
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/getCardListStripe?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
        method: .get,
        parameters: nil).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                self.tabBarController?.view.makeToast(message: Message.apiError)
                self.activityIndicator.stopAnimating()
                return
            }
            
            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
            self.activityIndicator.stopAnimating()
            let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
            if((result["status"] as? Int ?? 0) == 1){
                let finalCardResult = result["result"] as? NSDictionary ?? NSDictionary()
                if let _ = finalCardResult["error"] as? NSDictionary{
                    self.tabBarController?.view.makeToast(message: "Unable to fetch your cards.")
                }else{
                    let cardListData = finalCardResult["data"] as? NSArray ?? NSArray()
                    var cardData = CardModel()
                    for card in cardListData {
                        let cardItem = card as? NSDictionary ?? NSDictionary()
                        cardData.cardID = cardItem["id"] as? String ?? ""
                        let cardObject = cardItem["card"] as? NSDictionary ?? NSDictionary()
                        cardData.brand = cardObject["brand"] as? String ?? ""
                        cardData.expMonth = String(cardObject["exp_month"] as? Int ?? 0)
                        cardData.expYear = String(cardObject["exp_year"] as? Int ?? 0)
                        cardData.last4Digit = cardObject["last4"] as? String ?? ""
                        self.savedCardList.append(cardData)
                    }
                    
//                    self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
                }
            }else{
//                self.mySavedCardLabel.isHidden = (self.savedCardList.count == 0)
            }
            
//            self.tableView.reloadData()
            
        }
    }
    
    func updateCartPrice(){
        
        
        for index in 0..<self.appD.serviceCart.count{
            let priceString = self.appD.serviceCart[index].final_amount
            let price = Float(priceString) ?? 0.0
            totalPriceFloat += price
            
        }
        amountToPay = totalPriceFloat
        if amountToPay.isEqual(to: .zero) {
//            mySavedCardLabel.isEnabled = false
//            self.grandTotalAmount.text = "Book Now"
            payNowButton.setTitle("Book Now", for: UIControl.State())
        }else{
//            mySavedCardLabel.isEnabled = true
//            self.grandTotalAmount.text = "Grand Total: \(CURRENCY_SYMBOL)\(String(amountToPay).formattedString())"
            payNowButton.setTitle("Pay \(CURRENCY_SYMBOL)\(String(amountToPay).formattedString())", for: UIControl.State())
        }
        
    }
    fileprivate func paymentAction() {
        if amountToPay.isZero {
            finalBookng(paymentID: "wallet_booking_\(UUID().uuidString)")
            return
        }
        var fourDigitDate = ""
        var exp_month = ""
        var cardNumber = ""
        var cardCVV = ""
        var nameOnCard = ""
        if useSavedCard{
            let savedCard = self.savedCardList.first(where: {$0.isSelected}) ?? CardModel()
            fourDigitDate = savedCard.expYear
            exp_month = savedCard.expMonth
            cardNumber = savedCard.last4Digit
            cardCVV = savedCard.cvvText
        }else{
            cardNumber = paymentTextField.cardNumber ?? ""
            exp_month = "\(paymentTextField.expirationMonth)"
            let df = DateFormatter()
            df.dateFormat = "yy"
            guard let dt = df.date(from: paymentTextField.formattedExpirationYear ?? "") else { return }
            df.dateFormat = "yyyy"
            fourDigitDate = df.string(from: dt)
            cardCVV = paymentTextField.cvc ?? ""
            nameOnCard = cardNameTextField.text ?? ""
        }
        
        if cardNumber.isEmpty{
            self.tabBarController?.view.makeToast(message: "Please Enter Card Number")
            return
        }
        if cardCVV.isEmpty {
            self.tabBarController?.view.makeToast(message: "Please Enter Card CVV")
            return
        }
        
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId),
            "amount": "\(totalPriceFloat)",
            "tran_id": "PMP\(String().randomString(length: 12))",
            "card_number": cardNumber,
            "card_exp_month": exp_month,
            "card_exp_year": fourDigitDate,
            "card_cvc": cardCVV,
            "save_card": saveCardButton.isSelected ? "1" : "0",
            "new_card": useSavedCard ? "0" : "1",
            "name_on_card": nameOnCard
        ]as [String:String]
        
        Alamofire.request("\(Config.apiEndPoint)stripe_charge_card", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate().responseJSON { (response) in
                
                print(response)
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                self.activityIndicator.stopAnimating()
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                    //                    self.navigationController?.popViewController(animated: true)
                    break
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let json = (jSONResponse as? NSDictionary ?? NSDictionary())
                    self.finalBookng(paymentID: json["id"] as? String ?? "")
                    //                    delegate?.onPaymentSuccess(paymentID: json["id"] as? String ?? "", paymentResponse: json)
                    //                    self.navigationController?.popViewController(animated: true)
                    break
                }
            }
    }
    
    @objc
    func payNowButtonAction(){
        
        self.view.endEditing(true)
        if !amountToPay.isZero {
            if paymentTextField.cardNumber?.isEmpty ?? true {
                self.tabBarController?.view.makeToast(message: "Please Enter Card Number")
                return
            }
            
            if paymentTextField.expirationMonth == 0 {
                self.tabBarController?.view.makeToast(message: "Please Enter Card Expiry Month")
                return
            }
            
            if paymentTextField.expirationYear == 0 {
                self.tabBarController?.view.makeToast(message: "Please Enter Card Expiry Year")
                return
            }
            
            if paymentTextField.cvc?.isEmpty ?? true {
                self.tabBarController?.view.makeToast(message: "Please Enter CVV/CVC Number")
                return
            }
        }
        
        let alert = UIAlertController(title: "PLEASE NOTE",
                                      message: """
When making your payment to Manchester Massage via this site, please be aware that the transaction will appear on your bank statement as PamperTree who act as our 3rd party advertisers.
""", preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "I UNDERSTAND", style: .destructive) { _ in
            self.paymentAction()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelButton)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    fileprivate func finalBookng(paymentID: String){
        
        var totalAmount = Float(0)
        var finalAmount = Float(0)
        var currentServiceCart = [[String: Any]()]
        currentServiceCart.removeAll()
        
        for index in 0..<appD.serviceCart.count{
            if appD.serviceCart[index].isForMe{
                appD.serviceCart[index].cust_phone = UserDefaults.standard.fetchData(forKey: .userPhone)
                appD.serviceCart[index].cust_name = UserDefaults.standard.fetchData(forKey: .userFirstName) + " " + UserDefaults.standard.fetchData(forKey: .userLastName)
                appD.serviceCart[index].cust_email = UserDefaults.standard.fetchData(forKey: .userEmail)
            }
            let therapistStringArr = appD.serviceCart[index].therapists.map { (therapist) -> String in
                return therapist.id
            }
            
            let currentServiceCartData = [
                "id": appD.serviceCart[index].services[0].serviceID,
                "name": appD.serviceCart[index].services[0].name,
                "qty": appD.serviceCart[index].qty,
                "price": (appD.serviceCart[index].services[0].offerPrice).formattedString(),
                "subtotal": String((Float(appD.serviceCart[index].services[0].offerPrice) ?? 0) * (Float(appD.serviceCart[index].qty) ?? 0)).formattedString(),
                "therapist": "[\(therapistStringArr.joined(separator: ","))]",
                "cust_name": appD.serviceCart[index].cust_name,
                "cust_email": appD.serviceCart[index].cust_email,
                "book_dt": appD.serviceCart[index].bookingDate,
                "book_tym": appD.serviceCart[index].bookingTime,
                "book_end_tym": appD.serviceCart[index].bookingEndTime,
                "notes": appD.serviceCart[index].notes
            ] as [String : Any]
            currentServiceCart.append(currentServiceCartData)
            
            totalAmount += ((Float(appD.serviceCart[index].services[0].offerPrice) ?? 0) * (Float(appD.serviceCart[index].qty) ?? 0))
            finalAmount += ((Float(appD.serviceCart[index].services[0].offerPrice) ?? 0) * (Float(appD.serviceCart[index].qty) ?? 0))
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
        let jsonData = try? JSONSerialization.data(withJSONObject: currentServiceCart, options: [])
        
        var jsonDataString = String(bytes: jsonData!, encoding: .utf8)
        jsonDataString = jsonDataString?.replacingOccurrences(of: "\\", with: "")
        
        let bodyParams = [
            "key": Config.key,
            "salt": Config.salt,
            "cardType": "stripe",
            "vendor_id": appD.serviceCart[0].vendor_id,
            "cust_id": UserDefaults.standard.fetchData(forKey: .userId),
            "fname": UserDefaults.standard.fetchData(forKey: .userFirstName),
            "lname": UserDefaults.standard.fetchData(forKey: .userLastName),
            "email": UserDefaults.standard.fetchData(forKey: .userEmail),
            "phone": UserDefaults.standard.fetchData(forKey: .userPhone),
            "bookdt": appD.serviceCart[0].bookingDate,
            "therapist_id":appD.serviceCart[0].therapists[0].id,
            "timeslot": appD.serviceCart[0].bookingTime,
            "tot_amount": String(totalAmount),
            "final_amount": String(finalAmount),
            "tran_info": paymentID,
            "services": jsonDataString ?? "",
            "notes": self.notes,
            "device": "IOS",
            "wallet_amount": String(totalAmount - amountToPay)
        ] as [String:String]
        
        self.activityIndicator.startAnimating()
        
        Alamofire.request( Config.apiEndPoint + "FinalBooking", method: .post, parameters: bodyParams, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                print(response)
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                let status = result["status"] as? String ?? ""
                let message = result["type"] as? String ?? ""
                let txnID = result["orderid"] as? String ?? ""
                
                if status == "Ok"{
                    let alert = UIAlertController(title: "Success!", message: "Booking successfully. \n Transaction ID: #\(txnID)", preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Ok", style: .destructive) { (action) in
                        self.appD.serviceCart.removeAll()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCart"), object: nil)
//                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                        destination.isComingFromFinalBooking = true
//                        let navigationController = UINavigationController(rootViewController: destination)
//                        self.appD.window?.rootViewController = navigationController
//                        self.appD.window?.makeKeyAndVisible()
                        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
                        destination.isComingFromFinalBooking = true
                        self.appD.window?.rootViewController = destination
                        self.appD.window?.makeKeyAndVisible()
                    }
                    alert.addAction(yesAction)
                    self.present(alert, animated: false, completion: nil)
                }
                else{
                    self.tabBarController?.view.makeToast(message: message)
                }
        }
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if useSavedCard{
            for index in 0..<savedCardList.count{
                savedCardList[index].isSelected = false
            }
            useSavedCard = false
//            self.tableView.reloadData()
        }
        creditCardForm.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: UInt(textField.expirationYear), expirationMonth: UInt(textField.expirationMonth), cvc: textField.cvc)
    }
    
    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidEndEditingExpiration(expirationYear: UInt(textField.expirationYear))
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidBeginEditingCVC()
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardForm.paymentCardTextFieldDidEndEditingCVC()
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        for index in 0 ..< self.savedCardList.count{
            if self.savedCardList[index].isSelected{
                self.savedCardList[index].cvvText = textField.text ?? ""
            }
        }
        
    }
}

extension PaymentViewController: STPAuthenticationContext{
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}

extension PaymentViewController: UITextFieldDelegate{
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == cardNameTextField {
            creditCardForm.cardHolderString = textField.text!
        }
        
        if textField == giftCardTextBox {
            giftCardTextBox.text = giftCardTextBox.text?.uppercased()
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        view.endEditing(true)
        self.view.bounds = self.tabBarController?.selectedViewController?.view.bounds ?? .zero
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == cardNameTextField {
            textField.resignFirstResponder()
            paymentTextField.becomeFirstResponder()
        } else if textField == paymentTextField  {
            textField.resignFirstResponder()
        }
        return true
    }
}

extension PaymentViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCardList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedCardTableViewCell", for: indexPath) as! SavedCardTableViewCell
        cell.data = self.savedCardList[indexPath.row]
        cell.cvvTextField.delegate = self
        cell.cvvTextField.keyboardType = .numberPad
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for index in 0 ..< self.savedCardList.count{
            if indexPath.row == index{
                if self.savedCardList[index].isSelected{
                    self.savedCardList[index].isSelected = false
                    useSavedCard = false
                }else{
                    self.savedCardList[index].isSelected = true
                    useSavedCard = true
                }
            }else{
                self.savedCardList[index].isSelected = false
            }
            self.savedCardList[index].cvvText = ""
        }
//        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}


extension PaymentViewController: MySavedCardViewControllerDelegate{
    func response(data: [String : Any]?, error: [String : String]?) {
        if data != nil, let paymentID = data?["id"] as? String{
            self.finalBookng(paymentID: paymentID)
        }else{
            let alert = UIAlertController(title: "Alert!", message: "Sorry! Something went wrong. Please try again!!", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default)
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

//Mark:- Add wallet balance while making payment
extension PaymentViewController {
    
    @objc fileprivate func getWalletData(){
        self.activityIndicator.startAnimating()
        let requestURL = "\(Config.mainUrl)Data/GetwalletInfo?key=alphader67bgi9m0098htg&salt=swswsw33er4y84drfgtyhin30j&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        Alamofire.request(requestURL)
            .validate()
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                
                print("Response JSON: \(resposeJSON)")
                
                self.walletBalance = (resposeJSON["current_balance"] as? String ?? "0").value()
                self.walletBalanceString = (resposeJSON["current_balance"] as? String ?? "0")
                
                if self.walletBalance.isZero {
                    self.walletBalanceButton.isHidden = true
                    self.walletCheckBox.isHidden = true
                    self.walletImageView.isHidden = true
                }else {
                    let firstAttributedText = NSMutableAttributedString(string: "Wallet Balance ")
                    firstAttributedText.addAttributes(
                        [
                            NSAttributedString.Key.foregroundColor : UIColor.black,
                            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium),
                        ],
                        range: NSRange(location: 0, length: firstAttributedText.length))
                    
                    let secondAttributedText = NSMutableAttributedString(string: "\(CURRENCY_SYMBOL)\(self.walletBalanceString.formattedString())")
                    secondAttributedText.addAttributes(
                        [
                            NSAttributedString.Key.foregroundColor : ThemeColor.buttonColor,
                            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .medium),
                        ],
                        range: NSRange(location: 0, length: secondAttributedText.length))
                    firstAttributedText.append(secondAttributedText)
                    self.walletBalanceButton.setAttributedTitle(firstAttributedText, for: .normal)
                    
                    self.walletBalanceButton.isHidden = false
                    self.walletCheckBox.isHidden = false
                    self.walletImageView.isHidden = false
                    
                    if self.walletCheckBox.isSelected {
                        if self.totalPriceFloat.isLessThanOrEqualTo(self.walletBalance) {
                            self.amountToPay = 0
                        }else {
                            self.amountToPay = self.totalPriceFloat - self.walletBalance
                        }
                    }else {
                        self.amountToPay = self.totalPriceFloat
                    }
                    
                    if self.amountToPay.isEqual(to: .zero) {
                        self.mySavedCardButton.isEnabled = false
                        self.payNowButton.setTitle("Book Now", for: UIControl.State())
                    }else{
                        self.mySavedCardButton.isEnabled = true
                        self.payNowButton.setTitle("Pay \(CURRENCY_SYMBOL)\(String(self.amountToPay).formattedString())", for: UIControl.State())
                    }
                }
            }
    }
}
