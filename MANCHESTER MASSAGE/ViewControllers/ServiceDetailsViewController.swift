//
//  ServiceDetailsViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by Users on 19/01/20.
//  Copyright © 2020 Users. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import NVActivityIndicatorView
import WebKit
import Cosmos

class ServiceDetailsViewController: UIViewController {
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceDescriptionTextView: WKWebView!
    @IBOutlet weak var bookNowButton: UIButton!
    
    var cartButton: UIButton!
    var rightBarButton: ENMBadgedBarButtonItem!
    
    @IBOutlet weak var priceLabel: UILabel!
    var activityIndicator: NVActivityIndicatorView!
    var catID = ""
    var serviceDetailsData: ServiceModel!
    var serviceData: ServiceDetailsModel!
    var isFromBookingPage = false
    
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var favouriteStatus: Bool = false
    var mainServiceID: String = ""
    
    @IBOutlet weak var ratingView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        self.setNavigationBar(titleText: "", isBackButtonRequired: true, isRightMenuRequired: true)
        serviceDescriptionTextView.isUserInteractionEnabled = true
        serviceDescriptionTextView.backgroundColor = .clear
        
        bookNowButton.setTitle("BOOK NOW", for: .normal)
        bookNowButton.backgroundColor = ThemeColor.buttonColor
        bookNowButton.setTitleColor(UIColor.white, for: .normal)
        bookNowButton.addTarget(self, action: #selector(self.bookNowAction), for: .touchUpInside)
        
        setData()
        
        cartButton = UIButton(type: .custom)
        cartButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        cartButton.addTarget(self, action: #selector(self.cartButtonAction), for: .touchUpInside)
        cartButton.setLeftImage(image: FAType.FAShoppingCart, color: .black, state: .normal)
        rightBarButton = ENMBadgedBarButtonItem(customView: cartButton, value: "0")
        rightBarButton.badgeBackgroundColor = .red
        rightBarButton.badgeTextColor = .white
        rightBarButton.badgeValue = "\(self.appD.serviceCart.count)"
        self.navigationItem.rightBarButtonItem = rightBarButton

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadgeValue), name: NSNotification.Name("changeCart"), object: nil)
        
        self.favouriteButton.setLeftImage(image: .FAHeartO, color: ThemeColor.buttonColor, state: .normal)
        self.favouriteButton.setLeftImage(image: .FAHeart, color: ThemeColor.buttonColor, state: .selected)
        
        self.favouriteButton.addTarget(self, action: #selector(favouriteButtonAction), for: .touchUpInside)
        
        self.shareButton.setLeftImage(image: .FAShareAlt, color: ThemeColor.buttonColor, state: .normal)
        self.shareButton.addTarget(self, action: #selector(self.shareButtonAction(sender:)), for: .touchUpInside)
        
        self.ratingView.settings.fillMode = .precise
        self.ratingView.rating = 0
        self.ratingView.text = ""
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gotoReviewList))
        tapGesture.numberOfTapsRequired = 1
        self.ratingView.addGestureRecognizer(tapGesture)
        
        bottomHeight.constant = (tabBarController?.tabBar.bounds.height ?? 0) + 5.0
        
    }
    
    @objc func gotoReviewList(){
        let destination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ReviewListViewController") as! ReviewListViewController
//        ReviewListViewController(nibName: "ReviewListViewController", bundle: nil)
        destination.catId = self.serviceData.smId
        destination.serviceName = self.serviceData.serviceTitle
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func updateBadgeValue(){
        rightBarButton.badgeValue = "\(self.appD.serviceCart.count)"
    }
    
    func setData(){
        
        
        
        self.activityIndicator.startAnimating()
        
        var reuestURL = (Config.mainUrl + "Data/Getsinglecat?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&catid=" + catID)
        
        if (UserDefaults.standard.fetchData(forKey: .userId) != ""){
            reuestURL += "&cust_id=\(UserDefaults.standard.fetchData(forKey: .userId))"
        }
        
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil).validate().responseJSON { (response) in
                            
                            guard response.result.isSuccess else {
                                self.tabBarController?.view.makeToast(message: Message.apiError)
                                self.activityIndicator.stopAnimating()
                                return
                            }
                            
                            let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                            self.activityIndicator.stopAnimating()
                            
                            if ((resposeJSON["status"] as? String ?? "") == "Y") && ((resposeJSON["message"] as? String ?? "") == "Success"){
                                let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                                let serviceDescription = result["content"] as? String ?? ""
                                let banner = result["banner"] as? String ?? ""
                                let cat_name = result["cat_nm"] as? String ?? ""
                                let cat_id = result["cat_id"] as? String ?? ""
                                let parent_cat_id = result["parent_cat_id"] as? String ?? ""
                                
                                self.title = cat_name
                                self.serviceImage.kf.setImage(with: URL(string: banner), placeholder: UIImage(), options: nil, progressBlock: nil) { (result) in
                                    self.serviceImage.contentMode = .scaleAspectFill
                                }
                                let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
                                self.serviceDescriptionTextView.loadHTMLString(headerString + serviceDescription, baseURL: nil)
                                
                                
                                var serviceItemsList = [ServiceItemsModel]()
                                
                                let services = resposeJSON["services"] as? NSArray ?? NSArray()
                                for index in 0..<services.count{
                                    let itmDict = services[index] as? NSDictionary ?? NSDictionary()
                                    self.favouriteStatus = (itmDict["fav"] as? String ?? "0") == "1"
                                    self.favouriteButton.isSelected = self.favouriteStatus
                                    self.mainServiceID = itmDict["sm_id"] as? String ?? ""
                                    self.ratingView.rating = Double(itmDict["rating"] as? String ?? "0.0") ?? 0.0
                                    self.ratingView.text = "(\(itmDict["rating_count"] as? String ?? "0") reviews)"
                                    self.ratingView.settings.textFont = .boldSystemFont(ofSize: 14)
                                    
                                    let main_price = itmDict["main_price"] as? String ?? "0"
                                    let offer_price = itmDict["offer_price"] as? String ?? "0"
                                    
                                    let main_priceInt = (Double(main_price) ?? 0)
                                    let offer_priceInt = (Double(offer_price) ?? 0)
                                    
                                    let cleanUpTime = (Int(itmDict["cleanup_time"] as? String ?? "0") ?? 0) * 60
                                    let durationTime = (Int(itmDict["duration"] as? String ?? "0") ?? 0) * 60
                                    let finalDurationTime = (durationTime - cleanUpTime).secondsToTime()
                                    
                                    if main_priceInt != .zero{
                                      
                                        self.priceLabel.numberOfLines = 2
                                        self.priceLabel.text = "Was \(CURRENCY_SYMBOL)\(String(main_priceInt).formattedString()), now \(CURRENCY_SYMBOL)\(String(offer_priceInt).formattedString())\n for \(finalDurationTime) Massage"
                                        if parent_cat_id == "11" {
                                            self.priceLabel.text = "Was \(CURRENCY_SYMBOL)\(String(main_priceInt).formattedString()), now \(CURRENCY_SYMBOL)\(String(offer_priceInt).formattedString())\n for \(finalDurationTime) Treatment"
                                        }
                                        self.priceLabel.backgroundColor = ThemeColor.bookNowButtonColor
                                        self.priceLabel.layer.cornerRadius = self.priceLabel.bounds.height / 2
                                        self.priceLabel.clipsToBounds = true
                                        self.priceLabel.textColor = .white
                                    }
                                    
                                    let itm = itmDict["itm"] as? NSArray ?? NSArray()
                                    for ind1 in 0..<itm.count{
                                        let currentItem = itm[ind1] as? NSDictionary ?? NSDictionary()
                                        let code = currentItem["code"] as? String ?? ""
                                        let name = currentItem["name"] as? String ?? ""
                                        let duration = currentItem["duration"] as? String ?? ""
                                        let cleanupTime = currentItem["cleanup_time"] as? String ?? "0"
                                        let main_price = currentItem["main_price"] as? String ?? ""
                                        let offer_price = currentItem["offer_price"] as? String ?? ""
                                        let therapist_no = currentItem["therapist_no"] as? String ?? "0"
                                        let share_link = currentItem["share-link"] as? String ?? ""
                                        let serviceItems = ServiceItemsModel(serviceID: code, name: name, duration: duration, cleanupTime: cleanupTime, main_price: main_price, offerPrice: offer_price, therapistNumber: therapist_no, shareLink: share_link)
                                        serviceItemsList.append(serviceItems)
                                        
                                    }
                                    let serviceDataTemp = ServiceDetailsModel(
                                        smId: (itmDict["sm_id"] as? String ?? ""),
                                        serviceID: (itmDict["service_id"] as? String ?? ""),
                                        catId: cat_id,
                                        serviceTitle: (itmDict["service_title"] as? String ?? ""),
                                        serviceItems: serviceItemsList)
                                    
                                    self.serviceData = serviceDataTemp
                                    
                                    if self.isFromBookingPage{
                                        self.bookNowAction()
                                    }
                                }
                            }
                            
                            
                          }
        
    }
    override func viewDidLayoutSubviews() {
        bookNowButton.layer.cornerRadius = bookNowButton.frame.height / 2
        bookNowButton.clipsToBounds = true
    }
    
    @objc func bookNowAction(){
        let destination = BookNowPopUp(nibName: "BookNowPopUp", bundle: nil)
        destination.modalPresentationStyle = .overCurrentContext
        self.definesPresentationContext = true
        destination.delegate = self
        destination.serviceData = self.serviceData
        destination.fromPage = .serviceDetails
        self.present(destination, animated: true, completion: nil)
    }
    
    override func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func shareButtonAction(sender: UIButton){
        let firstActivityItem = self.title ?? ""
        let secondActivityItem : NSURL = NSURL(string: self.serviceData.serviceItems.first?.shareLink ?? "")!
        let image : UIImage = self.serviceImage.image ?? UIImage()
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.shareButton
        
//        // This line remove the arrow of the popover to show in iPad
//        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = self.shareButton.bounds
        
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToTencentWeibo,
        ]
        
//        if #available(iOS 13.0, *) {
//            activityViewController.isModalInPresentation = true
//        } else {
//            // Fallback on earlier versions
//        }
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @objc func addRemoveFavourite(apiName: String){
        if UserDefaults.standard.fetchData(forKey: .userId) == ""{
            let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
            destination.isFromLeftMenu = true
            let navigationController = UINavigationController(rootViewController: destination)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: false, completion: nil)
            return
        }
        let paramsHead = ["Content-Type": "multipart/form-data"]
        let bodyParams = [
            "key": Config.key.toData,
            "salt": Config.salt.toData,
            "typ_id": self.mainServiceID.toData,
            "typ": "SERVICE_CAT".toData,
            "cust_id": (UserDefaults.standard.string(forKey: "userId") ?? "").toData
            
        ] as [String : Data]
        self.activityIndicator.startAnimating()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in bodyParams{
                multipartFormData.append(value, withName: key)
            }
        }, usingThreshold: UInt64.init(), to: Config.apiEndPoint + apiName , method: .post, headers: paramsHead, encodingCompletion: { (encodingResult) in
            
            switch encodingResult{
            case .failure( _):
                self.activityIndicator.stopAnimating()
                break;
            case .success(let request, true, _):
                request.responseJSON(completionHandler: { (response) in
                    
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    
                    
                    let status = resposeJSON["status"] as? Int ?? 0
                    let message = resposeJSON["msg"] as? String ?? ""
                    
                    self.favouriteButton.isSelected = (apiName == "addFavourite")
                    if status == 1{
                        if(apiName == "addFavourite"){
                            self.tabBarController?.view.makeToast(message: "Service has been added to favourite successfully.")
                            self.favouriteStatus = true
                        }else{
                            self.tabBarController?.view.makeToast(message: "Service has been removed from favourite successfully.")
                            self.favouriteStatus = false
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getFavList"), object: nil)
                        return
                    }
                    self.tabBarController?.view.makeToast(message: message)
                })
            default:
                self.activityIndicator.stopAnimating()
                self.tabBarController?.view.makeToast(message: Message.apiError)
                break;
            }})
    }
    
    @objc func favouriteButtonAction(){
        if self.favouriteStatus{
            self.addRemoveFavourite(apiName: "removeFavourite")
        }else{
            self.addRemoveFavourite(apiName: "addFavourite")
        }
    }
}


extension ServiceDetailsViewController: BookNowPopUpDelegate{
    func confirmButtonAction() {
        
    }
    
    func addServiceButtonActions() {
//        if UserDefaults.standard.fetchData(forKey: .skipLogin){
//            let alert = UIAlertController(title: "Alert!", message: "Please login first to view the cart.", preferredStyle: .alert)
//            let yesAction = UIAlertAction(title: "Login", style: .default) { (action) in
//                let destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreLoginViewController") as! PreLoginViewController
//                destination.isFromLeftMenu = true
//                let navigationController = UINavigationController(rootViewController: destination)
//                navigationController.modalPresentationStyle = .fullScreen
//                self.present(navigationController, animated: false, completion: nil)
//            }
//            alert.addAction(yesAction)
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//
//            }
//            alert.addAction(cancelAction)
//
//            self.present(alert, animated: false, completion: nil)
//        }else{
            let detination = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! NewCartViewController
            self.navigationController?.pushViewController(detination, animated: true)
        //}
        
    }
}
