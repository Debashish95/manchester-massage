//
//  AddressListViewController.swift
//  MANCHESTER MASSAGE
//
//  Created by User on 12/04/21.
//  Copyright © 2021 Users. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class AddressListViewController: UIViewController {

    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var activityIndicator: NVActivityIndicatorView!
    
    var addressData = [AddressModel]()
    var isFromCartPage = false
    var totalPriceFloat = Float(0.0)
    var buyAgainItems = [ProductCartModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if isFromCartPage{
            self.setNavigationBar(titleText: "Choose Address", isBackButtonRequired: true, isRightMenuRequired: false)
        }else{
            self.setNavigationBar(titleText: "My Address", isBackButtonRequired: true, isRightMenuRequired: false)
        }
        
        
        activityIndicator = addActivityIndicator()
        self.view.addSubview(activityIndicator)
        
        addAddressButton.backgroundColor = ThemeColor.buttonColor
        if !isFromCartPage{
            addAddressButton.setFAText(prefixText: "", icon: .FAPlus, postfixText: " Add Address", size: 17, forState: .normal, color: .white)
            addAddressButton.addTarget(self, action: #selector(addAddress), for: .touchUpInside)
        }else{
            addAddressButton.setTitle("Proceed to Pay", for: .normal)
            addAddressButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
            addAddressButton.setTitleColor(.white, for: .normal)
            addAddressButton.addTarget(self, action: #selector(goToPaymentPage), for: .touchUpInside)
        }
        
        
        
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "AddreessListTableViewCell", bundle: nil), forCellReuseIdentifier: "AddreessListTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getCustAddress), name: NSNotification.Name("updateAddressList"), object: nil)
        getCustAddress()
        
        if isFromCartPage{
            let rightBarButton = UIButton(type: .custom)
            rightBarButton.setTitle("+ Add Address", for: .normal)
            rightBarButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            rightBarButton.setTitleColor(ThemeColor.buttonColor, for: .normal)
            rightBarButton.addTarget(self, action: #selector(self.addAddress), for: .touchUpInside)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarButton)
        }
        
    }
    
    @objc func goToPaymentPage(){
        let destination = PhysicalCartPaymentViewController(nibName: "PhysicalCartPaymentViewController", bundle: nil)
        if buyAgainItems.count > 0{
            destination.buyAgainItems = buyAgainItems
        }
        destination.totalPriceFloat = totalPriceFloat
        destination.addressID = self.addressData.first(where: {$0.isPrimary})?.addressId ?? "\(self.addressData.first?.addressId ?? "0")"
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func getCustAddress(){
        addressData.removeAll()
        self.activityIndicator.startAnimating()
        let reuestURL = Config.mainUrl + "Data/GetCustAddress?key=alphamassage876ty67&salt=iuhnji8fd7659kij9j02wed440wq&cust_id=" + UserDefaults.standard.fetchData(forKey: .userId)
        let urlConvertible = URL(string: reuestURL)!
        Alamofire.request(urlConvertible,
                          method: .get,
                          parameters: nil)
            .validate().responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    self.tabBarController?.view.makeToast(message: Message.apiError)
                    self.activityIndicator.stopAnimating()
                    return
                }
                
                let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                self.activityIndicator.stopAnimating()
                let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                let results = result["result"] as? NSArray ?? NSArray()
                
                for result in results{
                    let currentAddress = result as? NSDictionary ?? NSDictionary()
                    self.addressData.append(self.parseAddressModel(currentAddress))
                }
                self.tableView.reloadData()
            }
        
        
        tableView.reloadData()
    }
    
    @objc func addAddress(){
        let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        destination.pageTitle = "Add a new Address"
        self.navigationController?.pushViewController(destination, animated: true)
    }

    @objc func editButtonAction(sender: UIButton){
        let destination = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        destination.addressData = addressData[sender.tag]
        destination.pageTitle = "Edit Address"
        destination.shouldEdit = true
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func deleteButtonAction(sender: UIButton){
        let alertContoller = UIAlertController(title: "Alert!", message: "Are you sure you want to delete this address?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.deleteAddress(addressID: self.addressData[sender.tag].addressId)
        }
        
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertContoller.addAction(yesAction)
        alertContoller.addAction(noAction)
        self.present(alertContoller, animated: true, completion: nil)
    }
    
    fileprivate func deleteAddress(addressID: String){
        self.addressData.removeAll()
        self.activityIndicator.startAnimating()
        let parameters = [
            "key": Config.key,
            "salt": Config.salt,
            "cust_id": UserDefaults.standard.string(forKey: "userId") ?? "",
            "address_id": addressID
            ] as [String : String]
        Alamofire.request(Config.apiEndPoint + "CustAddressDelete", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let jSONResponse):
                    print(jSONResponse)
                    let resposeJSON = response.value as? NSDictionary ?? NSDictionary()
                    self.activityIndicator.stopAnimating()
                    let result = resposeJSON["result"] as? NSDictionary ?? NSDictionary()
                    let results = result["result"] as? NSArray ?? NSArray()
                    for result in results{
                        let currentAddress = result as? NSDictionary ?? NSDictionary()
                        self.addressData.append(self.parseAddressModel(currentAddress))
                    }
                    self.tableView.reloadData()
                    
                }
            }
        
    }
    
    fileprivate func parseAddressModel(_ currentAddress: NSDictionary)->AddressModel{
        var addressData = AddressModel()
        addressData.addressId = currentAddress["address_id"] as? String ?? ""
        addressData.fullName = currentAddress["name"] as? String ?? ""
        addressData.addressName = currentAddress["address_name"] as? String ?? ""
        addressData.isPrimary = (currentAddress["is_primary"] as? String ?? "0" == "1")
        addressData.phone = currentAddress["phone"] as? String ?? ""
        addressData.addressLine1 = currentAddress["address_one"] as? String ?? ""
        addressData.addressLine2 = currentAddress["address_two"] as? String ?? ""
        addressData.town = currentAddress["city"] as? String ?? ""
        addressData.country = currentAddress["country"] as? String ?? ""
        addressData.postalCode = currentAddress["zip"] as? String ?? ""
        addressData.security = currentAddress["security_code"] as? String ?? ""
        addressData.weekendDelivery = currentAddress["weekend_delivery"] as? String ?? ""
        return addressData
    }
}

extension AddressListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddreessListTableViewCell", for: indexPath) as! AddreessListTableViewCell
        cell.isFromCartPage = self.isFromCartPage
        cell.data = addressData[indexPath.row]
        
        if isFromCartPage{
            cell.defaultAddressButton.isUserInteractionEnabled = false
            cell.defaultAddressButton.setLeftImage(image: .FACircleO, color: .gray, state: .normal)
            cell.defaultAddressButton.setLeftImage(image: .FADotCircleO, color: ThemeColor.buttonColor, state: .selected)
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }else{
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(editButtonAction(sender:)), for: .touchUpInside)
            
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteButtonAction(sender:)), for: .touchUpInside)
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for index in 0..<addressData.count{
            addressData[index].isPrimary = index == indexPath.row
        }
        tableView.reloadData()
    }
}
